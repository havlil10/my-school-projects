package processTests;

import me.Muchysta.Chess.Controller.*;
import me.Muchysta.Chess.Controller.Abstracts.Communicator;
import me.Muchysta.Chess.Controller.Figures.King;
import me.Muchysta.Chess.Controller.Figures.Pawn;
import me.Muchysta.Chess.Controller.Figures.Queen;
import me.Muchysta.Chess.Controller.Figures.Rook;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class PromotionProcessTest {


    @Test
    public void pawnPromotionProcessTest() {
        //arrange
        Object jedna = new Object();
        Object dva = new Object();
        GameSettings gameSettings = new GameSettings(jedna, dva, 50, false, "");
        Communicator communicator = new Communicator(jedna, dva){
            @Override
            protected void sendMessage(Object player, String text, MessageFormat format) {
            }

            @Override
            public void displayTimes(Timer timer, Game game, Object player) {
            }

            @Override
            public void vanishTimes(Object player) {
            }
        };
        Game game = new Game(gameSettings, communicator);

        Pawn whitePawnToPromote = (Pawn) game.getFigure(1, 0);
        Pawn blackPawnToPromote = (Pawn) game.getFigure(6, 6);

        game.getGameManager().executeTurn(whitePawnToPromote, 3, 0);
        game.getGameManager().executeTurn(blackPawnToPromote, 4, 6);

        game.getGameManager().executeTurn(whitePawnToPromote, 4, 0);
        game.getGameManager().executeTurn(blackPawnToPromote, 3, 6);

        game.getGameManager().executeTurn(whitePawnToPromote, 5, 0);
        game.getGameManager().executeTurn(blackPawnToPromote, 2, 6);

        //here comes the first bug
        game.getGameManager().executeTurn(whitePawnToPromote, 6, 1);
        game.getGameManager().executeTurn(blackPawnToPromote, 1, 7);

        //promoting moves
        game.getGameManager().executeTurn(whitePawnToPromote, 7, 0);
        game.getGameManager().executeTurn(blackPawnToPromote, 0, 6);

        assertTrue(game.getFigure(7, 0) instanceof Queen);
        assertTrue(game.getFigure(0, 6) instanceof Queen);

        game.getGameManager().executeTurn(game.getFigure(7, 0), 7, 1);
        game.getGameManager().executeTurn(game.getFigure(0, 6), 0, 7);

    }

}
