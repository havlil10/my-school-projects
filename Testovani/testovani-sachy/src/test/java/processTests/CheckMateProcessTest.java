package processTests;

import me.Muchysta.Chess.Controller.*;
import me.Muchysta.Chess.Controller.Abstracts.Communicator;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class CheckMateProcessTest {


    @Test
    public void checkMateProcessTest() {
        //arrange
        Object jedna = new Object();
        Object dva = new Object();
        GameSettings gameSettings = new GameSettings(jedna, dva, 50, false, "");
        Communicator communicator = new Communicator(jedna, dva){
            @Override
            protected void sendMessage(Object player, String text, MessageFormat format) {
            }

            @Override
            public void displayTimes(Timer timer, Game game, Object player) {
            }

            @Override
            public void vanishTimes(Object player) {
            }
        };
        Game game = new Game(gameSettings, communicator);

        //quickest check mate possible - 4 turns
        game.getGameManager().executeTurn(game.getFigure(1, 6), 3, 6);
        assertNull(game.getFigure(1, 6));
        assertNotNull(game.getFigure(3, 6));

        game.getGameManager().executeTurn(game.getFigure(6, 4), 5, 4);
        assertNull(game.getFigure(6, 4));
        assertNotNull(game.getFigure(5, 4));

        game.getGameManager().executeTurn(game.getFigure(1, 5), 3, 5);
        assertNull(game.getFigure(1, 5));
        assertNotNull(game.getFigure(3, 5));

        game.getGameManager().executeTurn(game.getFigure(7, 3), 3, 7);
        assertNull(game.getFigure(7, 3));
        assertNotNull(game.getFigure(3, 7));

        assertSame(game.getStage(), Stage.ENDED);
        assertSame(game.getResult(), GameResult.BLACK_WIN);




    }

}
