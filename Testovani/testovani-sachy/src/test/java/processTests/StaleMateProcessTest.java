package processTests;

import me.Muchysta.Chess.Controller.*;
import me.Muchysta.Chess.Controller.Abstracts.Communicator;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class StaleMateProcessTest {

    @Test
    public void staleMateProcessTest() {
        //arrange
        Object jedna = new Object();
        Object dva = new Object();
        GameSettings gameSettings = new GameSettings(jedna, dva, 50, false, "");
        Communicator communicator = new Communicator(jedna, dva){
            @Override
            protected void sendMessage(Object player, String text, MessageFormat format) {
            }

            @Override
            public void displayTimes(Timer timer, Game game, Object player) {
            }

            @Override
            public void vanishTimes(Object player) {
            }
        };
        Game game = new Game(gameSettings, communicator);

        game.getGameManager().executeTurn(game.getFigure(1, 2), 3, 2);
        game.getGameManager().executeTurn(game.getFigure(6, 7), 4, 7);
        assertNull(game.getFigure(1, 2));
        assertNotNull(game.getFigure(3, 2));
        assertNull(game.getFigure(6, 7));
        assertNotNull(game.getFigure(4, 7));

        game.getGameManager().executeTurn(game.getFigure(1, 7), 3, 7);
        game.getGameManager().executeTurn(game.getFigure(6, 0), 4, 0);

        game.getGameManager().executeTurn(game.getFigure(0, 3), 3, 0);
        game.getGameManager().executeTurn(game.getFigure(7, 0), 5, 0);

        game.getGameManager().executeTurn(game.getFigure(3, 0), 4, 0);
        game.getGameManager().executeTurn(game.getFigure(5, 0), 5, 7);

        game.getGameManager().executeTurn(game.getFigure(4, 0), 6, 2);
        game.getGameManager().executeTurn(game.getFigure(6, 5), 5, 5);

        game.getGameManager().executeTurn(game.getFigure(6, 2), 6, 3);
        game.getGameManager().executeTurn(game.getFigure(7, 4), 6, 5);

        game.getGameManager().executeTurn(game.getFigure(6, 3), 6, 1);
        game.getGameManager().executeTurn(game.getFigure(7, 3), 2, 3);

        game.getGameManager().executeTurn(game.getFigure(6, 1), 7, 1);
        game.getGameManager().executeTurn(game.getFigure(2, 3), 6, 7);

        game.getGameManager().executeTurn(game.getFigure(7, 1), 7, 2);
        game.getGameManager().executeTurn(game.getFigure(6, 5), 5, 6);

        //this turn caused a draw
        game.getGameManager().executeTurn(game.getFigure(7, 2), 5, 4);

        assertSame(game.getStage(), Stage.ENDED);
        assertSame(game.getResult(), GameResult.DRAW);

    }

}
