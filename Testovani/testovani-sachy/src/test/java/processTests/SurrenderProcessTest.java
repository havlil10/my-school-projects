package processTests;

import me.Muchysta.Chess.Controller.*;
import me.Muchysta.Chess.Controller.Abstracts.Communicator;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class SurrenderProcessTest {


    @Test
    public void surrenderProcessTest() {
        //arrange
        Object jedna = new Object();
        Object dva = new Object();
        GameSettings gameSettings = new GameSettings(jedna, dva, 50, false, "");
        Communicator communicator = new Communicator(jedna, dva){
            @Override
            protected void sendMessage(Object player, String text, MessageFormat format) {
            }

            @Override
            public void displayTimes(Timer timer, Game game, Object player) {
            }

            @Override
            public void vanishTimes(Object player) {
            }
        };
        Game game = new Game(gameSettings, communicator);

        game.getGameManager().executeTurn(game.getFigure(1, 6), 3, 6);

        game.makeSurrender(false);

        assertSame(game.getStage(), Stage.ENDED);
        assertSame(game.getResult(), GameResult.WHITE_WIN);

    }

}
