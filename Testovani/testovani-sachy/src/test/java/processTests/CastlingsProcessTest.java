package processTests;

import me.Muchysta.Chess.Controller.*;
import me.Muchysta.Chess.Controller.Abstracts.Communicator;
import me.Muchysta.Chess.Controller.Figures.King;
import me.Muchysta.Chess.Controller.Figures.Knight;
import me.Muchysta.Chess.Controller.Figures.Queen;
import me.Muchysta.Chess.Controller.Figures.Rook;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class CastlingsProcessTest {


    @Test
    public void gameWithBothCastlingsProcessTest() {
        //arrange
        Object jedna = new Object();
        Object dva = new Object();
        GameSettings gameSettings = new GameSettings(jedna, dva, 50, false, "");
        Communicator communicator = new Communicator(jedna, dva){
            @Override
            protected void sendMessage(Object player, String text, MessageFormat format) {
            }

            @Override
            public void displayTimes(Timer timer, Game game, Object player) {
            }

            @Override
            public void vanishTimes(Object player) {
            }
        };
        Game game = new Game(gameSettings, communicator);

        game.getGameManager().executeTurn(game.getFigure(0, 1), 2, 0);
        assertNull(game.getFigure(0, 1));
        assertNotNull(game.getFigure(2, 0));

        game.getGameManager().executeTurn(game.getFigure(7, 6), 5, 7);
        assertNull(game.getFigure(7, 6));
        assertNotNull(game.getFigure(5, 7));

        game.getGameManager().executeTurn(game.getFigure(1, 3), 3, 3);
        assertNull(game.getFigure(1, 3));
        assertNotNull(game.getFigure(3, 3));

        game.getGameManager().executeTurn(game.getFigure(6, 4), 4, 4);
        assertNull(game.getFigure(6, 4));
        assertNotNull(game.getFigure(4, 4));

        game.getGameManager().executeTurn(game.getFigure(0, 2), 2, 4);
        assertNull(game.getFigure(0, 2));
        assertNotNull(game.getFigure(2, 4));

        game.getGameManager().executeTurn(game.getFigure(7, 5), 5, 3);
        assertNull(game.getFigure(7, 5));
        assertNotNull(game.getFigure(5, 3));

        //first bug with queen
        assertTrue(game.getFigure(0, 3) instanceof Queen);
        assertNull(game.getFigure(1, 3));
        game.getGameManager().executeTurn(game.getFigure(0, 3), 1, 3);

        //rooks on A1 and H8 are now ready for castling move test
        game.getGameManager().executeTurn(game.getFigure(0, 4), 0, 2);
        assertTrue(game.getFigure(0, 2) instanceof King);
        assertTrue(game.getFigure(0, 3) instanceof Rook);


        game.moveFigure(game.getFigure(7, 4), 7, 6);
        assertTrue(game.getFigure(7, 6) instanceof King);
        assertTrue(game.getFigure(7, 7) instanceof Rook);


    }

}
