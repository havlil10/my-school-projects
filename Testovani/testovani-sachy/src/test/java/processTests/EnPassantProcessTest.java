package processTests;

import me.Muchysta.Chess.Controller.*;
import me.Muchysta.Chess.Controller.Abstracts.Communicator;
import me.Muchysta.Chess.Controller.Figures.King;
import me.Muchysta.Chess.Controller.Figures.Rook;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class EnPassantProcessTest {


    @Test
    public void gameWithEnPassantProcessTest() {
        //arrange
        Object jedna = new Object();
        Object dva = new Object();
        GameSettings gameSettings = new GameSettings(jedna, dva, 50, false, "");
        Communicator communicator = new Communicator(jedna, dva){
            @Override
            protected void sendMessage(Object player, String text, MessageFormat format) {
            }

            @Override
            public void displayTimes(Timer timer, Game game, Object player) {
            }

            @Override
            public void vanishTimes(Object player) {
            }
        };
        Game game = new Game(gameSettings, communicator);

        game.getGameManager().executeTurn(game.getFigure(1, 3), 3, 3);
        assertNull(game.getFigure(1, 3));
        assertNotNull(game.getFigure(3, 3));

        game.getGameManager().executeTurn(game.getFigure(7, 6), 5, 7);
        assertNull(game.getFigure(7, 6));
        assertNotNull(game.getFigure(5, 7));

        game.getGameManager().executeTurn(game.getFigure(3, 3), 4, 3);
        assertNull(game.getFigure(3, 3));
        assertNotNull(game.getFigure(4, 3));

        game.getGameManager().executeTurn(game.getFigure(6, 2), 4, 2);
        assertNull(game.getFigure(6, 2));
        assertNotNull(game.getFigure(4, 2));

        game.getGameManager().executeTurn(game.getFigure(4, 3), 5, 2);
        assertNull(game.getFigure(4, 3));
        assertNotNull(game.getFigure(5, 2));

        assertNull(game.getFigure(4, 2));

    }

}
