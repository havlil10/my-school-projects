package figuresUnitTests.Figures;

import me.Muchysta.Chess.Controller.Figures.King;
import me.Muchysta.Chess.Controller.Game;
import me.Muchysta.Chess.Controller.Position;

import java.util.HashSet;
import java.util.Set;

import static org.mockito.Mockito.doReturn;

public class TestableKing extends King {

    public TestableKing(Game game, int rank, int file, boolean isWhite) {
        super(game, rank, file, isWhite);
    }

    @Override
    protected Set<Position> possibleMovesByOne() {
        Set<Position> possibleMovesByOne = new HashSet<>();
        possibleMovesByOne.add(new Position(5, 3));
        possibleMovesByOne.add(new Position(5, 4));
        possibleMovesByOne.add(new Position(5, 5));
        possibleMovesByOne.add(new Position(4, 3));
        possibleMovesByOne.add(new Position(4, 5));
        possibleMovesByOne.add(new Position(3, 3));
        possibleMovesByOne.add(new Position(3, 4));
        possibleMovesByOne.add(new Position(3, 5));
        return possibleMovesByOne;
    }

    @Override
    protected Set<Position> possibleCastles() {
        return new HashSet<>();
    }
}
