package figuresUnitTests.Figures;

import me.Muchysta.Chess.Controller.Figures.Rook;
import me.Muchysta.Chess.Controller.Game;
import me.Muchysta.Chess.Controller.Position;

import java.util.HashSet;
import java.util.Set;

public class TestableRook extends Rook {
    public TestableRook(Game game, int rank, int file, boolean isWhite) {
        super(game, rank, file, isWhite);
    }

    @Override
    protected Set<Position> movesInRank() {
        Set<Position> movesInRank = new HashSet<>();
        movesInRank.add(new Position(4, 0));
        movesInRank.add(new Position(4, 1));
        movesInRank.add(new Position(4, 2));
        movesInRank.add(new Position(4, 4));
        movesInRank.add(new Position(4, 5));
        movesInRank.add(new Position(4, 6));
        movesInRank.add(new Position(4, 7));
        return movesInRank;
    }

    @Override
    protected Set<Position> movesInFile() {
        Set<Position> movesInFile = new HashSet<>();
        movesInFile.add(new Position(2, 3));
        movesInFile.add(new Position(3, 3));
        movesInFile.add(new Position(5, 3));
        movesInFile.add(new Position(6, 3));
        return movesInFile;
    }
}
