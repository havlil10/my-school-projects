package figuresUnitTests.Figures;

import me.Muchysta.Chess.Controller.Position;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

public class PositionSetComparator {
    static void arePositionsContainedInSetsTheSame(Set<Position> actualCandidateMoves, Set<Position> expectedCandidateMoves) {

        if (actualCandidateMoves.size() != expectedCandidateMoves.size()) {
            fail("all possible moves are not of the same size as expected");

        } else {
            for (Position actualCandidateMove : actualCandidateMoves) {

                boolean foundSamePosition = false;
                for (Position expectedCandidateMove : expectedCandidateMoves) {
                    if (actualCandidateMove.getRank() == expectedCandidateMove.getRank() && actualCandidateMove.getFile() == expectedCandidateMove.getFile()) {
                        foundSamePosition = true;
                        break;
                    }
                }
                assertTrue(foundSamePosition);

            }
        }
    }
}
