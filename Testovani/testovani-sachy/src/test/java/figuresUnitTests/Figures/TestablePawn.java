package figuresUnitTests.Figures;

import me.Muchysta.Chess.Controller.Figures.Pawn;
import me.Muchysta.Chess.Controller.Game;
import me.Muchysta.Chess.Controller.Position;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

public class TestablePawn extends Pawn {
    public TestablePawn(Game game, int rank, int file, boolean isWhite) {
        super(game, rank, file, isWhite);
    }

    @Override
    protected Set<Position> possibleStraightMoves() {
        Set<Position> possibleStraightMoves = new HashSet<>();
        possibleStraightMoves.add(new Position(5, 2));
        possibleStraightMoves.add(new Position(4, 2));
        return possibleStraightMoves;
    }

    @Override
    protected Set<Position> possibleDiagonalMoves() {
        Set<Position> possibleDiagonalMoves = new HashSet<>();
        possibleDiagonalMoves.add(new Position(5, 3));
        return possibleDiagonalMoves;
    }
}
