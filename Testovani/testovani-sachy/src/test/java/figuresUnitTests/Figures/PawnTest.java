package figuresUnitTests.Figures;

import figuresUnitTests.TestableGame;
import me.Muchysta.Chess.Controller.*;
import me.Muchysta.Chess.Controller.Abstracts.Communicator;
import me.Muchysta.Chess.Controller.Figures.Pawn;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.mockito.Mockito.mock;

public class PawnTest {

    @Test
    public void getPossibleMoves_newGameWithWhitePawnOnD6thenPawnOnC7_returnsCorrectSet() {
        //arrange
        GameSettings settings = mock(GameSettings.class);
        Communicator communicator = mock(Communicator.class);
        Game game = new TestableGame(settings, communicator);

        Pawn pawn = new Pawn(game, 5, 3, true);
        game.moveFigure(pawn, 5, 3);

        TestablePawn testedPawn = new TestablePawn(game , 6, 2, false);

        Set<Position> expectedCandidateMoves = new HashSet<>();
        expectedCandidateMoves.add(new Position(5, 2));
        expectedCandidateMoves.add(new Position(4, 2));
        expectedCandidateMoves.add(new Position(5, 3));

        //act
        Set<Position> resultCandidateMoves = testedPawn.getPossibleMoves();

        //assert
        PositionSetComparator.arePositionsContainedInSetsTheSame(resultCandidateMoves, expectedCandidateMoves);
    }

}

