package figuresUnitTests.Figures;

import figuresUnitTests.TestableGame;
import me.Muchysta.Chess.Controller.*;
import me.Muchysta.Chess.Controller.Abstracts.Communicator;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.mockito.Mockito.mock;

public class KnightTest {

    @Test
    public void getPossibleMoves_newGameWithPawnOnA7ThenKnightOnB8_returnsCorrectSet() {
        //arrange
        GameSettings settings = mock(GameSettings.class);
        Communicator communicator = mock(Communicator.class);
        Game game = new TestableGame(settings, communicator);

        TestableKnight testedKnight = new TestableKnight (game, 7, 1, false);
        game.moveFigure(testedKnight, 7, 1);

        Set<Position> expectedCandidateMoves = new HashSet<>();
        expectedCandidateMoves.add(new Position(5, 0));
        expectedCandidateMoves.add(new Position(5, 2));

        //act
        Set<Position> resultCandidateMoves = testedKnight.getPossibleMoves();

        //assert
        PositionSetComparator.arePositionsContainedInSetsTheSame(resultCandidateMoves, expectedCandidateMoves);
    }

}
