package figuresUnitTests.Figures;

import figuresUnitTests.TestableGame;
import me.Muchysta.Chess.Controller.*;
import me.Muchysta.Chess.Controller.Abstracts.Communicator;
import me.Muchysta.Chess.Controller.Figures.Queen;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;

import java.util.HashSet;
import java.util.Set;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockStatic;

public class QueenTest {


    @Test
    public void getPossibleMoves_newGameWithNewQueenOnD4_returnsCorrectSet() {
        //arrange
        GameSettings settings = mock(GameSettings.class);
        Communicator communicator = mock(Communicator.class);
        Game game = new TestableGame(settings, communicator);

        try (MockedStatic mockedStatic = mockStatic(Game.class)) {
            final int a = anyInt();
            final int b = anyInt();
            mockedStatic.when(() -> Game.isPositionLegal(a, b)).thenReturn(TestableGame.areBetween0and8(a, b));
        }

        Queen testedQueen = new Queen(game, 3, 3, true);
        game.moveFigure(testedQueen, 3, 3);


        Set<Position> expectedCandidateMoves = new HashSet<>();
        expectedCandidateMoves.add(new Position(3, 0));
        expectedCandidateMoves.add(new Position(3, 1));
        expectedCandidateMoves.add(new Position(3, 2));
        expectedCandidateMoves.add(new Position(3, 4));
        expectedCandidateMoves.add(new Position(3, 5));
        expectedCandidateMoves.add(new Position(3, 6));
        expectedCandidateMoves.add(new Position(3, 7));
        expectedCandidateMoves.add(new Position(2, 3));
        expectedCandidateMoves.add(new Position(4, 3));
        expectedCandidateMoves.add(new Position(5, 3));
        expectedCandidateMoves.add(new Position(6, 3));
        //diagonals
        expectedCandidateMoves.add(new Position(4, 2));
        expectedCandidateMoves.add(new Position(4, 4));
        expectedCandidateMoves.add(new Position(5, 1));
        expectedCandidateMoves.add(new Position(5, 5));
        expectedCandidateMoves.add(new Position(6, 0));
        expectedCandidateMoves.add(new Position(6, 6));
        expectedCandidateMoves.add(new Position(2, 2));
        expectedCandidateMoves.add(new Position(2, 4));


        //act
        Set<Position> resultCandidateMoves = testedQueen.getPossibleMoves();

        //assert
        PositionSetComparator.arePositionsContainedInSetsTheSame(resultCandidateMoves, expectedCandidateMoves);
    }

}

