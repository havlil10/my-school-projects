package figuresUnitTests.Figures;

import figuresUnitTests.TestableGame;
import me.Muchysta.Chess.Controller.*;
import me.Muchysta.Chess.Controller.Abstracts.Communicator;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.mockito.Mockito.*;

public class KingTest {

    @Test
    public void getPossibleMoves_newGameWithKingMovedToE5_returnsCorrectSet() {
        //arrange
        GameSettings settings = mock(GameSettings.class);
        Communicator communicator = mock(Communicator.class);
        Game game = new TestableGame(settings, communicator);

        TestableKing testedWhiteKing = new TestableKing(game, 4, 4, true);
        game.moveFigure(testedWhiteKing, 4, 4);

        Set<Position> expectedCandidateMoves = new HashSet<>();
        expectedCandidateMoves.add(new Position(5, 3));
        expectedCandidateMoves.add(new Position(5, 4));
        expectedCandidateMoves.add(new Position(5, 5));
        expectedCandidateMoves.add(new Position(4, 3));
        expectedCandidateMoves.add(new Position(4, 5));
        expectedCandidateMoves.add(new Position(3, 3));
        expectedCandidateMoves.add(new Position(3, 4));
        expectedCandidateMoves.add(new Position(3, 5));

        //act
        Set<Position> resultCandidateMoves = testedWhiteKing.getPossibleMoves();

        //assert
        PositionSetComparator.arePositionsContainedInSetsTheSame(resultCandidateMoves, expectedCandidateMoves);
    }

}

