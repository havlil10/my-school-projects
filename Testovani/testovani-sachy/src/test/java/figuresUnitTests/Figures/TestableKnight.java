package figuresUnitTests.Figures;

import me.Muchysta.Chess.Controller.Figures.Knight;
import me.Muchysta.Chess.Controller.Game;

public class TestableKnight extends Knight {
    public TestableKnight(Game game, int rank, int file, boolean isWhite) {
        super(game, rank, file, isWhite);
    }

    @Override
    protected boolean canMoveThere(int testedPositionRank, int testedPositionFile) {
        return testedPositionRank == 5 && (testedPositionFile == 2 || testedPositionFile == 0);
    }
}
