package figuresUnitTests;

import me.Muchysta.Chess.Controller.Abstracts.Communicator;
import me.Muchysta.Chess.Controller.Figures.Figure;
import me.Muchysta.Chess.Controller.Game;
import me.Muchysta.Chess.Controller.GameSettings;

public class TestableGame extends Game {

    public TestableGame(GameSettings gameSettings, Communicator communicator) {
        super(gameSettings, communicator);
    }

    @Override
    public Figure getFigure(int positionRank, int positionFile) {
        return battleField[positionRank][positionFile];
    }

    public static Object areBetween0and8(int a, int b) {
        boolean aGE0 = a >= 0;
        boolean bGE0 = b >= 0;
        boolean aL8 = a < 8;
        boolean bL8 = b < 8;
        return aGE0 && bGE0 && aL8 && bL8;
    }
}
