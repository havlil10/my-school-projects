package integrationTests;

import me.Muchysta.Chess.Controller.*;
import me.Muchysta.Chess.Controller.Abstracts.Communicator;
import me.Muchysta.Chess.Controller.Figures.*;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class CommunicatorTest {


    @Test
    public void executeMove_sendsCorrectMessages() {
        //arrange
        Object jedna = new Object();
        Object dva = new Object();
        GameSettings gameSettings = new GameSettings(jedna, dva, 50, false, "");
        ArrayList<String> sentMessages = new ArrayList<>();
        ArrayList<MessageFormat> messageFormats = new ArrayList<>();
        Communicator communicator = new Communicator(jedna, dva){
            @Override
            protected void sendMessage(Object player, String text, MessageFormat format) {
                sentMessages.add(text);
                messageFormats.add(format);
            }

            @Override
            public void displayTimes(Timer timer, Game game, Object player) {
            }

            @Override
            public void vanishTimes(Object player) {
            }
        };
        Game game = new Game(gameSettings, communicator);

        game.getGameManager().executeTurn(game.getFigure(1, 6), 3, 6);
        game.getGameManager().executeTurn(game.getFigure(6, 4), 5, 4);
        game.getGameManager().executeTurn(game.getFigure(1, 5), 3, 5);
        game.getGameManager().executeTurn(game.getFigure(7, 3), 3, 7);

        assertSame(game.getStage(), Stage.ENDED);
        assertSame(game.getResult(), GameResult.BLACK_WIN);

        assertTrue(messageFormats.contains(MessageFormat.INSTRUCTION));
        assertTrue(messageFormats.contains(MessageFormat.FLEETING));
        assertTrue(messageFormats.contains(MessageFormat.GREAT));

        assertEquals(messageFormats.size(), 10);

        String expectedFirstString = "Opponent's move: PAWN from g2 to g4.";
        String expectedCheckMateAnnouncement = "Black win!";
        assertEquals(sentMessages.get(0), expectedFirstString);
        assertEquals(sentMessages.get(sentMessages.size()-2), expectedCheckMateAnnouncement);

    }

}

