package integrationTests;

import me.Muchysta.Chess.Controller.*;
import me.Muchysta.Chess.Controller.Abstracts.Communicator;
import me.Muchysta.Chess.Controller.Figures.*;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class GameManagerTest {


    @Test
    public void executeMove_moveEveryPieceClass_leavesCorrectBoard() {
        //arrange
        Object jedna = new Object();
        Object dva = new Object();
        GameSettings gameSettings = new GameSettings(jedna, dva, 50, false, "");
        Communicator communicator = new Communicator(jedna, dva){
            @Override
            protected void sendMessage(Object player, String text, MessageFormat format) {
            }

            @Override
            public void displayTimes(Timer timer, Game game, Object player) {
            }

            @Override
            public void vanishTimes(Object player) {
            }
        };
        Game game = new Game(gameSettings, communicator);

        Pawn testedWhitePawn = (Pawn) game.getFigure(1, 3);
        Knight testedBlackKnight = (Knight) game.getFigure(7, 1);
        Bishop testedWhiteBishop = (Bishop) game.getFigure(0, 2);
        Rook testedBlackRook = (Rook) game.getFigure(7, 0);
        Queen testedWhiteQueen = (Queen) game.getFigure(0, 3);
        King testedWhiteKing = (King) game.getFigure(0, 4);



        game.getGameManager().executeTurn(testedWhitePawn, 3, 3);
        assertNull(game.getFigure(1, 3));
        assertTrue(game.getFigure(3, 3) instanceof Pawn);

        game.getGameManager().executeTurn(testedBlackKnight, 5, 0);
        assertNull(game.getFigure(7, 1));
        assertTrue(game.getFigure(5, 0) instanceof Knight);

        game.getGameManager().executeTurn(testedWhiteBishop, 3, 5);
        assertNull(game.getFigure(0, 2));
        assertTrue(game.getFigure(3, 5) instanceof Bishop);

        game.getGameManager().executeTurn(testedBlackRook, 7, 1);
        assertNull(game.getFigure(7, 0));
        assertTrue(game.getFigure(7, 1) instanceof Rook);

        game.getGameManager().executeTurn(testedWhiteQueen, 2, 3);
        assertNull(game.getFigure(0, 3));
        assertTrue(game.getFigure(2, 3) instanceof Queen);

        game.getGameManager().executeTurn(testedBlackRook, 7, 0);
        assertNull(game.getFigure(7, 1));
        assertTrue(game.getFigure(7, 0) instanceof Rook);

        game.getGameManager().executeTurn(testedWhiteKing, 1, 3);
        assertNull(game.getFigure(0, 4));
        assertTrue(game.getFigure(1, 3) instanceof King);


    }

}

