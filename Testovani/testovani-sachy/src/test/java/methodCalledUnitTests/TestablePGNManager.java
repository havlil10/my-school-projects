package methodCalledUnitTests;

import me.Muchysta.Chess.Controller.Figures.Figure;
import me.Muchysta.Chess.Controller.Figures.FigureType;
import me.Muchysta.Chess.Controller.Game;
import me.Muchysta.Chess.Controller.PGNExtras.PGNManager;

import java.util.HashMap;

import static me.Muchysta.Chess.Controller.Figures.FigureType.KNIGHT;


public class TestablePGNManager extends PGNManager {

    protected static HashMap<FigureType, String> figureTypesToAbbreviations = new HashMap<>();

    static {
        figureTypesToAbbreviations.put(KNIGHT, "N");
    }

    public TestablePGNManager(Game game) {
        super(game);
    }

    @Override
    protected String specifyConcernedFigure(Figure movedFigure, int rankTo, int fileTo) {
        return "";
    }
}
