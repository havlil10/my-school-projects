package methodCalledUnitTests;

import me.Muchysta.Chess.Controller.Abstracts.Communicator;
import me.Muchysta.Chess.Controller.Figures.King;
import me.Muchysta.Chess.Controller.Game;
import me.Muchysta.Chess.Controller.MessageFormat;
import me.Muchysta.Chess.Controller.SquareToTexter;
import me.Muchysta.Chess.Controller.Timer;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;

import static org.mockito.Mockito.*;

public class CastlesTest {

    @Test
    public void informPlayersAboutMove_validParameters_getTypeCalleOnceAndSquareToTextOnlyTwiceAndWithCorrectArguments() {
        // arrange
        King king = mock(King.class);
        Communicator communicator = new TestableCommunicator(mock(Object.class), mock(Object.class)) {
            @Override
            protected void sendMessage(Object player, String text, MessageFormat format) {
            }

            @Override
            public void displayTimes(Timer timer, Game game, Object player) {
            }

            @Override
            public void vanishTimes(Object player) {
            }
        };

        try (MockedStatic<SquareToTexter> mockedStaticSquareToTexter = mockStatic(SquareToTexter.class)) {
            mockedStaticSquareToTexter.when(() -> SquareToTexter.squareToText(anyInt(), anyInt())).thenReturn("a1");

            // act
            communicator.informPlayersAboutMove(true, king, 1, 2, 3, 4);

            // assert
            verify(king, times(1)).getType();

            mockedStaticSquareToTexter.verify(() -> SquareToTexter.squareToText(1, 2), times(1));
            mockedStaticSquareToTexter.verify(() -> SquareToTexter.squareToText(3, 4), times(1));
            mockedStaticSquareToTexter.verify(() -> SquareToTexter.squareToText(anyInt(), anyInt()), atMost(2));
        }
    }
}
