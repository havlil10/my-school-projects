package methodCalledUnitTests;

import me.Muchysta.Chess.Controller.Abstracts.Communicator;
import me.Muchysta.Chess.Controller.Figures.*;
import me.Muchysta.Chess.Controller.Game;
import me.Muchysta.Chess.Controller.GameSettings;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;

public class PlacedFiguresTest {

    @Test
    public void prepareNewBattleField_placedAllChessFiguresOnDefaultSquaresAndAtMost16PawnsAnd4RooksAnd4KnightsAnd4BishopsAnd2QueendAnd2Kings() {
        //arrange
        GameSettings mockedSettings = mock(GameSettings.class);
        Communicator mockedCommunicator = mock(Communicator.class);
        Game spiedGame = spy(new Game(mockedSettings, mockedCommunicator));

        //act
        spiedGame.prepareNewBattleField();

        //assert
        //pawns
        verify(spiedGame, atMost(16)).moveFigure(any(Pawn.class), anyInt(), anyInt());
        verify(spiedGame, times(1)).moveFigure(any(Pawn.class), eq(1), eq(0));
        verify(spiedGame, times(1)).moveFigure(any(Pawn.class), eq(1), eq(1));
        verify(spiedGame, times(1)).moveFigure(any(Pawn.class), eq(1), eq(2));
        verify(spiedGame, times(1)).moveFigure(any(Pawn.class), eq(1), eq(3));
        verify(spiedGame, times(1)).moveFigure(any(Pawn.class), eq(1), eq(4));
        verify(spiedGame, times(1)).moveFigure(any(Pawn.class), eq(1), eq(5));
        verify(spiedGame, times(1)).moveFigure(any(Pawn.class), eq(1), eq(6));
        verify(spiedGame, times(1)).moveFigure(any(Pawn.class), eq(1), eq(7));
        verify(spiedGame, times(1)).moveFigure(any(Pawn.class), eq(6), eq(0));
        verify(spiedGame, times(1)).moveFigure(any(Pawn.class), eq(6), eq(1));
        verify(spiedGame, times(1)).moveFigure(any(Pawn.class), eq(6), eq(2));
        verify(spiedGame, times(1)).moveFigure(any(Pawn.class), eq(6), eq(3));
        verify(spiedGame, times(1)).moveFigure(any(Pawn.class), eq(6), eq(4));
        verify(spiedGame, times(1)).moveFigure(any(Pawn.class), eq(6), eq(5));
        verify(spiedGame, times(1)).moveFigure(any(Pawn.class), eq(6), eq(6));
        verify(spiedGame, times(1)).moveFigure(any(Pawn.class), eq(6), eq(7));

        //others
        verify(spiedGame, atMost(4)).moveFigure(any(Rook.class), anyInt(), anyInt());
        verify(spiedGame, times(1)).moveFigure(any(Rook.class), eq(0), eq(0));
        verify(spiedGame, times(1)).moveFigure(any(Rook.class), eq(0), eq(7));
        verify(spiedGame, times(1)).moveFigure(any(Rook.class), eq(7), eq(0));
        verify(spiedGame, times(1)).moveFigure(any(Rook.class), eq(7), eq(7));

        verify(spiedGame, atMost(4)).moveFigure(any(Knight.class), anyInt(), anyInt());
        verify(spiedGame, times(1)).moveFigure(any(Knight.class), eq(0), eq(1));
        verify(spiedGame, times(1)).moveFigure(any(Knight.class), eq(0), eq(6));
        verify(spiedGame, times(1)).moveFigure(any(Knight.class), eq(7), eq(1));
        verify(spiedGame, times(1)).moveFigure(any(Knight.class), eq(7), eq(6));

        verify(spiedGame, atMost(4)).moveFigure(any(Bishop.class), anyInt(), anyInt());
        verify(spiedGame, times(1)).moveFigure(any(Bishop.class), eq(0), eq(2));
        verify(spiedGame, times(1)).moveFigure(any(Bishop.class), eq(0), eq(5));
        verify(spiedGame, times(1)).moveFigure(any(Bishop.class), eq(7), eq(2));
        verify(spiedGame, times(1)).moveFigure(any(Bishop.class), eq(7), eq(5));

        verify(spiedGame, atMost(2)).moveFigure(any(Queen.class), anyInt(), anyInt());
        verify(spiedGame, times(1)).moveFigure(any(Queen.class), eq(0), eq(3));
        verify(spiedGame, times(1)).moveFigure(any(Queen.class), eq(7), eq(3));

        verify(spiedGame, atMost(2)).moveFigure(any(King.class), anyInt(), anyInt());
        verify(spiedGame, times(1)).moveFigure(any(King.class), eq(0), eq(4));
        verify(spiedGame, times(1)).moveFigure(any(King.class), eq(7), eq(4));
    }
}
