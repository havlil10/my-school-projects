package methodCalledUnitTests;

import me.Muchysta.Chess.View.Minecraft.BlockModificators.MinecraftBoardBuilder;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;

import static org.mockito.Mockito.*;

public class MinecraftBoardBuilderTest {


    @Test
    public void buildMinecraftBoardOfAPlannedGameIfEnoughSpace_notEnoughSpace_correspondingMessageSentToThePlayer() {
        //arrange
        Player mockedPlayer = mock(Player.class);
        Block mockedBlock = mock(Block.class);
        Block mockedBlockAbove = mock(Block.class);
        Location mockedLocation = mock(Location.class);

        when(mockedPlayer.getEyeLocation()).thenReturn(mockedLocation);
        float testingYaw = 1;
        when(mockedLocation.getYaw()).thenReturn(testingYaw);

        try (MockedStatic<MinecraftBoardBuilder> mockedMCBoardBuilder = mockStatic(MinecraftBoardBuilder.class)) {
            mockedMCBoardBuilder.when(() -> MinecraftBoardBuilder.getBlockAbove(mockedBlock)).thenReturn(mockedBlockAbove);
            int[] testingIterationUnits = new int[]{1, 1};
            mockedMCBoardBuilder.when(() -> MinecraftBoardBuilder.getIterationUnitsByYaw(testingYaw)).thenReturn(testingIterationUnits);
            mockedMCBoardBuilder.when(() -> MinecraftBoardBuilder.isSpaceForChess(mockedBlockAbove, testingIterationUnits[0], testingIterationUnits[1])).thenReturn(false);
            mockedMCBoardBuilder.when(() -> MinecraftBoardBuilder.buildMinecraftBoardOfAPlannedGameIfEnoughSpace(mockedPlayer, mockedBlock)).thenCallRealMethod();

            //act
            MinecraftBoardBuilder.buildMinecraftBoardOfAPlannedGameIfEnoughSpace(mockedPlayer, mockedBlock);

            //assert
            verify(mockedPlayer, times(1)).sendMessage("Not enough space to play chess here.");
        }
    }
}
