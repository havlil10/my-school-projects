package methodCalledUnitTests;

import me.Muchysta.Chess.Controller.Abstracts.Communicator;
import me.Muchysta.Chess.Controller.Game;
import me.Muchysta.Chess.Controller.MessageFormat;
import me.Muchysta.Chess.Controller.Timer;

public class TestableCommunicator extends Communicator {

    /**
     * Constructor initializing players to send messages during the game to.
     *
     * @param whitePlayer the player who plays for white.
     * @param blackPlayer the player who plays for black.
     */
    public TestableCommunicator(Object whitePlayer, Object blackPlayer) {
        super(whitePlayer, blackPlayer);
    }

    @Override
    protected void sendMessage(Object player, String text, MessageFormat format) {

    }

    @Override
    public void displayTimes(Timer timer, Game game, Object player) {

    }

    @Override
    public void vanishTimes(Object player) {

    }

    @Override
    protected void sendMessage(Object player, String text) {
    }
}
