package methodCalledUnitTests;

import me.Muchysta.Chess.Controller.Abstracts.Communicator;
import me.Muchysta.Chess.Controller.Figures.Figure;
import me.Muchysta.Chess.Controller.Game;
import me.Muchysta.Chess.Controller.GameSettings;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.mockito.MockedStatic;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

public class GameTest {

    @Test
    public void moveFigure_invalidParameters_IllegalArgumentExceptionThrownAndIsPositionLegalCalledAtMostOnceAndOnlyWithTheseParameters() {
        //arrange
        Game game = new Game(mock(GameSettings.class), mock(Communicator.class));
        Figure mockedFigure = mock(Figure.class);
        try (MockedStatic<Game> mockedStaticGame = mockStatic(Game.class)) {

            //act
            Executable executable = () -> game.moveFigure(mockedFigure, 10, 10);

            //assert
            assertThrows(IllegalArgumentException.class, executable);
            mockedStaticGame.verify(() -> Game.isPositionLegal(10, 10), times(1));
            mockedStaticGame.verify(() -> Game.isPositionLegal(anyInt(), anyInt()), atMostOnce());
        }
    }
}
