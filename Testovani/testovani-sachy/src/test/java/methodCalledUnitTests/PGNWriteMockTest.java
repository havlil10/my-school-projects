package methodCalledUnitTests;

import me.Muchysta.Chess.Controller.*;
import me.Muchysta.Chess.Controller.Figures.FigureType;
import me.Muchysta.Chess.Controller.Figures.Knight;
import me.Muchysta.Chess.Controller.PGNExtras.PGNManager;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

public class PGNWriteMockTest {

    @Test
    public void twoFiguresInSameFile_makingPGN_squareToTextCalled() {
        //arrange
        Game game = mock(Game.class);
        when(game.getTurnNumber()).thenReturn(0);

        Knight mockedKnight = mock(Knight.class);
        when(mockedKnight.isWhite()).thenReturn(true);
        when(mockedKnight.getType()).thenReturn(FigureType.KNIGHT);

        game.moveFigure(mockedKnight, 4, 6);

        PGNManager spiedPGNManager = spy(new TestablePGNManager(game));

        try (MockedStatic<SquareToTexter> mockedSquareToTexter = mockStatic(SquareToTexter.class)) {
            mockedSquareToTexter.when(() -> SquareToTexter.squareToText(anyInt(), anyInt())).then(invocation -> null);

            //act
            spiedPGNManager.generateFirstPartByTurn(
                    mockedKnight, 2, 5,
                    false, false, false);

            //assert
            mockedSquareToTexter.verify(() -> SquareToTexter.squareToText(2, 5), times(1));
        }

    }
}
