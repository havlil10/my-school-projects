package me.Muchysta.Chess.View.Minecraft.FigureShapes;

import me.Muchysta.Chess.Controller.Abstracts.FigureShape;
import me.Muchysta.Chess.Controller.Figures.*;
import me.Muchysta.Chess.View.Minecraft.BlockModificators.MinecraftFigureBuilder;
import me.Muchysta.Chess.View.Minecraft.BoardShapes.MinecraftBoard;
import me.Muchysta.Chess.View.Minecraft.Registers.MinecraftFigureRegister;
import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.plugin.Plugin;

import java.util.HashSet;
import java.util.Set;

import static me.Muchysta.Chess.Controller.Figures.FigureType.*;

public class MinecraftFigure extends FigureShape {

    private Set<Block> blocks;
    private MinecraftFigurePattern pattern;

    /**
     * Creates a new minecraft figure shape representing the specified figure.
     * @param figure the figure the new shape should represent
     */
    public MinecraftFigure(Figure figure) {
        super(figure);
        this.blocks = new HashSet<>();
        this.pattern = determinePattern(figure.getType());
    }

    private MinecraftFigurePattern determinePattern(FigureType figureType) {

        switch (figureType) {
            case BISHOP:
                return MinecraftFigurePattern.MCBISHOP;
            case KING:
                return MinecraftFigurePattern.MCKING;
            case KNIGHT:
                return MinecraftFigurePattern.MCKNIGHT;
            case PAWN:
                return MinecraftFigurePattern.MCPAWN;
            case QUEEN:
                return MinecraftFigurePattern.MCQUEEN;
            case ROOK:
                return MinecraftFigurePattern.MCROOK;
            default:
                return MinecraftFigurePattern.MCDEFAULT;
        }
    }

    @Override
    public void draw() {
        MinecraftFigureBuilder.buildMinecraftFigure(this.getFigure(), (MinecraftBoard) this.getFigure().getGame().getBoardShape());
        register();
    }

    @Override
    public void vanish() {
        MinecraftFigureBuilder.vanishFigureOnBoard(this.getFigure(), (MinecraftBoard) this.getFigure().getGame().getBoardShape());
        deactivate();
    }

    @Override
    public MinecraftFigure transformIntoQueenShape() {
        vanish();
        this.pattern = determinePattern(QUEEN);
        draw();
        return this;
    }

    /**
     * Tells whether the specified block is a part of this shape.
     * @param clickedBlock the specified block of potential figure
     * @return true if the block is a part of this figure shape, false otherwise
     */
    public boolean hasBlock(Block clickedBlock) {
        /*todo: optimalizace - urči podle hodnoty v poli matter*/
        for (Block b : this.blocks) {
            if (b.equals(clickedBlock)) {
                return true;
            }
        }
        return false;
    }

    private void register() {
        MinecraftFigureRegister.registerMinecraftFigure(this);
    }

    private void deactivate() {
        MinecraftFigureRegister.deactivateMinecraftFigure(this);
    }

    /**
     * Returns the pattern this shape is made by.
     * @return this' pattern
     */
    public MinecraftFigurePattern getPattern() {
        return pattern;
    }

    /**
     * Returns this pattern's three dimensional array determining how this shape looks like.
     * @return this pattern's three dimensional array determining how this shape looks like
     */
    public BuildingMaterialType[][][] getMatter() {
        return pattern.getMatter();
    }

    /**
     * Returns a set of all this' blocks.
     * @return a set of all this' blocks
     */
    public Set<Block> getBlocks() {
        return blocks;
    }

}
