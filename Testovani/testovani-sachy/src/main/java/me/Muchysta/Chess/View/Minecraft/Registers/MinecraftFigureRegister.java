package me.Muchysta.Chess.View.Minecraft.Registers;

import me.Muchysta.Chess.Controller.Figures.Figure;
import me.Muchysta.Chess.View.Minecraft.FigureShapes.MinecraftFigure;
import org.bukkit.block.Block;

import java.util.HashSet;
import java.util.Set;

public class MinecraftFigureRegister {

    private static Set<MinecraftFigure> MinecraftFigures = new HashSet<>();

    /**
     * Registers a specified figure shapes, which means it adds it to the list of registered figure shapes for Minecraft.
     *
     * @param minecraftFigure the figure to be registered
     */
    public static void registerMinecraftFigure(MinecraftFigure minecraftFigure) {
        MinecraftFigures.add(minecraftFigure);
    }

    /**
     * Iterates all the registered figures and looks for the one that includes the specified block.
     *
     * @param clickedBlock the specified block which figure this method looks for
     * @return
     */
    public static Figure getFigureByBlock(Block clickedBlock) {
        for (MinecraftFigure minecraftFigure : MinecraftFigures) {
            if (minecraftFigure.hasBlock(clickedBlock)) {
                return minecraftFigure.getFigure();
            }
        }
        return null;
    }

    /**
     * Deactivates a specified figure, which means it removes it from the list of registered figures for Minecraft.
     *
     * @param minecraftFigure the figure to be deactivated
     */
    public static void deactivateMinecraftFigure(MinecraftFigure minecraftFigure) {
        MinecraftFigures.remove(minecraftFigure);
    }
}
