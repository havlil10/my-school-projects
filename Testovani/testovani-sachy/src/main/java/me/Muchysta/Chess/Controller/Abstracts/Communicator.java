package me.Muchysta.Chess.Controller.Abstracts;

import me.Muchysta.Chess.Controller.*;
import me.Muchysta.Chess.Controller.Figures.Figure;

import java.util.Optional;

import static me.Muchysta.Chess.Controller.GameResult.*;
import static me.Muchysta.Chess.Controller.MessageFormat.*;

public abstract class Communicator {
    protected Object whitePlayer;
    protected Object blackPlayer;

    /**
     * Constructor initializing players to send messages during the game to.
     * @param whitePlayer the player who plays for white.
     * @param blackPlayer the player who plays for black.
     */
    public Communicator(Object whitePlayer, Object blackPlayer) {
        this.whitePlayer = whitePlayer;
        this.blackPlayer = blackPlayer;
    }

    /**
     * Sends a message saying that they are on turn
     * @param playerOnTurnIsTheWhiteOne
     */
    public void informPlayerWhoIsOnTurn(boolean playerOnTurnIsTheWhiteOne) {
        if (playerOnTurnIsTheWhiteOne) {
            sendMessage(whitePlayer, "White turn.", FLEETING);
        } else {
            sendMessage(blackPlayer, "Black turn.", FLEETING);
        }
    }

    /**
     * Informs specified player that they have selected figure and which one it is.
     * @param playerOnTurnIsTheWhiteOne true if the selecting player is the white one, false otherwise
     * @param selectedFigure figure the player has selected
     */
    public void informPlayerAboutTheirSelection(boolean playerOnTurnIsTheWhiteOne, Optional<Figure> selectedFigure) {

        Object player = playerOnTurnIsTheWhiteOne ? whitePlayer : blackPlayer;
        String text = selectedFigure.map(figure -> "selected " + figure).orElse("selection canceled");
        sendMessage(player, text, FLEETING);
    }

    /**
     * Decides the game by informing players it has ended and about it's result.
     * @param result information about who has won or about the tie
     */
    public void informPlayersAboutResult(GameResult result) {
        MessageFormat messageFormat = GREAT;
        if (result.equals(WHITE_WIN)) {
            sendMessage(whitePlayer, "White win!", messageFormat);
            sendMessage(blackPlayer, "Black loose.", messageFormat);
        } else if (result.equals(BLACK_WIN)) {
            sendMessage(blackPlayer, "Black win!", messageFormat);
            sendMessage(whitePlayer, "White loose.", messageFormat);
        } else {
            sendMessage(whitePlayer, "It's a draw!", messageFormat);
            sendMessage(blackPlayer, "It's a draw!", messageFormat);
        }
    }

    /**
     * Sends a message in a specified way.
     * @param player addressee of the message
     * @param text what exactly the message says
     * @param format says what type of information is in the message, and therefore defines the way the message will be displayed.
     * @see me.Muchysta.Chess.Controller.Abstracts.Communicator#sendMessage(Object, String)
     */
    protected abstract void sendMessage(Object player, String text, MessageFormat format);

    /**
     * Sends a message as a simple instruction.
     * @param player addressee of the message
     * @param text what exactly the message says
     * @see me.Muchysta.Chess.Controller.Abstracts.Communicator#sendMessage(Object, String, MessageFormat)
     */
    protected void sendMessage(Object player, String text) {
        sendMessage(player, text, MessageFormat.INSTRUCTION);
    }


    /**
     * Sends a message with the informations about the opponent's move so the player don't need to remember what the move was.
     * @param playerOnTurnWasTheWhiteOne true if the player that has finished their move is the white one, false otherwise
     * @param movedFigure figure the player has moved with
     * @param rankFrom the initial rank of the moved figure
     * @param fileFrom the initial file of the moved figure
     * @param rankTo the target rank of the moved figure
     * @param fileTo the target file of the moved figure
     */
    public void informPlayersAboutMove(boolean playerOnTurnWasTheWhiteOne, Figure movedFigure, int rankFrom, int fileFrom, int rankTo, int fileTo) {

        Object reactingPlayer;

        if (playerOnTurnWasTheWhiteOne) {
            reactingPlayer = blackPlayer;
        } else {
            reactingPlayer = whitePlayer;
        }

        String textForReactingPlayer = "Opponent's move: " + movedFigure.getType() +
                " from " + SquareToTexter.squareToText(rankFrom, fileFrom) +
                " to " + SquareToTexter.squareToText(rankTo, fileTo) + ".";

        sendMessage(reactingPlayer, textForReactingPlayer);
    }

    /**
     * Displays timer numbers.
     * @param timer timer taking care about times left for each player.
     * @param game the played game
     * @param player player to whom the times should be displayed
     */
    public abstract void displayTimes(Timer timer, Game game, Object player);

    public abstract void vanishTimes(Object player);

    public void vanishTimesForAll() {
        vanishTimes(this.whitePlayer);
        vanishTimes(this.blackPlayer);
    }
}
