package me.Muchysta.Chess.Controller.PGNExtras;

import java.io.*;
import java.util.Scanner;
import java.util.logging.Logger;

public class PGNFiler {

    private final static Logger LOGGER = Logger.getLogger(PGNFiler.class.getName());
    private static String PATH;

    static {
        File helperFile = new File(".");
        PATH = helperFile.getAbsolutePath().substring(0, helperFile.getAbsolutePath().length() - helperFile.getName().length()) + "plugins\\saved";
    }

    /**
     * Reeds and returns the content of the specified file.
     *
     * @param fileName name of the file of the content to be returned
     * @return the content of the file with the given name
     */
    public static String loadPGN(String fileName) throws FileNotFoundException {

        File file = new File(PATH + "\\" + fileName);
        Scanner inner = new Scanner(file);
        StringBuilder inned = new StringBuilder();
        while (inner.hasNextLine()) {
            inned.append(inner.nextLine());
            if (inner.hasNextLine()) {
                inned.append("\n");
            }
        }
        LOGGER.fine("Successfully loaded content of this file: " + fileName + ".");
        return inned.toString();

    }

    /**
     * Saves the string into a file of the given name if that file doesn't exist yet.
     *
     * @param stringPGN the string to be saved
     * @param fileName  the name under which the string should be saved
     * @return true if the saving was successfull
     */
    public static void savePGN(String stringPGN, String fileName) throws IOException {

        if (new File(PATH).mkdir()) {
            LOGGER.severe("Created a directory for saved games, because it was missing! Path: \"" + PATH + "\".");
        }

        File file = new File(PATH + "\\" + fileName);
        if (file.createNewFile()) {
            FileWriter outer = new FileWriter(file);
            outer.write(stringPGN);
            outer.close();
            LOGGER.fine("Successfully created " + fileName + ".");
        }
    }

    /**
     * Deletes the file with the given name.
     *
     * @param fileName the name of the file to be deleted
     * @return Successfullness of the deletion
     */
    public static boolean deletePGN(String fileName) {
        LOGGER.info("Deleting " + fileName + ".");
        return new File(PATH + "\\" + fileName).delete();
    }
}
