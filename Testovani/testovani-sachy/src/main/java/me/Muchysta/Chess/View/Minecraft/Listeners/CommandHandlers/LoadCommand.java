package me.Muchysta.Chess.View.Minecraft.Listeners.CommandHandlers;

import me.Muchysta.Chess.Controller.PGNExtras.PGNFiler;
import org.bukkit.command.CommandSender;

import java.io.FileNotFoundException;
import java.util.logging.Logger;

public class LoadCommand {

    private final static Logger LOGGER = Logger.getLogger(PGNFiler.class.getName());

    protected static void handle(CommandSender sender, String[] args) {

        String[] notPGNArgs = new String[args.length - 1];
        for (int i = 0; i < notPGNArgs.length; i++) {
            notPGNArgs[i] = args[i + 1];
        }

        try {
            String stringPGN = PGNFiler.loadPGN(args[0]);
            sender.sendMessage("Successfully loaded " + args[0] + ".");
            ChessCommand.handle(sender, notPGNArgs, stringPGN);

        } catch (FileNotFoundException e) {
            sender.sendMessage("Loading failed. " + args[0] + "is missing");
            LOGGER.info(args[0] + " not found amongst saved games.");
            e.printStackTrace();
        }
    }
}
