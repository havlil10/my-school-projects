package me.Muchysta.Chess.Controller;

/**
 * Simple enumeration used to distinguish types of information sended to chess players.
 */
public enum MessageFormat {
    INSTRUCTION,
    GREAT,
    FLEETING
}
