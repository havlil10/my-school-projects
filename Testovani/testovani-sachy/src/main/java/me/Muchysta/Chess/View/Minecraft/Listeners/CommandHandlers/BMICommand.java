package me.Muchysta.Chess.View.Minecraft.Listeners.CommandHandlers;

import org.bukkit.command.CommandSender;

import java.util.Calendar;

public class BMICommand {
    public static void handle(CommandSender sender, String[] args) {

        double height = Double.parseDouble(args[0]);
        if (height > 10) {
            height /= 100;
        }
        double weight = Double.parseDouble(args[1]);
        int yearOfBirth = Integer.parseInt(args[2]);
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        int age = currentYear - yearOfBirth;

        if (age<0 || age>120) {
            sender.sendMessage("invalid age");
        } else if (weight <= 0) {
            sender.sendMessage("invalid weight");
        } else if (height <=0) {
            sender.sendMessage("invalid height");
        } else if (age <= 20) {
            sender.sendMessage("You are too young to get your BMI calculated.");
        } else {
            double BMI = weight / (height * height);
            sender.sendMessage("Calculated BMI: " + BMI);
        }
    }
}
