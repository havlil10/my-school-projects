package me.Muchysta.Chess.View.Minecraft.BlockModificators;

import me.Muchysta.Chess.Controller.Abstracts.Communicator;
import me.Muchysta.Chess.Controller.Game;
import me.Muchysta.Chess.Controller.GameSettings;
import me.Muchysta.Chess.View.Minecraft.BoardShapes.MinecraftBoard;
import me.Muchysta.Chess.View.Minecraft.BoardShapes.MinecraftHorizontalBoard;
import me.Muchysta.Chess.View.Minecraft.MinecraftCommunicator;
import me.Muchysta.Chess.View.Minecraft.Registers.MinecraftPlannedSettingsRegister;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

public class MinecraftBoardBuilder {

    public static final int NUMBER_OF_RANKS_OR_FILES = 8;
    public static final Material WHITE_SQUARE_MATERIAL = Material.IRON_BLOCK;
    public static final Material BLACK_SQUARE_MATERIAL = Material.COAL_BLOCK;
    private static final int FIGURE_HEIGHT = 21;
    public static final int SQUARE_SIZE = 9;
    private static final float YAW_OF_POSITIVE_Z = 0;
    private static final float YAW_OF_NEGATIVE_X = 90;
    private static final float YAW_OF_NEGATIVE_Z = 180;
    private static final float YAW_OF_POSITIVE_X = 270;


    /**
     * Looks for a block in the way of building a new chess board and returns true if it finds any, false otherwise.
     *
     * @param originalBlock
     * @param xIterationUnit
     * @param zIterationUnit
     * @return true if there is enough space to build a new chess board, false otherwise
     */
    public static boolean isSpaceForChess(Block originalBlock, int xIterationUnit, int zIterationUnit) {

        World world = originalBlock.getWorld();

        int startingX = originalBlock.getX();
        int endingX = originalBlock.getX() + xIterationUnit * NUMBER_OF_RANKS_OR_FILES * SQUARE_SIZE;
        int startingY = originalBlock.getY();
        int endingY = originalBlock.getY() + 1 + FIGURE_HEIGHT;
        int startingZ = originalBlock.getZ();
        int endingZ = originalBlock.getZ() + zIterationUnit * NUMBER_OF_RANKS_OR_FILES * SQUARE_SIZE;

        for (int x = startingX; x != endingX; x += xIterationUnit) {
            for (int z = startingZ; z != endingZ; z += zIterationUnit) {
                for (int y = startingY; y < endingY; y++) {
                    if (!world.getBlockAt(x, y, z).getType().isAir()) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    private static void buildSquare(Material buildingMaterial, World world, Location initialLocation, int squareXShift, int squareZShift, int squareSide) {
        for (int x = 0; x < squareSide; x++) {
            for (int z = 0; z < squareSide; z++) {

                world.getBlockAt(
                        initialLocation.getBlockX() + squareXShift * squareSide + x,
                        initialLocation.getBlockY(),
                        initialLocation.getBlockZ() + squareZShift * squareSide + z
                ).setType(buildingMaterial);

            }
        }
    }

    private static Material determineBuildingMaterial(boolean startingWithBlack) {
        return startingWithBlack ? BLACK_SQUARE_MATERIAL : WHITE_SQUARE_MATERIAL;
    }

    private static Material changeMaterial(Material currentBuildingMaterial) {
        return currentBuildingMaterial == BLACK_SQUARE_MATERIAL ? WHITE_SQUARE_MATERIAL : BLACK_SQUARE_MATERIAL;
    }

    /**
     * Builds a chess board for a specified player if there is enough space for it.
     *
     * @param player      player whose settings should be loaded from a register
     * @param targetBlock block marked as the position of the new chess board
     */
    public static void buildMinecraftBoardOfAPlannedGameIfEnoughSpace(Player player, Block targetBlock) {

        Block originalBlock = getBlockAbove(targetBlock);

        int[] iterationUnits = getIterationUnitsByYaw((player.getEyeLocation().getYaw() + 360) % 360);      //getYaw() někdy vrací opačná čísla
        int xIterationUnit = iterationUnits[0];
        int zIterationUnit = iterationUnits[1];

        if (isSpaceForChess(originalBlock, xIterationUnit, zIterationUnit)) {

            GameSettings settings = MinecraftPlannedSettingsRegister.usePlannedSettings(player);
            Communicator communicator = new MinecraftCommunicator(settings.getWhitePlayer(), settings.getBlackPlayer());
            Game game = new Game(settings, communicator);
            MinecraftHorizontalBoard mhb = new MinecraftHorizontalBoard(game, xIterationUnit, zIterationUnit, originalBlock);
            game.setBoardShape(mhb);

            buildMinecraftBoard(mhb);
            MinecraftFigureBuilder.buildAndConnectMinecraftFiguresOnBoard(game);

            game.getPGNManager().initializePGN(game.getGameSettings().getStartingStringPGN());
            player.sendMessage("Board is built. The timer starts when the player who is on turn selects a figure.");

        } else {
            player.sendMessage("Not enough space to play chess here.");
        }
    }

    public static Block getBlockAbove(Block bottomBlock) {
        return bottomBlock.getWorld().getBlockAt(bottomBlock.getX(), bottomBlock.getY() + 1, bottomBlock.getZ());
    }

    public static int[] getIterationUnitsByYaw(float yaw) {
        if (isLookingSoutheast(yaw)) {
            return new int[]{+1, +1};
        } else if (isLookingSouthwest(yaw)) {
            return new int[]{-1, +1};
        } else if (isLookingNorthwest(yaw)) {
            return new int[]{-1, -1};
        } else { //isLookingNorthwest(yaw)
            return new int[]{+1, -1};
        }
    }

    private static boolean isLookingSoutheast(Float yaw) {
        return (yaw >= YAW_OF_POSITIVE_X);
    }

    private static boolean isLookingSouthwest(Float yaw) {
        return (yaw < YAW_OF_NEGATIVE_X);
    }

    private static boolean isLookingNorthwest(Float yaw) {
        return (yaw >= YAW_OF_NEGATIVE_X && yaw < YAW_OF_NEGATIVE_Z);
    }

    private static boolean isLookingNortheast(Float yaw) {
        return (yaw >= MinecraftBoardBuilder.YAW_OF_NEGATIVE_Z && yaw < MinecraftBoardBuilder.YAW_OF_POSITIVE_X);
    }

    /**
     * Builds all the blocks of a chess board.
     *
     * @param minecraftHorizontalBoard board which blocks should be built
     */
    public static void buildMinecraftBoard(MinecraftHorizontalBoard minecraftHorizontalBoard) {
        Material buildingMaterial = determineBuildingMaterial(minecraftHorizontalBoard.doFiguresAttackAlongXNotZAxe());
        Location initialLocation = minecraftHorizontalBoard.getOriginalCornerBlock().getWorld().getBlockAt(
                minecraftHorizontalBoard.getLowestX(), minecraftHorizontalBoard.getY(), minecraftHorizontalBoard.getLowestZ()).getLocation();
        World world = minecraftHorizontalBoard.getOriginalCornerBlock().getWorld();
        int squareSide = minecraftHorizontalBoard.getSquareSide();

        for (int rankOrFile = 0; rankOrFile < NUMBER_OF_RANKS_OR_FILES; rankOrFile++) {
            for (int theOtherOneOfRankAndFile = 0; theOtherOneOfRankAndFile < NUMBER_OF_RANKS_OR_FILES; theOtherOneOfRankAndFile++) {

                buildSquare(buildingMaterial, world, initialLocation, rankOrFile, theOtherOneOfRankAndFile, squareSide);

                buildingMaterial = changeMaterial(buildingMaterial);
            }
            buildingMaterial = changeMaterial(buildingMaterial);
        }
    }

    /**
     * Vanishes all the blocks of a chess board (sets their type to air).
     *
     * @param board board which blocks should be vanished
     */
    public static void vanishBoard(MinecraftBoard board) {
        World world = board.getOriginalCornerBlock().getWorld();
        for (int x = board.getLowestX(); x <= board.getHighestX(); x++) {
            for (int y = board.getLowestY(); y <= board.getHighestY(); y++) {
                for (int z = board.getLowestZ(); z <= board.getHighestZ(); z++) {
                    world.getBlockAt(x, y, z).setType(Material.AIR);
                }
            }
        }
    }
}
