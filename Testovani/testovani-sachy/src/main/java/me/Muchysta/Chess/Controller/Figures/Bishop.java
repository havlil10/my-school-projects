package me.Muchysta.Chess.Controller.Figures;

import me.Muchysta.Chess.Controller.Game;
import me.Muchysta.Chess.Controller.Position;

import java.util.HashSet;
import java.util.Set;

public class Bishop extends Figure {

    /**
     * Constructs a bishop and initializes its coordinations and other parameters.
     * @param game game this figure plays in
     * @param rank rank this figure starts at
     * @param file file this figure starts at
     * @param isWhite true if this figure stands at the white side, false otherwise
     */
    public Bishop(Game game, int rank, int file, boolean isWhite) {
        super(game, rank, file, isWhite);
    }

    @Override
    public FigureType getType() {
        return FigureType.BISHOP;
    }

    @Override
    public Set<Position> getPossibleMoves() {
        Set<Position> possibleMoves = new HashSet<>();

        int testedPositionRank;
        int testedPositionFile;

        for (int rankShift = -1; rankShift <= 1; rankShift += 2) {
            for (int fileShift = -1; fileShift <= 1; fileShift += 2) {

                testedPositionRank = this.getRank() + rankShift;
                testedPositionFile = this.getFile() + fileShift;

                while (Game.isPositionLegal(testedPositionRank, testedPositionFile)) {

                    if (this.getGame().getFigure(testedPositionRank, testedPositionFile) != null) {
                        if (this.getGame().getFigure(testedPositionRank, testedPositionFile).isWhite() != this.isWhite()) {
                            possibleMoves.add(new Position(testedPositionRank, testedPositionFile));
                        }
                        break;
                    }

                    possibleMoves.add(new Position(testedPositionRank, testedPositionFile));

                    testedPositionRank += rankShift;
                    testedPositionFile += fileShift;
                }
            }
        }

        return possibleMoves;
    }
}
