package me.Muchysta.Chess.Controller;

/**
 * Simple enumeration to distinguish possible stages of games
 */
public enum Stage {
    PREPARED, CUSTOM, STARTED, ENDED
}
