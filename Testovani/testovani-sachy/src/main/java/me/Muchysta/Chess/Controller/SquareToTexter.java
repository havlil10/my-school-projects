package me.Muchysta.Chess.Controller;

public class SquareToTexter {

    private static char[] fileNames = new char[]{'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'};

    /**
     * Converts square coordinates into its name
     * @param rank the rank of the square
     * @param file the file of the square
     * @return the name of the specified square
     */
    public static String squareToText(int rank, int file) {
        return fileToText(file) + rankToText(rank);
    }

    /**
     * Converts a specified rank into its text form
     * @param rank the specified rank
     * @return the name of the specified rank
     */
    public static String rankToText(int rank) {
        return "" + (rank + 1);
    }

    /**
     * Converts a specified file into its text form
     * @param file the specified file
     * @return the name of the specified file
     */
    public static String fileToText(int file) {
        return "" + fileNames[file];
    }

    /**
     * Converts a name of a specified square into its coordinates
     * @param squareText the name of the specified square
     * @return the coordinates of the specified square
     */
    public static int[] textToSquare(String squareText) {
        int rank = textToRank(squareText.charAt(1));
        int file = textToFile(squareText.charAt(0));
        return new int[]{rank, file};
    }

    /**
     * Converts a name of a specified rank to its coordinate (index)
     * @param rankText the name of the specified rank
     * @return the coordinate (index) of the specified rank
     */
    public static int textToRank(char rankText) {
        return Integer.parseInt("" + rankText) - 1;
    }

    /**
     * Converts a name of a specified file to its coordinate (index)
     * @param fileText the name of the specified file
     * @return the coordinate (index) of the specified file
     */
    public static int textToFile(char fileText) {
        int file = 0;
        while (file < fileNames.length) {
            if (fileNames[file] == fileText) {
                break;
            }
            file++;
        }
        return file;
    }
}
