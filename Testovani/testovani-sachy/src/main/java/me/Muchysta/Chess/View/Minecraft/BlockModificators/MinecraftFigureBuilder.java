package me.Muchysta.Chess.View.Minecraft.BlockModificators;

import me.Muchysta.Chess.Controller.Figures.Figure;
import me.Muchysta.Chess.Controller.Game;
import me.Muchysta.Chess.View.Minecraft.BoardShapes.MinecraftBoard;
import me.Muchysta.Chess.View.Minecraft.BoardShapes.MinecraftHorizontalBoard;
import me.Muchysta.Chess.View.Minecraft.FigureShapes.BuildingMaterialType;
import me.Muchysta.Chess.View.Minecraft.FigureShapes.MinecraftFigure;
import me.Muchysta.Chess.View.Minecraft.FigureShapes.MinecraftFigurePattern;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;

import java.util.HashMap;

public class MinecraftFigureBuilder {

    public static final Material WHITE_ALTERNATIVE = Material.BLACK_WOOL;
    public static final Material BLACK_ALTERNATIVE = Material.SMOOTH_QUARTZ;
    public static final Material WHITE_DEFAULT = Material.SNOW_BLOCK;
    public static final Material BLACK_DEFAULT = Material.NETHERITE_BLOCK;
    public static final Material ENVIRONMENT_MATERIAL = Material.AIR;


    /**
     * Builds all the figures of a specified game
     *
     * @param game
     */
    public static void buildAndConnectMinecraftFiguresOnBoard(Game game) {
        for (Figure figure : game.getFiguresOnBoard()) {
            MinecraftFigure minecraftFigure = new MinecraftFigure(figure);
            figure.setShape(minecraftFigure);
            minecraftFigure.draw();
        }
    }

    /**
     * Builds blocks of a specified figure
     *
     * @param figure         figure which blocks should be built
     * @param minecraftBoard board on which the specified figure should stand
     */
    public static void buildMinecraftFigure(Figure figure, MinecraftBoard minecraftBoard) {
        HashMap<Block, BuildingMaterialType> blocksToBuild = getBlocksOfFigure(figure, (MinecraftHorizontalBoard) minecraftBoard);
        changeBlocks((MinecraftFigure) figure.getShape(), blocksToBuild, true, figure.isWhite());
    }

    /**
     * Vanishes blocks of a specified figure
     *
     * @param figure         figure which blocks should be vanished
     * @param minecraftBoard board on which the specified figure stands
     */
    public static void vanishFigureOnBoard(Figure figure, MinecraftBoard minecraftBoard) {
        HashMap<Block, BuildingMaterialType> blocksToVanish = getBlocksOfFigure(figure, (MinecraftHorizontalBoard) minecraftBoard);
        changeBlocks((MinecraftFigure) figure.getShape(), blocksToVanish, false, true);
    }

    private static HashMap<Block, BuildingMaterialType> getBlocksOfFigure(Figure figure, MinecraftHorizontalBoard minecraftBoard) {

        HashMap<Block, BuildingMaterialType> blocksOfFigure = new HashMap<>();

        Location figureOriginalLocation = getFigureOriginalLocation(
                minecraftBoard.getOriginalBlockOfSquare(figure.getRank(), figure.getFile()).getLocation(),
                minecraftBoard,
                (MinecraftFigure) figure.getShape());
        MinecraftFigure figureShape = (MinecraftFigure) figure.getShape();
        boolean doFiguresAttackAlongXNotZAxe = minecraftBoard.doFiguresAttackAlongXNotZAxe();
        boolean doesWhiteAttackToHigherCoordinate = minecraftBoard.doesWhiteAttackToHigherCoordinate();
        boolean isWhite = figure.isWhite();
        MinecraftFigurePattern pattern = figureShape.getPattern();
        BuildingMaterialType[][][] matter = pattern.getMatter();

        for (int dexterI = 0; dexterI < pattern.getWidth(); dexterI++) {
            for (int inferiorI = 0; inferiorI < pattern.getHeight(); inferiorI++) {
                for (int posteriorI = 0; posteriorI < pattern.getLength(); posteriorI++) {

                    BuildingMaterialType materialType = matter[dexterI][inferiorI][posteriorI];
                    if (materialType != BuildingMaterialType.NOTHING) {
                        blocksOfFigure.put(
                                getBlockToChange(figureOriginalLocation, pattern, dexterI, inferiorI, posteriorI, doFiguresAttackAlongXNotZAxe, doesWhiteAttackToHigherCoordinate, isWhite),
                                materialType);
                    }

                }
            }
        }

        return blocksOfFigure;
    }

    private static Location getFigureOriginalLocation(
            Location squareOriginalBlockLocation,
            MinecraftHorizontalBoard minecraftBoard,
            MinecraftFigure shape) {

        int shiftValue = (minecraftBoard.getSquareSide() - shape.getMatter().length) / 2;
        int xShift = shiftValue;
        int zShift = shiftValue;

        if (minecraftBoard.doFiguresAttackAlongXNotZAxe() != minecraftBoard.doesWhiteAttackToHigherCoordinate()) {
            xShift *= -1;
        }
        if (!minecraftBoard.doesWhiteAttackToHigherCoordinate()) {
            zShift *= -1;
        }

        Location OriginalLocation = new Location(squareOriginalBlockLocation.getWorld(),
                squareOriginalBlockLocation.getBlockX() + xShift,
                squareOriginalBlockLocation.getBlockY() + 1,
                squareOriginalBlockLocation.getBlockZ() + zShift);
        return OriginalLocation;
    }

    private static Block getBlockToChange(Location figureOriginalLocation,
                                          MinecraftFigurePattern pattern,
                                          int dexterI, int inferiorI, int posteriorI,
                                          boolean doFiguresAttackAlongXNotZAxe, boolean doesWhiteAttackToHigherCoordinate, boolean isFigureWhite) {

        World world = figureOriginalLocation.getWorld();

        if (doFiguresAttackAlongXNotZAxe) {
            if (doesWhiteAttackToHigherCoordinate) {
                if (isFigureWhite) {
                    return world.getBlockAt(
                            figureOriginalLocation.getBlockX() + pattern.getLength() - posteriorI - 1,
                            figureOriginalLocation.getBlockY() + pattern.getHeight() - inferiorI - 1,
                            figureOriginalLocation.getBlockZ() + dexterI);
                } else {
                    return world.getBlockAt(
                            figureOriginalLocation.getBlockX() + posteriorI,
                            figureOriginalLocation.getBlockY() + pattern.getHeight() - inferiorI - 1,
                            figureOriginalLocation.getBlockZ() + pattern.getWidth() - dexterI - 1);
                }
            } else {
                if (isFigureWhite) {
                    return world.getBlockAt(
                            figureOriginalLocation.getBlockX() - pattern.getLength() + posteriorI + 1,
                            figureOriginalLocation.getBlockY() + pattern.getHeight() - inferiorI - 1,
                            figureOriginalLocation.getBlockZ() - dexterI);
                } else {
                    return world.getBlockAt(
                            figureOriginalLocation.getBlockX() - posteriorI,
                            figureOriginalLocation.getBlockY() + pattern.getHeight() - inferiorI - 1,
                            figureOriginalLocation.getBlockZ() - pattern.getWidth() + dexterI + 1);
                }
            }
        } else { //!doFiguresAttackAlongXNotZAxe
            if (doesWhiteAttackToHigherCoordinate) {
                if (isFigureWhite) {
                    return world.getBlockAt(
                            figureOriginalLocation.getBlockX() - dexterI,
                            figureOriginalLocation.getBlockY() + pattern.getHeight() - inferiorI - 1,
                            figureOriginalLocation.getBlockZ() + pattern.getLength() - posteriorI - 1);
                } else {
                    return world.getBlockAt(
                            figureOriginalLocation.getBlockX() - pattern.getWidth() + dexterI + 1,
                            figureOriginalLocation.getBlockY() + pattern.getHeight() - inferiorI - 1,
                            figureOriginalLocation.getBlockZ() + posteriorI);
                }
            } else {
                if (isFigureWhite) {
                    return world.getBlockAt(
                            figureOriginalLocation.getBlockX() + dexterI,
                            figureOriginalLocation.getBlockY() + pattern.getHeight() - inferiorI - 1,
                            figureOriginalLocation.getBlockZ() - pattern.getLength() + posteriorI + 1);
                } else {
                    return world.getBlockAt(
                            figureOriginalLocation.getBlockX() + pattern.getWidth() - dexterI - 1,
                            figureOriginalLocation.getBlockY() + pattern.getHeight() - inferiorI - 1,
                            figureOriginalLocation.getBlockZ() - posteriorI);
                }
            }
        }
    }

    private static void changeBlocks(
            MinecraftFigure figureShape,
            HashMap<Block, BuildingMaterialType> blocksToChange,
            boolean buildNotVanish,
            boolean isWhite) {

        for (Block blockOfFigure
                : blocksToChange.keySet()) {

            Material buildingMaterial = (buildNotVanish ? determineBuildingMaterial(isWhite, blocksToChange.get(blockOfFigure)) : ENVIRONMENT_MATERIAL);
            blockOfFigure.setType(buildingMaterial);

            if (buildNotVanish) {
                figureShape.getBlocks().add(blockOfFigure);
            } else {
                figureShape.getBlocks().remove(blockOfFigure);
            }

        }
    }

    private static Material determineBuildingMaterial(boolean isWhite, BuildingMaterialType buildingMaterialType) {
        return isWhite ?
                (buildingMaterialType.equals(BuildingMaterialType.DEFAULT) ? WHITE_DEFAULT : WHITE_ALTERNATIVE) :
                (buildingMaterialType.equals(BuildingMaterialType.DEFAULT) ? BLACK_DEFAULT : BLACK_ALTERNATIVE);
    }
}
