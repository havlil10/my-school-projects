package me.Muchysta.Chess.Controller.Figures;

import me.Muchysta.Chess.Controller.Abstracts.FigureShape;
import me.Muchysta.Chess.Controller.Game;
import me.Muchysta.Chess.Controller.Position;

import java.util.HashSet;
import java.util.Set;

public class Pawn extends Figure {

    private static final int INITIAL_RANK_OF_WHITE_PAWNS = 1;
    private static final int INITIAL_RANK_OF_BLACK_PAWNS = 6;

    private boolean endangeredEnPassant;

    /**
     * Constructs a pawn and initializes its coordinations and other parameters.
     * @param game game this figure plays in
     * @param rank rank this figure starts at
     * @param file file this figure starts at
     * @param isWhite true if this figure stands at the white side, false otherwise
     */
    public Pawn(Game game, int rank, int file, boolean isWhite) {
        super(game, rank, file, isWhite);
    }

    @Override
    public FigureType getType() {
        return FigureType.PAWN;
    }

    @Override
    public Set<Position> getPossibleMoves() {
        Set<Position> possibleMoves = new HashSet<>();

        possibleMoves.addAll(possibleStraightMoves());
        possibleMoves.addAll(possibleDiagonalMoves());

        return possibleMoves;
    }

    protected Set<Position> possibleStraightMoves() {
        Set<Position> possibleStraightMoves = new HashSet<>();

        int directionOfAttack = (this.isWhite() ? 1 : -1);

        if (isPossibleMoveByOne(this.getRank(), directionOfAttack, this.getFile())) {
            possibleStraightMoves.add(new Position(this.getRank() + directionOfAttack, this.getFile()));
            if (isPossibleMoveByTwoConsideringMoveByOneIsPossible(this.getRank(), directionOfAttack, this.getFile())) {
                possibleStraightMoves.add(new Position(this.getRank() + 2 * directionOfAttack, this.getFile()));
            }
        }

        return possibleStraightMoves;
    }

    private boolean isPossibleMoveByOne(int initialRank, int directionOfAttack, int file) {
        return Game.isPositionLegal(initialRank + directionOfAttack, file) && this.getGame().getFigure(initialRank + directionOfAttack, file) == null;
    }

    private boolean isPossibleMoveByTwoConsideringMoveByOneIsPossible(int initialRank, int directionOfAttack, int file) {
        return this.hasBeenAlreadyMoved() && this.getGame().getFigure(initialRank + 2 * directionOfAttack, file) == null;
    }

    public boolean hasBeenAlreadyMoved() {
        return (this.getRank() == INITIAL_RANK_OF_WHITE_PAWNS && this.isWhite() ||
                this.getRank() == INITIAL_RANK_OF_BLACK_PAWNS && !this.isWhite());
    }

    protected Set<Position> possibleDiagonalMoves() {
        Set<Position> possibleDiagonalMoves = new HashSet<>();

        int targetRank = this.getRank() + (this.isWhite() ? 1 : -1);

        int previousFile = this.getFile() - 1;
        int nextFile = this.getFile() + 1;

        if (canCaptureEnPassant(this.getRank(), previousFile) || canCaptureAndMoveDiagonaly(targetRank, previousFile)) {
            possibleDiagonalMoves.add(new Position(targetRank, previousFile));
        }
        if (canCaptureEnPassant(this.getRank(), nextFile) || canCaptureAndMoveDiagonaly(targetRank, nextFile)) {
            possibleDiagonalMoves.add(new Position(targetRank, nextFile));
        }

        return possibleDiagonalMoves;
    }

    private boolean canCaptureEnPassant(int positionRankWhereTheOpponentShouldBe, int positionFileWhereTheOpponentShouldBe) {
        if (Game.isPositionLegal(positionRankWhereTheOpponentShouldBe, positionFileWhereTheOpponentShouldBe)) {
            Figure figure = this.getGame().getFigure(positionRankWhereTheOpponentShouldBe, positionFileWhereTheOpponentShouldBe);
            return figure != null &&
                    figure.isWhite() != this.isWhite() &&
                    figure instanceof Pawn && ((Pawn) figure).isEndangeredEnPassant();
        }
        return false;
    }

    private boolean canCaptureAndMoveDiagonaly(int testedPositionRank, int testedPositionFile) {
        return Game.isPositionLegal(testedPositionRank, testedPositionFile) &&
                this.getGame().getFigure(testedPositionRank, testedPositionFile) != null &&
                this.getGame().getFigure(testedPositionRank, testedPositionFile).isWhite() != this.isWhite();
    }


    private boolean isEndangeredEnPassant() {
        return endangeredEnPassant;
    }

    /**
     * Sets the attribute {@code endangeredEnPassant} to the specified value.
     * @param endangeredEnPassant the specified value the attribute {@code endangeredEnPassant} should be set to
     */
    public void setEndangeredEnPassant(boolean endangeredEnPassant) {
        this.endangeredEnPassant = endangeredEnPassant;
    }
}
