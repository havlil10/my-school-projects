package me.Muchysta.Chess.View.Minecraft.BoardShapes;

import me.Muchysta.Chess.Controller.Abstracts.BoardShape;
import me.Muchysta.Chess.Controller.Abstracts.Communicator;
import me.Muchysta.Chess.Controller.Figures.Figure;
import me.Muchysta.Chess.Controller.Game;
import me.Muchysta.Chess.Controller.GameSettings;
import me.Muchysta.Chess.View.Minecraft.BlockModificators.MinecraftBoardBuilder;
import me.Muchysta.Chess.View.Minecraft.MinecraftCommunicator;
import me.Muchysta.Chess.View.Minecraft.Registers.MinecraftBoardRegister;
import me.Muchysta.Chess.View.Minecraft.Registers.MinecraftPlannedSettingsRegister;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

import static me.Muchysta.Chess.View.Minecraft.BlockModificators.MinecraftBoardBuilder.NUMBER_OF_RANKS_OR_FILES;

public abstract class MinecraftBoard extends BoardShape {

    static Plugin plugin = Bukkit.getPluginManager().getPlugin("Chess");

    private int lowestX;
    private int highestX;
    private int lowestY;
    private int highestY;
    private int lowestZ;
    private int highestZ;

    /**
     * Finds out whether the specified block is one of this' blocks and returns the appropriate value.
     * @param block the specified block
     * @return true if the specified block is one of this' blocks, flase otherwise.
     */
    public boolean hasBlock(Block block) {
        return this.getLowestX() <= block.getX() && this.getHighestX() >= block.getX() &&
                this.getLowestY() <= block.getY() && this.getHighestY() >= block.getY() &&
                this.getLowestZ() <= block.getZ() && this.getHighestZ() >= block.getZ();
    }

    /**
     * Returns the lowest x coordinate of this' blocks.
     * @return the lowest x coordinate of this' blocks
     */
    public int getLowestX() {
        return lowestX;
    }

    /**
     * Sets the lowest x coordinate of this' blocks.
     * @param lowestX the new lowest x coordinate of this' blocks
     */
    public void setLowestX(int lowestX) {
        this.lowestX = lowestX;
    }

    /**
     * Returns the highest x coordinate of this' blocks.
     * @return the highest x coordinate of this' blocks
     */
    public int getHighestX() {
        return highestX;
    }

    /**
     * Sets the highest x coordinate of this' blocks.
     * @param highestX the new highest x coordinate of this' blocks
     */
    public void setHighestX(int highestX) {
        this.highestX = highestX;
    }

    /**
     * Returns the lowest y coordinate of this' blocks.
     * @return the lowest y coordinate of this' blocks
     */
    public int getLowestY() {
        return lowestY;
    }

    /**
     * Sets the lowest y coordinate of this' blocks.
     * @param lowestY the new lowest y coordinate of this' blocks
     */
    public void setLowestY(int lowestY) {
        this.lowestY = lowestY;
    }

    /**
     * Returns the highest y coordinate of this' blocks.
     * @return the highest y coordinate of this' blocks
     */
    public int getHighestY() {
        return highestY;
    }

    /**
     * Sets the highest y coordinate of this' blocks.
     * @param highestY the new highest y coordinate of this' blocks
     */
    public void setHighestY(int highestY) {
        this.highestY = highestY;
    }

    /**
     * Returns the lowest z coordinate of this' blocks.
     * @return the lowest z coordinate of this' blocks
     */
    public int getLowestZ() {
        return lowestZ;
    }

    /**
     * Sets the lowest z coordinate of this' blocks.
     * @param lowestZ the new lowest z coordinate of this' blocks
     */
    public void setLowestZ(int lowestZ) {
        this.lowestZ = lowestZ;
    }

    /**
     * Returns the highest z coordinate of this' blocks.
     * @return the highest z coordinate of this' blocks
     */
    public int getHighestZ() {
        return highestZ;
    }

    /**
     * Sets the highest z coordinate of this' blocks.
     * @param highestZ the new highest z coordinate of this' blocks
     */
    public void setHighestZ(int highestZ) {
        this.highestZ = highestZ;
    }

    /**
     * Calculates and returns the length of side of one square.
     * @return side of one square
     */
    public int getSquareSide() {
        return (this.getHighestX() - this.getLowestX() + 1) / NUMBER_OF_RANKS_OR_FILES;
    }

    /**
     * Rohový blok souřadnic A1 (rank = 0, file = 0)
     */
    private Block originalCornerBlock;

    /**
     * Returns the block of square a1 (rank index 0, file index 0) lying at the location where the board should be built from.
     * @return the block where the board should be built from
     */
    public Block getOriginalCornerBlock() {
        return originalCornerBlock;
    }

    /**
     * Sets the block of square a1 (rank index 0, file index 0) lying at the location where the board should be built from.
     * @param originalCornerBlock the new block board should be built from
     */
    public void setOriginalCornerBlock(Block originalCornerBlock) {
        this.originalCornerBlock = originalCornerBlock;
    }

    /**
     * Returns the block of square defined by specified coordinations.
     * @param rank the specified rank of the square in question
     * @param file the specified file of the square in question
     * @return the block of square defined by arguments
     */
    public Block getOriginalBlockOfSquare(int rank, int file) {

        World world = originalCornerBlock.getWorld();

        int x;
        int y = originalCornerBlock.getY();
        int z;

        if (originalCornerBlock.getX() == lowestX && originalCornerBlock.getZ() == lowestZ) {
            x = originalCornerBlock.getX() + getSquareSide() * rank;
            z = originalCornerBlock.getZ() + getSquareSide() * file;

        } else if (originalCornerBlock.getX() == lowestX && originalCornerBlock.getZ() == highestZ) {
            x = originalCornerBlock.getX() + getSquareSide() * file;
            z = originalCornerBlock.getZ() - getSquareSide() * rank;

        } else if (originalCornerBlock.getX() == highestX && originalCornerBlock.getZ() == lowestZ) {
            x = originalCornerBlock.getX() - getSquareSide() * file;
            z = originalCornerBlock.getZ() + getSquareSide() * rank;

        } else { // (originalCornerBlock.getX() == lowestX && originalCornerBlock.getZ() == highestZ)
            x = originalCornerBlock.getX() - getSquareSide() * rank;
            z = originalCornerBlock.getZ() - getSquareSide() * file;
        }

        return world.getBlockAt(x, y, z);
    }

    /**
     * Returns the rank of the square the specified block is a part of.
     * @param clickedAtBlock the specified block
     * @return the rank of square the block is a part of
     */
    public abstract int getRankByBlock(Block clickedAtBlock);

    /**
     * Returns the file of the square the specified block is a part of.
     * @param clickedAtBlock the specified block
     * @return the file of square the block is a part of
     */
    public abstract int getFileByBlock(Block clickedAtBlock);

    @Override
    public void pourTurnQueue() {
        /*Game actualGame = this.getGame();

        for (Figure figure : actualGame.getFiguresOnBoard()) {
            figure.getShape().vanish();
        }

        GameSettings settingsForLoadedGame = new GameSettings(null, null, 0, true, "");
        Communicator communicator = new MinecraftCommunicator(settingsForLoadedGame.getWhitePlayer(), settingsForLoadedGame.getBlackPlayer());
        Game loadedGame = new Game(settingsForLoadedGame, communicator);
        this.setGame(loadedGame);
        loadedGame.setBoardShape(this);
        buildWithFigures();

        for (int turnI = 0; turnI < turnPositionsFromQueue.size(); turnI++) {
            final int thisTurnI = turnI;
            new BukkitRunnable() {
                @Override
                public void run() {
                    loadedGame.getGameManager().executeTurn(loadedGame.getFigure(turnPositionsFromQueue.get(thisTurnI).getRank(), turnPositionsFromQueue.get(thisTurnI).getFile()), turnPositionsToQueue.get(thisTurnI).getRank(), turnPositionsToQueue.get(thisTurnI).getFile());
                }
            }.runTaskLater(plugin, 20 * turnI);
        }

        new BukkitRunnable() {
            @Override
            public void run() {
                for (Figure figure : loadedGame.getFiguresOnBoard()) {
                    figure.getShape().vanish();
                }
                setGame(actualGame);
                buildWithFigures();
            }
        }.runTaskLater(plugin, 20 * turnPositionsFromQueue.size());*/
    }

    /**
     * Calls a method of this' view's board building class which builds this board and figures as well
     */
    public abstract void buildWithFigures();

    /**
     * Makes this board disappear and removes it from the register.
     */
    public void vanish() {
        MinecraftBoardBuilder.vanishBoard(this);
        deactivate();
    }

    /**
     * Adds this board to the register.
     */
    public void register() {
        MinecraftBoardRegister.registerMinecraftBoard(this);
    }

    /**
     * Removes this board from the register.
     */
    public void deactivate() {
        MinecraftBoardRegister.deactivateBoard(this);
    }
}
