package me.Muchysta.Chess.Controller.Figures;

import me.Muchysta.Chess.Controller.Game;
import me.Muchysta.Chess.Controller.Position;

import java.util.HashSet;
import java.util.Set;

public class King extends Figure implements Castleable {

    public static final int QUEENSIDE_CASTLE_TARGET_FILE = 2;
    public static final int KINGSIDE_CASTLE_TARGET_FILE = 6;
    public static final int WHITE_CASTLE_RANK = 0;
    public static final int BLACK_CASTLE_RANK = 7;

    private boolean hasBeenAlreadyMoved;

    /**
     * Constructs a king and initializes its coordinations and other parameters.
     * @param game game this figure plays in
     * @param rank rank this figure starts at
     * @param file file this figure starts at
     * @param isWhite true if this figure stands at the white side, false otherwise
     */
    public King(Game game, int rank, int file, boolean isWhite) {
        super(game, rank, file, isWhite);
        this.hasBeenAlreadyMoved = false;
    }

    @Override
    public FigureType getType() {
        return FigureType.KING;
    }

    @Override
    public Set<Position> getPossibleMoves() {
        Set<Position> possibleMoves = new HashSet();
        possibleMoves.addAll(possibleMovesByOne());
        possibleMoves.addAll(possibleCastles());

        return possibleMoves;
    }

    protected Set<Position> possibleMovesByOne() {
        Set<Position> possibleMoves = new HashSet<>();

        int testedPositionRank;
        int testedPositionFile;

        for (int rankShift = -1; rankShift <= 1; rankShift++) {
            for (int fileShift = -1; fileShift <= 1; fileShift++) {
                if (rankShift != 0 || fileShift != 0) {

                    testedPositionRank = this.getRank() + rankShift;
                    testedPositionFile = this.getFile() + fileShift;

                    if (Game.isPositionLegal(testedPositionRank, testedPositionFile) &&
                            (this.getGame().getFigure(testedPositionRank, testedPositionFile) == null ||
                            this.getGame().getFigure(testedPositionRank, testedPositionFile).isWhite() != this.isWhite())) {
                        possibleMoves.add(new Position(testedPositionRank, testedPositionFile));
                    }
                }
            }
        }

        return possibleMoves;
    }

    protected Set<Position> possibleCastles() {
        Set<Position> possibleCastles = new HashSet<>();

        if (!this.hasBeenAlreadyMoved()) {
            possibleCastles.addAll(possibleQueensideCastle()); // add(null) by vyhodil chybu
            possibleCastles.addAll(possibleKingsideCastle());
        }

        return possibleCastles;
    }

    private Set<Position> possibleQueensideCastle() {
        Set<Position> possibleQueensideCastles = new HashSet<>();

        if (rookHasNotBeenMoved(this.getRank(), Rook.LEFT_ROOK_FILE) &&
                noFiguresInTheQueensideWay()) {
            if (noEndangeredSquareForQueensideCastle()) {
                possibleQueensideCastles.add(new Position(this.getRank(), QUEENSIDE_CASTLE_TARGET_FILE));
            }
        }

        return possibleQueensideCastles;
    }

    private boolean rookHasNotBeenMoved(int rookRank, int rookFile) {
        Figure figureAtCastleRookPosition = this.getGame().getFigure(rookRank, rookFile);
        return figureAtCastleRookPosition instanceof Rook && !((Rook) figureAtCastleRookPosition).hasBeenAlreadyMoved();
    }

    private boolean noFiguresInTheQueensideWay() {
        for (int file = this.getFile() - 1; file > Rook.LEFT_ROOK_FILE; file--) {
            if (getGame().getFigure(this.getRank(), file) != null) {
                return false;
            }
        }
        return true;
    }

    private boolean noEndangeredSquareForQueensideCastle() {
        for (int file = this.getFile(); file >= QUEENSIDE_CASTLE_TARGET_FILE; file--) {
            if (getGame().isSquareDangerous(this.getRank(), file, this.isWhite())) {
                return false;
            }
        }
        return true;
    }

    private Set<Position> possibleKingsideCastle() {
        Set<Position> possibleKingsideCastles = new HashSet<>();

        if (rookHasNotBeenMoved(this.getRank(), Rook.RIGHT_ROOK_FILE) &&
                noFiguresInTheKingsideWay()) {
            if (noEndangeredSquareForKingsideCastle()) {
                possibleKingsideCastles.add(new Position(this.getRank(), KINGSIDE_CASTLE_TARGET_FILE));
            }
        }

        return possibleKingsideCastles;
    }

    private boolean noFiguresInTheKingsideWay() {
        for (int file = this.getFile() + 1; file < Rook.RIGHT_ROOK_FILE; file++) {
            if (getGame().getFigure(this.getRank(), file) != null) {
                return false;
            }
        }
        return true;
    }

    private boolean noEndangeredSquareForKingsideCastle() {
        for (int file = this.getFile(); file <= KINGSIDE_CASTLE_TARGET_FILE; file++) {
            if (getGame().isSquareDangerous(this.getRank(), file, this.isWhite())) {
                return false;
            }
        }
        return true;
    }

    /**
     * Returns if the specified square is in range of this king's simple moves (castle moves NOT included).
     * @param rankTo the inspected rank
     * @param fileTo the inspected file
     * @return true if this king can capture opponent's figure appearing there now or in future before this king moves to another square, false otherwise
     */
    public boolean isPossibleCapture(int rankTo, int fileTo) {
        return (rankTo - this.getRank()) * (rankTo - this.getRank()) <= 1 &&
                (fileTo - this.getFile()) * (fileTo - this.getFile()) <= 1 &&
                !(this.getRank() == rankTo && this.getFile() == fileTo);
    }

    @Override
    public boolean hasBeenAlreadyMoved() {
        return this.hasBeenAlreadyMoved;
    }

    @Override
    public void alreadyHasBeenMoved() {
        hasBeenAlreadyMoved = true;
    }
}
