package me.Muchysta.Chess.View.Minecraft.BlockModificators;

import org.bukkit.Material;
import org.bukkit.World;

import static me.Muchysta.Chess.View.Minecraft.BlockModificators.MinecraftBoardBuilder.BLACK_SQUARE_MATERIAL;
import static me.Muchysta.Chess.View.Minecraft.BlockModificators.MinecraftBoardBuilder.WHITE_SQUARE_MATERIAL;
import static me.Muchysta.Chess.View.Minecraft.BlockModificators.MinecraftFigureBuilder.BLACK_DEFAULT;
import static me.Muchysta.Chess.View.Minecraft.BlockModificators.MinecraftFigureBuilder.WHITE_DEFAULT;

public class HighLighter {

    public static final Material HIGHLIGHTING_MATERIAL = Material.GLOWSTONE;

    private static void highlightNotAirBlocks(World world, int lowestX, int highestX, int lowestY, int highestY, int lowestZ, int highestZ) {
        for (int x = lowestX; x <= highestX; x++) {
            for (int y = lowestY; y <= highestY; y++) {
                for (int z = lowestZ; z <= highestZ; z++) {
                    if (!world.getBlockAt(x, y, z).getType().isAir()) {
                        world.getBlockAt(x, y, z).setType(HIGHLIGHTING_MATERIAL);
                    }
                }
            }
        }
    }

    private static void lowlightHighlighted(World world, int lowestX, int highestX, int lowestY, int highestY, int lowestZ, int highestZ, boolean isBoardNotFigure, boolean isWhite) {

        Material originalMaterial = determineOriginalMaterial(isBoardNotFigure, isWhite);

        for (int x = lowestX; x <= highestX; x++) {
            for (int y = lowestY; y <= highestY; y++) {
                for (int z = lowestZ; z <= highestZ; z++) {
                    if (!world.getBlockAt(x, y, z).getType().equals(HIGHLIGHTING_MATERIAL)) {
                        world.getBlockAt(x, y, z).setType(originalMaterial);
                    }
                }
            }
        }
    }

    private static Material determineOriginalMaterial(boolean isBoardNotFigure, boolean isWhite) {
        return (isBoardNotFigure ?
                (isWhite ? WHITE_SQUARE_MATERIAL : BLACK_SQUARE_MATERIAL) :
                (isWhite ? WHITE_DEFAULT : BLACK_DEFAULT));
    }
}
