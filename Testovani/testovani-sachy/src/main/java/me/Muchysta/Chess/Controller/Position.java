package me.Muchysta.Chess.Controller;

public class Position {

    private int rank;
    private int file;

    /**
     * Obviously behaving constructor for Position.
     *
     * @param rank rank of the constructed position
     * @param file rank of the constructed position
     */
    public Position(int rank, int file) {
        this.rank = rank;
        this.file = file;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public int getFile() {
        return file;
    }

    public void setFile(int file) {
        this.file = file;
    }
}
