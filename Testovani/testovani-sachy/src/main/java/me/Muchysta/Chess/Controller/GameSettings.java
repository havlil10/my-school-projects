package me.Muchysta.Chess.Controller;

import java.util.Random;

public class GameSettings {

    private Random rnd = new Random();

    private Object white;
    private Object black;
    boolean randomSelectOfWhitePlayers;
    private long timeLimit;
    private boolean isACustomGame;
    private String startingStringPGN;

    /**
     * Initializes new settings including players, time limit, mode and initial PGN string.
     * @param white
     * @param black
     * @param timeInMilliseconds
     * @param isACustomGame
     * @param stringPGN
     */
    public GameSettings(Object white, Object black, long timeInMilliseconds, boolean isACustomGame, String stringPGN) {
        this.white = white;
        this.black = black;
        this.timeLimit = timeInMilliseconds;
        this.isACustomGame = isACustomGame;
        this.startingStringPGN = stringPGN;
    }

    /**
     * Returns the white player.
     * @return the white player
     */
    public Object getWhitePlayer() {
        return white;
    }

    /**
     * Returns the black player.
     * @return the black player
     */
    public Object getBlackPlayer() {
        return black;
    }

    protected void determineWhoIsWhite() {
        if (randomSelectOfWhitePlayers && rnd.nextBoolean()) {
            Object oldWhite = this.white;
            this.white = this.black;
            this.black = oldWhite;
        }
    }

    protected long getTimeLimit() {
        return timeLimit;
    }

    protected boolean isACustomGame() {
        return isACustomGame;
    }

    /**
     * Returns the initial PGN string.
     * @return the initial PGN string
     */
    public String getStartingStringPGN() {
        return this.startingStringPGN;
    }
}
