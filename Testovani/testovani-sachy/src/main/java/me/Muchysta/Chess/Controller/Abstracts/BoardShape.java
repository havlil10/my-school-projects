package me.Muchysta.Chess.Controller.Abstracts;

import me.Muchysta.Chess.Controller.Figures.Figure;
import me.Muchysta.Chess.Controller.Game;
import me.Muchysta.Chess.Controller.Position;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public abstract class BoardShape {

    private Game game;
    protected List<Position> turnPositionsFromQueue = new ArrayList<>();
    protected List<Position> turnPositionsToQueue = new ArrayList<>();

    public Game getGame() {
        return game;
    }

    protected void setGame(Game game) {
        this.game = game;
    }

    /**
     * There must be a register for all boards of that view. This method should add this BoardShape into it.
     */
    public abstract void register();

    /**
     * There must be a register for all boards of that view. This method should removes this BoardShape from it.
     */
    public abstract void deactivate();

    /**
     * Used for highlight each square in the Set.
     * @param possibleMoves Set of squares to be highlighted.
     */
    public abstract void highlightPossibleMoves(Set<Position> possibleMoves);

    /**
     * Used for stop highlighting each square in the Set.
     * @param possibleMoves Set of squares to stop highlighting.
     */
    public abstract void lowlightPossibleMoves(Set<Position> possibleMoves);

    /**
     * Let this chess board disappear.
     */
    public abstract void vanish();

    /**
     * Adds given arguments to this queue of turns to be played when the game is loaded from a file.
     * @param figure figure which the turn is performed with
     * @param rankTo the target rank of that figure
     * @param fileTo the target file of that figure
     */
    public void addTurnToQueue(Figure figure, int rankTo, int fileTo) {
        turnPositionsFromQueue.add(new Position(figure.getRank(), figure.getFile()));
        turnPositionsToQueue.add(new Position(rankTo, fileTo));
    }

    /**
     * Play all the turns in this' queue for turns loaded from a file.
     */
    public abstract void pourTurnQueue();
}