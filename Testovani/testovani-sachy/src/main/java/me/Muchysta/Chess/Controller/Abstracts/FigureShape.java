package me.Muchysta.Chess.Controller.Abstracts;

import me.Muchysta.Chess.Controller.Figures.*;

public abstract class FigureShape {

    private Figure figure;

    /**
     * Initializes new shape of a given figure. This doesn't contain it's displaying.
     * @param figure figure this shape depicts
     * @see FigureShape#draw()
     */
    public FigureShape(Figure figure) {
        this.figure = figure;
    }

    /**
     * Accesses the figure this shape depicts.
     * @return this' figure
     */
    public Figure getFigure() {
        return figure;
    }

    /**
     * Changes the figure this shape depicts.
     * @param figure new figure this shape should depict.
     */
    public void setFigure(Figure figure) {
        this.figure = figure;
    }

    /**
     * Displays this shape on the correct square on the battleground
     */
    public abstract void draw();

    /**
     * Make this shape disappear
     */
    public abstract void vanish();

    /**
     * Make this shape adapt to the process of promoting pawn to a queen or other figures.
     * @return shape the figure has been promoted to
     */
    public abstract FigureShape transformIntoQueenShape();
}
