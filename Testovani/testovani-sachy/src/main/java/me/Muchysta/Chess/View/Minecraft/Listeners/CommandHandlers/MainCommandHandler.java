package me.Muchysta.Chess.View.Minecraft.Listeners.CommandHandlers;

import me.Muchysta.Chess.Main;
import org.apache.commons.lang.math.NumberUtils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class MainCommandHandler implements CommandExecutor {

    private Main plugin;

    public static final int MAX_ARGS_FOR_CHESS_COMMAND = 3;
    public static final int MIN_ARGS_FOR_CHESS_COMMAND = 2;

    public MainCommandHandler(Main plugin) {
        this.plugin = plugin;
    }

    /**
     * Calls proper actions following the command according to the specified args.
     *
     * @param sender  the player who sends the command
     * @param command the command itself
     * @param label   label of that command
     * @param args    arguments to closer specification of what shall be done; these are written by the player after the name of the command
     * @return true if the command's args are valid, false otherwise
     */
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (isCorrectChessCommand(label, args)) {
            ChessCommand.handle(sender, args, "");
            return true;

        } else if (isCorrectMoveCommand(label, args)) {
            MoveCommand.handle(sender, args);
            return true;

        } else if (isCorrectUndoCommand(label, args)) {
            UndoCommand.handle(sender, args);
            return true;

        } else if (isCorrectSurrenderCommand(label, args)) {
            SurrenderCommand.handle(sender, args);
            return true;

        } else if (isCorrectStopCustomCommand(label, args)) {
            StopCustomCommand.handle(sender, args);
            return true;

        } else if (isCorrectSaveCommand(label, args)) {
            SaveCommand.handle(sender, args);
            return true;

        } else if (isCorrectLoadCommand(label, args)) {
            LoadCommand.handle(sender, args);
            return true;

        } else if (isCorrectDeleteCommand(label, args)) {
            DeleteCommand.handle(sender, args);
            return true;

        } else if (isCorrectBMICommand(label, args)) {
            BMICommand.handle(sender, args);
            return true;

        } else if (isCorrectCustomCommand(label, args)) {
            CustomCommand.handle(sender, args);
            return true;
        }

        return false;
    }

    private boolean isCorrectChessCommand(String label, String[] args) {
        return label.equalsIgnoreCase("chess") &&
                args.length >= MIN_ARGS_FOR_CHESS_COMMAND && args.length <= MAX_ARGS_FOR_CHESS_COMMAND;
    }

    private boolean isCorrectMoveCommand(String label, String[] args) {
        return label.equalsIgnoreCase("move");
    }

    private boolean isCorrectUndoCommand(String label, String[] args) {
        return label.equalsIgnoreCase("undo");
    }

    private boolean isCorrectSurrenderCommand(String label, String[] args) {
        return label.equalsIgnoreCase("surrender");
    }

    private boolean isCorrectStopCustomCommand(String label, String[] args) {
        return label.equalsIgnoreCase("stopcustom") && args.length == 1 &&
                (args[0].equalsIgnoreCase("white") || args[0].equalsIgnoreCase("black"));
    }

    private boolean isCorrectSaveCommand(String label, String[] args) {
        return label.equalsIgnoreCase("save") && args.length == 1 &&
                areNoSpecialChars(args[0]);
    }

    private boolean isCorrectLoadCommand(String label, String[] args) {
        return label.equalsIgnoreCase("load") && args.length == 3 &&
                areNoSpecialChars(args[0]);
    }

    private boolean isCorrectDeleteCommand(String label, String[] args) {
        return label.equalsIgnoreCase("delete") && args.length == 1 &&
                areNoSpecialChars(args[0]);
    }

    private boolean areNoSpecialChars(String arg) {
        for (char ch : arg.toCharArray()) {
            if (!((int) ch >= (int) 'a' && (int) ch <= (int) 'z' ||
                    (int) ch >= (int) 'A' && (int) ch <= (int) 'Z' ||
                    (int) ch >= (int) '0' && (int) ch <= (int) '9')) {
                return false;
            }
        }
        return true;
    }

    private boolean isCorrectBMICommand(String label, String[] args) {
        return label.equalsIgnoreCase("calculatebmi") && args.length == 3 &&
                isDoubleNumber(args[0]) && isDoubleNumber(args[1]) && isIntNumber(args[2]);
    }

    private static boolean isIntNumber(String maybeNumber) {
        try {
            Integer.parseInt(maybeNumber);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    private static boolean isDoubleNumber(String maybeNumber) {
        try {
            Double.parseDouble(maybeNumber);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    private boolean isCorrectCustomCommand(String label, String[] args) {
        return label.equalsIgnoreCase("custom") && args.length % 2 == 0;
    }
}
