package me.Muchysta.Chess.Controller.Figures;

/**
 * Pretty simple interface to mark figures where it matters whether they have been already moved or not yet.
 */
public interface Castleable {

    /**
     * Asks whether the attribute {@code hasBeenAlreadyMoved} is true or not and returns the answer.
     */
    boolean hasBeenAlreadyMoved();

    /**
     * Sets the attribute {@code hasBeenAlreadyMoved} to true.
     */
    void alreadyHasBeenMoved();
}
