package me.Muchysta.Chess.View.Minecraft.BoardShapes;

import me.Muchysta.Chess.Controller.Game;
import me.Muchysta.Chess.Controller.Position;
import me.Muchysta.Chess.View.Minecraft.BlockModificators.MinecraftBoardBuilder;
import me.Muchysta.Chess.View.Minecraft.BlockModificators.MinecraftFigureBuilder;
import me.Muchysta.Chess.View.Minecraft.Registers.MinecraftBoardRegister;
import org.bukkit.block.Block;

import java.util.Set;

public class MinecraftHorizontalBoard extends MinecraftBoard {

    /**
     * Constructs a new minecraft horizontally oriented board.
     * @param game game this board depicts state of.
     * @param xIterationUnit integer which works as an iteration addend when iterating from the original block to the other side in the direction of x-axis
     * @param zIterationUnit integer which works as an iteration addend when iterating from the original block to the other side in the direction of z-axis
     * @param originalCornerBlock the most corner block of the new board which belongs to the square a1
     */
    public MinecraftHorizontalBoard(Game game,
                                    int xIterationUnit, int zIterationUnit,
                                    Block originalCornerBlock) {

        this.setGame(game);
        setOriginalCornerBlock(originalCornerBlock);
        initLowestAndHighestCoordinates(xIterationUnit, zIterationUnit);
        MinecraftBoardRegister.registerMinecraftBoard(this);
    }

    private void initLowestAndHighestCoordinates(int xIterationUnit, int zIterationUnit) {
        initLowestAndHighestCoordinatesX(xIterationUnit);
        this.setLowestY(getOriginalCornerBlock().getY());
        this.setHighestY(getOriginalCornerBlock().getY());
        initLowestAndHighestCoordinatesZ(zIterationUnit);
    }

    private void initLowestAndHighestCoordinatesX(int xIterationUnit) {
        if (xIterationUnit == 1) {
            this.setLowestX(getOriginalCornerBlock().getX());
            this.setHighestX(getOriginalCornerBlock().getX() + MinecraftBoardBuilder.NUMBER_OF_RANKS_OR_FILES * MinecraftBoardBuilder.SQUARE_SIZE - 1);
        } else {
            this.setLowestX(getOriginalCornerBlock().getX() - MinecraftBoardBuilder.NUMBER_OF_RANKS_OR_FILES * MinecraftBoardBuilder.SQUARE_SIZE + 1);
            this.setHighestX(getOriginalCornerBlock().getX());
        }
    }

    private void initLowestAndHighestCoordinatesZ(int zIterationUnit) {
        if (zIterationUnit == 1) {
            this.setLowestZ(getOriginalCornerBlock().getZ());
            this.setHighestZ(getOriginalCornerBlock().getZ() + MinecraftBoardBuilder.NUMBER_OF_RANKS_OR_FILES * MinecraftBoardBuilder.SQUARE_SIZE - 1);
        } else {
            this.setLowestZ(getOriginalCornerBlock().getZ() - MinecraftBoardBuilder.NUMBER_OF_RANKS_OR_FILES * MinecraftBoardBuilder.SQUARE_SIZE + 1);
            this.setHighestZ(getOriginalCornerBlock().getZ());
        }
    }

    /**
     * Highlights all the specified squares.
     * @param possibleMoves Set of squares to be highlighted.
     */
    @Override
    public void highlightPossibleMoves(Set<Position> possibleMoves) {
    }

    /**
     * Stops highlighting all the specified squares.
     * @param possibleMoves Set of squares to stop highlighting.
     */
    public void lowlightPossibleMoves(Set<Position> possibleMoves) {
    }

    /**
     * Builds this board and its figures on it.
     */
    public void buildWithFigures (){
        MinecraftBoardBuilder.buildMinecraftBoard(this);
        MinecraftFigureBuilder.buildAndConnectMinecraftFiguresOnBoard(this.getGame());
    }

    /**
     * Tells the axis of attack, in other words, the direction simple pawn moves.
     * @return true if pawns moves usually move along the x-axis, false otherwise
     */
    public boolean doFiguresAttackAlongXNotZAxe() {
        return getLowestX() == getOriginalCornerBlock().getX() && getOriginalCornerBlock().getZ() == getLowestZ() ||
                getHighestX() == getOriginalCornerBlock().getX() && getOriginalCornerBlock().getZ() == getHighestZ();
    }

    /**
     * Tells how a white pawn's coordination of the axis of attack changes when it moves
     * @return true if the coordination raises, false otherwise.
     */
    public boolean doesWhiteAttackToHigherCoordinate() {
        return getOriginalCornerBlock().getZ() == getLowestZ();
    }

    @Override
    public int getRankByBlock(Block clickedAtBlock) {
        if (doFiguresAttackAlongXNotZAxe()) {
            if (doesWhiteAttackToHigherCoordinate()) {
                return (clickedAtBlock.getX() - this.getLowestX()) / this.getSquareSide();
            } else {
                return (this.getHighestX() - clickedAtBlock.getX()) / this.getSquareSide();
            }
        } else {
            if (doesWhiteAttackToHigherCoordinate()) {
                return (clickedAtBlock.getZ() - this.getLowestZ()) / this.getSquareSide();
            } else {
                return (this.getHighestZ() - clickedAtBlock.getZ()) / this.getSquareSide();
            }
        }
    }

    @Override
    public int getFileByBlock(Block clickedAtBlock) {

        if (doFiguresAttackAlongXNotZAxe()) {
            if (doesWhiteAttackToHigherCoordinate()) {
                return (clickedAtBlock.getZ() - this.getLowestZ()) / this.getSquareSide();
            } else {
                return (this.getHighestZ() - clickedAtBlock.getZ()) / this.getSquareSide();
            }
        } else {
            if (doesWhiteAttackToHigherCoordinate()) {
                return (this.getHighestX() - clickedAtBlock.getX()) / this.getSquareSide();
            } else {
                return (clickedAtBlock.getX() - this.getLowestX()) / this.getSquareSide();
            }
        }
    }

    /**
     * Tells the layer (y coordinate) of this board.
     * @return y coordinate of this board
     */
    public int getY() {
        return this.getHighestY();
    }
}
