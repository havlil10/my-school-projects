package me.Muchysta.Chess.Controller;

import me.Muchysta.Chess.Controller.Abstracts.FigureShape;
import me.Muchysta.Chess.Controller.Figures.*;
import me.Muchysta.Chess.Controller.Abstracts.BoardShape;
import me.Muchysta.Chess.Controller.Abstracts.Communicator;
import me.Muchysta.Chess.Controller.PGNExtras.PGNManager;

import java.util.*;

import static me.Muchysta.Chess.Controller.Figures.FigureType.KING;
import static me.Muchysta.Chess.Controller.Figures.FigureType.QUEEN;
import static me.Muchysta.Chess.Controller.GameResult.*;
import static me.Muchysta.Chess.Controller.Stage.*;

public class Game {

    private static final int NEITHER_CAP_NOR_PAWN_MOVE_TURN_NUMBER_LIMIT = 2 * 50;
    public static final int NUMBER_OF_RANKS_OR_FILES = 8;
    private static final int LAST_RANK_OF_WHITE_PAWNS = 7;
    private static final int LAST_RANK_OF_BLACK_PAWNS = 0;
    public FigureType PROMOTION_FIGURE_TYPE = QUEEN;

    private GameSettings gameSettings;
    private GameManager gameManager;
    private Communicator communicator;
    private Timer timer;
    private PGNManager PGNManager;
    private BoardShape boardShape;

    private Stage stage;
    protected Figure[][] battleField;
    private boolean playerOnTurnIsTheWhiteOne;
    private Optional<Figure> selectedFigure;
    private int turnNumber;
    private int neitherCapNorPawnMoveSingleSideTurns;
    private GameResult result;

    /**
     * Constructs a new game with the given settings and communication tool and puts figures onto the battlefield into the configuration following the rules.
     *
     * @param gameSettings the settings of this game
     * @param communicator the communicating tool
     */
    public Game(GameSettings gameSettings, Communicator communicator) {
        this.gameManager = new GameManager(this);
        this.gameSettings = gameSettings;
        this.communicator = communicator;
        this.timer = new Timer(gameSettings.getTimeLimit());
        this.PGNManager = new PGNManager(this);

        this.stage = gameSettings.isACustomGame() ? CUSTOM : PREPARED;
        this.battleField = new Figure[8][8];
        this.playerOnTurnIsTheWhiteOne = true;
        this.selectedFigure = Optional.empty();
        this.turnNumber = 1;
        this.neitherCapNorPawnMoveSingleSideTurns = 0;
        this.result = null;

        gameManager.performActionsBeforeGame();
    }

    /**
     * Puts figures onto the battlefield into the configuration following the rules.
     */
    public void prepareNewBattleField() {
        //rank 0
        moveFigure(new Rook(this, 0, 0, true), 0, 0);
        moveFigure(new Knight(this, 0, 1, true), 0, 1);
        moveFigure(new Bishop(this, 0, 2, true), 0, 2);
        moveFigure(new Queen(this, 0, 3, true), 0, 3);
        moveFigure(new King(this, 0, 4, true), 0, 4);
        moveFigure(new Bishop(this, 0, 5, true), 0, 5);
        moveFigure(new Knight(this, 0, 6, true), 0, 6);
        moveFigure(new Rook(this, 0, 7, true), 0, 7);
        //rank 1
        for (int file = 0; file < 8; file++) {
            moveFigure(new Pawn(this, 1, file, true), 1, file);
        }
        //rank 6
        for (int file = 0; file < 8; file++) {
            moveFigure(new Pawn(this, 6, file, false), 6, file);
        }
        //rank 7
        moveFigure(new Rook(this, 7, 0, false), 7, 0);
        moveFigure(new Knight(this, 7, 1, false), 7, 1);
        moveFigure(new Bishop(this, 7, 2, false), 7, 2);
        moveFigure(new Queen(this, 7, 3, false), 7, 3);
        moveFigure(new King(this, 7, 4, false), 7, 4);
        moveFigure(new Bishop(this, 7, 5, false), 7, 5);
        moveFigure(new Knight(this, 7, 6, false), 7, 6);
        moveFigure(new Rook(this, 7, 7, false), 7, 7);
    }

    /**
     * Returns true, only if there are valid coordinations of a chess square in the arguments. In other words, only if there are somewhere from 0 and 8 (exclusively).
     *
     * @param positionRank rank of the inspected position
     * @param positionFile file of the inspected position
     * @return validity of the specified position
     */
    public static boolean isPositionLegal(int positionRank, int positionFile) {
        return positionRank >= 0 && positionRank < NUMBER_OF_RANKS_OR_FILES &&
                positionFile >= 0 && positionFile < NUMBER_OF_RANKS_OR_FILES;
    }


    /**
     * Returns the figure on the specified position on this' battlefield.
     *
     * @param positionRank the rank of the given position
     * @param positionFile the file of the given position
     * @return the figure on the given position
     */
    public Figure getFigure(int positionRank, int positionFile) {
        if (isPositionLegal(positionRank, positionFile)) {
            return battleField[positionRank][positionFile];
        } else {
            throw new IllegalArgumentException();
        }
    }

    /**
     * Perform a simple move like IRL with vanishes and drawing. Vanishes also the figure at the target position.
     *
     * @param movedFigure figure the move is performed with
     * @param targetRank  the target rank of the move
     * @param targetFile  the target file of the move
     */
    public void moveFigure(Figure movedFigure, int targetRank, int targetFile) {
        if (isPositionLegal(targetRank, targetFile)) {

            if (movedFigure != null) {
                if (movedFigure.getShape() != null) {
                    movedFigure.getShape().vanish();
                }
            }

            Figure replacedFigure;
            if ((replacedFigure = battleField[targetRank][targetFile]) != null) {
                if (replacedFigure.getShape() != null) {
                    replacedFigure.getShape().vanish();
                }
            }

            if (movedFigure != null) {
                battleField[movedFigure.getRank()][movedFigure.getFile()] = null;
                movedFigure.setPosition(targetRank, targetFile);
            }

            battleField[targetRank][targetFile] = movedFigure;

            if (movedFigure != null) {
                if (movedFigure.getShape() != null) {
                    movedFigure.getShape().draw();
                }
            }

        } else {
            throw new IllegalArgumentException();
        }
    }

    public void switchTurn() {
        this.playerOnTurnIsTheWhiteOne = !this.playerOnTurnIsTheWhiteOne;
        if (this.playerOnTurnIsTheWhiteOne) {
            turnNumber++;
        }
        neitherCapNorPawnMoveSingleSideTurns++;

        if (this.stage != CUSTOM) {
            communicator.informPlayerWhoIsOnTurn(this.playerOnTurnIsTheWhiteOne);
        }
    }

    protected void checkForEnding() {
        if (isItDraw()) {
            this.result = DRAW;
            this.stage = ENDED;
        } else if (isItLoss()) {
            this.result = isPlayerOnTurnTheWhiteOne() ? BLACK_WIN : WHITE_WIN;
            this.stage = ENDED;
        }
    }

    private boolean isItDraw() {
        return neitherCapNorPawnMoveSingleSideTurns >= NEITHER_CAP_NOR_PAWN_MOVE_TURN_NUMBER_LIMIT || betterNoTurnForKingSafe();
    }

    private boolean betterNoTurnForKingSafe() {
        King king = getTheKingToBeDefeated(playerOnTurnIsTheWhiteOne);
        if (king == null) {
            return false;
        }
        if (isSquareDangerous(king.getRank(), king.getFile(), playerOnTurnIsTheWhiteOne)) {
            return false;
        }
        return !doesAnyMoveLeadingToKingSafeExist(king);
    }

    private boolean isItLoss() {
        King king = getTheKingToBeDefeated(playerOnTurnIsTheWhiteOne);
        if (king == null) {
            return true;
        }
        if (!isSquareDangerous(king.getRank(), king.getFile(), playerOnTurnIsTheWhiteOne)) {
            return false;
        }
        return !doesAnyMoveLeadingToKingSafeExist(king);
    }

    private boolean doesAnyMoveLeadingToKingSafeExist(King king) {
        boolean AnyMoveLeadingToKingSafeFound = false;
        Figure cappedFigure;
        for (Figure figure : getFiguresOnBoard()) {
            if (figure.isWhite() == king.isWhite()) {
                for (Position possibleMove : figure.getPossibleMoves()) {

                    cappedFigure = battleField[possibleMove.getRank()][possibleMove.getFile()];

                    int originalRank = figure.getRank();
                    int originalFile = figure.getFile();

                    battleField[originalRank][originalFile] = null;
                    battleField[possibleMove.getRank()][possibleMove.getFile()] = figure;
                    figure.setPosition(possibleMove.getRank(), possibleMove.getFile());

                    if (!isSquareDangerous(king.getRank(), king.getFile(), king.isWhite())) {
                        AnyMoveLeadingToKingSafeFound = true;
                    }

                    battleField[possibleMove.getRank()][possibleMove.getFile()] = cappedFigure;
                    battleField[originalRank][originalFile] = figure;
                    figure.setPosition(originalRank, originalFile);

                    if (AnyMoveLeadingToKingSafeFound) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Returns the king of the given side.
     *
     * @param thatKingIsWhite specifies the side: true if it's white, false otherwise.
     * @return the desired king's figure
     */
    public King getTheKingToBeDefeated(boolean thatKingIsWhite) {

        for (Figure figure : getFiguresOnBoard()) {

            if (figure instanceof King &&
                    figure.isWhite() == thatKingIsWhite) {
                return (King) figure;
            }

        }
        return null;
    }

    /**
     * Looks for and returns all the figures on this' battlefield.
     *
     * @return unmodifyable set of all the figures on this' battlefield
     */
    public Set<Figure> getFiguresOnBoard() {

        Set<Figure> figuresOnBoard = new HashSet<>();
        for (int rank = 0; rank < battleField.length; rank++) {
            for (int file = 0; file < battleField[0].length; file++) {
                Figure figure = getFigure(rank, file);
                if (figure != null) {
                    figuresOnBoard.add(figure);
                }
            }
        }
        Set<Figure> unmodifiableFigureSet = Collections.unmodifiableSet(figuresOnBoard);
        return unmodifiableFigureSet;
    }

    /**
     * Looks for a figure that makes the specified square dangerous for the other side and returns true if it found any, false otherwise.
     *
     * @param inspectedRank     the rank of the inspected position
     * @param inspectedFile     the file of the inspected position
     * @param dangerousForWhite the side which is asking about danger
     * @return true if it any figure can move to the specified place to capture the figure on it, false otherwise
     */
    public boolean isSquareDangerous(int inspectedRank, int inspectedFile, boolean dangerousForWhite) {

        for (Figure figure : getFiguresOnBoard()) {
            if (dangerousForWhite != figure.isWhite()) {

                if (figure instanceof King) {
                    if (((King) figure).isPossibleCapture(inspectedRank, inspectedFile)) {
                        return true;
                    }

                } else {
                    if(figure.isPossibleMove(inspectedRank, inspectedFile)) {
                        return true;
                    }
                }

            }
        }
        return false;
    }

    /**
     * Initializes this' result and stage.
     *
     * @param surrenderringPlayerIsTheWhiteOne true if the surrenderring player is the white one, false otherwise
     */
    public void makeSurrender(boolean surrenderringPlayerIsTheWhiteOne) {
        this.result = surrenderringPlayerIsTheWhiteOne ? BLACK_WIN : WHITE_WIN;
        this.stage = ENDED;
    }

    /**
     * Stops the customizing process and starts the classic game
     *
     * @param playerOnTurnIsTheWhiteOne value specifying the player on turn after exiting the custom mode: true for the white one, false otherwise
     */
    public void stopCustom(boolean playerOnTurnIsTheWhiteOne) {
        this.stage = STARTED;
        this.playerOnTurnIsTheWhiteOne = playerOnTurnIsTheWhiteOne;
    }

    /**
     * Looks for all the pawns of the given side and makes them not endangered by capturing en passant anymore.
     *
     * @param notEndangeredPawnsAreWhite specifies the side of the pawns looked for
     */
    public void makePawnsNotEndangeredEnPassant(boolean notEndangeredPawnsAreWhite) {
        for (Figure figure : getFiguresOnBoard()) {
            if (figure instanceof Pawn && figure.isWhite() == notEndangeredPawnsAreWhite) {
                ((Pawn) figure).setEndangeredEnPassant(false);
            }
        }
    }

    protected void resetMovesWithoutCap() {
        neitherCapNorPawnMoveSingleSideTurns = 0;
    }

    protected boolean isCastle(Figure movedFigure, int fileFrom, int fileTo) {
        return movedFigure.getType() == KING && (fileFrom - fileTo) * (fileFrom - fileTo) == 4;
    }

    protected boolean isKingsideNotQueensideCastle(Figure movedFigure, int fileTo) {
        return fileTo == King.KINGSIDE_CASTLE_TARGET_FILE;
    }

    protected boolean isPawnAtTheOtherSide(Pawn movedPawn) {
        return (movedPawn.getRank() == LAST_RANK_OF_WHITE_PAWNS && movedPawn.isWhite()) ||
                (movedPawn.getRank() == LAST_RANK_OF_BLACK_PAWNS && !movedPawn.isWhite());
    }

    protected FigureType promotePawn(Figure promotement) {
        FigureShape newShape = promotement.getShape().transformIntoQueenShape();
        FigureType promotedFigureType = PROMOTION_FIGURE_TYPE;
        promotement = Figure.newFigure(promotedFigureType, this, promotement.getRank(), promotement.getFile(), promotement.isWhite());
        newShape.setFigure(promotement);
        promotement.setShape(newShape);
        moveFigure(promotement, promotement.getRank(), promotement.getFile());
        return promotedFigureType;
    }

    /**
     * Returns this' settings.
     *
     * @return this' settings
     */
    public GameSettings getGameSettings() {
        return gameSettings;
    }

    /**
     * Returns this' game manager.
     *
     * @return this' game manager
     */
    public GameManager getGameManager() {
        return gameManager;
    }

    /**
     * Returns this' communication tool.
     *
     * @return this' communication tool
     */
    public Communicator getCommunicator() {
        return communicator;
    }

    /**
     * Returns this' timer.
     *
     * @return this' timer
     */
    public Timer getTimer() {
        return timer;
    }

    /**
     * Returns this' PGN manager.
     *
     * @return this' PGN manager
     */
    public PGNManager getPGNManager() {
        return PGNManager;
    }

    /**
     * Returns this board's shape.
     *
     * @return this board's shape
     */
    public BoardShape getBoardShape() {
        return boardShape;
    }

    /**
     * Sets this board's shape.
     *
     * @param boardShape this board's new shape
     */
    public void setBoardShape(BoardShape boardShape) {
        this.boardShape = boardShape;
    }

    /**
     * Returns this' stage.
     *
     * @return this' stage
     */
    public Stage getStage() {
        return stage;
    }

    /**
     * Sets this' stage.
     *
     * @param stage this' new stage
     */
    public void setStage(Stage stage) {
        this.stage = stage;
    }

    /**
     * Finds out who is on turn. Returns true if it's the white player, false otherwise.
     *
     * @return true if it's the white player, false otherwise
     */
    public boolean isPlayerOnTurnTheWhiteOne() {
        return playerOnTurnIsTheWhiteOne;
    }

    /**
     * Returns the selected figure
     *
     * @return the figure selected by one of the chess players
     */
    public Optional<Figure> getSelectedFigure() {
        return selectedFigure;
    }

    /**
     * Initializes selected figure and highlighted its possible moves' squares.
     *
     * @param selectedFigureOptional the new selected figure
     */
    public void setSelectedFigure(Optional<Figure> selectedFigureOptional) {
        this.selectedFigure.ifPresent(figure -> this.boardShape.lowlightPossibleMoves(figure.getPossibleMoves()));
        selectedFigureOptional.ifPresent(figure -> this.boardShape.highlightPossibleMoves(figure.getPossibleMoves()));
        this.selectedFigure = selectedFigureOptional;
    }

    /**
     * Returns the number of the current turn.
     *
     * @return the number of the current turn
     */
    public int getTurnNumber() {
        return turnNumber;
    }

    /**
     * Returns the result of this game, null if the game hasn't ended yet.
     *
     * @return the result of this game, null if the game hasn't ended yet
     */
    public GameResult getResult() {
        return result;
    }
}
