package me.Muchysta.Chess.Controller.Figures;

/**
 * Simple enumeration to name and classify figure types.
 */
public enum FigureType {
    BISHOP, KING, KNIGHT, PAWN, QUEEN, ROOK;
}
