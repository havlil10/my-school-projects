package me.Muchysta.Chess.Controller.Figures;

import me.Muchysta.Chess.Controller.Game;
import me.Muchysta.Chess.Controller.Position;

import java.util.HashSet;
import java.util.Set;

public class Knight extends Figure {

    /**
     * Constructs a knight and initializes its coordinations and other parameters.
     * @param game game this figure plays in
     * @param rank rank this figure starts at
     * @param file file this figure starts at
     * @param isWhite true if this figure stands at the white side, false otherwise
     */
    public Knight(Game game, int rank, int file, boolean isWhite) {
        super(game, rank, file, isWhite);
    }

    @Override
    public FigureType getType() {
        return FigureType.KNIGHT;
    }

    @Override
    public Set<Position> getPossibleMoves() {
        Set<Position> possibleMoves = new HashSet<>();

        int testedPositionRank;
        int testedPositionFile;

        for (int rankShift = -2; rankShift <= 2; rankShift += 4) {
            for (int fileShift = -1; fileShift <= 1; fileShift += 2) {

                testedPositionRank = this.getRank() + rankShift;
                testedPositionFile = this.getFile() + fileShift;

                if (canMoveThere(testedPositionRank, testedPositionFile)) {

                    possibleMoves.add(new Position(testedPositionRank, testedPositionFile));

                }
            }
        }

        for (int rankShift = -1; rankShift <= 1; rankShift += 2) {
            for (int fileShift = -2; fileShift <= 2; fileShift += 4) {

                testedPositionRank = this.getRank() + rankShift;
                testedPositionFile = this.getFile() + fileShift;

                if (canMoveThere(testedPositionRank, testedPositionFile)) {

                    possibleMoves.add(new Position(testedPositionRank, testedPositionFile));

                }
            }
        }

        return possibleMoves;
    }

    /**
     * Vrací true, právě když souřadnice neukazují mimo hrací plochu a na souřadnicích nestojí figurka stejné barvy.
     *
     * @param testedPositionRank
     * @param testedPositionFile
     * @return
     */
    protected boolean canMoveThere(int testedPositionRank, int testedPositionFile) {
        return Game.isPositionLegal(testedPositionRank, testedPositionFile) &&
                (this.getGame().getFigure(testedPositionRank, testedPositionFile) == null ||
                this.getGame().getFigure(testedPositionRank, testedPositionFile).isWhite() != this.isWhite());
    }
}
