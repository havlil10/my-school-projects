package me.Muchysta.Chess.Controller;

/**
 * Simple enumeration to classify possible results of a game
 */
public enum GameResult {
    DRAW,
    WHITE_WIN,
    BLACK_WIN
}
