package me.Muchysta.Chess.View.Minecraft.Listeners.CommandHandlers;

import me.Muchysta.Chess.Controller.Stage;
import me.Muchysta.Chess.View.Minecraft.BoardShapes.MinecraftBoard;
import me.Muchysta.Chess.View.Minecraft.Registers.MinecraftBoardRegister;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CustomCommand {

    protected static void handle(CommandSender sender, String[] args) {
        if (sender instanceof Player) airifyBlocks((Player) sender, args);
        printGames();
    }

    private static void airifyBlocks(Player sender, String[] args) {
        int lowerX = Integer.parseInt(args[0]);
        int lowerY = Integer.parseInt(args[1]);
        int lowerZ = Integer.parseInt(args[2]);
        int higherX = Integer.parseInt(args[3]);
        int higherY = Integer.parseInt(args[4]);
        int higherZ = Integer.parseInt(args[5]);

        if (higherX < lowerX) {
            int helper = lowerX;
            lowerX = higherX;
            higherX = helper;
        }
        if (higherY < lowerY) {
            int helper = lowerY;
            lowerY = higherY;
            higherY = helper;
        }
        if (higherZ < lowerZ) {
            int helper = lowerZ;
            lowerZ = higherZ;
            higherZ = helper;
        }

        if (higherX - lowerX < 1000 && lowerY > 0 && higherY < 256 && higherX - lowerZ < 1000) {
            World world = (sender).getWorld();
            for (int x = lowerX; x <= higherX; x++) {
                for (int y = lowerY; y <= higherY; y++) {
                    for (int z = lowerZ; z <= higherZ; z++) {
                        Block changedBlock = world.getBlockAt(x, y, z);
                        if (!changedBlock.getType().isAir()) {
                            world.getBlockAt(x, y, z).setType(Material.AIR);
                        }
                    }
                }
            }
        } else {
            System.out.println("too large differents in specified coordinates (max 1000), y must be between 0 and 255 inclusively");
        }
    }

    private static void printGames() {
        for (MinecraftBoard minecraftBoard : MinecraftBoardRegister.getMinecraftBoards()) {
            System.out.println((minecraftBoard.getGame().getStage() == Stage.CUSTOM ? "CUSTOM " : "") + "game: ");
            for (int i = 0; i < 8; i++) {
                for (int j = 0; j < 8; j++) {
                    System.out.println("rank: " + i + ", file: " + j + " - " + minecraftBoard.getGame().getFigure(i, j));
                }
            }
        }
        System.out.println("custom command handled");
    }
}
