package me.Muchysta.Chess.Controller;

public class Timer {

    private long whiteTime;     //time of the white player in milliseconds
    private long blackTime;     //time of the black player in milliseconds

    private boolean timeIsElapsing;

    private long startOfMeasuring;

    /**
     * Constructs a new timer with the specified time limits - for both players the same.
     * @param timeInMilliseconds
     */
    public Timer(long timeInMilliseconds) {
        this.whiteTime = timeInMilliseconds;
        this.blackTime = timeInMilliseconds;
    }

    /**
     * Starts measuring the time
     */
    public void startTimer() {
        startOfMeasuring = System.currentTimeMillis();
        timeIsElapsing = true;
    }

    /**
     * Stops measuring the time by setting {@code this.timeIsElapsing} to false.
     */
    public void stopTimer() {
        timeIsElapsing = false;
    }

    /**
     * Lowers the time of a specified side.
     * @param timeToLowerIsTheWhiteOne the specified side
     * @param subtrahend the time to be subtracted in milliseconds
     */
    private void lowerTime(boolean timeToLowerIsTheWhiteOne, long subtrahend) {
        if (timeToLowerIsTheWhiteOne) {
            whiteTime -= subtrahend;
        } else {
            blackTime -= subtrahend;
        }
    }

    protected boolean isWhiteTimeOut() {
        return whiteTime <= 0;
    }

    protected boolean isBlackTimeOut() {
        return blackTime <= 0;
    }

    /**
     * Returns the time remaining for the white side in milliseconds.
     * @return the time remaining for the white side in milliseconds
     */
    public long getWhiteTime() {
        return whiteTime;
    }

    /**
     * Returns the time remaining for the black side in milliseconds.
     * @return the time remaining for the black side in milliseconds
     */
    public long getBlackTime() {
        return blackTime;
    }

    /**
     * Refreshes the time remaining for the player whose time is elapsing.
     * @param timeIsElapsingForTheWhitePlayer specifies the player whose time is elapsing
     */
    public void refreshTime(boolean timeIsElapsingForTheWhitePlayer) {

        if (this.timeIsElapsing) {

            long endOfMeasuring = System.currentTimeMillis();
            long subtrahend = endOfMeasuring - this.startOfMeasuring;
            lowerTime(timeIsElapsingForTheWhitePlayer, subtrahend);
            this.startOfMeasuring = endOfMeasuring;

            if (isWhiteTimeOut() || isBlackTimeOut()) {
                stopTimer();
            }

        }
    }
}
