package me.Muchysta.Chess.Controller;

import me.Muchysta.Chess.Controller.Figures.*;

import java.util.Optional;
import java.util.logging.Logger;

import static me.Muchysta.Chess.Controller.Stage.CUSTOM;
import static me.Muchysta.Chess.Controller.Stage.ENDED;

public class GameManager {

    private final static Logger LOGGER = Logger.getLogger(GameManager.class.getName());

    private Game game;
    private String gamePGN;

    /**
     * Constructs a new game manager for a specified game.
     * @param game the specified game
     */
    public GameManager(Game game) {
        this.game = game;
        this.gamePGN = "";
    }

    /**
     * Sets the selected figure depending on what figure the player wants to select. If the figure is null or it's the opponent's figure, selected figure is set to null.
     * @param clickedAtFigure figure the player wants to select
     */
    public void makeNewSelection(Figure clickedAtFigure) {

        Optional<Figure> figureToSelect = getFigureToSelect(Optional.ofNullable(clickedAtFigure));
        if (figureToSelect != game.getSelectedFigure()) {
            game.setSelectedFigure(figureToSelect);
            game.getCommunicator().informPlayerAboutTheirSelection(game.isPlayerOnTurnTheWhiteOne(), game.getSelectedFigure());
        }
    }

    private Optional<Figure> getFigureToSelect(Optional<Figure> clickedAtFigureOptional) {
        if (clickedAtFigureOptional.isPresent()) {
            Figure clickedAtFigure = clickedAtFigureOptional.get();
            return (clickedAtFigure.isWhite() == game.isPlayerOnTurnTheWhiteOne() ||
                    this.game.getStage() == CUSTOM ? Optional.of(clickedAtFigure) : Optional.empty());
        }
        return Optional.empty();
    }

    /**
     * Moves specified figure, generates the PGN of the turn and properly switches the turning side.
     * @param movedFigure the moved figure
     * @param rankTo the target rank of the move
     * @param fileTo the target file of the move
     */
    public void executeTurn(Figure movedFigure, int rankTo, int fileTo) {

        int rankFrom = movedFigure.getRank();
        int fileFrom = movedFigure.getFile();

        game.getPGNManager().generateFirstPartByTurn(movedFigure, rankTo, fileTo,
                game.isCastle(movedFigure, fileFrom, fileTo),
                game.isKingsideNotQueensideCastle(movedFigure, fileTo),
                game.getFigure(rankTo, fileTo) != null);

        takeCareOfSpecialMoves(movedFigure, rankFrom, rankTo, fileTo);
        this.game.moveFigure(movedFigure, rankTo, fileTo);

        performActionsBetweenMoves(movedFigure, rankFrom, fileFrom);
    }

    /**
     * Executes the turn ignoring rules for moving the specified figure, switches the turn and takes care of the special turn if it is any.
     * @param movedFigure the moved figure
     * @param rankTo the target rank of the turn
     * @param fileTo the target file of the turn
     */
    public void executeCustomTurn(Figure movedFigure, int rankTo, int fileTo) {

        int rankFrom = movedFigure.getRank();
        int fileFrom = movedFigure.getFile();

        if (movedFigure instanceof Pawn && !((Pawn) movedFigure).hasBeenAlreadyMoved() &&
                (rankTo - rankFrom) * (rankTo - rankFrom) == 4 && fileFrom == fileTo) {
            ((Pawn) movedFigure).setEndangeredEnPassant(true);
        }

        this.game.moveFigure(movedFigure, rankTo, fileTo);

        game.setSelectedFigure(Optional.empty());

        game.switchTurn();

        if (movedFigure instanceof Pawn && game.isPawnAtTheOtherSide((Pawn) movedFigure)) {
            game.promotePawn(movedFigure);
        }

        game.makePawnsNotEndangeredEnPassant(game.isPlayerOnTurnTheWhiteOne());

        game.checkForEnding();
        if (game.getStage() == ENDED) {
            performActionsAfterGame();
        }
    }

    /**
     * Deals with classic captures, captures en passant, getting endangered by capturing enpassant (in case of pawn) and castles
     * @param movedFigure
     * @param rankFrom
     * @param targetRank
     * @param targetFile
     */
    private void takeCareOfSpecialMoves(Figure movedFigure, int rankFrom, int targetRank, int targetFile) {

        Figure cappedFigure = game.getFigure(targetRank, targetFile);
        if (cappedFigure != null) {
            executeCapture(cappedFigure);

        } else {
            if (movedFigure instanceof Pawn) {

                Pawn cappedEnPassantPawn;
                if (isCaptureEnPassant((cappedEnPassantPawn = (Pawn) game.getFigure(movedFigure.getRank(), targetFile)), movedFigure.getFile(), targetFile)) {
                    executeCaptureEnPassant(cappedEnPassantPawn);

                } else {
                    if ((targetRank - rankFrom) * (targetRank - rankFrom) > 1) {
                        ((Pawn) movedFigure).setEndangeredEnPassant(true);
                    }
                }

            } else {
                takeCareOfCastles(movedFigure, targetFile);
            }
        }
    }

    private void executeCapture(Figure cappedFigure) {
        game.resetMovesWithoutCap();
    }

    private void executeCaptureEnPassant(Pawn cappedPawn) {
        game.resetMovesWithoutCap();

        this.game.moveFigure(null, cappedPawn.getRank(), cappedPawn.getFile());
    }

    /**
     * Prepares the default configuration of figures according to the rules, determines which player starts and displays timers for both players.
     */
    public void performActionsBeforeGame() {
        getGame().prepareNewBattleField();
        if (game.getGameSettings().getWhitePlayer() == null && game.getGameSettings().getBlackPlayer() == null) {
            LOGGER.severe("whiteplayer: " + game.getGameSettings().getWhitePlayer() + ", blackplayer: " + game.getGameSettings().getBlackPlayer());
        }
        game.getGameSettings().determineWhoIsWhite();
        game.getCommunicator().displayTimes(this.game.getTimer(), this.game, game.getGameSettings().getWhitePlayer());
        game.getCommunicator().displayTimes(this.game.getTimer(), this.game, game.getGameSettings().getBlackPlayer());
    }

    /**
     * Returns the game this manager manages.
     * @return the game this manager manages
     */
    public Game getGame() {
        return game;
    }

    /**
     * Vanishes timer and informs players about the result.
     */
    public void performActionsAfterGame() {
        game.getCommunicator().vanishTimesForAll();
        game.getCommunicator().informPlayersAboutResult(game.getResult());
    }

    private void performActionsBetweenMoves(Figure movedFigure, int rankFrom, int fileFrom) {

        game.getCommunicator().informPlayersAboutMove(game.isPlayerOnTurnTheWhiteOne(), movedFigure, rankFrom, fileFrom, movedFigure.getRank(), movedFigure.getFile());

        game.setSelectedFigure(Optional.empty());

        game.switchTurn();

        FigureType promotedFigureType = null;
        if (movedFigure instanceof Pawn && game.isPawnAtTheOtherSide((Pawn) movedFigure)) {
            promotedFigureType = game.promotePawn(movedFigure);
        }

        game.makePawnsNotEndangeredEnPassant(game.isPlayerOnTurnTheWhiteOne());

        game.checkForEnding();
        if (game.getStage() == ENDED) {
            performActionsAfterGame();
        }

        game.getPGNManager().generateSecondPartByTurn(promotedFigureType);
    }

    private boolean isCaptureEnPassant(Pawn cappedPawn, int initialFile, int targetFile) {
        return targetFile != initialFile && cappedPawn != null;
    }

    private boolean takeCareOfCastles(Figure movedFigure, int targetFile) {
        if (movedFigure instanceof King) {

            //kingside castle
            if (targetFile - movedFigure.getFile() > 1) {
                game.moveFigure(game.getFigure(movedFigure.getRank(), Rook.RIGHT_ROOK_FILE), movedFigure.getRank(), movedFigure.getFile() + 1);
                return true;

                //queenside castle
            } else if (targetFile - movedFigure.getFile() < -1) {
                game.moveFigure(game.getFigure(movedFigure.getRank(), Rook.LEFT_ROOK_FILE), movedFigure.getRank(), movedFigure.getFile() - 1);
                return true;
            }

        }
        return false;
    }

}
