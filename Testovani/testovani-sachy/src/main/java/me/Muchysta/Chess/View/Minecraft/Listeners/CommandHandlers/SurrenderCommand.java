package me.Muchysta.Chess.View.Minecraft.Listeners.CommandHandlers;

import me.Muchysta.Chess.Controller.Game;
import me.Muchysta.Chess.Controller.GameResult;
import me.Muchysta.Chess.Controller.GameSettings;
import me.Muchysta.Chess.View.Minecraft.BoardShapes.MinecraftBoard;
import me.Muchysta.Chess.View.Minecraft.Registers.MinecraftBoardRegister;
import org.bukkit.command.CommandSender;

import static me.Muchysta.Chess.Controller.GameResult.BLACK_WIN;
import static me.Muchysta.Chess.Controller.GameResult.WHITE_WIN;
import static me.Muchysta.Chess.Controller.Stage.ENDED;

public class SurrenderCommand {

    protected static void handle(CommandSender sender, String[] args) {
        for (MinecraftBoard minecraftBoard : MinecraftBoardRegister.getMinecraftBoards()) {
            Game game = minecraftBoard.getGame();
            GameSettings settings = game.getGameSettings();
            if (sender.equals(settings.getWhitePlayer())) {

                game.makeSurrender(true);
                game.getGameManager().performActionsAfterGame();

            } else if (sender.equals(settings.getBlackPlayer())) {

                game.makeSurrender(false);
                game.getGameManager().performActionsAfterGame();

            }
        }
    }
}
