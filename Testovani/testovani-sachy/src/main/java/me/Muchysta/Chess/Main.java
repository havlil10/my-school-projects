package me.Muchysta.Chess;

import me.Muchysta.Chess.View.Minecraft.Listeners.EventHandlers;
import me.Muchysta.Chess.View.Minecraft.Listeners.CommandHandlers.MainCommandHandler;
import org.bukkit.command.CommandExecutor;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

    static org.bukkit.craftbukkit.Main Plugin;

    public static org.bukkit.craftbukkit.Main getPlugin() {
        return Plugin;
    }

    /**
     * Launched when Chess plugin is loaded on the server (e.g. when the server starts or reloads).
     * Connects commands and events to this plugin's command and event handling methods,
     */
    @Override
    public void onEnable() {
        getLogger().info("Chess plugin enabled!");

        CommandExecutor ce = new MainCommandHandler(this);
        this.getCommand("chess").setExecutor(ce);
        this.getCommand("move").setExecutor(ce);
        this.getCommand("undo").setExecutor(ce);
        this.getCommand("surrender").setExecutor(ce);
        this.getCommand("stopcustom").setExecutor(ce);
        this.getCommand("save").setExecutor(ce);
        this.getCommand("load").setExecutor(ce);
        this.getCommand("delete").setExecutor(ce);
        this.getCommand("calculateBMI").setExecutor(ce);
        this.getCommand("custom").setExecutor(ce);

        Listener l = new EventHandlers(this);
        this.getServer().getPluginManager().registerEvents(l, this);
    }

    /**
     * Launched when Chess plugin is disabled, (e.g. if the server is shut down)
     */
    @Override
    public void onDisable() {
        getLogger().info("Chess plugin disabled!");
    }
}
