package me.Muchysta.Chess.View.Minecraft.Registers;

import me.Muchysta.Chess.Controller.GameSettings;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;

public class MinecraftPlannedSettingsRegister {

    private static final int TICKS_PER_SECOND = 20;
    private static final int TICKS_PER_MINUTE = 60 * TICKS_PER_SECOND;
    private static Plugin plugin = Bukkit.getPluginManager().getPlugin("Chess");

    private static HashMap<Player, GameSettings> plannedSettings = new HashMap<>();

    /**
     * Tells whether a player can load settings they have planned.
     *
     * @param player the player whose settings are looked for
     * @return true if the player's settings are found, false otherwise
     */
    public static boolean hasPlannedAGame(Player player) {
        return plannedSettings.keySet().contains(player);
    }

    /**
     * Saves settings for a future game and removes them after a few minutes
     *
     * @param challenger the player setting the settings
     * @param settings   the settings the player is setting
     */
    public static void planSettings(Player challenger, GameSettings settings) {
        plannedSettings.put(challenger, settings);
        new BukkitRunnable() {
            @Override
            public void run() {
                if (plannedSettings.keySet().contains(challenger)) {
                    challenger.sendMessage("Your game is not ready anymore, it waited too long for you to find a place for it.");
                    plannedSettings.remove(challenger);
                }
            }
        }.runTaskLater(plugin, 2 * TICKS_PER_MINUTE);
    }

    /**
     * Returns the settings set by the specified player.
     *
     * @param challenger the specified setter of the settings
     * @return the settings set by the specified player
     */
    public static GameSettings usePlannedSettings(Player challenger) {
        return plannedSettings.remove(challenger);
    }
}
