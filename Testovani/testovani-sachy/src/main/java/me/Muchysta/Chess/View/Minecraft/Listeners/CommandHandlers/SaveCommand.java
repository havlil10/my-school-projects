package me.Muchysta.Chess.View.Minecraft.Listeners.CommandHandlers;

import me.Muchysta.Chess.Controller.GameSettings;
import me.Muchysta.Chess.Controller.PGNExtras.PGNFiler;
import me.Muchysta.Chess.View.Minecraft.BoardShapes.MinecraftBoard;
import me.Muchysta.Chess.View.Minecraft.Registers.MinecraftBoardRegister;
import org.bukkit.command.CommandSender;

import java.io.IOException;
import java.util.logging.Logger;

public class SaveCommand {
    private final static Logger LOGGER = Logger.getLogger(SaveCommand.class.getName());

    protected static void handle(CommandSender sender, String[] args) {
        for (MinecraftBoard minecraftBoard : MinecraftBoardRegister.getMinecraftBoards()) {
            GameSettings settings = minecraftBoard.getGame().getGameSettings();
            if (sender.equals(settings.getWhitePlayer()) || sender.equals(settings.getBlackPlayer())) {

                try {
                    PGNFiler.savePGN(minecraftBoard.getGame().getPGNManager().getStringPGN(), args[0]);
                    sender.sendMessage("Game is successfully saved.");

                } catch (IOException e) {

                    sender.sendMessage("We are sorry, but an error occured while saving " + args[0] + ".");
                    LOGGER.severe("IOException while saving " + args[0] + ".");
                    e.printStackTrace();
                }
            }
        }
    }
}
