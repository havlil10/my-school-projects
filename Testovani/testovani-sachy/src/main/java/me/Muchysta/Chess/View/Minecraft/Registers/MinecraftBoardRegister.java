package me.Muchysta.Chess.View.Minecraft.Registers;

import me.Muchysta.Chess.View.Minecraft.BoardShapes.MinecraftBoard;
import org.bukkit.block.Block;

import java.util.HashSet;
import java.util.Set;

public class MinecraftBoardRegister {

    private static Set<MinecraftBoard> MinecraftBoards = new HashSet<>();

    /**
     * Registers a specified board, which means it adds it to the list of registered boards for Minecraft.
     *
     * @param minecraftBoard board to get registered
     */
    public static void registerMinecraftBoard(MinecraftBoard minecraftBoard) {
        MinecraftBoards.add(minecraftBoard);
    }

    /**
     * Iterates all the registered boards and looks for the board that includes the specified block.
     *
     * @param block the specified block which board this method looks for
     * @return the board with the specified block
     */
    public static MinecraftBoard getBoardByBlock(Block block) {
        for (MinecraftBoard minecraftBoard : MinecraftBoards) {
            if (minecraftBoard.hasBlock(block)) {
                return minecraftBoard;
            }
        }
        return null;
    }

    /**
     * Deactivates a specified board, which means it removes it from the list of registered boards for Minecraft.
     *
     * @param minecraftBoard the board to be deactivated
     */
    public static void deactivateBoard(MinecraftBoard minecraftBoard) {
        MinecraftBoards.remove(minecraftBoard);
    }

    /**
     * Returns registered Minecraft boards.
     *
     * @return registered Minecraft boards
     */
    public static Set<MinecraftBoard> getMinecraftBoards() {
        return MinecraftBoards;
    }
}
