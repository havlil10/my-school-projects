package me.Muchysta.Chess.Controller.Figures;

import me.Muchysta.Chess.Controller.Abstracts.FigureShape;
import me.Muchysta.Chess.Controller.Game;
import me.Muchysta.Chess.Controller.Position;
import me.Muchysta.Chess.Controller.SquareToTexter;

import java.util.Set;

public abstract class Figure {
    Game game;
    FigureType type;
    private boolean isWhite;
    private Position position;
    private FigureShape shape;

    /**
     * Constructs a figure and initializes its coordinations and other parameters.
     *
     * @param game    game this figure plays in
     * @param rank    rank this figure starts at
     * @param file    file this figure starts at
     * @param isWhite true if this figure stands at the white side, false otherwise
     */
    public Figure(Game game, int rank, int file, boolean isWhite) {
        this.game = game;
        this.position = new Position(rank, file);
        this.isWhite = isWhite;
    }

    /**
     * Returns figure depending on the given type: if the figure's type is not queen type, it returns null, new queen otherwise.
     *
     * @param promotionfiguretype type of figure to be returned
     * @param game                game the figure plays in
     * @param rank                initial rank of the returned figure
     * @param file                initial file of the returned figure
     * @param isWhite             true if this figure should stand at the white side, false otherwise
     * @return new figure of attributes described by the arguments
     */
    public static Figure newFigure(FigureType promotionfiguretype, Game game, int rank, int file, boolean isWhite) {
        if (promotionfiguretype == FigureType.QUEEN) {
            return new Queen(game, rank, file, isWhite);
        } else {
            return null;
        }
    }

    /**
     * Returns this' type.
     *
     * @return this' type
     */
    public abstract FigureType getType();

    /**
     * Returns this' shape.
     *
     * @return this' shape
     */
    public FigureShape getShape() {
        return shape;
    }


    /**
     * Sets this' shape.
     */
    public void setShape(FigureShape shape) {
        this.shape = shape;
    }

    /**
     * Returns this' playing side.
     *
     * @return this' playing side
     */
    public boolean isWhite() {
        return isWhite;
    }

    /**
     * Returns the game this figure plays in.
     *
     * @return this' game
     */
    public Game getGame() {
        return game;
    }

    /**
     * Returns the index of rank this figure thinks it stands at.
     *
     * @return this' rank
     */
    public int getRank() {
        return position.getRank();
    }

    /**
     * Returns the index of file this figure thinks it stands at.
     *
     * @return this' file
     */
    public int getFile() {
        return position.getFile();
    }

    /**
     * Sets the index of rank this figure thinks it stands at.
     *
     * @return this' rank
     */
    public void setRank(int rank) {
        if (this instanceof Castleable &&
                this.position.getRank() != rank) {
            ((Castleable) this).alreadyHasBeenMoved();
        }
        this.position.setRank(rank);
    }

    /**
     * Sets the index of file this figure thinks it stands at.
     *
     * @return this' file
     */
    public void setFile(int file) {
        if (this instanceof Castleable &&
                this.position.getFile() != file) {
            ((Castleable) this).alreadyHasBeenMoved();
        }
        this.position.setFile(file);
    }

    /**
     * Sets the indeces of rank and file this figure thinks it stands at.
     *
     * @param rank this' new rank
     * @param file this' new file
     */
    public void setPosition(int rank, int file) {
        this.setRank(rank);
        this.setFile(file);
    }

    /**
     * Puts this' attributes into a sense making text and returns it.
     *
     * @return string describing this figure
     */
    public String toString() {
        return (this.isWhite() ? "white " : "black ") + this.getType() + " " + SquareToTexter.squareToText(this.getRank(), this.getFile());
    }

    /**
     * Calculates and returns the set of arrays of length 2 representing the coordinations of squares this figure can move to.
     *
     * @return set of arrays representing the coordinations of squares this figure can move to
     */
    public abstract Set<Position> getPossibleMoves();

    /**
     * Looks for a possible move in the set of possible moves of this figure and returns true if the specified position was found amongst them, false if it wasn't.
     *
     * @param rank the rank of position asked to look for
     * @param file the file of position asked to look for
     * @return true if the figure can move onto the specified square, false otherwise
     */
    public boolean isPossibleMove(int rank, int file) {
        for (Position possibleMove : this.getPossibleMoves()) {
            if (possibleMove.getRank() == rank && possibleMove.getFile() == file) {
                return true;
            }
        }
        return false;
    }
}
