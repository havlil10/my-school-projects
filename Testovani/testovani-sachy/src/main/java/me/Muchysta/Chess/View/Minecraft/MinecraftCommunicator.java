package me.Muchysta.Chess.View.Minecraft;

import me.Muchysta.Chess.Controller.Abstracts.Communicator;
import me.Muchysta.Chess.Controller.Game;
import me.Muchysta.Chess.Controller.MessageFormat;
import me.Muchysta.Chess.Controller.Timer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.*;

import java.util.HashSet;
import java.util.Set;

import static me.Muchysta.Chess.Controller.Stage.ENDED;

public class MinecraftCommunicator extends Communicator {

    private static Plugin plugin = Bukkit.getPluginManager().getPlugin("Chess");

    private Scoreboard scoreboard;
    private boolean scoreboardOn;
    private Set<Object> playersWithDisplayOn = new HashSet();
    private Timer timer;
    private Game game;


    /**
     * Constructs a new communication tool for Minecraft
     *
     * @param whitePlayer the player on the white side
     * @param blackPlayer the player on the black side
     */
    public MinecraftCommunicator(Object whitePlayer, Object blackPlayer) {
        super(whitePlayer, blackPlayer);
        this.scoreboard = createBoard();
    }

    private Scoreboard createBoard() {
        ScoreboardManager manager = Bukkit.getScoreboardManager();
        Scoreboard scoreboard = manager.getNewScoreboard();
        Objective obj = scoreboard.registerNewObjective("Chess match", "dummy", "Chess");
        obj.setDisplaySlot(DisplaySlot.SIDEBAR);

        return scoreboard;
    }


    @Override
    protected void sendMessage(Object player, String text, MessageFormat format) {
        switch (format) {
            case INSTRUCTION:
                ((Player) player).sendMessage(text);
                break;
            case GREAT:
                ((Player) player).sendTitle(text, "", 5, 50, 5);
                break;
            default: //FLEETING
                ((Player) player).sendTitle( "", text, 5, 10, 5);
                break;
        }
    }


    @Override
    public void displayTimes(Timer timer, Game game, Object player) {
        playersWithDisplayOn.add(player);
        ((Player) player).setScoreboard(scoreboard);

        if (!scoreboardOn) {
            this.timer = timer;
            this.game = game;
            switchScoreboardRefreshingOn();
        }
    }

    @Override
    public void vanishTimesForAll() {
        for (Object playerWithDisplayOn : playersWithDisplayOn) {
            vanishTimes(playerWithDisplayOn);
        }
    }

    @Override
    public void vanishTimes(Object player) {
        playersWithDisplayOn.remove(player);

        if (playersWithDisplayOn.isEmpty()) {

            switchScoreBoardRefreshingOff();

            if (game.getStage() == ENDED) {
                ((Player) player).getScoreboard().getObjective("Chess match").setDisplaySlot(null);
                ((Player) player).getScoreboard().getObjective("Chess match").unregister();
            }

        }
    }

    private void switchScoreboardRefreshingOn() {

        scoreboardOn = true;

        new BukkitRunnable() {
            @Override
            public void run() {

                if (scoreboardOn) {

                    if (!playersWithDisplayOn.isEmpty()) {
                        timer.refreshTime(game.isPlayerOnTurnTheWhiteOne());
                        scoreboard.getObjective("Chess match").getScore("White: ").setScore((int) (timer.getWhiteTime() / 1000));
                        scoreboard.getObjective("Chess match").getScore("Black: ").setScore((int) (timer.getBlackTime() / 1000));
                    }

                } else {
                    this.cancel();
                }
            }
        }.runTaskTimer(plugin, 0, 10);
    }

    private void switchScoreBoardRefreshingOff() {
        scoreboardOn = false;
    }
}
