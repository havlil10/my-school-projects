package me.Muchysta.Chess.View.Minecraft.FigureShapes;

/**
 * Distinguishes roles of the materials used to build figures.
 */
public enum BuildingMaterialType {
    DEFAULT(),
    ALTERNA(),
    NOTHING()
}
