package me.Muchysta.Chess.View.Minecraft.Listeners.CommandHandlers;

import me.Muchysta.Chess.Controller.Game;
import me.Muchysta.Chess.Controller.GameSettings;
import me.Muchysta.Chess.Controller.Stage;
import me.Muchysta.Chess.View.Minecraft.BoardShapes.MinecraftBoard;
import me.Muchysta.Chess.View.Minecraft.Registers.MinecraftBoardRegister;
import org.bukkit.command.CommandSender;

public class StopCustomCommand {

    protected static void handle(CommandSender sender, String[] args) {

        for (MinecraftBoard minecraftBoard : MinecraftBoardRegister.getMinecraftBoards()) {
            Game game = minecraftBoard.getGame();
            GameSettings settings = game.getGameSettings();

            if (sender.equals(settings.getWhitePlayer()) || sender.equals(settings.getBlackPlayer())) {

                if (game.getStage() == Stage.CUSTOM) {
                    game.stopCustom(args[0].equalsIgnoreCase("white"));
                }
            }
        }
    }
}
