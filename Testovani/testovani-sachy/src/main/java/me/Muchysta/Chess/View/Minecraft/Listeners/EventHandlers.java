package me.Muchysta.Chess.View.Minecraft.Listeners;

import me.Muchysta.Chess.Controller.Figures.Figure;
import me.Muchysta.Chess.Controller.Game;
import me.Muchysta.Chess.Main;
import me.Muchysta.Chess.View.Minecraft.BlockModificators.MinecraftBoardBuilder;
import me.Muchysta.Chess.View.Minecraft.BoardShapes.MinecraftBoard;
import me.Muchysta.Chess.View.Minecraft.Registers.MinecraftBoardRegister;
import me.Muchysta.Chess.View.Minecraft.Registers.MinecraftFigureRegister;
import me.Muchysta.Chess.View.Minecraft.Registers.MinecraftPlannedSettingsRegister;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;

import java.util.HashSet;
import java.util.Set;

import static me.Muchysta.Chess.Controller.Stage.*;
import static me.Muchysta.Chess.View.Minecraft.BlockModificators.HighLighter.HIGHLIGHTING_MATERIAL;
import static me.Muchysta.Chess.View.Minecraft.BlockModificators.MinecraftBoardBuilder.BLACK_SQUARE_MATERIAL;
import static me.Muchysta.Chess.View.Minecraft.BlockModificators.MinecraftBoardBuilder.WHITE_SQUARE_MATERIAL;
import static me.Muchysta.Chess.View.Minecraft.BlockModificators.MinecraftFigureBuilder.*;


public class EventHandlers implements Listener {

    private Main plugin;

    private static final int PLAYER_CLICK_RANGE = 144;
    private static final Set<Material> TRANSPARENT_MATERIALS = setOfMaterials(new Material[]{Material.VOID_AIR, Material.AIR, Material.WATER, Material.LAVA});
    private static final Set<Material> CHESS_MATERIALS = setOfMaterials(new Material[]{WHITE_SQUARE_MATERIAL, BLACK_SQUARE_MATERIAL, WHITE_DEFAULT, BLACK_DEFAULT, WHITE_ALTERNATIVE, BLACK_ALTERNATIVE, HIGHLIGHTING_MATERIAL});
    public static final Material CHESS_HANDLING_TOOL = Material.STICK;

    private static Set<Material> setOfMaterials(Material[] materials) {
        Set<Material> setOfTransparentMaterials = new HashSet<>();
        for (Material material : materials) {
            setOfTransparentMaterials.add(material);
        }
        return setOfTransparentMaterials;
    }

    /**
     * Constructs new set of eventhandlers.
     * @param plugin the plugin these event handlers are called within
     */
    public EventHandlers(Main plugin) {
        this.plugin = plugin;
    }

    /**
     * Reacts on players' interactions (mainly on clicks).
     * @param pie the event of a player's interaction
     */
    @EventHandler
    public void OnPlayerInteractEvent(PlayerInteractEvent pie) {

        Block targetBlock = pie.getPlayer().getTargetBlock(TRANSPARENT_MATERIALS, PLAYER_CLICK_RANGE);
        if (isAChessEvent(pie, targetBlock)) {
            performChessAction(pie.getPlayer(), targetBlock);
        }
    }

    private boolean isAChessEvent(PlayerInteractEvent pie, Block targetBlock) {
        return pie.getMaterial() == CHESS_HANDLING_TOOL &&
                targetBlock != null &&
                (pie.getAction().equals(Action.RIGHT_CLICK_AIR) || pie.getAction().equals(Action.RIGHT_CLICK_BLOCK)) &&
                 pie.getHand().equals(EquipmentSlot.HAND);
    }

    private void performChessAction(Player player, Block targetBlock) {
        if (!performActionIfShapeExists(player, targetBlock)) {
            if (MinecraftPlannedSettingsRegister.hasPlannedAGame(player)) {
                MinecraftBoardBuilder.buildMinecraftBoardOfAPlannedGameIfEnoughSpace(player, targetBlock);
            }
        }
    }

    /**
     * @param player
     * @param targetBlock
     * @return an game for action was found
     */
    private boolean performActionIfShapeExists(Player player, Block targetBlock) {

        if (isAPossibleChessBlock(targetBlock)) {

            MinecraftBoard board = MinecraftBoardRegister.getBoardByBlock(targetBlock);
            if (board == null) {

                Figure figureOnSquare = MinecraftFigureRegister.getFigureByBlock(targetBlock);
                if (figureOnSquare == null) {
                    return false;
                }

                performAGameAction(player, figureOnSquare.getGame(), figureOnSquare.getRank(), figureOnSquare.getFile());
                return true;

            } else {
                performAGameAction(player, board.getGame(), board.getRankByBlock(targetBlock), board.getFileByBlock(targetBlock));
                return true;
            }

        }
        return false;
    }

    private boolean isAPossibleChessBlock(Block targetBlock) {
        return CHESS_MATERIALS.contains(targetBlock.getType());
    }

    private void performAGameAction(Player player, Game game, int rank, int file) {
        if (game.getStage() == CUSTOM) {

            launchMoveOrSelectFigure(game, rank, file);

        } else if(game.getStage() == ENDED) {

            for(Figure figure : game.getFiguresOnBoard()) {
                figure.getShape().vanish();
            }
            game.getBoardShape().vanish();

        } else if (isPlayerOnTurn(player, game)) {

            if(game.getStage() == PREPARED) {
                game.setStage(STARTED);
                game.getTimer().startTimer();
            }

            launchMoveOrSelectFigure(game, rank, file);
        }
    }


    private void launchMoveOrSelectFigure(Game game, int clickedAtRank, int clickedAtFile) {

        Figure figureOfSquare = game.getFigure(clickedAtRank, clickedAtFile);

        if (!game.getSelectedFigure().isPresent()) {
            game.getGameManager().makeNewSelection(figureOfSquare);

        } else {

            if (game.getStage() == CUSTOM) {
                game.getGameManager().executeCustomTurn(game.getSelectedFigure().get(), clickedAtRank, clickedAtFile);

            } else if (game.getSelectedFigure().get().isPossibleMove(clickedAtRank, clickedAtFile)) {
                game.getGameManager().executeTurn(game.getSelectedFigure().get(), clickedAtRank, clickedAtFile);

            } else {
                game.getGameManager().makeNewSelection(figureOfSquare);
            }

        }
    }

    private boolean isPlayerOnTurn(Player player, Game game) {
        return player == game.getGameSettings().getWhitePlayer() && game.isPlayerOnTurnTheWhiteOne() ||
                player == game.getGameSettings().getBlackPlayer() && !game.isPlayerOnTurnTheWhiteOne();
    }
}
