package me.Muchysta.Chess.View.Minecraft.Listeners.CommandHandlers;

import me.Muchysta.Chess.Controller.PGNExtras.PGNFiler;
import org.bukkit.command.CommandSender;

public class DeleteCommand {

    protected static void handle(CommandSender sender, String[] args) {
        if (PGNFiler.deletePGN(args[0])) {
            sender.sendMessage("Game was successfully deleted.");
        }
    }
}
