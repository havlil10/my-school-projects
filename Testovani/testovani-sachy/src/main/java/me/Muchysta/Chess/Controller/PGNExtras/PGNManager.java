package me.Muchysta.Chess.Controller.PGNExtras;

import me.Muchysta.Chess.Controller.Figures.Figure;
import me.Muchysta.Chess.Controller.Figures.FigureType;
import me.Muchysta.Chess.Controller.Figures.King;
import me.Muchysta.Chess.Controller.Game;
import me.Muchysta.Chess.Controller.GameResult;
import me.Muchysta.Chess.Controller.SquareToTexter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import static me.Muchysta.Chess.Controller.Figures.FigureType.*;
import static me.Muchysta.Chess.Controller.Figures.King.*;
import static me.Muchysta.Chess.Controller.Stage.ENDED;

public class PGNManager {

    private final static Logger LOGGER = Logger.getLogger(PGNManager.class.getName());
    private static final String RESULT_MARK = "RESULT";
    private static final int CHARS_AFTER_TAGS = 2;
    Game game;
    String stringPGN;

    protected static HashMap<FigureType, String> figureTypesToAbbreviations = new HashMap<>();

    static {
        figureTypesToAbbreviations.put(BISHOP, "B");
        figureTypesToAbbreviations.put(KING, "K");
        figureTypesToAbbreviations.put(KNIGHT, "N");
        figureTypesToAbbreviations.put(PAWN, "");
        figureTypesToAbbreviations.put(QUEEN, "Q");
        figureTypesToAbbreviations.put(ROOK, "R");
    }

    private static HashMap<String, FigureType> figureAbbreviationsToTypes = new HashMap<>();

    static {
        figureAbbreviationsToTypes.put("B", BISHOP);
        figureAbbreviationsToTypes.put("K", KING);
        figureAbbreviationsToTypes.put("N", KNIGHT);
        figureAbbreviationsToTypes.put("", PAWN);
        figureAbbreviationsToTypes.put("Q", QUEEN);
        figureAbbreviationsToTypes.put("R", ROOK);
    }

    /**
     * Constructs a new manager of PGN string for the specified game.
     *
     * @param game game this manager manages PGN for
     */
    public PGNManager(Game game) {
        this.game = game;
        this.stringPGN = "";
    }

    private String determineCurrentDate() {
        return java.time.LocalDate.now().toString().replace('-', '.');
    }

    /**
     * Adds default tags to this'PGN and alternatively runs the game based on the move content of the given pgn.
     *
     * @param startingStringPGN the PGN string of the loaded game
     */
    public void initializePGN(String startingStringPGN) {
        if (startingStringPGN.length() > 0) {
            game.getPGNManager().addTags();
            game.getPGNManager().runGameByText(startingStringPGN);
        } else {
            game.getPGNManager().addTags();
        }
    }

    /**
     * Adds default tags to the beginning of this' PGN string
     */
    private void addTags() {

        String tags = "" +
                "[Event \"friendly match\"]\n" +
                "[Site \"abc\"]\n" +
                "[Date \"" + determineCurrentDate() + "\"]\n" +
                "[Round \"1\"]\n" +
                "[White \"" + game.getGameSettings().getWhitePlayer() + "\"]\n" +
                "[Black \"" + game.getGameSettings().getWhitePlayer() + "\"]\n" +
                "[Result \"" + RESULT_MARK + "\"]\n" +
                "\n";
        stringPGN = tags + stringPGN;
    }

    private void runGameByText(String stringPGN) {
        replaceTags(stringPGN);
        ArrayList<String> turns = determineSplitTurns(stringPGN);
        executeTurns(turns);
    }

    private void replaceTags(String stringPGN) {
        int lastIndexOfNewTags = stringPGN.lastIndexOf(']');
        int lastIndexOfOldTags = this.stringPGN.lastIndexOf(']');
        this.stringPGN = stringPGN.substring(0, lastIndexOfNewTags + 1) + this.stringPGN.substring(lastIndexOfOldTags + 1);
    }

    private static ArrayList<String> determineSplitTurns(String stringPGN) {

        String stringPGNWithNewLinesRemoved = stringPGN.replace("\n", " ");
        String stringPGNWithReducedSpaces = stringPGNWithNewLinesRemoved.replace("  ", " ");
        String turnsWithNumbers = stringPGNWithReducedSpaces.substring(stringPGNWithReducedSpaces.lastIndexOf(']') + CHARS_AFTER_TAGS + 1);
        String turnsWithoutNumbers = removeTurnNumbers(turnsWithNumbers);
        String[] splitTurnsWithoutNumbers = turnsWithoutNumbers.split(" ");

        ArrayList<String> splitTurns = new ArrayList<>();
        for (int i = 0; i < splitTurnsWithoutNumbers.length; i++) {
            splitTurns.add(splitTurnsWithoutNumbers[i]);
        }

        return splitTurns;
    }

    private static String removeTurnNumbers(String turnsWithNumbers) {


        String turnsWithoutNumbers = "";

        for (int charI = 0; charI < turnsWithNumbers.length(); charI++) {

            char ch = turnsWithNumbers.charAt(charI);
            if (ch != '.') {
                turnsWithoutNumbers += ch;
            } else {
                turnsWithoutNumbers = eraseDigitSequenceAtTheEnd(turnsWithoutNumbers);
                charI += getNumberOfFollowingSpaces(turnsWithNumbers, charI);
            }

        }

        return turnsWithoutNumbers;
    }

    private static String eraseDigitSequenceAtTheEnd(String turnsWithoutNumbers) {
        while (turnsWithoutNumbers.length() != 0 &&
                isDigit(turnsWithoutNumbers.charAt(turnsWithoutNumbers.length() - 1))) {
            turnsWithoutNumbers = turnsWithoutNumbers.substring(0, turnsWithoutNumbers.length() - 1);
        }
        return turnsWithoutNumbers;
    }

    /**
     * Tells whether the specified char is a digit (0-9) or not.
     *
     * @param maybeDigit the char the information is wanted about
     * @return true if the string of digits contains the specified char, false otherwise
     */
    public static boolean isDigit(char maybeDigit) {
        return "0123456789".contains("" + maybeDigit);
    }

    private static int getNumberOfFollowingSpaces(String turnsWithNumbers, int charI) {
        int followingSpaces = 0;
        while (turnsWithNumbers.charAt(charI + 1 + followingSpaces) == ' ') {
            followingSpaces++;
        }
        return followingSpaces;
    }


    private void executeTurns(ArrayList<String> turns) {

        for (int i = 0; i < turns.size(); i++) {
            String turnText = turns.get(i);

            boolean areWhiteOnTurn = i % 2 == 0;

            int[] targetPosition = getTargetPositionFromTurnText(turnText, areWhiteOnTurn);
            int rankTo = targetPosition[0];
            int fileTo = targetPosition[1];

            Figure figure = getFigureFromTurnText(turnText, areWhiteOnTurn, rankTo, fileTo);

            game.getGameManager().executeTurn(figure, rankTo, fileTo);
            game.getBoardShape().addTurnToQueue(figure, rankTo, fileTo);
        }
        game.getBoardShape().pourTurnQueue();
    }

    private Figure getFigureFromTurnText(String turnText, boolean areWhiteOnTurn, int rankTo, int fileTo) {

        if (turnText.charAt(0) == 'O') {
            for (Figure figure : this.game.getFiguresOnBoard()) {
                if (figure.getType() == KING && figure.isWhite() == areWhiteOnTurn) {
                    return figure;
                }
            }
            throw new IllegalArgumentException();

        } else {
            String figureAbbreviationInText = "" + (figureAbbreviationsToTypes.keySet().contains("" + turnText.charAt(0)) ? turnText.charAt(0) : "");
            FigureType figureType = figureAbbreviationsToTypes.get(figureAbbreviationInText);

            List<Figure> figuresInQuestion = new ArrayList<>();
            for (Figure figure : this.game.getFiguresOnBoard()) {
                if (figure.getType() == figureType && figure.isWhite() == areWhiteOnTurn && figure.isPossibleMove(rankTo, fileTo)) {
                    figuresInQuestion.add(figure);
                }
            }

            if (figuresInQuestion.size() > 1) {
                char fileSpecification = (figureType == PAWN ? turnText.charAt(0) : turnText.charAt(1));
                figuresInQuestion.removeIf(figureInQuestion -> (SquareToTexter.textToFile(fileSpecification) != figureInQuestion.getFile()));
                if (figuresInQuestion.size() > 1) {
                    char rankSpecification = (figureType == PAWN ? turnText.charAt(1) : turnText.charAt(2));
                    figuresInQuestion.removeIf(figureInQuestion -> (SquareToTexter.textToRank(rankSpecification) != figureInQuestion.getRank()));
                }
            }

            if (figuresInQuestion.size() == 0) {
                System.out.println("PGNManager, turnText: " + turnText);
                System.out.println("figureAbbreviationInText: " + figureAbbreviationInText);
                System.out.println("figureType: " + figureType);
                System.out.println("areWhiteOnTurn: " + areWhiteOnTurn);
            }
            return figuresInQuestion.get(0);
        }
    }

    private int[] getTargetPositionFromTurnText(String turnText, boolean areWhiteOnTurn) {
        String squareText;
        if (turnText.contains("O-O")) {
            int targetRank = areWhiteOnTurn ? WHITE_CASTLE_RANK : BLACK_CASTLE_RANK;
            if (turnText.contains("O-O-O")) {
                return new int[]{targetRank, QUEENSIDE_CASTLE_TARGET_FILE};
            }
            return new int[]{targetRank, KINGSIDE_CASTLE_TARGET_FILE};
        }
        if (turnText.contains("=")) {
            squareText = turnText.substring(turnText.indexOf('=') - 2, turnText.indexOf('='));
        } else if (turnText.charAt(turnText.length() - 1) == '+' || turnText.charAt(turnText.length() - 1) == '#') {
            squareText = turnText.substring(turnText.length() - 1 - 2, turnText.length() - 1);
        } else {
            squareText = turnText.substring(turnText.length() - 2);
        }
        return SquareToTexter.textToSquare(squareText);
    }


    /**
     * Adds the first part of PGN informations about the turn to this' PGN string. This method has to be called before the moves are executed.
     *
     * @param movedFigure                  moved figure
     * @param rankTo                       the target rank of the moved figure
     * @param fileTo                       the target file of the moved figure
     * @param isCastle                     castle beingness of the turn
     * @param isKingsideNotQueensideCastle kingside castle beingness of the turn (this value doesn't matter if the turn is not a castle)
     * @param isCapture                    capture beingness of the turn
     */
    public void generateFirstPartByTurn(Figure movedFigure, int rankTo, int fileTo,
                                        boolean isCastle, boolean isKingsideNotQueensideCastle,
                                        boolean isCapture) {

        String turnText = "";
        if (movedFigure.isWhite()) {
            turnText += game.getTurnNumber() + ". ";
        }

        if (isCastle) {
            turnText += (isKingsideNotQueensideCastle ? "O-O" : "O-O-O");

        } else {
            turnText += figureTypesToAbbreviations.get(movedFigure.getType()) +
                    specifyConcernedFigure(movedFigure, rankTo, fileTo) +
                    (isCapture ? "x" : "") +
                    SquareToTexter.squareToText(rankTo, fileTo);
        }
        this.stringPGN += turnText;
    }

    protected String specifyConcernedFigure(Figure movedFigure, int rankTo, int fileTo) {

        boolean fileNeededToSpecify = false;

        for (Figure figure : this.game.getFiguresOnBoard()) {
            if (figure.getType() == movedFigure.getType() && figure.isWhite() == movedFigure.isWhite() && !figure.equals(movedFigure) && figure.isPossibleMove(rankTo, fileTo)) {
                fileNeededToSpecify = true;
                if (figure.getFile() == movedFigure.getFile()) {
                    return SquareToTexter.squareToText(movedFigure.getRank(), movedFigure.getFile());
                }
            }
        }

        return (fileNeededToSpecify ? SquareToTexter.fileToText(movedFigure.getFile()) : "");
    }

    /**
     * Adds the second and at the same time the last part of PGN informations about the turn to this' PGN string. This method has to be called after the moves are executed.
     *
     * @param promotionFigureType the type of figure which has been promoted, should be {@code null} if there is not any.
     */
    public void generateSecondPartByTurn(FigureType promotionFigureType) {

        if (promotionFigureType != null) {
            this.stringPGN += "=" + figureTypesToAbbreviations.get(promotionFigureType);
        }

        King king = game.getTheKingToBeDefeated(game.isPlayerOnTurnTheWhiteOne());
        if (king == null) {
            String result = determineResult(game.getResult());
            this.stringPGN.replace(RESULT_MARK, result);
            this.stringPGN += "#" + result;
        }
        if (game.isSquareDangerous(king.getRank(), king.getFile(), king.isWhite())) {

            if (game.getStage() == ENDED) {
                String result = determineResult(game.getResult());
                this.stringPGN.replace(RESULT_MARK, result);
                this.stringPGN += "#" + result;
            } else {
                this.stringPGN += "+";
            }
        }
        this.stringPGN += " ";
    }

    private String determineResult(GameResult result) {
        switch (result) {
            case WHITE_WIN:
                return "1-0";
            case BLACK_WIN:
                return "0-1";
            default:
                return "1/2-1/2";
        }
    }

    /**
     * Returns this' PGN string.
     *
     * @return this' PGN string
     */
    public String getStringPGN() {
        return stringPGN;
    }
}
