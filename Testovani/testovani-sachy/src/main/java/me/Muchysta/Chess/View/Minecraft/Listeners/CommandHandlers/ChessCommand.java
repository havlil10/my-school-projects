package me.Muchysta.Chess.View.Minecraft.Listeners.CommandHandlers;

import me.Muchysta.Chess.Controller.GameSettings;
import me.Muchysta.Chess.Controller.PGNExtras.PGNFiler;
import me.Muchysta.Chess.View.Minecraft.Registers.MinecraftPlannedSettingsRegister;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static me.Muchysta.Chess.View.Minecraft.Listeners.EventHandlers.CHESS_HANDLING_TOOL;

public class ChessCommand {

    protected static void handle(CommandSender sender, String[] args, String stringPGN) {

        if (sender instanceof Player) {

            Player opponent;
            int timeInSecs;
            boolean isCustom = false;

            if ((opponent = Bukkit.getPlayer(args[0])) == null) {
                sender.sendMessage("Opponent not found!");
                return;
            }
            if ((timeInSecs = Integer.parseInt(args[1]) * 1000) <= 0) {
                sender.sendMessage("Time limit has to be higher than 0!");
                return;

            } else if (timeInSecs <= 10000) {
                sender.sendMessage("We are sorry, but players having set the time limit this low usually demolish their accessories and sue the author of the plugin for it." +
                        "(JK, I just need another CE) Set the time limit higher than 10 seconds.");
                return;
            }
            if (args.length == 3) {
                isCustom = args[2].equalsIgnoreCase("custom");
                if (!isCustom) {
                    sender.sendMessage("To use custom mode, write \"custom\" as the third argument.");
                }
            }

            GameSettings settings = new GameSettings(sender, opponent, timeInSecs, isCustom, stringPGN);
            MinecraftPlannedSettingsRegister.planSettings((Player) sender, settings);
            sender.sendMessage("Game ready! Use a " + CHESS_HANDLING_TOOL.name() + " to create a chess board.");
        }
    }
}
