    class LocalStorageManager {
        addRecord(name, time) {
            window.localStorage.setItem(localStorage.length, name);
            window.localStorage.setItem(localStorage.length, time);
        }

        getRecords(stopWatchHtml) {
            stopWatchHtml.innerHTML = "";
            for(let i = localStorage.length; i > 0; i-=2) {
                let seconds = localStorage.getItem(i-1);
                stopWatchHtml.innerHTML += "<span><b>Název:</b> " + localStorage.getItem(i-2) 
                + "</span>, <span class='unbreakableSpan'><b>Strávený čas:</b> " + Math.floor(seconds/3600) + " h, " 
                + Math.floor((seconds/60)%60) + " min, " + seconds%60 + " s</span><br><hr>";
            }

            if (localStorage.length==0) {
                stopWatchHtml.innerHTML = "Zatím žádné záznamy";
            }
            
        }
    }


    class Stopwatch {
        seconds;
        isStarted = false;
        watchInterval;
        stopWatchHtml;
        localStorageManager;
        recordsSummaryHtml;
        nameOfWorkInput;

        
        constructor(seconds, stopWatchHtml, localStorageManager, recordsSummaryHtml, nameOfWorkInput) {
            this.seconds = seconds;
            this.stopWatchHtml = stopWatchHtml;
            this.localStorageManager = localStorageManager;
            this.recordsSummaryHtml = recordsSummaryHtml;
            this.nameOfWorkInput = nameOfWorkInput;
        }
        
        //i.e. returns 00:12:21
        getWatch() {
            let hours = Math.floor(this.seconds/3600);
            let minutes = (Math.floor(this.seconds/60))%60
            let secs = this.seconds%60
            if(hours<=9) {
                hours = "0" + hours;
            }
            if(minutes<=9) {
                minutes = "0" + minutes;
            }
            if(secs<=9) {
                secs = "0" + secs;
            }
            return hours + ":" + minutes + ":" + secs;
        }
        
        startWatch() {
            if(!this.isStarted) {
                this.isStarted = true;
                this.watchInterval = setInterval(function () {
                    this.seconds +=1;
                    this.stopWatchHtml.innerHTML = this.getWatch();
                }.bind(this), 1000);
            }
        }

        endWatch() {
            if (nameOfWorkInput.value == "" || this.seconds<2 || nameOfWorkInput.value.length > 40) {
                nameOfWorkInput.style.borderColor = "red";
            } else {
                nameOfWorkInput.style.borderColor = "black";
                clearInterval(this.watchInterval);
                localStorageManager.addRecord(nameOfWorkInput.value, this.seconds);
                localStorageManager.getRecords(this.recordsSummaryHtml);
                this.seconds = 0;
                this.stopWatchHtml.innerHTML = this.getWatch();
                nameOfWorkInput.value = "";
                this.isStarted = false;
            }
        }
    }
    const recordsSummaryHtml = document.querySelector(".recordsSummaryHtml");
    const nameOfWorkInput = document.querySelector(".nameOfWorkInput");


    const localStorageManager = new LocalStorageManager();
    localStorageManager.getRecords(recordsSummaryHtml);

    const stopButton = document.querySelector(".stopButton");
    const startButton = document.querySelector(".startButton");

    const stopwatchArea = document.querySelector(".stopwatch");
    const stopwatch = new Stopwatch(0, stopwatchArea, localStorageManager, recordsSummaryHtml);

    stopwatchArea.innerHTML=stopwatch.getWatch();

    const audioPlayer = document.querySelector(".audioPlayer");

    //button animation
    stopButton.addEventListener("click", () => {
        stopwatch.endWatch();
        startButton.classList.remove("startButtonAnimation");
    });
    startButton.addEventListener("click", () => {
        stopwatch.startWatch();
        startButton.classList.add("startButtonAnimation");
        audioPlayer.play();
    });

    //input validation
    nameOfWorkInput.addEventListener("blur", () => {
        if(nameOfWorkInput.value == "" || nameOfWorkInput.value.length > 40) {
            nameOfWorkInput.style.borderColor = "red";
        } else {
            nameOfWorkInput.style.borderColor = "black";
        }
    });

    /* audio */
    const responsiveStartAudioButton = document.querySelector(".responsiveStartAudio");
    responsiveStartAudioButton.addEventListener("click", ()=>{
        audioPlayer.play();
    });

    const responsiveStopAudioButton = document.querySelector(".responsiveStopAudio");
    responsiveStopAudioButton.addEventListener("click", ()=>{
        audioPlayer.pause();
    });

    const clearAllRecordsButton = document.querySelector(".clearAllRecords");
    clearAllRecordsButton.addEventListener("click", () => {
        localStorage.clear();
        localStorageManager.getRecords(recordsSummaryHtml);
    })