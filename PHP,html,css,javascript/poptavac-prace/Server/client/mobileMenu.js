// ---curtain menu---
function curtainMenu() {
    var links = document.querySelector(".myLinks");
    if (links.style.display === "block") {
      links.style.display = "none";
    } else {
      links.style.display = "block";
    }
  }

var mobileMenu = document.querySelector(".mobileMenu");
mobileMenu.addEventListener("click", curtainMenu);

