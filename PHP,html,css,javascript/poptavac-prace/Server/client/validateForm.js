// ---validate form---
var demandName = document.querySelector('input[name="demandName"]');
var nameError = document.querySelector(".nameError");

var demandPrice = document.querySelector('input[name="demandPrice"]');
var priceError = document.querySelector(".priceError");

var preference = document.getElementsByName("preference");
var preferenceError = document.querySelector(".preferenceError");

var specification = document.querySelector('[name="specification"]');
var specificError = document.querySelector(".specificError");

var email = document.querySelector('input[name="email"]');
var emailError = document.querySelector(".emailError");

var phoneNumber = document.querySelector('input[name="phone"]');
var phoneError = document.querySelector(".phoneError");

function validateForm(e) {
  var isFormOk = validateName() * validatePrice() * validatePreference() * validateSpecific() * validateEmail() * validatePhone();
  if (isFormOk==0) {
    e.preventDefault();
    document.querySelector(".submitError").innerHTML = "formulář se neodeslal";
  } else {
    document.querySelector(".submitError").innerHTML = "";
  }
}

function validateName() {
  if(demandName.value.length < 5 || demandName.value.length > 70) {
    nameError.innerHTML = "příliš krátký (min 5 znaků) nebo příliš dlouhý název";
    return false;
  } else {
    nameError.innerHTML = "";
    return true;
  }
}

function validatePrice() {
  splited = demandPrice.value.split(" ").join("");
  if(isNaN(splited) || splited < 1 || splited > 99999999 || parseInt(splited) != splited || splited.toString()[0]=="0") {
    priceError.innerHTML = "cena musí být celé číslo od 1 do 9 999 999 Kč";
    return false;
  } else {
    priceError.innerHTML = "";
    return true;
  }
}

function validatePreference() {
  var isChecked = false;
  for(var i = 0; i < preference.length; i++) {
    if(preference[i].checked==true) {
      isChecked = true;
    }
  }
  if(isChecked == false) {
    preferenceError.innerHTML = "zadejte prosím Vaši preferenci";
    return false;
  } else {
    preferenceError.innerHTML = "";
    return true;
  }
}

function validateSpecific() {
  if(specification.value.length < 10 || specification.value.length > 3000) {
    specificError.innerHTML = "specifikace Vaší poptávky by měla být přiměřeně dlouhá(10 - 3000 znaků)";
    return false;
  } else {
    specificError.innerHTML = "";
    return true;
  }
}

function validateEmail() {
  // the var emailExpression was copied from the internet - regular expression
  var emailExpression = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
  if (emailExpression.test(email.value) == false) {
    emailError.innerHTML = "byl zadán neplatný email";
    return false;
  } else {
    emailError.innerHTML = "";
    return true;
  }
}

function validatePhone() {  
  if (phoneNumber.value=="") {
    phoneError.innerHTML = "";
    return true;
  } else if(isNaN(phoneNumber.value)==false && phoneNumber.value.toString().length==9 && parseInt(phoneNumber.value) == phoneNumber.value) {
    phoneError.innerHTML = "";
    return true;
  } else {
    phoneError.innerHTML = "nesprávně zadáno telefonní číslo (ujištěte se, že jste zadali telefonní číslo bez mezer)";
    return false;
  }
}

var submit = document.querySelector(".submit");
submit.addEventListener("click", validateForm);

demandName.addEventListener("blur", validateName);
demandPrice.addEventListener("blur", validatePrice);
specification.addEventListener("blur", validateSpecific);
email.addEventListener("blur", validateEmail);
phoneNumber.addEventListener("blur", validatePhone);

