    <!-- menu -->
    <ul class="menu">
        <!-- when clicked on this curtain menu link, myLinks will be shown -->
        <li class="mobileMenu">
            <a href="javascript:void(0);">&#9776;</a>
        </li>

        <li class="logo"><a href="mainPage.php"><img src="client/logo3.png" alt="logo" width="138" height="50"></a></li>
        <li><a href="mainPage.php">Poptávky</a></li>
        <li><a href="createDemand.php">Vytvořit poptávku</a></li>
        <li><a href="skinSelection.php">Vybrat vzhled</a></li>
        
        <!-- if user is signed in, show logout link, otherwise show sign in link -->
        <?php if(isset($_SESSION['username'])) { ?>
        <li class="login"><a href="logout.php">Odhlásit se</a></li>
        <?php } else { ?>
        <li class="login"><a href="login.php">Přihlásit se</a></li>    
        <?php } ?>
        
        <!-- myLinks - showed only when clicked on javascript link while screen width < 710px -->
        <li class="myLinks">
            <a href="mainPage.php">Poptávky</a>
            <a href="createDemand.php">Vytvořit poptávku</a>
            <a href="skinSelection.php">Vybrat vzhled</a>
        </li>
    </ul>