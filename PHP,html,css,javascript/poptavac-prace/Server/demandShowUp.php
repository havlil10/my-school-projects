<?php
    session_start();
    include 'dbhandlers/dbManager.php';

    //try to find the demand by GET info -> null if not found
    if(isset($_GET['id'])) {
        $currDemand = findDemandById($_GET['id'], $dbContent);
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Poptávka</title>
    <?php
    $skin1 = "<link rel=\"stylesheet\" href=\"client/styles1.css\">";
    $skin2 = "<link rel=\"stylesheet\" href=\"client/styles2.css\">";
    $currskin = isset($_COOKIE['skin']) ? $_COOKIE['skin'] : 'skin1';
    $skinToEcho = $currskin == 'skin2' ? $skin2 : $skin1;
    echo $skinToEcho;
    ?>
</head>
<body>
    <!-- menu -->
    <?php include 'client/menu.php'; ?>
    
    <!-- content -->
    <div class="main">
        <div class="inMain">
            <!-- information about the demand got by GET -->
            <div class="nameOfDemand"><?php echo isset($_GET['id']) && $currDemand != null ?
            htmlspecialchars($currDemand['demandName']) : 'Název poptávky nenalezen' ?></div>
            <hr>
            <p>Odhadovaná cena: <?php echo isset($_GET['id']) && $currDemand != null ? htmlspecialchars($currDemand['demandPrice']) : '' ?> Kč</p>
            <p>Poptávající preferuje: <?php
            if (isset($currDemand) && $currDemand != null) {
                if($currDemand['preference'] == 'pricePerformance') {
                    $pref = 'poměr cena/výkon';
                } else if($currDemand['preference'] == 'minPrice') {
                    $pref = 'nízkou cenu';
                } else {
                    $pref = 'vysoký výkon';
                }
            }
            echo isset($pref) ? $pref : '';
            ?></p> 
            <p>Technologie: <?php
                if(isset($_GET['id']) && $currDemand != null) {
                    foreach($currDemand['technologies'] as $tech) {
                        echo $tech."  ";
                    }
                }
            ?></p>
            <hr>
            <h4>Popis poptávky: </h4>
            <p class="textarea"><?php echo isset($_GET['id']) && $currDemand != null ? htmlspecialchars($currDemand['specification']) : '' ?></p>
            <hr>
            <p>Kontaktní email: <?php echo isset($_GET['id']) && $currDemand != null ? htmlspecialchars($currDemand['email']) : '' ?></p>
            <p>Telefonní číslo: <?php echo isset($_GET['id']) && $currDemand != null ? htmlspecialchars($currDemand['phone']) : '' ?></p>
        </div>
    </div>
    <script src="client/mobileMenu.js"></script>
</body>
</html>