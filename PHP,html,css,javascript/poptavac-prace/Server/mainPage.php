<?php
    session_start();
    include 'dbhandlers/dbManager.php';

    if(isset($_POST['deleteDemand'])) {
        deleteDemand($_POST['deleteDemand'], $dbContent);
        header("Location: mainPage.php");
    }

    if (isset($_GET['sort'])) {
        if($_GET['sort']=='price_asc') {
            $dbContent = sortByPrice('ascendingly', $dbContent);
            $oppositeSortPrice = 'price_desc';
        } else if($_GET['sort']=='price_desc') {
            $dbContent = sortByPrice('descendingly', $dbContent);
            $oppositeSortPrice = 'price_asc';
        } else if($_GET['sort']=='updt_asc') {
            $dbContent = sortByDate('ascendingly', $dbContent);
            $oppositeSortDate = 'updt_desc';
        } else if($_GET['sort']=='updt_desc') {
            $dbContent = sortByDate('descendingly', $dbContent);
            $oppositeSortDate = 'updt_asc';
        }
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Poptávky</title>
    <?php
    $skin1 = "<link rel=\"stylesheet\" href=\"client/styles1.css\">";
    $skin2 = "<link rel=\"stylesheet\" href=\"client/styles2.css\">";
    $currskin = isset($_COOKIE['skin']) ? $_COOKIE['skin'] : 'skin1';
    $skinToEcho = $currskin == 'skin2' ? $skin2 : $skin1;
    echo $skinToEcho;
    ?>
</head>
<body>

    <?php echo isset($dbError) ? $dbError : '' ?>
    
    <!-- menu -->
    <?php include 'client/menu.php'; ?>
    
    <!-- content -->
    <div class="main">
        <div class="inMain">
            <table>
                <thead>
                <tr>
                    <th>Název poptávky</th>
                    <th><a href=<?php echo isset($oppositeSortPrice) ? '"?sort='.$oppositeSortPrice.'"' : '"?sort=price_desc"' ?>>Cena</a></th>
                    <th><a href=<?php echo isset($oppositeSortDate) ? '"?sort='.$oppositeSortDate.'"' : '"?sort=updt_desc"' ?>>Aktualizováno</a></th>
                    <th class="hidden"></th>
                    <th class="hidden"></th>
                </tr>
                </thead>
                <tbody>
                <?php
                    if (!isset($dbError) && count($dbContent['demands']) > 0) {
                        $limit = 6;
                        $total = count($dbContent['demands']);
                        $pages = ceil($total/$limit);
                        $page = min($pages, filter_input(INPUT_GET, 'page', FILTER_VALIDATE_INT, array(
                            'options' => array(
                                'default'   => 1,
                                'min_range' => 1,
                            ),
                        )));
                        $offset = ($page - 1) * $limit;
                        
                        $demands = getDemandsByPaging($offset, $limit, $dbContent);
                        
                        foreach ($demands as $demand) {
                            $demandId = $demand['id'];
                            echo "<tr>";
                            echo "<td><a href=\"demandShowUp.php?id=$demandId\">".htmlspecialchars($demand['demandName'])."</a></td>";
                            echo "<td>".htmlspecialchars($demand['demandPrice'])." Kč"."</td>";
                            echo "<td>".getDateDifference($demand['date'])."</td>";
                            //only admin or creator of demand can see edit and delete button
                            if(isset($_SESSION['username'])) {
                                $user = findUserByUsername($_SESSION['username'], $dbContent['users']);
                                if($_SESSION['username']=='admin'|| in_array($demandId, $user['createdDemandIds'])) {
                                    echo "<td class=\"hidden\"><form method=\"post\" action=\"createDemand.php\"><button name=
                                    \"editDemand\" value=\"$demandId\">Uprav</button></form></td>";
                                    echo "<td class=\"hidden\"><form method=\"post\"><button name=
                                    \"deleteDemand\" value=\"$demandId\">Smaž</button></form></td>";
                                } else {
                                    echo "<td class=\"hidden\"></td>";
                                    echo "<td class=\"hidden\"></td>";
                                }
                            } else {
                                echo "<td class=\"hidden\"></td>";
                                echo "<td class=\"hidden\"></td>";
                            }

                            echo "</tr>";
                        }
                    }   
                    
                ?>
                </tbody>
            </table>
            <?php
                if(isset($pages)) {
                    $links = "";
                    for ($i=0; $i < $pages; $i++) {
                        if ($i == $page-1) {
                            $links .= " ".($i+1)." ";
                        } else {
                            $sortVal = isset($_GET['sort']) ? 'sort='.$_GET['sort'].'&' : '';
                            $links .= " <a href=\"?".$sortVal."page=".($i+1)."\">".($i+1)."</a> ";
                        }
                    }
                    
                    echo "<div class=\"paginationBar\">$links</div>";
                }
            ?>

        </div>
    </div>
    <script src="client/mobileMenu.js"></script>
</body>
</html>