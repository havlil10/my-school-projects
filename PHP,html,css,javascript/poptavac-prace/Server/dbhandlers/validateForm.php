<?php
    function validateName($demandName) {
        if(strlen($demandName)<5 || strlen($demandName)>70) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function validatePrice($demandPrice) {
        $demandPrice = str_replace(' ', '', $demandPrice);
        if(is_numeric($demandPrice) && $demandPrice >= 1 && $demandPrice <= 9999999
         && strpos(strval($demandPrice), ".")==FALSE && $demandPrice[0]!=0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function validateSpecific($specification) {
        if(strlen($specification)<10 || strlen($specification)>3000) {
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function validateEmail($email) {
        if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function validatePhone($phoneNumber) {
        if($phoneNumber == "") {
            return TRUE;
        } else if(is_numeric($phoneNumber) && strlen($phoneNumber) == 9 && strpos(strval($phoneNumber), ".")==FALSE) {
            return TRUE;
        } else {
            return FALSE;
        }
    }
?>