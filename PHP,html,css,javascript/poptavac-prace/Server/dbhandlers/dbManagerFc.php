<?php

    /**
     * adds demand from post to database
     * represented by $dbContent and
     * rewrites the database
     *
     * @param  array $postData
     * @param  array $dbContent
     * @return void
     */
    function storeDemand($postData, $dbContent) {
        $technologies = array();
        if(isset($postData['tech'])) {
            foreach($postData['tech'] as $tech) {
                array_push($technologies, $tech);
            }
        }
        $newDemand = array(
            'id' => uniqid(),
            'demandName' => $postData['demandName'],
            'demandPrice' => str_replace(' ', '', $postData['demandPrice']),
            'preference' => $postData['preference'],
            'technologies' => $technologies,
            'specification' => $postData['specification'],
            'email' => $postData['email'],
            'phone' => $postData['phone'],
            'date' => date("y-m-d")
        );
        //push demand to the top of the stack
        array_unshift($dbContent['demands'], $newDemand);
        if($_SESSION['username']!='admin') {
            $dbContent = assignDemand($newDemand['id'], $_SESSION['username'], $dbContent);
        }
    
        $encoded = json_encode($dbContent);
        file_put_contents(DBFILE, $encoded);
    }
    
    /**
     * finds user by $username and adds $id
     * to array - createdDemandIds
     * 
     * if the user is admin, do nothing
     *
     * @param  string $id
     * @param  string $username
     * @param  array $dbContent
     * @return array $dbContent
     */
    function assignDemand($id, $username, $dbContent) {
        if($username=='admin') {
            return $dbContent;
        }
        foreach($dbContent['users'] as $key => $user) {
            if($user['username']==$username) {
                array_push($dbContent['users'][$key]['createdDemandIds'], $id);
                return $dbContent;
            }
        }
        return $dbContent;
    }
    
    /**
     * finds demand by $id and
     * unsets the demand from database
     * 
     * then calls function unassignDemand if admin is not
     * the actual user and rewrites the database
     *
     * @param  mixed $id
     * @param  mixed $dbContent
     * @return void
     */
    function deleteDemand($id, $dbContent) {
        foreach($dbContent['demands'] as $key => $demand) {
            if($demand['id'] == $id) {
                unset($dbContent['demands'][$key]);
                break;
            }
        }
        if($_SESSION['username']!='admin') {
            $dbContent = unassignDemand($id, $_SESSION['username'], $dbContent);
        }
        $dbContent['demands'] = array_values($dbContent['demands']);
        $encoded = json_encode($dbContent);
        file_put_contents(DBFILE, $encoded);
    }
    
    /**
     * finds user by username and
     * unsets $id from createdDemandIds
     *
     * @param  string $id
     * @param  string $username
     * @param  array $dbContent
     * @return array $dbContent
     */
    function unassignDemand($id, $username, $dbContent) {
        foreach($dbContent['users'] as $key => $user) {
            if($user['username']==$username) {
                foreach($dbContent['users'][$key]['createdDemandIds'] as $key2 => $demandId) {
                    if($demandId==$id) {
                        unset($dbContent['users'][$key]['createdDemandIds'][$key2]);
                        return $dbContent;
                    }
                }
            }
        }
        return $dbContent;
    }
    
    /**
     * deletes demand and adds new demand
     * with info from $postData to database
     *
     * @param  string $id
     * @param  array $postData
     * @param  array $dbContent
     * @return void
     */
    function replaceDemand($id, $postData, $dbContent) {
        foreach($dbContent['demands'] as $key => $demand) {
            if($demand['id'] == $id) {
                unset($dbContent['demands'][$key]);
                break;
            }
        }
        $technologies = array();
        if(isset($postData['tech'])) {
            foreach($postData['tech'] as $tech) {
                array_push($technologies, $tech);
            }
        }
        $newDemand = array(
            'id' => $id,
            'demandName' => $postData['demandName'],
            'demandPrice' => str_replace(' ', '', $postData['demandPrice']),
            'preference' => $postData['preference'],
            'technologies' => $technologies,
            'specification' => $postData['specification'],
            'email' => $postData['email'],
            'phone' => $postData['phone'],
            'date' => date("y-m-d")
        );
        //push to the top of the stack
        array_unshift($dbContent['demands'], $newDemand);
        
        $encoded = json_encode($dbContent);
        file_put_contents(DBFILE, $encoded);
    }
    
    /**
     * finds demand with $id
     *
     * @param  string $id
     * @param  array $dbContent
     * @return null|array
     */
    function findDemandById($id, $dbContent) {
        foreach($dbContent['demands'] as $demand) {
            if($demand['id'] == $id) {
                return $demand;
            }
        }
        return null;
    }
        
    /**
     * finds user with $username
     *
     * @param  string $username
     * @param  array $users
     * @return FALSE|array
     */
    function findUserByUsername($username, $users) {
        foreach($users as $user) {
            if($user['username'] == $username) {
                return $user;
            }
        }
        return FALSE;
    }
    
    /**
     * registers user by adding
     * $username and $password to database
     *
     * @param  string $username
     * @param  string $password
     * @param  array $dbContent
     * @return void
     */
    function storeUser($username, $password, $dbContent) {
        $newUser = array(
            'username' => $username,
            'password' => password_hash($password, PASSWORD_DEFAULT),
            'createdDemandIds' => array()
        );
        array_push($dbContent['users'], $newUser);
        $encoded = json_encode($dbContent);
        file_put_contents(DBFILE, $encoded);
    }
    
    /**
     * gets demands that should be generated
     * on specific page (pagination)
     *
     * @param  int $offset
     * @param  int $limit
     * @param  array $dbContent
     * @return array
     */
    function getDemandsByPaging($offset, $limit, $dbContent) {
        $chosen = array();
        $last = min(($offset+$limit), count($dbContent['demands']));
        for($i = $offset; $i < $last; $i++) {
            array_push($chosen, $dbContent['demands'][$i]);
        }
        return $chosen;
    }
    
    /**
     * counts days diffence of $demandDate and
     * actual date and returns specific text
     *
     * @param  string $demandDate
     * @return string
     */
    function getDateDifference($demandDate) {
        $dateDifference = (strtotime(date('y-m-d'))-strtotime($demandDate))/(3600*24);
        if($dateDifference<1) {
            $updated = 'právě dnes';
        } else if($dateDifference<2) {
            $updated = 'právě včera';
        } else {
            $updated = 'před '.$dateDifference.' dny';
        }
        return $updated;
    }
    
    /**
     * returns database with demands sorted by price
     * - either ascendingly or descendingly
     *
     * @param  string $order
     * @param  array $dbContent
     * @return array
     */
    function sortByPrice($order, $dbContent) {
        if($order == 'ascendingly') {
            $demandPrice = array_column($dbContent['demands'], 'demandPrice');
            array_multisort($demandPrice, SORT_ASC, $dbContent['demands']);
        } else {
            $demandPrice = array_column($dbContent['demands'], 'demandPrice');
            array_multisort($demandPrice, SORT_DESC, $dbContent['demands']);
        }
        return $dbContent;
    }
    
    /**
     * returns database with demands sorted by date
     *
     * @param  string $order
     * @param  array $dbContent
     * @return array
     */
    function sortByDate($order, $dbContent) {
        //dbContent is already sorted by date
        if($order == 'descendingly') {
            $dbContent['demands'] = array_reverse($dbContent['demands']);
        }
        return $dbContent;
    }

?>