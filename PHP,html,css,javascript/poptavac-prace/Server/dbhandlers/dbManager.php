<?php
    include 'dbManagerFc.php';
    define('DBFILE', 'dbhandlers/db.json');

    try {
        if(!file_exists(DBFILE) || !is_readable(DBFILE)) {
            throw new Exception();
        }
        $dbContent = file_get_contents(DBFILE);
        if($dbContent==null) {
            $dbData = array(
                'demands'=>array(),
                'users'=>array()
            );
            if(!is_writable(DBFILE)) {
                throw new Exception();
            }
            file_put_contents(DBFILE, json_encode($dbData));
            $dbContent = file_get_contents(DBFILE);
        }
    
        $dbContent = json_decode($dbContent, JSON_OBJECT_AS_ARRAY);
        
    } catch (Exception $exception) {
        $dbError = 'Omlouváme se, z důvodu problémů se spojením s databází funguje momentálně služba v omezeném režimu';
    }

    
    

?>