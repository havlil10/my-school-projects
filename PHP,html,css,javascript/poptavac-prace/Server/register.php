<?php
    session_start();
    include 'dbhandlers/dbManager.php';
    
    //validate length and whether username is not used -> and register if it is ok
    if(isset($_POST['register']) && is_writable(DBFILE)) {
        $username = $_POST['username'];
        $password = $_POST['password'];
        if(strlen($username) >= 4 && strlen($password) >= 4) {
            if(findUserByUsername($username, $dbContent['users'])==FALSE) {
                storeUser($username, $password, $dbContent);
                $error = '';
            } else {
                $error = 'Vámi zadané jméno již někdo používá, zvolte prosím jiné';
            }
        } else {
            $error = 'Jméno i heslo musí obsahovat alespoň 4 znaky';
        }
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registrace</title>
    <?php
    $skin1 = "<link rel=\"stylesheet\" href=\"client/styles1.css\">";
    $skin2 = "<link rel=\"stylesheet\" href=\"client/styles2.css\">";
    $currskin = isset($_COOKIE['skin']) ? $_COOKIE['skin'] : 'skin1';
    $skinToEcho = $currskin == 'skin2' ? $skin2 : $skin1;
    echo $skinToEcho;
    ?>
</head>
<body>

    <?php echo is_writable(DBFILE) ? '' : 'Omlouváme se, z důvodu problému s databází se momentálně nelze zaregistrovat' ?>
    <!-- menu -->
    <?php include 'client/menu.php'; ?>
    
    <!-- content -->
    <div class="main">
        <div class="inMain">
        <?php echo isset($error) && $error=="" ? "<h4 class=\"green\">Vaše registrace proběhla v pořádku, nyní se můžete přihlásit</h4>" : "" ?>
            <h4>Registrace</h4>
            <!-- register form -->
            <form method="post">
                <label>Jméno: <input type="text" name="username"
                <?php echo isset($error) && $error!='' ? "value='".htmlspecialchars($_POST['username'])."'" : "" ?>></label>
                <label>Heslo: &nbsp; <input type="password" name="password"></label>
                <span class="loginFailure"><?php echo isset($error) ? $error : "" ?></span>
                <input type="submit" name="register" value="Zaregistrovat se" class="submit">
            </form>
            <p>Již u nás máte účet? <a href="login.php">Přihlaste se</a></p>
        </div>
    </div>
    <script src="client/mobileMenu.js"></script>
</body>
</html>