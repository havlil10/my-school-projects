<?php
    session_start();
    include 'dbhandlers/validateForm.php';
    include 'dbhandlers/dbManager.php';
    
    //user has to be signed in to be able to create demand
    if(!isset($_SESSION['username'])) {
        header('Location: createDemError.php');
    }

    //validate submited data and: replace existing demand if it is edited or store submited demand
    if(isset($_POST['submitForm'])) {
        $valDemandName = validateName($_POST['demandName']);
        $valDemandPrice = validatePrice($_POST['demandPrice']);
        $valPreference = isset($_POST['preference']);
        $valSpecific = validateSpecific($_POST['specification']);
        $valEmail = validateEmail($_POST['email']);
        $valPhoneNumber = validatePhone($_POST['phone']);
        $isFormOk = $valDemandName * $valDemandPrice * $valPreference * $valSpecific * $valEmail * $valPhoneNumber;
        if($isFormOk == 1 && is_writable(DBFILE)) {
            if(isset($_POST['editAccess'])) {
                replaceDemand($_POST['editAccess'], $_POST, $dbContent);
            } else {
                storeDemand($_POST, $dbContent);
            }
            header('Location: mainPage.php');
        }   
    }

    if(isset($_POST['editDemand'])) {
        $edited = findDemandById($_POST['editDemand'], $dbContent);
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Vytvořit poptávku</title>
    <?php
    $skin1 = "<link rel=\"stylesheet\" href=\"client/styles1.css\">";
    $skin2 = "<link rel=\"stylesheet\" href=\"client/styles2.css\">";
    $currskin = isset($_COOKIE['skin']) ? $_COOKIE['skin'] : 'skin1';
    $skinToEcho = $currskin == 'skin2' ? $skin2 : $skin1;
    echo $skinToEcho;
    ?>
</head>
<body>

    <?php echo is_writable(DBFILE) ? '' : 'Omlouváme se, momentálně není možné vytvořit ani upravit poptávku z důvodu
    problému spojení s databází. Na vyřešení problému úpěnlivě pracujeme' ?>

    <!-- menu -->
    <?php include 'client/menu.php'; ?>
    
    <!-- content -->
    <div class="main">
        <div class="inMain">
            <h2>Poptávka</h2>
            <form method="post" action="createDemand.php">
                
                <!-- name of demand -->
                <label class="displayBlock"><span class="required">*</span>Název poptávky (minimálně 5 znaků): 
                <input type="text" name="demandName" required size="60" 
                <?php echo isset($_POST['submitForm']) && $isFormOk == 0 ? 'value = "'.htmlspecialchars($_POST['demandName']).'"' : '';
                echo isset($_POST['editDemand']) ? 'value = "'.htmlspecialchars($edited['demandName']).'"' : '' ?>></label> 
                <span class="error nameError">
                <?php echo isset($_POST['submitForm']) && $valDemandName == FALSE ? 'příliš krátký (min 5 znaků) nebo příliš dlouhý
                název' : '' ?></span>
                
                <!-- price of demand -->
                <label class="displayBlock"><span class="required">*</span>Odhadovaná cena (1 - 9 999 999 Kč): 
                <input type="text" name="demandPrice" required size="40"
                <?php echo isset($_POST['submitForm']) && $isFormOk == 0 ? 'value = "'.htmlspecialchars($_POST['demandPrice']).'"' : '';
                echo isset($_POST['editDemand']) ? 'value = "'.htmlspecialchars($edited['demandPrice']).'"' : '' ?>> Kč</label>
                <span class="error priceError">
                <?php echo isset($_POST['submitForm']) && $valDemandPrice == FALSE ? 'cena musí být
                 celé číslo od 1 do 9 999 999 Kč' : '' ?></span>
                
                <!-- preferences -->
                <h4><span class="required">*</span>Preferuji: </h4>
                <label class="czechDiacritics"><input type="radio" name="preference" required value="pricePerformance"
                <?php echo isset($_POST['preference']) && $_POST['preference'] == 'pricePerformance' && $isFormOk == 0 ? 'checked' : '';
                echo isset($_POST['editDemand']) && $edited['preference'] == 'pricePerformance' ? 'checked' : '' ?>> Poměr cena/výkon</label>
                
                <label class="czechDiacritics"><input type="radio" name="preference" value="minPrice"
                <?php echo isset($_POST['preference']) && $_POST['preference'] == 'minPrice' && $isFormOk == 0 ? 'checked' : '';
                echo isset($_POST['editDemand']) && $edited['preference'] == 'minPrice' ? 'checked' : '' ?>> Nízkou cenu</label>
                
                <label class="czechDiacritics"><input type="radio" name="preference" value="maxPerformance"
                <?php echo isset($_POST['preference']) && $_POST['preference'] == 'maxPerformance' && $isFormOk == 0 ? 'checked' : '';
                echo isset($_POST['editDemand']) && $edited['preference'] == 'maxPerformance' ? 'checked' : '' ?>> Vysoký výkon</label>
                
                <span class="error preferenceError">
                <?php echo isset($_POST['submitForm']) && isset($_POST['preference']) == FALSE ? 'zadejte prosím Vaši preferenci' : '' ?></span>
                
                <!-- technologies checkbox -->
                <h4>Technologie, které se budou využívat</h4>
                <label class="chbox"><input type="checkbox" name="tech[]" value="HTML/CSS"
                <?php echo isset($_POST['tech']) && in_array('HTML/CSS', $_POST['tech']) && $isFormOk == 0 ? 'checked' : '';
                echo isset($edited['technologies']) && in_array('HTML/CSS', $edited['technologies']) ? 'checked' : '' ?>> HTML/CSS</label>
                
                <label class="chbox"><input type="checkbox" name="tech[]" value="Javascript"
                <?php echo isset($_POST['tech']) && in_array('Javascript', $_POST['tech']) && $isFormOk == 0 ? 'checked' : '';
                echo isset($edited['technologies']) && in_array('Javascript', $edited['technologies']) ? 'checked' : '' ?>> JavaScript</label>
                
                <label class="chbox"><input type="checkbox" name="tech[]" value="PHP"
                <?php echo isset($_POST['tech']) && in_array('PHP', $_POST['tech']) && $isFormOk == 0 ? 'checked' : '';
                echo isset($edited['technologies']) && in_array('PHP', $edited['technologies']) ? 'checked' : '' ?>> PHP</label>
                
                <label class="chbox"><input type="checkbox" name="tech[]" value="Wordpress"
                <?php echo isset($_POST['tech']) && in_array('Wordpress', $_POST['tech']) && $isFormOk == 0 ? 'checked' : '';
                echo isset($edited['technologies']) && in_array('Wordpress', $edited['technologies']) ? 'checked' : '' ?>> Wordpress</label>
                
                <label class="chbox"><input type="checkbox" name="tech[]" value="Prestashop"
                <?php echo isset($_POST['tech']) && in_array('Prestashop', $_POST['tech']) && $isFormOk == 0 ? 'checked' : '';
                echo isset($edited['technologies']) && in_array('Prestashop', $edited['technologies']) ? 'checked' : '' ?>> Prestashop</label>
                
                <label class="chbox"><input type="checkbox" name="tech[]" value="JQuery"
                <?php echo isset($_POST['tech']) && in_array('JQuery', $_POST['tech']) && $isFormOk == 0 ? 'checked' : '';
                echo isset($edited['technologies']) && in_array('JQuery', $edited['technologies']) ? 'checked' : '' ?>> JQuery</label>
                
                <label class="chbox"><input type="checkbox" name="tech[]" value="Python"
                <?php echo isset($_POST['tech']) && in_array('Python', $_POST['tech']) && $isFormOk == 0 ? 'checked' : '';
                echo isset($edited['technologies']) && in_array('Python', $edited['technologies']) ? 'checked' : '' ?>> Python</label>
                
                <label class="chbox"><input type="checkbox" name="tech[]" value="Java"
                <?php echo isset($_POST['tech']) && in_array('Java', $_POST['tech']) && $isFormOk == 0 ? 'checked' : '';
                echo isset($edited['technologies']) && in_array('Java', $edited['technologies']) ? 'checked' : '' ?>> Java</label><br>
                
                <label class="chbox"><input type="checkbox" name="tech[]" value="Nette"
                <?php echo isset($_POST['tech']) && in_array('Nette', $_POST['tech']) && $isFormOk == 0 ? 'checked' : '';
                echo isset($edited['technologies']) && in_array('Nette', $edited['technologies']) ? 'checked' : '' ?>> Nette</label>
                
                <label class="chbox"><input type="checkbox" name="tech[]" value="Android"
                <?php echo isset($_POST['tech']) && in_array('Android', $_POST['tech']) && $isFormOk == 0 ? 'checked' : '';
                echo isset($edited['technologies']) && in_array('Android', $edited['technologies']) ? 'checked' : '' ?>> Android</label>
                
                <label class="chbox"><input type="checkbox" name="tech[]" value="iOS"
                <?php echo isset($_POST['tech']) && in_array('iOS', $_POST['tech']) && $isFormOk == 0 ? 'checked' : '';
                echo isset($edited['technologies']) && in_array('iOS', $edited['technologies']) ? 'checked' : '' ?>> iOS</label>
                
                <label class="chbox"><input type="checkbox" name="tech[]" value="Windows"
                <?php echo isset($_POST['tech']) && in_array('Windows', $_POST['tech']) && $isFormOk == 0 ? 'checked' : '';
                echo isset($edited['technologies']) && in_array('Windows', $edited['technologies']) ? 'checked' : '' ?>> Windows</label>
                
                <!-- demand specification -->
                <label for="specify" class="specify"><span class="required">*</span>Specifikace poptávky</label>
                <textarea name="specification" id="specify" rows="30" cols="100" required><?php 
                echo isset($_POST['submitForm']) && $isFormOk == 0 ? htmlspecialchars($_POST['specification']) : '';
                echo isset($_POST['editDemand']) ? htmlspecialchars($edited['specification']) : '' ?></textarea>
                <span class="error specificError">
                <?php echo isset($_POST['submitForm']) && $valSpecific == FALSE ? 'specifikace Vaší poptávky by měla být
                přiměřeně dlouhá (10 až 3000 znaků)' : '' ?></span>
                
                <!-- contact email -->
                <label for="email" class="displayBlock"><span class="required">*</span>Kontaktní email: </label>
                <input type="email" id="email" required name="email"
                <?php echo isset($_POST['submitForm']) && $isFormOk == 0 ? 'value = "'.htmlspecialchars($_POST['email']).'"' : '';
                echo isset($_POST['editDemand']) ? 'value = "'.htmlspecialchars($edited['email']).'"' : '' ?>>
                <span class="error emailError">
                <?php echo isset($_POST['submitForm']) && $valEmail == FALSE ? 'byl zadán neplatný email' : '' ?></span>

                <!-- phone number -->
                <label for="phone" class="displayBlock">&nbsp;&nbsp;Telefonní číslo(+420): </label>
                <input type="tel" id="phone" name="phone" pattern="[0-9]{9}"
                <?php echo isset($_POST['submitForm']) && $isFormOk == 0 ? 'value = "'.htmlspecialchars($_POST['phone']).'"' : '';
                echo isset($_POST['editDemand']) ? 'value = "'.htmlspecialchars($edited['phone']).'"' : '' ?>>
                <span class="error phoneError">
                <?php echo isset($_POST['submitForm']) && $valPhoneNumber == FALSE ? 'nesprávně zadáno telefonní
                 číslo (ujištěte se, že jste zadali telefonní číslo bez mezer)' : '' ?></span>
                
                <!-- generate input hidden if demand is edited and keep its id -->
                <?php echo isset($_POST['editDemand']) ? "<input type=\"hidden\" name=\"editAccess\" value=\"".$_POST['editDemand']."\">" : "";
                echo isset($_POST['editAccess']) ? "<input type=\"hidden\" name=\"editAccess\" value=\"".$_POST['editAccess']."\">" : "" ?>
                
                <!-- submit -->
                <span class="error submitError">
                <?php echo isset($_POST['submitForm']) && ($isFormOk == 0 || !is_writable(DBFILE)) ? 'formulář se neodeslal' : '' ?></span>                
                <input type="submit" value="Odeslat" class="submit" name="submitForm">
              </form> 
        </div>
    </div>
    <script src="client/validateForm.js"></script>
    <script src="client/mobileMenu.js"></script>
</body>
</html>