<?php
    session_start();

    //if submited, set cookie with value of user chosen skin
    if(isset($_POST['skinSelect'])) {
        setcookie('skin', $_POST['skin'], time()+3600*24*365, '/~havlil10','.toad.cz');
        header('Location: mainPage.php');
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Výběr vzhledu</title>
    <?php
    //apply this to every page so that every page looks, how the user wants
    $skin1 = "<link rel=\"stylesheet\" href=\"client/styles1.css\">";
    $skin2 = "<link rel=\"stylesheet\" href=\"client/styles2.css\">";
    $currskin = isset($_COOKIE['skin']) ? $_COOKIE['skin'] : 'skin1';
    $skinToEcho = $currskin == 'skin2' ? $skin2 : $skin1;
    echo $skinToEcho;
    ?>
</head>
<body>
    <!-- menu -->
    <?php include 'client/menu.php'; ?>
    
    <!-- content -->
    <div class="main">
        <div class="inMain">
            <h3>Vyberte si vzhled webové aplikace</h3>
            <!-- select skin form -->
            <form method="post">
            <div class="firstSkin">
                <label><input type="radio" name="skin" value="skin1"
                <?php echo $currskin == 'skin1' ? 'checked' : '' ?>> Konzervativní vzhled (výchozí)
                <img src="client/skin1.PNG" alt="First skin" width="400" height="200"></label>
            </div>
            <div class="secondSkin">   
                <label><input type="radio" name="skin" value="skin2"
                <?php echo $currskin == 'skin2' ? 'checked' : '' ?>> Liberální vzhled
                <img src="client/skin2.PNG" alt="First skin" width="400" height="200"></label>
            </div>
            <input type="submit" name="skinSelect" class="submit">
            
            </form>
        </div>
    </div>
    <script src="client/mobileMenu.js"></script>
</body>
</html>