<?php
    session_start();
    include 'dbhandlers/dbManager.php';
    
    //prevent logged user from logging again
    if(isset($_SESSION['username'])) {
        header('Location: mainPage.php');
    }

    //validate login information -> if ok, start session, go to main page
    if(isset($_POST['login']) && !isset($dbError)) {
        $username = $_POST['username'];
        $password = $_POST['password'];
        if($username!='' && $password!='') {
            $user = findUserByUsername($username, $dbContent['users']);
            if($user!=FALSE) {
                if(password_verify($password, $user['password'])) {
                    $_SESSION['username'] = $user['username'];
                    header('Location: mainPage.php');
                } else {
                    $error = 'Zadali jste neplatné jméno nebo heslo';
                }  
            } else {
                $error = 'Zadali jste neplatné jméno nebo heslo';
            }
        } else {
            $error = 'Zadejte prosím jméno a heslo';
        }
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Přihlásit se</title>
    <?php
    $skin1 = "<link rel=\"stylesheet\" href=\"client/styles1.css\">";
    $skin2 = "<link rel=\"stylesheet\" href=\"client/styles2.css\">";
    $currskin = isset($_COOKIE['skin']) ? $_COOKIE['skin'] : 'skin1';
    $skinToEcho = $currskin == 'skin2' ? $skin2 : $skin1;
    echo $skinToEcho;
    ?>
</head>
<body>

    <?php echo !isset($dbError) ? '' : 'Omlouváme se, z důvodu problému s databází se momentálně nelze přihlásit' ?>
    <!-- menu -->
    <?php include 'client/menu.php'; ?>
    
    <!-- content -->
    <div class="main">
        <div class="inMain">
            <h4>Přihlásit se</h4>
            <!-- login form -->
            <form method="post">
                <label>Jméno: <input type="text" name="username"
                <?php echo isset($error) ? "value='".htmlspecialchars($_POST['username'])."'" : "" ?>></label>
                <label>Heslo: &nbsp; <input type="password" name="password"></label>
                <span class="loginFailure"><?php echo isset($error) ? $error : "" ?></span>
                <input type="submit" name="login" value="Přihlásit se" class="submit">
            </form>
            <p>Ještě u nás nemáte účet? <a href="register.php">Zaregistrujte se</a></p>
        </div>
    </div>
    <script src="client/mobileMenu.js"></script>
</body>
</html>