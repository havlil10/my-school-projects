<?php
    session_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Vytvořit poptávku</title>
    <?php
    $skin1 = "<link rel=\"stylesheet\" href=\"client/styles1.css\">";
    $skin2 = "<link rel=\"stylesheet\" href=\"client/styles2.css\">";
    $currskin = isset($_COOKIE['skin']) ? $_COOKIE['skin'] : 'skin1';
    $skinToEcho = $currskin == 'skin2' ? $skin2 : $skin1;
    echo $skinToEcho;
    ?>
</head>
<body>
    <!-- menu -->
    <?php include 'client/menu.php'; ?>
    
    <div class="main">
        <div class="inMain">
            <h4>Abyste mohli vytvořit poptávku, musíte být <a href="login.php">přihlášeni</a></h4>
            <h4>Jestliže u nás ještě nemáte účet, <a href="register.php">zaregistrujte se</a></h4>
        </div>
    </div>
    <script src="client/mobileMenu.js"></script>
</body>
</html>