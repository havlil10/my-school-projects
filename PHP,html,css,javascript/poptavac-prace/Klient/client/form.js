// ---curtain menu---
function curtainMenu() {
    var links = document.querySelector(".myLinks");
    if (links.style.display === "block") {
      links.style.display = "none";
    } else {
      links.style.display = "block";
    }
  }

var mobileMenu = document.querySelector(".mobileMenu");
mobileMenu.addEventListener("click", curtainMenu);

// ---validate form---
function validateForm(e) {
  var isFormOk = validateName() * validatePrice() * validatePreference() * validateSpecify() * validateEmail() * validatePhone();
  if (isFormOk==0) {
    e.preventDefault();
    document.querySelector(".submitError").innerHTML = "formulář se neodeslal";
  } else {
    alert("cus");
  }
}

function validateName() {
  var demandName = document.querySelector('input[name="demandName"]').value;
  var nameError = document.querySelector(".nameError");
  if(demandName.length < 5 || demandName.length > 60) {
    nameError.innerHTML = "neplatný název poptávky";
    return false;
  } else {
    nameError.innerHTML = "";
    return true;
  }
}

function validatePrice() {
  var demandPrice = document.querySelector('input[name="demandPrice"]').value;
  var priceError = document.querySelector(".priceError");
  if(isNaN(demandPrice) || demandPrice < 1 || demandPrice > 99999999 || parseInt(demandPrice) != demandPrice || demandPrice.toString()[0]=="0") {
    priceError.innerHTML = "cena musí být celé číslo od 1 do 99 999 999 Kč";
    return false;
  } else {
    priceError.innerHTML = "";
    return true;
  }
}

function validatePreference() {
  var preference = document.getElementsByName("preference");
  var preferenceError = document.querySelector(".preferenceError");
  var isChecked = false;
  for(var i = 0; i < preference.length; i++) {
    if(preference[i].checked==true) {
      isChecked = true;
    }
  }
  if(isChecked == false) {
    preferenceError.innerHTML = "zadejte prosím Vaši preferenci";
    return false;
  } else {
    preferenceError.innerHTML = "";
    return true;
  }
}

function validateSpecify() {
  var specification = document.querySelector('[name="specification"]').value;
  var specifyError = document.querySelector(".specifyError");
  if(specification.length < 10) {
    specifyError.innerHTML = "zadejte prosím specifikaci Vaší poptávky(alespoň 10 znaků)";
    return false;
  } else {
    specifyError.innerHTML = "";
    return true;
  }
}

function validateEmail() {
  var email = document.querySelector('input[name="email"]').value;
  var emailError = document.querySelector(".emailError");
  // the var emailExpression was copied from the internet - regular expression
  var emailExpression = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
  if (emailExpression.test(email) == false) {
    emailError.innerHTML = "byl zadán neplatný email";
    return false;
  } else {
    emailError.innerHTML = "";
    return true;
  }
}

function validatePhone() {
  var phoneNumber = document.querySelector('input[name="phone"]').value;
  var phoneError = document.querySelector(".phoneError");
  
  if (phoneNumber=="") {
    phoneError.innerHTML = "";
    return true;
  } else if(isNaN(phoneNumber)==false && phoneNumber.toString().length==9 && parseInt(phoneNumber) == phoneNumber) {
    phoneError.innerHTML = "";
    return true;
  } else {
    phoneError.innerHTML = "nesprávně zadáno telefonní číslo (ujištěte se, že jste zadali telefonní číslo bez mezer)";
    return false;
  }
}

var submit = document.querySelector(".submit");
submit.addEventListener("click", validateForm);

/*
function tester() {
  var a = 1234567;
  alert(a.toString().length);
}
tester();
*/