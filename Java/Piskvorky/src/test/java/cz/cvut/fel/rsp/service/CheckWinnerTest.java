package cz.cvut.fel.rsp.service;

import cz.cvut.fel.rsp.model.temporaryData.GameData;
import cz.cvut.fel.rsp.service.utils.CheckWinnerUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CheckWinnerTest {
    private CheckWinnerUtil checkWinnerUtil;

    private GameData gameData;

    @BeforeEach
    public void setUp(){
        this.checkWinnerUtil = new CheckWinnerUtil();
        this.gameData = new GameData();
    }

    @Test
    public void noWinnerEmptyBoard(){
        int winner = checkWinnerUtil.checkForWinner(gameData, 3, 4);
        assertEquals(winner, 0);
    }

    @Test
    public void noWinnerBoardWithSymbols(){
        int[][] board = gameData.getBoard();
        board[0][0] = 1;
        board[1][1] = 1;
        board[0][1] = 2;
        board[0][2] = 2;
        gameData.setBoard(board);

        int winner = checkWinnerUtil.checkForWinner(gameData, 2, 0);
        assertEquals(winner, 0);
    }
    @Test
    public void winnerXBoardInCorner(){
        int[][] board = gameData.getBoard();
        board[0][0] = 1;
        board[1][1] = 1;
        board[2][2] = 1;
        board[3][3] = 1;
        board[4][4] = 1;
        board[0][1] = 2;
        board[0][2] = 2;
        board[0][3] = 2;
        board[5][5] = 2;
        gameData.setBoard(board);

        int winner1 = checkWinnerUtil.checkForWinner(gameData, 4, 4);
        int winner2 = checkWinnerUtil.checkForWinner(gameData, 0, 0);
        assertEquals(winner1, 1);
        assertEquals(winner2, 1);
    }

    @Test
    public void winnerOBoardInCorner(){
        int[][] board = gameData.getBoard();
        board[14][14] = 2;
        board[13][13] = 2;
        board[12][12] = 2;
        board[11][11] = 2;
        board[10][10] = 2;
        board[9][10] = 1;
        board[12][11] = 1;
        board[14][13] = 1;
        board[9][9] = 1;
        board[0][0] = 1;
        gameData.setBoard(board);

        int winner1 = checkWinnerUtil.checkForWinner(gameData, 14, 14);
        int winner2 = checkWinnerUtil.checkForWinner(gameData, 10, 10);
        assertEquals(winner1, 2);
        assertEquals(winner2, 2);
    }

    @Test
    public void winnerOBoardInCornerInvert(){
        int[][] board = gameData.getBoard();
        board[14][0] = 2;
        board[13][1] = 2;
        board[12][2] = 2;
        board[11][3] = 2;
        board[10][4] = 2;
        board[9][10] = 1;
        board[12][11] = 1;
        board[14][13] = 1;
        board[9][9] = 1;
        board[0][0] = 1;
        gameData.setBoard(board);

        int winner1 = checkWinnerUtil.checkForWinner(gameData, 0, 14);
        int winner2 = checkWinnerUtil.checkForWinner(gameData, 4, 10);
        assertEquals(winner1, 2);
        assertEquals(winner2, 2);
    }
    @Test
    public void winnerXBoardInCornerInverted(){
        int[][] board = gameData.getBoard();
        board[7][9] = 1;
        board[8][8] = 1;
        board[9][7] = 1;
        board[10][6] = 1;
        board[11][5] = 1;
        board[0][1] = 2;
        board[0][2] = 2;
        board[0][3] = 2;
        board[5][5] = 2;
        gameData.setBoard(board);

        int winner2 = checkWinnerUtil.checkForWinner(gameData, 5, 11);
        assertEquals(winner2, 1);
    }

    @Test
    public void noWinnerDiagonalCrossBoard(){
        int[][] board = gameData.getBoard();
        board[6][6] = 1;
        board[5][7] = 1;
        board[3][9] = 1;
        board[2][10] = 1;
        board[1][11] = 1;
        board[4][8] = 2;
        board[5][9] = 2;
        board[6][10] = 2;
        board[7][11] = 2;

        gameData.setBoard(board);

        int winner = checkWinnerUtil.checkForWinner(gameData, 9, 3);
        assertEquals(winner, 0);
    }

    @Test
    public void winnerXColumnBoard(){
        int[][] board = gameData.getBoard();
        board[6][6] = 1;
        board[5][6] = 1;
        board[4][6] = 1;
        board[3][6] = 1;
        board[2][6] = 1;
        board[4][8] = 2;
        board[5][9] = 2;
        board[6][10] = 2;
        board[7][11] = 2;

        gameData.setBoard(board);

        int winner = checkWinnerUtil.checkForWinner(gameData, 6, 6);
        assertEquals(winner, 1);
        int winner2 = checkWinnerUtil.checkForWinner(gameData, 6, 2);
        assertEquals(winner2, 1);
    }

    @Test
    public void winnerXRowBoard(){
        int[][] board = gameData.getBoard();
        board[6][6] = 1;
        board[6][7] = 1;
        board[6][8] = 1;
        board[6][9] = 1;
        board[6][10] = 1;
        board[4][8] = 2;
        board[5][9] = 2;
        board[6][11] = 2;
        board[7][11] = 2;

        gameData.setBoard(board);

        int winner = checkWinnerUtil.checkForWinner(gameData, 6, 6);
        assertEquals(winner, 1);
        int winner2 = checkWinnerUtil.checkForWinner(gameData, 10, 6);
        assertEquals(winner2, 1);
    }

    @Test
    public void pavlovaDiagonala(){
        int[][] board = gameData.getBoard();
        board[6][5] = 1;
        board[6][6] = 2;
        board[5][6] = 1;
        board[6][7] = 2;
        board[4][7] = 1;
        board[7][6] = 2;
        board[3][8] = 1;
        board[5][7] = 2;
        board[2][9] = 1;

        gameData.setBoard(board);

        int winner = checkWinnerUtil.checkForWinner(gameData, 5, 6);
        assertEquals(winner, 1);
        int winner2 = checkWinnerUtil.checkForWinner(gameData, 9, 2);
        assertEquals(winner2, 1);
    }
}
