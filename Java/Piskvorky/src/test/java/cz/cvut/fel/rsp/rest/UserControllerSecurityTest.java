package cz.cvut.fel.rsp.rest;

import cz.cvut.fel.rsp.config.SecurityConfig;
import cz.cvut.fel.rsp.environment.Environment;
import cz.cvut.fel.rsp.environment.Generator;
import cz.cvut.fel.rsp.environment.TestConfiguration;
import cz.cvut.fel.rsp.environment.TestSecurityConfig;
import cz.cvut.fel.rsp.model.User;
import cz.cvut.fel.rsp.service.IUserService;
import cz.cvut.fel.rsp.util.UserType;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest
@ContextConfiguration(
        classes = {TestSecurityConfig.class,
                UserControllerSecurityTest.TestConfig.class,
                SecurityConfig.class})
public class UserControllerSecurityTest extends BaseControllerTestRunner {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private IUserService userService;

    private User user;

    @BeforeEach
    public void setUp() {
        this.objectMapper = Environment.getObjectMapper();
        this.user = Generator.generateUser();
    }

    @AfterEach
    public void tearDown() {
        Environment.clearSecurityContext();
        Mockito.reset(userService);
    }

    @Configuration
    @TestConfiguration
    public static class TestConfig {

        @MockBean
        private IUserService userService;

        @MockBean
        private AuthenticationManager authenticationManager;

        @Bean
        public UserController userController() {
            return new UserController(userService, authenticationManager);
        }
    }

    @WithAnonymousUser
    @Test
    public void registerSupportsAnonymousAccess() throws Exception {
        final User toRegister = Generator.generateUser();
        mockMvc.perform(
                        post("/rest/users").content(toJson(toRegister)).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isCreated());
        verify(userService).registerUser(any(User.class));
    }

    @WithAnonymousUser
    @Test
    public void registerAdminThrowsUnauthorizedForAnonymousUser() throws Exception {
        final User toRegister = Generator.generateUser();
        toRegister.setUserType(UserType.ADMIN);

        mockMvc.perform(
                        post("/rest/users").content(toJson(toRegister)).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isUnauthorized());
        verify(userService, never()).registerUser(any());
    }

    @WithMockUser
    @Test
    public void registerAdminThrowsForbiddenForNonAdminUser() throws Exception {
        user.setUserType(UserType.USER);
        Environment.setCurrentUser(user);
        final User toRegister = Generator.generateUser();
        toRegister.setUserType(UserType.ADMIN);

        mockMvc.perform(
                        post("/rest/users").content(toJson(toRegister)).contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isForbidden());
        verify(userService, never()).registerUser(any());
    }

    @WithMockUser(roles = "ADMIN")
    @Test
    public void registerAdminIsAllowedForAdminUser() throws Exception {
        user.setUserType(UserType.ADMIN);
        Environment.setCurrentUser(user);
        final User toRegister = Generator.generateUser();
        toRegister.setUserType(UserType.ADMIN);

        mockMvc.perform(
                        post("/rest/users").content(toJson(toRegister))
                                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isCreated());
        verify(userService).registerUser(any(User.class));
    }

    @WithMockUser
    @Test
    public void logoutRemovesUserAuthenticationForLoggedInUser() throws Exception {
        user.setUserType(UserType.USER);
        Environment.setCurrentUser(user);

        mockMvc.perform(
                get("/rest/users/logout"))
                .andExpect(status().isOk());
    }

    @WithMockUser
    @Test
    public void logoutThrowsUnauthorizedForNonLoggedInUser() throws Exception {
        Environment.clearSecurityContext();
        //There was still a user logged in, so we had to figure that with this 'hack'
        mockMvc.perform(
                get("/rest/users/logout"))
                .andExpect(status().isOk());
        mockMvc.perform(
                get("/rest/users/logout"))
                .andExpect(status().isUnauthorized());
    }


}
