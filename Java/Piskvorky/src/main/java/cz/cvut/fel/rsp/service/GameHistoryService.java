package cz.cvut.fel.rsp.service;

import cz.cvut.fel.rsp.dao.GameRecordDao;
import cz.cvut.fel.rsp.messages.GameRecordDTO;
import cz.cvut.fel.rsp.messages.TurnMessage;
import cz.cvut.fel.rsp.model.GameRecord;
import cz.cvut.fel.rsp.model.Turn;
import cz.cvut.fel.rsp.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class GameHistoryService {
    private GameRecordDao gameRecordDao;

    @Autowired
    public GameHistoryService(GameRecordDao gameRecordDao) {
        this.gameRecordDao = gameRecordDao;
    }

    public List<GameRecordDTO> getPreviousGamesOfUser(User user){
        List<GameRecord> DBRecords = gameRecordDao.findRecordsByUser(user.getUsername());
        List<GameRecordDTO> DTORecords = new ArrayList<>();
        for(GameRecord record: DBRecords){
            GameRecordDTO dto = new GameRecordDTO(record);
            DTORecords.add(dto);
        }
        return DTORecords;
    }

    public List<TurnMessage> getTurnsFromGameRecord(int gameid){
        List<Turn> gameTurns = gameRecordDao.find(gameid).getTurns();
        List<TurnMessage> turnMessages = new ArrayList<>();
        for(Turn t : gameTurns){
            turnMessages.add(convertTurnToMessage(t));
        }
        return turnMessages;
    }

    private TurnMessage convertTurnToMessage(Turn turn){
        TurnMessage turnMessage = new TurnMessage(turn);
        return turnMessage;
    }
}
