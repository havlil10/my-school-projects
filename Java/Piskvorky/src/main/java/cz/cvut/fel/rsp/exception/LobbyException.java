package cz.cvut.fel.rsp.exception;

public class LobbyException extends RuntimeException {

    public LobbyException() {
    }

    public LobbyException(String message) {
        super(message);
    }

    public LobbyException(String message, Throwable cause) {
        super(message, cause);
    }

    public LobbyException(Throwable cause) {
        super(cause);
    }
}
