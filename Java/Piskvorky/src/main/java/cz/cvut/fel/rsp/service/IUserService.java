package cz.cvut.fel.rsp.service;

import cz.cvut.fel.rsp.model.User;

public interface IUserService {
    User registerUser(User user);
    boolean login(String username, String password);
    void logout();
    boolean isLoggedInUserAdmin();
    boolean exists(String username);
    User getLoggedInUser();
}
