package cz.cvut.fel.rsp.dao;

import cz.cvut.fel.rsp.model.TournamentRecord;
import org.springframework.stereotype.Repository;

@Repository
public class TournamentRecordDao extends BaseDao<TournamentRecord>{

    public TournamentRecordDao() {
        super(TournamentRecord.class);
    }
}
