package cz.cvut.fel.rsp.exception;

/**
 * Base for all application-specific exceptions.
 */
public class TicTacToeException extends RuntimeException {

    public TicTacToeException() {
    }

    public TicTacToeException(String message) {
        super(message);
    }

    public TicTacToeException(String message, Throwable cause) {
        super(message, cause);
    }

    public TicTacToeException(Throwable cause) {
        super(cause);
    }
}
