package cz.cvut.fel.rsp.model;

public enum Category {
    CROSS_AND_CIRCLE, PROFILE_PICTURE, PLAYING_FIELD
}
