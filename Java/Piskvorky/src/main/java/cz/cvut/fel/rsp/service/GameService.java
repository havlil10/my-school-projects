package cz.cvut.fel.rsp.service;

import cz.cvut.fel.rsp.dao.GameRecordDao;
import cz.cvut.fel.rsp.dao.TurnDao;
import cz.cvut.fel.rsp.dao.UserDao;
import cz.cvut.fel.rsp.exception.InvalidTurnException;
import cz.cvut.fel.rsp.exception.LobbyException;
import cz.cvut.fel.rsp.messages.TurnMessage;
import cz.cvut.fel.rsp.model.GameRecord;
import cz.cvut.fel.rsp.model.Turn;
import cz.cvut.fel.rsp.model.User;
import cz.cvut.fel.rsp.model.temporaryData.*;
import cz.cvut.fel.rsp.service.utils.CheckWinnerUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/*
Použil bych standard, že X je 1 a O je 2.
 */
@Service
public class GameService {

    private final GameRecordDao dao;
    private final TurnDao turnDao;
    private final LobbyService lobbyService;
    private final UserDao userDao;

    @Autowired
    public GameService(GameRecordDao dao, TurnDao turnDao, LobbyService lobbyService, UserDao userDao) {
        this.dao = dao;
        this.turnDao = turnDao;
        this.lobbyService = lobbyService;
        this.userDao = userDao;
    }

    public GameData createNewGame(User player1){
        LobbyData lobbyData = LobbyData.getInstance();

        if(lobbyService.playerHasLobby(player1)){
            throw new LobbyException("Player already has a lobby.");
        }
        GameData game = new GameData(player1);
        lobbyData.insertGame(game);
        return game;
    }

    public void createNewGameMM(User player1, User player2){
        GameData game = new GameData(player1, player2);

        if(lobbyService.playerHasLobby(player1)){
            throw new LobbyException("Player already has a lobby.");
        }

        LobbyData.getInstance().insertGame(game);
    }

    @Transactional
    public TurnMessage turn(TurnMessage turnData, String gameUID, User user) throws InvalidTurnException {
        if(turnData.getCoordX() > 14 || turnData.getCoordX() < 0 || turnData.getCoordY() < 0 || turnData.getCoordY() > 14) {
            throw new InvalidTurnException("Turn coordinates are out of bounds.");
        }
        
        LobbyData lobbies = LobbyData.getInstance();

        GameData game = lobbies.getGame(gameUID);

        if(game == null){
            throw new InvalidTurnException("Invalid game ID in turn");
        }
        if (game.getState() != GameState.PLAYING) {
            throw new InvalidTurnException("The game is not in the playing phase");
        }

        int symbol = resolveSymbol(game, user);
        if(symbol != turnData.getSymbol()){
            throw new InvalidTurnException("Invalid symbol.");
        }

        if(symbol == 1 && game.isxTurn() || symbol == 2 && !game.isxTurn()){
            game.switchXTurn();
        }
        else {
            throw new InvalidTurnException("You are not on the move.");
        }

        if(game == null){
            throw new InvalidTurnException("Game doesn't exist.");
        }

        int field = game.getBoard()[turnData.getCoordY()][turnData.getCoordX()];
        int winner = 0;

        if(field == 0){
            game.getBoard()[turnData.getCoordY()][turnData.getCoordX()] = symbol;
            winner = CheckWinnerUtil.checkForWinner(game, turnData.getCoordX(), turnData.getCoordY());
        }
        else{
            throw new InvalidTurnException("Field [" + turnData.getCoordY() + "] ["+ turnData.getCoordX() +"] is not empty.");
        }

        game.addTurn(new TurnData(gameUID, turnData.getCoordX(), turnData.getCoordY(), symbol));

        if(winner != 0){
            endGame(game, winner);
        }
        if(boardIsFull(game)){
            endGame(game, 0);
        }
        return new TurnMessage(turnData.getCoordX(), turnData.getCoordY(), winner, symbol);
    }

    private int resolveSymbol(GameData game, User user){
        int symbol = 0;
        if(game.getPlayer1().equals(user)){
            if(game.isPlayer1IsX()){
                symbol = 1;
            }
            else{
                symbol = 2;
            }
        }
        else if(game.getPlayer2().equals(user)){
            if(game.isPlayer1IsX()){
                symbol = 2;
            }
            else{
                symbol = 1;
            }
        }
        else{
            throw new InvalidTurnException("Turn request sent by a user who isn't in the game.");
        }
        return symbol;
    }

    public GameData getGame(String gameUID){
        return LobbyData.getInstance().getGame(gameUID);
    }


    public void endGame(GameData game, int winner){
        LobbyData.getInstance().removeGame(game);
        game.setState(GameState.FINISHED);
        createGameRecord(game, winner);

        game.getPlayer1().setHasPlayingRequest(false);
        game.getPlayer2().setHasPlayingRequest(false);
    }

    @Transactional
    public void createGameRecord(GameData game, int winner){
        GameRecord record = new GameRecord();
        record.setDate(new Date());
        User winnerU = null;
        User loserU = null;

        switch (winner) {
            case 0:
                winnerU = game.getPlayer1();
                loserU = game.getPlayer2();
                if (game.isPlayer1IsX()) {
                    record.setWinnerIsX(true);
                } else {
                    record.setWinnerIsX(false);
                }
                record.setDraw(true);
                break;
            case 1:
                if (game.isPlayer1IsX()) {
                    winnerU = game.getPlayer1();
                    loserU = game.getPlayer2();
                    record.setWinnerIsX(true);
                }
                else {
                    loserU = game.getPlayer1();
                    winnerU = game.getPlayer2();
                    record.setWinnerIsX(false);
                }
                break;
            case 2:
                if (!game.isPlayer1IsX()) {
                    loserU = game.getPlayer1();
                    winnerU = game.getPlayer2();
                    record.setWinnerIsX(true);
                }
                else {
                    winnerU = game.getPlayer1();
                    loserU = game.getPlayer2();
                    record.setWinnerIsX(false);
                }
                break;
        }

        if(game.getGameType().equals(GameType.MMR_MATCH) && !record.isDraw()){
            setMMR(winnerU, loserU);
        }

        record.setWinner(winnerU);
        record.setLoser(loserU);
        record.setGameType(game.getGameType());

        for(TurnData turnData : game.getTurns()) {
            Turn turn = makeTurnFromData(turnData, game, record);
            record.addTurn(turn);
        }

        dao.persist(record);
    }

    @Transactional
    void setMMR(User winner, User loser){
        //lze udelat i sloziteji, ale asi nema smysl
        User w = userDao.findByUsername(winner.getUsername());
        User l = userDao.findByUsername(loser.getUsername());
        w.setMMR(winner.getMMR() + 5);
        l.setMMR(loser.getMMR() - 5);
        userDao.update(w);
        userDao.update(l);
    }

    private Turn makeTurnFromData(TurnData turnData, GameData game, GameRecord record) {
        Turn turn = new Turn();

        turn.setSign(turnData.getSymbol());
        turn.setGameRecord(record);
        turn.setX(turnData.getPositionX());
        turn.setY(turnData.getPositionY());

        turnDao.persist(turn);
        return turn;
    }

    private boolean boardIsFull(GameData data){
        int[][] board = data.getBoard();
        for(int[] row : board){
            for (int field : row){
                if(field == 0){
                    return false;
                }
            }
        }
        return true;
    }
}
