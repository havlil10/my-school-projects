package cz.cvut.fel.rsp.messages;

import cz.cvut.fel.rsp.model.Turn;

public class TurnMessage {

    private int coordX;
    private int coordY;
    private int symbol;
    private int winner; // winner -1 bude draw.

    public TurnMessage() {

    }

    public TurnMessage(int coordX, int coordY, int winner, int symbol) {
        this.coordX = coordX;
        this.coordY = coordY;
        this.winner = winner;
        this.symbol = symbol;
    }

    public TurnMessage(Turn turn) {
        this.coordX = turn.getX();
        this.coordY = turn.getY();
        this.winner = 0;
        this.symbol = turn.getSign();
    }

    public int getCoordX() {
        return coordX;
    }

    public void setCoordX(int coordX) {
        this.coordX = coordX;
    }

    public int getCoordY() {
        return coordY;
    }

    public void setCoordY(int coordY) {
        this.coordY = coordY;
    }

    public int getWinner() {
        return winner;
    }

    public void setWinner(int winner) {
        this.winner = winner;
    }

    public int getSymbol() {
        return symbol;
    }

    public void setSymbol(int symbol) {
        this.symbol = symbol;
    }

}
