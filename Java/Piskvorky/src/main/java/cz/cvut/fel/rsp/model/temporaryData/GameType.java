package cz.cvut.fel.rsp.model.temporaryData;

public enum GameType {
    CUSTOM_GAME, MMR_MATCH
}
