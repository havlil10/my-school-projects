package cz.cvut.fel.rsp.model.temporaryData;

import cz.cvut.fel.rsp.model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


public class GameData {
    private String gameUID;
    private User player1;
    private User player2;
    private boolean player1IsX;
    private boolean xTurn;
    private GameState state;
    private int[][] board;
    private User winner;
    private SwapData swapData;
    private List<TurnData> turns;
    private GameType gameType;

    public GameData(User player1) {
        this.player1 = player1;
        this.gameUID = UUID.randomUUID().toString();
        this.state = GameState.NEW;
        this.board = new int[15][15];
        this.turns = new ArrayList<>();
        xTurn = true;
    }

    public GameData(User player1, User player2) {
        this.player1 = player1;
        this.player2 = player2;
        this.gameUID = UUID.randomUUID().toString();
        this.state = GameState.PLAYING;
        this.board = new int[15][15];
        this.turns = new ArrayList<>();
        xTurn = true;
    }

    //only for tests
    public GameData() {
        this.board = new int[15][15];
    }

    public String getGameUID() {
        return gameUID;
    }

    public void setGameUID(String gameUID) {
        this.gameUID = gameUID;
    }

    public User getPlayer1() {
        return player1;
    }

    public void setPlayer1(User player1) {
        this.player1 = player1;
    }

    public User getPlayer2() {
        return player2;
    }

    public void setPlayer2(User player2) {
        this.player2 = player2;
    }

    public GameState getState() {
        return state;
    }

    public void setState(GameState state) {
        this.state = state;
    }

    public int[][] getBoard() {
        return board;
    }

    public void setBoard(int[][] board) {
        this.board = board;
    }

    public User getWinner() {
        return winner;
    }

    public void setWinner(User winner) {
        this.winner = winner;
    }

    public void addTurn(TurnData turn){
        turns.add(turn);
    }

    public List<TurnData> getTurns() {
        return turns;
    }

    public void setTurns(List<TurnData> turns) {
        this.turns = turns;
    }

    public boolean isPlayer1IsX() {
        return player1IsX;
    }

    public boolean isxTurn() {
        return xTurn;
    }

    public void switchXTurn() {
        this.xTurn = !xTurn;
    }

    public void setSwapData(SwapData swapData) { this.swapData = swapData; }

    public SwapData getSwapData() { return swapData; }

    public void setPlayer1IsX(boolean player1IsX) {
        this.player1IsX = player1IsX;
    }

    public GameType getGameType() {
        return gameType;
    }

    public void setGameType(GameType gameType) {
        this.gameType = gameType;
    }
}
