package cz.cvut.fel.rsp.model.temporaryData;

public enum TicToe {

    X(1),
    O(2);

    TicToe(int value) {
        this.value = value;
    }

    private final int value;

    public Integer getValue() {
        return value;
    }
    
}
