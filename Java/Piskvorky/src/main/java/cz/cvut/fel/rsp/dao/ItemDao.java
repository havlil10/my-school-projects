package cz.cvut.fel.rsp.dao;

import cz.cvut.fel.rsp.model.Item;
import org.springframework.stereotype.Repository;

@Repository
public class ItemDao extends BaseDao<Item>{

    public ItemDao() {
        super(Item.class);
    }

}
