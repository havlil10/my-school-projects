package cz.cvut.fel.rsp.exception;

public class PersistenceException extends TicTacToeException {

    public PersistenceException(String message, Throwable cause) {
        super(message, cause);
    }

    public PersistenceException(Throwable cause) {
        super(cause);
    }
}
