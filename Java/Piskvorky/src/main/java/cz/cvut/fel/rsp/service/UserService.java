package cz.cvut.fel.rsp.service;

import cz.cvut.fel.rsp.dao.UserDao;
import cz.cvut.fel.rsp.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.annotation.SessionScope;

@Service
@SessionScope
public class UserService implements IUserService {

    private static final Logger LOG = LoggerFactory.getLogger(UserService.class);

    private final UserDao userDao;
    private User loggedInUser;
    private final PasswordEncoder encoder;

    @Autowired
    public UserService(UserDao userDao, PasswordEncoder passwordEncoder) {
        this.userDao = userDao;
        this.encoder = passwordEncoder;
    }

    /**
     * Registers user. If user is registered, nothing happens.
     * @param u User to be registered
     * @return Registered user, if registering was successful, null otherwise.
     */
    @Transactional
    @Override
    public User registerUser(User u) {
        if (findByUsername(u.getUsername()) != null) {
            LOG.warn("User with username " + u.getUsername() + " already exists.");
            return null;
        }
        if (findByEmail(u.getEmail()) != null) {
            LOG.warn("User with email " + u.getEmail() + " already exists.");
            return null;
        }
        User newUser = new User(u.getEmail(), u.getUsername(), u.getPassword());
        newUser.encodePassword(encoder);
        if (u.getUserType() != null)
            newUser.setUserType(u.getUserType());
        userDao.persist(newUser);
        LOG.info("User with username " + newUser.getUsername() + " registered.");
        return newUser;
    }

    /**
     * Tries to login user.
     * @param username name of user
     * @param password password of user
     * @return True, if login was successful / False otherwise
     */
    @Transactional
    @Override
    public boolean login(String username, String password) {
        User potentialUser = userDao.findByUsername(username);
        if (potentialUser != null && encoder.matches(password, potentialUser.getPassword())) {
            loggedInUser = potentialUser;
            LOG.info("User with username " + username + " logged in.");
            return true;
        }
        LOG.warn("Invalid initials to login with username: " + username);
        return false;
    }


    /**
     * Logouts user.
     */
    @Override
    public void logout() {
        loggedInUser = null;
    }

    @Override
    public boolean isLoggedInUserAdmin() {
        return loggedInUser != null && loggedInUser.isAdmin();
    }

    @Override
    public boolean exists(String username) {
        return userDao.findByUsername(username) != null;
    }


    private boolean userIsLoggedIn(String username) {
        return this.loggedInUser != null && username.equals(this.loggedInUser.getUsername());
    }

    public User findByUsername(String username) {
        return userDao.findByUsername(username);
    }

    public User findByEmail(String email) {
        return userDao.findByEmail(email);
    }

    @Override
    public User getLoggedInUser() {
        return loggedInUser;
    }

    public User find(Integer userId) {
        return userDao.find(userId);
    }
}
