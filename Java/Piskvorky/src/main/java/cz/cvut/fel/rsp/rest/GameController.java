package cz.cvut.fel.rsp.rest;

import cz.cvut.fel.rsp.exception.TicTacToeException;
import cz.cvut.fel.rsp.messages.TurnMessage;
import cz.cvut.fel.rsp.model.User;
import cz.cvut.fel.rsp.model.temporaryData.GameData;
import cz.cvut.fel.rsp.model.temporaryData.SwapData;
import cz.cvut.fel.rsp.security.SecurityConstants;
import cz.cvut.fel.rsp.security.model.AuthenticationToken;
import cz.cvut.fel.rsp.service.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.*;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;


@RestController
//ted urceno pro vyzkouseni v postmanu... muze byt zmeneno
@CrossOrigin(origins = SecurityConstants.ORIGIN_CORS_URL, allowCredentials = "true")
@RequestMapping("/rest/game")
public class GameController {
    private final GameService gameService;
    private final SimpMessagingTemplate simpMessagingTemplate;

    @Autowired
    public GameController(GameService gameService, SimpMessagingTemplate simpMessagingTemplate) {
        this.gameService = gameService;
        this.simpMessagingTemplate = simpMessagingTemplate;
    }

    @MessageMapping("/doTurn/{gameId}")
    @SendTo("/topic/games/{gameId}")
    public TurnMessage doTurn(@Payload TurnMessage turnMessage, Principal principal, @DestinationVariable("gameId") String gameId){
        TurnMessage result;
        AuthenticationToken auth = (AuthenticationToken) principal;
        result = gameService.turn(turnMessage, gameId, auth.getPrincipal().getUser());
        return result;
    }

    @MessageExceptionHandler
    @SendToUser("/queue/errors")
    public String handleInvalidTurnException(TicTacToeException e){
        return "Error: " + e.getMessage();
    }
}
