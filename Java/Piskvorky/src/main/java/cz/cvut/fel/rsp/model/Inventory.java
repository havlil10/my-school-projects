package cz.cvut.fel.rsp.model;

import java.util.ArrayList;
import javax.persistence.*;
import java.util.List;
import java.util.Optional;

@Entity
public class Inventory extends AbstractEntity{
    @Basic(optional = false)
    @Column(nullable = false)
    private int currentBalance;

    @OneToMany
    @JoinColumn(name = "inv_item_id")
    private List<Item> playerItems;

    @OneToOne
    @JoinColumn(name = "inv_owner")
    private User owner;

    public int getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(int currentBalance) {
        this.currentBalance = currentBalance;
    }

    public List<Item> getPlayerItems() {
        return playerItems;
    }

    public void setPlayerItems(List<Item> playerItems) {
        this.playerItems = playerItems;
    }

    public void addItem(Item item) {
        if (playerItems == null) {
            playerItems = new ArrayList<>();
        }
        Optional<Item> friendTest = playerItems.stream()
                .filter(existing -> existing.getId().equals(item.getId()))
                .findAny();
        if (!friendTest.isPresent()) {
            playerItems.add(item);
        }
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }
}
