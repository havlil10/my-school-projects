package cz.cvut.fel.rsp.exception;

/**
 * Signifies that invalid data have been provided to the application.
 */
public class ValidationException extends TicTacToeException {

    public ValidationException(String message) {
        super(message);
    }
}
