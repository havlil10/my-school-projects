package cz.cvut.fel.rsp.model.temporaryData;

public class SwapData {

    private final int[] x;
    private final int[] y;

    public SwapData() {
        x = new int[3];
        y = new int[3];
    }

    public int[] getX() {
        return x;
    }

    public int[] getY() {
        return y;
    }

}