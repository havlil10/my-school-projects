package cz.cvut.fel.rsp.dao;

import cz.cvut.fel.rsp.model.GameRecord;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;

@Repository
public class GameRecordDao extends BaseDao<GameRecord> {

    public GameRecordDao() {
        super(GameRecord.class);
    }
    
    public List<GameRecord> findRecordsByUser(String username) {
        return em.createNamedQuery("GameRecord.findRecordsForUser", GameRecord.class)
                .setParameter("username", username)
                .setMaxResults(10)
                .getResultList();
    }
    
}
