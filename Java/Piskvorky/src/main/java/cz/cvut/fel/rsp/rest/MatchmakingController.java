package cz.cvut.fel.rsp.rest;

import cz.cvut.fel.rsp.model.User;
import cz.cvut.fel.rsp.model.temporaryData.GameData;
import cz.cvut.fel.rsp.model.temporaryData.GameType;
import cz.cvut.fel.rsp.security.model.AuthenticationToken;
import cz.cvut.fel.rsp.service.GameService;
import cz.cvut.fel.rsp.service.LobbyService;
import cz.cvut.fel.rsp.service.MatchmakingService;
import cz.cvut.fel.rsp.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;

import java.security.Principal;
import java.util.UUID;


@RestController
@RequestMapping("/rest/matchmaking")
public class MatchmakingController {
    private final GameService gameService;
    private final LobbyService lobbyService;
    private final MatchmakingService matchmakingService;
    //private ExecutorService executors = Executors.newFixedThreadPool(5);

    private static final Logger LOG = LoggerFactory.getLogger(MatchmakingController.class);

    @Autowired
    public MatchmakingController(GameService gameService, MatchmakingService matchmakingService, LobbyService lobbyService) {
        this.gameService = gameService;
        this.matchmakingService = matchmakingService;
        this.lobbyService = lobbyService;
    }



    //for testing purposes
    @GetMapping("/create")
    public GameData createGame() {
        GameData gameData = gameService.createNewGame(new User());
        return gameData;
    }

    @GetMapping("/findCustomMatch")
    public DeferredResult<GameData> findCustomMatch(Principal principal) {
        AuthenticationToken auth = (AuthenticationToken) principal;
        User user = auth==null ? new User(UUID.randomUUID().toString(), "guest", "guest") : auth.getPrincipal().getUser();

        return matchmakingService.findOpponent(user, GameType.CUSTOM_GAME);
    }

    @GetMapping("/findMMRMatch")
    public DeferredResult<GameData> findMMRMatch(Principal principal) {
        AuthenticationToken auth = (AuthenticationToken) principal;
        User user = auth==null ? new User(UUID.randomUUID().toString(), "guest", "guest") : auth.getPrincipal().getUser();

        DeferredResult<GameData> res = matchmakingService.findOpponent(user, GameType.MMR_MATCH);
        LOG.info(String.valueOf(res));

        return res;
    }

}
