package cz.cvut.fel.rsp.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import java.util.List;
import java.util.Map;

@Entity
public class TournamentRecord  extends AbstractEntity{
    @OneToMany
    @JoinColumn(name = "tournament_game_record_id")
    private List<TournamentGameRecord> tournamentGameRecords;


    @OneToMany
    @JoinColumn(name = "user_id")
    private Map<Integer, User> participantsLadder;
}
