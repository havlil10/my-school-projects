package cz.cvut.fel.rsp.service;

import cz.cvut.fel.rsp.model.User;
import cz.cvut.fel.rsp.model.temporaryData.GameData;
import cz.cvut.fel.rsp.model.temporaryData.GameType;
import cz.cvut.fel.rsp.model.temporaryData.LobbyData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.async.DeferredResult;


import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


@Service
public class MatchmakingService {
    private LobbyService lobbyService;
    private ExecutorService executors = Executors.newFixedThreadPool(250);

    @Autowired
    public MatchmakingService(LobbyService lobbyService) {
        this.lobbyService = lobbyService;
    }


    private void setGameProperties(GameData gameData, GameType gameType) {
        if(new Random().nextInt(2)==0) {
            gameData.setPlayer1IsX(true);
        } else {
            gameData.setPlayer1IsX(false);
        }

        gameData.setGameType(gameType);
    }

    public DeferredResult<GameData> findOpponent(User user, GameType gameType) {
        User opponentUser = lobbyService.findPossibleOpponent(gameType, user);
        LobbyData lobbyData = lobbyService.getLobbyData();
        DeferredResult<GameData> gameDataDeferredResult = new DeferredResult<>();
        if(opponentUser != null) {
            GameData gameData = new GameData(user, opponentUser);
            setGameProperties(gameData, gameType);
            lobbyData.insertGame(gameData);

            gameDataDeferredResult.setResult(gameData);
            opponentUser.setHasPlayingRequest(true);
            return gameDataDeferredResult;
        } else {
            lobbyData.addGameRequest(user, gameType);
            executors.execute(() -> {

                try {
                    int timeToLive = 60;
                    while(timeToLive>0) {
                        Thread.sleep(500);
                        if(user.hasPlayingRequest()) {
                            gameDataDeferredResult.setResult(lobbyData.findGameUserIsIn(user.getEmail()));

                            return;
                        }
                        timeToLive--;
                    }
                    lobbyData.removeRequest(user, gameType);
                } catch(Exception e) {
                    System.err.println("Thread exception or something");
                    System.err.println(Arrays.toString(e.getStackTrace()));
                }
            });

        }

        if(gameDataDeferredResult.getResult()!=null) {
            user.setHasPlayingRequest(false);
        }
        return gameDataDeferredResult;
    }
}
