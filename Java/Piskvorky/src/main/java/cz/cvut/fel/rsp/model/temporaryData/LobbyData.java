package cz.cvut.fel.rsp.model.temporaryData;

import cz.cvut.fel.rsp.model.User;

import java.util.HashMap;
import java.util.Stack;

public class LobbyData {
    private static HashMap<String, GameData> lobbies;
    private static HashMap<String, Integer> tournamentLobbies; //placeholder pro tournament data
    private static LobbyData instance;
    private static Stack<User> usersInCustomQueue = new Stack<>();
    private static Stack<User> usersInMMRMatchQueue = new Stack<>();

    private LobbyData() {
        lobbies = new HashMap<>();
    }

    public static synchronized LobbyData getInstance(){
        if(instance == null){
            instance = new LobbyData();
        }
        return instance;
    }

    public void insertGame(GameData game){
        lobbies.put(game.getGameUID(), game);
    }

    public void removeGame(GameData game){
        lobbies.remove(game.getGameUID());
    }

    public void insertTournament(){

    }

    public void removeTournament(){

    }

    public GameData getGame(String gameUID){
        return lobbies.get(gameUID);
    }

    public GameData getGameByPlayer(User player){
        for(GameData gameData : lobbies.values()){
            if(gameData.getPlayer1() == player || gameData.getPlayer2() == player){
                return gameData;
            }
        }
        return null;
    }

    public Stack<User> getUsersInQueue() {
        return usersInCustomQueue;
    }

    public void addGameRequest(User user, GameType gameType) {
        if(gameType==GameType.CUSTOM_GAME) {
            usersInCustomQueue.add(user);
        } else {
            usersInMMRMatchQueue.add(user);
        }
    }


    public Stack<User> getUsersInMMRMatchQueue() {
        return usersInMMRMatchQueue;
    }



    public GameData findGameUserIsIn(String userEmail) {
        for(GameData game : lobbies.values()) {
            if(game.getPlayer1().getEmail().equals(userEmail) || game.getPlayer2().getEmail().equals(userEmail)) {
                return game;
            }
        }
        return null;
    }

    public void removeRequest(User user, GameType gameType) {
        if(gameType==GameType.CUSTOM_GAME) {
            usersInCustomQueue.pop();
        } else {
            usersInMMRMatchQueue.pop();
        }
    }

}
