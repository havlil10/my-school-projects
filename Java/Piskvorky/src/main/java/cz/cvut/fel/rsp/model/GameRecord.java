package cz.cvut.fel.rsp.model;

import cz.cvut.fel.rsp.model.temporaryData.GameType;

import java.util.ArrayList;
import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@NamedQueries({
        @NamedQuery(name = "GameRecord.findRecordsForUser", query = "SELECT gr FROM GameRecord gr WHERE gr.winner.username = :username OR gr.loser.username = :username ORDER BY gr.date DESC ")
})
public class GameRecord extends AbstractEntity {

    @Column(name = "game_date", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date date;

    @JoinColumn(name = "winner_id")
    @ManyToOne
    private User winner;

    @JoinColumn(name = "loser_id")
    @ManyToOne
    private User loser;

    @OneToMany
    @JoinColumn(name = "turn_id")
    private List<Turn> turns;

    @Column(name = "winner_symbol", nullable = false)
    private boolean winnerIsX;

    @Column(name = "is_draw", nullable = false)
    private boolean draw;

    @Enumerated
    @Column(name = "game_type")
    private GameType gameType;

    public GameType getGameType() {
        return gameType;
    }

    public void setGameType(GameType gameType) {
        this.gameType = gameType;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public User getWinner() {
        return winner;
    }

    public void setWinner(User winner) {
        this.winner = winner;
    }

    public User getLoser() {
        return loser;
    }

    public void setLoser(User loser) {
        this.loser = loser;
    }

    public List<Turn> getTurns() {
        return turns;
    }

    public void setTurns(List<Turn> turns) {
        this.turns = turns;
    }

    public boolean winnerIsX() {
        return winnerIsX;
    }

    public void setWinnerIsX(boolean winnerIsX) {
        this.winnerIsX = winnerIsX;
    }

    public boolean isDraw() {
        return draw;
    }

    public void setDraw(boolean draw) {
        this.draw = draw;
    }

    public void addTurn(Turn turn) {
        if (turns == null) {
            turns = new ArrayList<>();
        }
        turns.add(turn);
    }
}
