package cz.cvut.fel.rsp.messages;

import cz.cvut.fel.rsp.model.GameRecord;
import cz.cvut.fel.rsp.model.User;

import java.util.Date;

public class GameRecordDTO {
    private Date date;
    private String winner;
    private String loser;
    private boolean winnerIsX;
    private boolean draw;
    private int id;

    public GameRecordDTO(GameRecord gameRecord) {
        date = gameRecord.getDate();
        winner = gameRecord.getWinner().getUsername();
        loser = gameRecord.getLoser().getUsername();
        winnerIsX = gameRecord.winnerIsX();
        draw = gameRecord.isDraw();
        id = gameRecord.getId();
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getWinner() {
        return winner;
    }

    public void setWinner(String winner) {
        this.winner = winner;
    }

    public String getLoser() {
        return loser;
    }

    public void setLoser(String loser) {
        this.loser = loser;
    }

    public boolean isWinnerIsX() {
        return winnerIsX;
    }

    public void setWinnerIsX(boolean winnerIsX) {
        this.winnerIsX = winnerIsX;
    }

    public boolean isDraw() {
        return draw;
    }

    public void setDraw(boolean draw) {
        this.draw = draw;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
