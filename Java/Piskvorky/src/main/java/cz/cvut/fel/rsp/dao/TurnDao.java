package cz.cvut.fel.rsp.dao;

import cz.cvut.fel.rsp.model.Turn;
import org.springframework.stereotype.Repository;

@Repository
public class TurnDao extends BaseDao<Turn>{
    public TurnDao() {
        super(Turn.class);
    }
}
