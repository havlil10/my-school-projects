package cz.cvut.fel.rsp.service;

import cz.cvut.fel.rsp.model.User;
import cz.cvut.fel.rsp.model.temporaryData.GameData;
import cz.cvut.fel.rsp.model.temporaryData.GameType;
import cz.cvut.fel.rsp.model.temporaryData.LobbyData;
import org.springframework.stereotype.Service;

@Service
public class LobbyService {
    LobbyData lobbyData = LobbyData.getInstance();

    public boolean playerHasLobby(User player){
        return lobbyData.getGameByPlayer(player) != null;
    }

    public User findPossibleOpponent(GameType gameType, User user) {
        if(gameType==GameType.CUSTOM_GAME) {
            if(lobbyData.getUsersInQueue().isEmpty()) {
                return null;
            } else {
                while(lobbyData.getUsersInQueue().peek().getEmail().equals(user.getEmail())) {
                    lobbyData.getUsersInQueue().pop();
                }

                return lobbyData.getUsersInQueue().pop();
            }
        } else {
            if(lobbyData.getUsersInMMRMatchQueue().isEmpty()) {
                return null;
            } else {
                while(lobbyData.getUsersInMMRMatchQueue().peek().getEmail().equals(user.getEmail())) {
                    lobbyData.getUsersInMMRMatchQueue().pop();
                }

                return lobbyData.getUsersInMMRMatchQueue().pop();
            }
        }
    }


    public LobbyData getLobbyData() {
        return lobbyData;
    }
}
