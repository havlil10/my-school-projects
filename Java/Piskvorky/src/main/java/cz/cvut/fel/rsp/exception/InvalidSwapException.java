package cz.cvut.fel.rsp.exception;

public class InvalidSwapException extends TicTacToeException{
    public InvalidSwapException() {
    }

    public InvalidSwapException(String message) {
        super(message);
    }

    public InvalidSwapException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidSwapException(Throwable cause) {
        super(cause);
    }
}
