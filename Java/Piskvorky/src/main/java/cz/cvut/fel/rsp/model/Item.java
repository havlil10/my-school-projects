package cz.cvut.fel.rsp.model;

import javax.persistence.*;

@Entity
public class Item  extends AbstractEntity{
    @Basic(optional = false)
    @Column(nullable = false)
    private int price;

    @Enumerated(EnumType.ORDINAL)
    private Category category;

    @Column(nullable = false)
    private boolean forSale;

    public Item() {
    }

    public int getPrice() {
        return price;
    }

    public Category getCategory() {
        return category;
    }

    public boolean isForSale() {
        return forSale;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public void setForSale(boolean forSale) {
        this.forSale = forSale;
    }
}
