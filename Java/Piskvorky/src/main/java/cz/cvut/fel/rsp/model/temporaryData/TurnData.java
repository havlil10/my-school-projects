package cz.cvut.fel.rsp.model.temporaryData;

public class TurnData {
    private String gameUID;
    private int positionX;
    private int positionY;
    private int symbol;

    public TurnData() {

    }

    public TurnData(String gameUID, int positionX, int positionY, int symbol) {
        this.gameUID = gameUID;
        this.positionX = positionX;
        this.positionY = positionY;
        this.symbol = symbol;
    }

    public String getGameUID() {
        return gameUID;
    }

    public int getPositionX() {
        return positionX;
    }

    public int getPositionY() {
        return positionY;
    }

    public int getSymbol() {
        return symbol;
    }
}
