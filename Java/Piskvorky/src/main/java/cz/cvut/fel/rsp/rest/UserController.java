package cz.cvut.fel.rsp.rest;

import cz.cvut.fel.rsp.model.User;
import cz.cvut.fel.rsp.rest.util.RestUtils;
import cz.cvut.fel.rsp.security.SecurityConstants;
import cz.cvut.fel.rsp.security.SecurityUtils;
import cz.cvut.fel.rsp.security.model.AuthenticationToken;
import cz.cvut.fel.rsp.security.model.UserDetails;
import cz.cvut.fel.rsp.service.IUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.Principal;

@RestController
@CrossOrigin(origins = SecurityConstants.ORIGIN_CORS_URL, allowCredentials = "true")
@RequestMapping("/rest/users")
public class UserController {

    private static final Logger LOG = LoggerFactory.getLogger(UserController.class);

    private final AuthenticationManager authenticationManager;
    private final IUserService userService;

    @Autowired
    public UserController(IUserService userService, AuthenticationManager authenticationManager) {
        this.userService = userService;
        this.authenticationManager = authenticationManager;
    }

    /**
     * Registers a new user.
     *
     * @param user User data
     */
//    @PreAuthorize("(!#user.isAdmin() && anonymous) || hasRole('ADMIN')")
    @PostMapping(value = "/register", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> register(@RequestBody User user) {
        final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/current");
        if (userService.registerUser(user)!=null) {
            LOG.debug("User {} successfully registered.", user);
            return new ResponseEntity<>(headers, HttpStatus.CREATED);
        }
        else {
            return new ResponseEntity<>(headers,HttpStatus.CONFLICT);
        }
    }

    /**
     * User login using Spring Security.
     *
     * @param user User data
     */
    @PreAuthorize("(!#user.isAdmin() && anonymous)")
    @PostMapping(value = "/login", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> login(@RequestBody User user) {
        boolean loggedIn = userService.login(user.getUsername(), user.getPassword());

        if (loggedIn) {
            UserDetails details = new UserDetails(userService.getLoggedInUser());
            authenticationManager.authenticate(SecurityUtils.setCurrentUser(details));
            ///final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/current");

            final HttpHeaders headers = new HttpHeaders();
            return new ResponseEntity<>(headers, HttpStatus.OK);
        }

        final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/users");
        return new ResponseEntity<>(headers, HttpStatus.CONFLICT);
    }

    /**
     * Get info about logged in user.
     */
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER', 'ROLE_GUEST')")
    @GetMapping(value = "/current", produces = MediaType.APPLICATION_JSON_VALUE)
    public User getCurrent(Principal principal) {
        AuthenticationToken auth = (AuthenticationToken) principal;
        User user = auth.getPrincipal().getUser();
        user.erasePassword();
        return user;
    }

    /**
     * Logouts user.
     */
    @PreAuthorize("isAuthenticated()")
    @GetMapping(value = "/logout")
    @ResponseStatus(HttpStatus.OK)
    public void logout(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null)
            new SecurityContextLogoutHandler().logout(request, response, auth);
    }

}
