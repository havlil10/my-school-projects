package cz.cvut.fel.rsp.service.utils;

import cz.cvut.fel.rsp.model.temporaryData.GameData;

public class CheckWinnerUtil {
    public CheckWinnerUtil() {
    }

    public static int checkForWinner(GameData game, int coordX, int coordY){
        int wonInRow = checkRow(game, coordX, coordY);
        if(wonInRow > 0){
            return wonInRow;
        }
        int wonInColumn = checkColumn(game, coordX, coordY);
        if(wonInColumn > 0){
            return wonInColumn;
        }
        int wonInDiagonalLeftUp = checkDiagonalLeftUp(game, coordX, coordY);
        if(wonInDiagonalLeftUp > 0){
            return wonInDiagonalLeftUp;
        }
        int wonInDiagonalRightUp = checkDiagonalRightUp(game, coordX, coordY);
        if(wonInDiagonalRightUp > 0){
            return wonInDiagonalRightUp;
        }
        return 0;
    }

    private static int checkColumn(GameData game, int coordX, int coordY){
        int[][] board = game.getBoard();

        int start = coordY - 4;
        if(start < 0){
            start = 0;
        }

        int end = coordY + 4;
        if(end > 14){
            end = 14;
        }

        int current = board[start][coordX];
        int currCount = 0;

        for(int i = start; i <= end; i++){
            if(current == board[i][coordX]){
                currCount++;
            }
            else{
                current = board[i][coordX];
                currCount = 1;
            }

            if(currCount == 5){
                return current;
            }
        }
        return 0;
    }

    private static int checkRow(GameData game, int coordX, int coordY){
        int[][] board = game.getBoard();

        int start = coordX - 4;
        if(start < 0){
            start = 0;
        }

        int end = coordX + 4;
        if(end > 14){
            end = 14;
        }

        int current = board[coordY][start];
        int currCount = 0;

        for(int i = start; i <= end; i++){
            if(current == board[coordY][i]){
                currCount++;
            }
            else{
                current = board[coordY][i];
                currCount = 1;
            }

            if(currCount == 5){
                return current;
            }
        }
        return 0;
    }
    private static int checkDiagonal(GameData game, int coordX, int coordY){
        int[][] board = game.getBoard();
        int[] coords = diagonalStartCoord(coordX, coordY);

        int current = board[coords[2]][coords[0]];
        int currCount = 0;

        for (int i = 0; i <= 8; i++){
            if(coords[2] + i > 14 || coords[0] + i > 14){
                break;
            }

            if(current == board[coords[2] + i][coords[0] + i]){
                currCount++;
            }
            else{
                current = board[coords[2] + i][coords[0] + i];
                currCount = 1;
            }

            if(currCount == 5){
                return current;
            }
        }

        current = board[coords[3]][coords[1]];
        currCount = 0;

        for (int i = 0; i <= 8; i++){
            if(coords[3] - i < 0 || coords[1] + i > 14){
                break;
            }

            if(current == board[coords[3] - i][coords[1] + i]){
                currCount++;
            }
            else{
                current = board[coords[3] - i][coords[1] + i];
                currCount = 1;
            }

            if(currCount == 5){
                return current;
            }
        }

        return 0;
    }

    private static int[] diagonalStartCoord(int coordX, int coordY){
        int[] ret = new int[4];
        int startX1 = coordX;
        int startX2 = coordX;
        int startY1 = coordY;
        int startY2 = coordY;
        boolean change1 = false;
        boolean change2 = false;

        for(int i = 0; i < 5; i++){
            if(startX1 - i == 0 || startY1 - i == 0){
                if(!change1){
                    startX1 -= i;
                    startY1 -= i;
                    change1 = true;
                }
            }
            if(startX2 - i == 0 || startY2 + i == 14){
                if(!change2){
                    startX2 -= i;
                    startY2 += i;
                    change2 = true;
                }
            }
        }

        if(!change1){
            startX1 -= 4;
            startY1 -= 4;
        }

        if(!change2){
            startX2 -= 4;
            startY2 += 4;
        }

        ret[0] = startX1;
        ret[1] = startX2;
        ret[2] = startY1;
        ret[3] = startY2;

        return ret;
    }



    private static int checkDiagonalLeftUp(GameData game, int coordX, int coordY){
        int[][] board = game.getBoard();

        int startX = coordX - 4;
        int startY = coordY - 4;
        if(startX < 0){
            startY -= startX;
            startX = 0;
        }

        if(startY < 0){
            startX-= startY;
            startY = 0;
        }

        int endX = coordX + 4;
        if(endX > 14){
            endX = 14;
        }

        int current = board[coordY][startX];
        int currCount = 0;
        int currY = startY;
        for(int i = startX; i <= endX; i++){
            if(current == board[currY][i]){
                currCount++;
                currY++;
            }
            else{
                current = board[currY][i];
                currCount = 1;
                currY++;
            }

            if(currCount == 5){
                return current;
            }
            if (currY>14) {
                return 0;
            }
        }
        return 0;
    }




    private static int checkDiagonalRightUp(GameData game, int coordX, int coordY){
        int[][] board = game.getBoard();

        int startX = coordX - 4;
        int startY = coordY + 4;
        int endX = coordX + 4;
        if(startX < 0){
            startY+=startX;
            startX = 0;
        }

        if(startY > 14){
            startX+=startY-14;
            startY = 14;
        }

        if(endX > 14){
            endX = 14;
        }

        int current = board[coordY][startX];
        int currCount = 0;
        int currY = startY;
        for(int i = startX; i <= endX; i++){
            if(current == board[currY][i]){
                currCount++;
                currY--;
            }
            else{
                current = board[currY][i];
                currCount = 1;
                currY--;
            }

            if(currCount == 5){
                return current;
            }
            if (currY<0) {
                return 0;
            }
        }
        return 0;
    }



}
