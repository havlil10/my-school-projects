package cz.cvut.fel.rsp.exception;

public class InvalidTurnException extends TicTacToeException {

    public InvalidTurnException() {
    }

    public InvalidTurnException(String message) {
        super(message);
    }

    public InvalidTurnException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidTurnException(Throwable cause) {
        super(cause);
    }
}
