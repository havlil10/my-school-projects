package cz.cvut.fel.rsp.dao;

import cz.cvut.fel.rsp.model.Inventory;
import org.springframework.stereotype.Repository;

@Repository
public class InventoryDao extends BaseDao<Inventory> {
    public InventoryDao() {
        super(Inventory.class);
    }
}
