package cz.cvut.fel.rsp.model;

import java.util.ArrayList;
import javax.persistence.*;
import java.util.List;
import java.util.Optional;

@Entity
public class FriendList extends AbstractEntity {

    @OneToOne(mappedBy = "friendList", optional = false)
    private User user;

    @OneToMany
    @JoinColumn(name = "user_id")
    private List<User> friends;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<User> getFriends() {
        return friends;
    }

    public void setFriends(List<User> users) {
        friends = users;
    }

    public void addFriend(User user) {
        instantiateFriendList();
        Optional<User> friendTest = friends.stream()
                .filter(friend -> friend.getId().equals(user.getId()))
                .findAny();
        if (!friendTest.isPresent()) {
            friends.add(user);
        }
    }

    public void removeFriend(User user) {
        instantiateFriendList();
        Optional<User> friendTest = friends.stream()
                .filter(friend -> friend.getId().equals(user.getId()))
                .findAny();
        if (friendTest.isPresent()) {
            friends.remove(user);
        }
    }

    private void instantiateFriendList() {
        if (friends == null) {
            friends = new ArrayList<>();
        }
    }

}
