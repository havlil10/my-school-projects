package cz.cvut.fel.rsp.rest;

import cz.cvut.fel.rsp.dao.UserDao;
import cz.cvut.fel.rsp.messages.GameRecordDTO;
import cz.cvut.fel.rsp.messages.TurnMessage;
import cz.cvut.fel.rsp.model.Turn;
import cz.cvut.fel.rsp.model.User;
import cz.cvut.fel.rsp.service.GameHistoryService;
import cz.cvut.fel.rsp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.awt.*;
import java.util.List;

@RestController
@RequestMapping("/rest/user/history")
public class GameHistoryController {
    private final GameHistoryService gameHistoryService;
    private final UserDao userDao;

    @Autowired
    public GameHistoryController(GameHistoryService gameHistoryService, UserDao userDao) {
        this.gameHistoryService = gameHistoryService;
        this.userDao = userDao;
    }

    @GetMapping(value = "/{username}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<GameRecordDTO> getUserGameHistory(@PathVariable String username){
        User user = userDao.findByUsername(username);
        System.out.println(user.getUsername());
        return gameHistoryService.getPreviousGamesOfUser(user);
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER', 'ROLE_GUEST')")
    @GetMapping(value = "/game/{gameid}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<TurnMessage> getGameTurns(@PathVariable int gameid){
        List<TurnMessage> turns = gameHistoryService.getTurnsFromGameRecord(gameid);
        return turns;
    }
}
