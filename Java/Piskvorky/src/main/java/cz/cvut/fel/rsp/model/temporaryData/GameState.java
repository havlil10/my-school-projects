package cz.cvut.fel.rsp.model.temporaryData;

public enum GameState {
    NEW, SWAPPING,PLAYING, FINISHED;
}
