package cz.cvut.fel.rsp.dao;

import cz.cvut.fel.rsp.model.FriendList;
import org.springframework.stereotype.Repository;

@Repository
public class FriendListDao extends BaseDao<FriendList>{
    public FriendListDao() {
        super(FriendList.class);
    }
}
