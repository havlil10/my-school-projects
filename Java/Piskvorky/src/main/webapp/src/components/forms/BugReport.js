/* eslint-disable no-unused-vars */
import React, { useContext } from "react";
import "../../App.css";
import { Modal, Button, Form } from "react-bootstrap/";
import { useState } from "react";
function BugReport(props) {
  const buttonStyle = {
    color: "white",
  };
  return (
    <>
      <Modal
        {...props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Bug Report
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group
              className="mb-3"
              controlId="exampleForm.ControlTextarea1"
            >
              <Form.Label>Describe your problem</Form.Label>
              <Form.Control as="textarea" rows={3} />
            </Form.Group>

            <Button
              variant="primary"
              type="submit"
              className="btn btn-outline-dark btn-secondary"
              style={buttonStyle}
            >
              Submit
            </Button>
          </Form>
        </Modal.Body>
      </Modal>
    </>
  );
}

export default BugReport;
