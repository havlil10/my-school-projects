import React, { useContext } from "react";
import "../../App.css";
import Button from "react-bootstrap/Button";
import { UserContext } from "../context/UserContext";
import { NavLink, useNavigate } from "react-router-dom";
import Register from "../forms/Register";

function Shop() {
  const { user, setUser } = useContext(UserContext);

  const headerStyle = {
    padding: "1em",
  };
  const buttonStyle = {
    padding: "0.5em",
    color: "white",
    fontSize: "2em",
    margin: "2px",
  };
  const justify = {
    justifyContent: "center",
    alignItems: "center",
  };

  const [modalShow, setModalShow] = React.useState(false);

  return (
    <div style={justify}>
      <h1 style={headerStyle}>Shop page</h1>
      <div className="d-grid gap-4">Not implemented</div>
    </div>
  );
}

export default Shop;
