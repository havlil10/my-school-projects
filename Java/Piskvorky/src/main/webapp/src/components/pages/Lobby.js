/* eslint-disable no-unused-vars */
import React from "react";
import { useState } from "react";
import {
	Container,
	Row,
	Col,
	Form,
	Button,
	ButtonGroup,
	ToggleButton,
} from "react-bootstrap";
import "./Lobby.css";

function Lobby() {
	const [privateLobby, setLobbyType] = useState(true);
	const [mode, setMode] = useState(1);
	const [timeMode, setTimeMode] = useState(5);
	const [ruleMode, setRuleMode] = useState(1);
	const [isCopied, setIsCopied] = useState(false);
	const link = "https://urmom.com/asdfg45ASD4848A5S4D";

	const lobbyTypes = [
		{ name: "Private", value: true },
		{ name: "Public", value: false },
	];
	const ruleModes = [
		{ name: "Classic", value: 1 },
		{ name: "SWAP", value: 2 },
	];

	const gameTypes = [
		{ name: "1v1", value: 1 },
		{ name: "Tournament", value: 2 },
	];

	const timeModes = [
		{ name: "1 minute", value: 1 },
		{ name: "5 minutes", value: 5 },
		{ name: "10 minutes", value: 10 },
	];

	async function copyTextToClipboard(text) {
		if ("clipboard" in navigator) {
			return await navigator.clipboard.writeText(text);
		} else {
			return document.execCommand("copy", true, text);
		}
	}

	return (
		<Container>
			<Form>
				<Row className="top-row">
					<Col className="lobby-type">
						<Row className="name-row">
							<h1>Lobby</h1>
						</Row>
						<Row>
							<ButtonGroup>
								{lobbyTypes.map((lobby, idx) => (
									<ToggleButton
										key={idx}
										type="radio"
										value={lobby.value}
										name="lobbyType"
										checked={privateLobby === lobby.value}
										variant="outline-success"
										onClick={(e) => setLobbyType(lobby.value)}
									>
										{lobby.name}
									</ToggleButton>
								))}
							</ButtonGroup>
						</Row>
					</Col>
					<Col className="copy-link">
						<Form.Group className="mb-3" controlId="formBasicPassword">
							<fieldset disabled>
								<h2>Send link:</h2>
								<Form.Control
									type="text"
									value={link}
									onClick={() => {
										copyTextToClipboard(link);
										setIsCopied(true);
									}}
									readOnly
								/>
								<Form.Text className="text-muted">
									{isCopied ? "Copied!" : "Click to copy"}
								</Form.Text>
							</fieldset>
						</Form.Group>
					</Col>
				</Row>
				<Row>
					<Col className="settings">
						<Row>
							{" "}
							<h2>Mode: </h2>
							<ButtonGroup className="setting-two-buttons">
								{gameTypes.map((type, idx) => (
									<ToggleButton
										key={idx}
										type="radio"
										value={type.value}
										name="gameMode"
										checked={mode === type.value}
										variant="outline-success"
										onClick={(e) => setMode(type.value)}
									>
										{type.name}
									</ToggleButton>
								))}
							</ButtonGroup>
						</Row>
						<Row>
							{" "}
							<h2>Time: </h2>
							<ButtonGroup className="setting-three-buttons">
								{timeModes.map((mode, idx) => (
									<ToggleButton
										key={idx}
										type="radio"
										value={mode.value}
										name="timeMode"
										checked={timeMode === mode.value}
										variant="outline-success"
										onClick={(e) => setTimeMode(mode.value)}
									>
										{mode.name}
									</ToggleButton>
								))}
							</ButtonGroup>
						</Row>
						<Row>
							{" "}
							<h2>Rules: </h2>
							<ButtonGroup className="setting-two-buttons">
								{ruleModes.map((mode, idx) => (
									<ToggleButton
										key={idx}
										type="radio"
										value={mode.value}
										name="RuleMode"
										checked={ruleMode === mode.value}
										variant="outline-success"
										onClick={(e) => setRuleMode(mode.value)}
									>
										{mode.name}
									</ToggleButton>
								))}
							</ButtonGroup>
						</Row>
					</Col>
					<Col></Col>
				</Row>
			</Form>
		</Container>
	);
}

export default Lobby;
