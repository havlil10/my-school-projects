import React, { useContext, useEffect, useState } from "react";
import "../../App.css";
import "./Profile.css";
import Button from "react-bootstrap/Button";
import { UserContext } from "../context/UserContext";
import Form from "react-bootstrap/Form";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Table from "react-bootstrap/Table";
import Container from "react-bootstrap/Container";
import { InfoIcon } from "../icons/InfoIcon";
import { ProfileIcon } from "../icons/ProfileIcon";
import FriendList from "../shared/FriendList";
import { StatsIcon } from "../icons/StatsIcon";
import { getMatchHistory } from "../../MockApi";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import axios from "../../api/axios";
import { useNavigate } from "react-router";

const formStyle = {
	padding: "1%",
	backgroundColor: "#585858",
	borderRadius: "15px",
	maxWidth: "75%",
	marginTop: "25px",
};

function ProfilePage() {
	const { user } = useContext(UserContext);
	const [pageContent, setPageContent] = useState("info");
	const [matchHistory, setMatchHistory] = useState(null);
	const navigate = useNavigate();
	useEffect(() => {
		if (user && !matchHistory) {
			let games = getGameHistory();
			console.log(games);
			setMatchHistory(games);
		}
		if (user == null) {
			navigate("/");
		}
	}, [user]);

	useEffect(() => {
		document
			.querySelectorAll("a")
			.forEach((node) => node.classList.remove("selected"));
		pageContent &&
			document.querySelector("." + pageContent).classList.add("selected");
	}, [pageContent]);

	const getGameHistory = async () => {
		let gameHistory;
		try {
			await axios
				.get("/rest/user/history/" + user.username, {
					headers: { "Content-Type": "application/json" },
				})
				.then((resp) => {
					gameHistory = resp.data;
				});
		} catch (err) {
			if (!err?.response) {
				console.log("Server not available");
				gameHistory = getMatchHistory;
			}
		}
		setMatchHistory(gameHistory);
	};

	return (
		<>
			<Container style={{ margin: "0" }} className="mx-auto">
				<Row className="mt-5 mb-5">
					<Col xs={12}>
						<Navbar bg="dark" expand="lg" className={"profile-navbar"}>
							<Container>
								<Navbar.Toggle aria-controls="basic-navbar-nav" />
								<Navbar.Collapse id="basic-navbar-nav">
									<Nav className="me-auto">
										<Nav.Link
											className={"info"}
											onClick={() => setPageContent("info")}
										>
											Account info
										</Nav.Link>
										<Nav.Link
											className={"friends"}
											onClick={() => setPageContent("friends")}
										>
											Friends
										</Nav.Link>
										<Nav.Link
											className={"stats"}
											onClick={() => setPageContent("stats")}
										>
											Stats
										</Nav.Link>
										<Nav.Link
											className={"mh"}
											onClick={() => setPageContent("mh")}
										>
											Match history
										</Nav.Link>
									</Nav>
								</Navbar.Collapse>
							</Container>
						</Navbar>

						{pageContent === "info" && (
							<Container>
								<Row className={"align-items-center justify-content-center"}>
									<Form style={formStyle}>
										<Row className="mb-1">
											<Col xs={2} className="my-auto text-start">
												<InfoIcon />
											</Col>
											<Col xs={8} className="title">
												Account information
											</Col>
											<Col xs={2} className="my-auto text-end">
												<ProfileIcon />
											</Col>
										</Row>
										<Row>
											<Form.Group
												className="mb-3"
												md="4"
												controlId="formBasicUsername"
											>
												<Form.Label>Username</Form.Label>
												<Form.Control
													type="text"
													placeholder={user ? user.username : "not logged in"}
													disabled
												/>
											</Form.Group>
										</Row>
										<Row>
											<Form.Group className="mb-3" controlId="formBasicEmail">
												<Form.Label>Email address</Form.Label>
												<Form.Control
													type="email"
													placeholder={user ? user.email : ""}
													disabled
												/>
											</Form.Group>
										</Row>
									</Form>
									<Form className="mt-4" style={formStyle}>
										<Row>
											<Form.Group
												className="mb-3 md"
												controlId="formBasicPassword"
											>
												<Form.Label>New password</Form.Label>
												<Form.Control type="password" placeholder="******" />
											</Form.Group>
										</Row>
										<Row>
											<Form.Group
												className="mb-3 md"
												controlId="formRepeatPassword"
											>
												<Form.Label>Repeat new password</Form.Label>
												<Form.Control type="password" placeholder="******" />
											</Form.Group>
										</Row>
										<Row className="w-50 mx-auto">
											<Button variant="dark" type="submit">
												Change password
											</Button>
										</Row>
									</Form>
								</Row>
							</Container>
						)}

						{pageContent === "friends" && (
							<Col>
								<FriendList />
							</Col>
						)}

						{pageContent === "stats" && (
							<Row
								style={formStyle}
								className={"align-items-center justify-content-center mx-auto"}
							>
								<Col>
									<Row className="mb-1">
										<Col xs={2} className="my-auto">
											<StatsIcon />
										</Col>
										<Col xs={8} className="title">
											Stats
										</Col>
										<Col xs={2} />
									</Row>
									<Table borderless>
										<tbody>
											<tr>
												<td>WINS</td>
												<td>1</td>
											</tr>
											<tr>
												<td>LOSSES</td>
												<td>2</td>
											</tr>
										</tbody>
									</Table>
								</Col>
							</Row>
						)}

						{pageContent === "mh" && (
							<Row className={"align-items-center justify-content-center"}>
								<Col className="match-history" style={formStyle}>
									<Row className="mb-3">
										<Col xs={2} className="my-auto">
											<StatsIcon />
										</Col>
										<Col xs={8} className="title">
											Match history
										</Col>
										<Col xs={2} />
									</Row>
									<Row
										className="mb-1"
										style={{ backgroundColor: "transparent" }}
									>
										{matchHistory
											? matchHistory.map((match) => {
													let date = new Date(match.date)
														.toUTCString()
														.slice(0, 22);
													if (match.winner == user.username) {
														return (
															<Row
																id={match.id}
																className="mr"
																style={{
																	paddingLeft: "20px",
																	display: "flex",
																	flexWrap: "nowrap",
																	justifyContent: "space-between",
																}}
															>
																<div className="text" style={{ width: "auto" }}>
																	{"Won against: " + match.loser}
																</div>
																<div
																	className="date"
																	style={{ width: "auto", marginRight: "20px" }}
																>
																	{date}
																</div>
															</Row>
														);
													}

													return (
														<Row
															id={match.id}
															className="mr loss"
															style={{
																paddingLeft: "20px",
																display: "flex",
																flexWrap: "nowrap",
																justifyContent: "space-between",
															}}
														>
															<div className="text" style={{ width: "auto" }}>
																{"Lost against: " + match.winner}
															</div>
															<div
																className="date"
																style={{ width: "auto", marginRight: "20px" }}
															>
																{date}
															</div>
														</Row>
													);
											  })
											: "Login before accessing match history"}
									</Row>
								</Col>
							</Row>
						)}
					</Col>
				</Row>
			</Container>
		</>
	);
}

export default ProfilePage;
