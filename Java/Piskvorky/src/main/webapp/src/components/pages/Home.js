import React, { useContext } from "react";
import "../../App.css";
import Button from "react-bootstrap/Button";
import { UserContext } from "../context/UserContext";
import { NavLink, useNavigate } from "react-router-dom";
import Register from "../forms/Register";
import Login from "../forms/Login";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Container from "react-bootstrap/Container";

function Home() {
  const { user, setUser } = useContext(UserContext);

  const buttonStyle = {
    padding: "1em",
    fontSize: "2em",
    margin: "0.2em",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    "&:hover": {
      color: "red",
    },
  };

  const headerStyle = {
    paddingTop: "0.5em",
    paddingBottom: "1.5em",

    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    fontSize: "400%",
  };

  const [registerModalShow, setRegisterModalShow] = React.useState(false);
  const [loginModalShow, setLoginModalShow] = React.useState(false); //login

  return (
    <Container>
      <h1 style={headerStyle}>Project RSP - 5inRow.com</h1>
      <Row className="hmm">
        <Col style={{ paddingTop: "30px" }}>
          <Row className="d-grid col-10 gap-4">
            <NavLink
              type="button"
              className="btn btn-outline-dark btn-secondary"
              style={buttonStyle}
              to="/game"
            >
              <div className="flexando">
                <div className="mainText">Quick Match </div>
              </div>
            </NavLink>
          </Row>
          <Row className="d-grid col-10 gap-4">
            <NavLink
              type="button"
              className="btn btn-outline-dark btn-secondary"
              style={buttonStyle}
              to="/lobby"
            >
              <div className="flexando">
                <div className="mainText">Play With Friend </div>
              </div>
            </NavLink>
          </Row>

          {user ? (
            <></>
          ) : (
            <Row className="d-grid col-10 gap-4">
              <Button
                type="button"
                variant="btn btn-outline-dark btn-secondary"
                style={buttonStyle}
                onClick={() => setRegisterModalShow(true)}
              >
                <div className="flexando">
                  <div className="mainText">Create Account </div>
                </div>
              </Button>
            </Row>
          )}

          <Row>
            <div style={{ paddingTop: "4em" }}>
              About 5inRow.com :Lorem ipsum dolor sit amet, consectetur
              adipiscing elit. Purus elit pellentesque sed dolor vestibulum
              lorem. Dapibus cras sit id consectetur potenti vitae, massa
              commodo ipsum. Venenatis ullamcorper sollicitudin integer molestie
              arcu id. Sit dolor etiam sed habitant. Purus elit pellentesque sed
              dolor vestibulum lorem. Dapibus cras sit id consectetur potenti
              vitae, massa commodo ipsum.
            </div>
          </Row>
        </Col>
        <Col>
          <div style={{ position: "sticky" }}>
            <svg viewBox="0 15 200 200">
              <circle
                cx="100"
                cy="100"
                r="69"
                stroke="black"
                fill="yellow"
                strokeWidth="0.5"
              />

              <circle
                cx="80"
                cy="75"
                r="10"
                stroke="black"
                fill="black"
                strokeWidth="1"
              />
              <circle
                cx="120"
                cy="75"
                r="10"
                stroke="black"
                fill="black"
                strokeWidth="1"
              />
              <path
                d="M 60 125 Q 110 170 140 115"
                fill="none"
                stroke="black"
                strokeWidth="5"
              ></path>
              <polyline
                points="65 55 95 55"
                stroke="black"
                fill="transparent"
                strokeWidth="5"
              />
              <polyline
                points="105 55 135 55"
                stroke="black"
                fill="transparent"
                strokeWidth="5"
              />
              <polyline
                points="100 95 100 115"
                stroke="black"
                fill="transparent"
                strokeWidth="5"
              />
              <ellipse
                cx="100"
                cy="30"
                rx="55"
                ry="12"
                stroke="gold"
                fill="transparent"
                strokeWidth="5"
              />
            </svg>
          </div>
        </Col>
      </Row>
      <Register
        show={registerModalShow}
        onHide={() => setRegisterModalShow(false)}
        onSwap={() => {
          setLoginModalShow(true);
        }}
      />
      <Login
        show={loginModalShow}
        onHide={() => setLoginModalShow(false)}
        onSwap={() => {
          setRegisterModalShow(true);
        }}
      />
    </Container>
  );
}

export default Home;
