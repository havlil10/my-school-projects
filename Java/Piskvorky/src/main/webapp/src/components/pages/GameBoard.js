import React, {useContext, useEffect, useState} from 'react';
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Board from "../game/Board";
import Chat from "../shared/Chat";
import "./Game.css"
import SockJsClient from "react-stomp";
import {UserContext} from "../context/UserContext";
import Matchmaking from "../game/Matchmaking";
import axios from "axios";
import Button from "react-bootstrap/Button";
import { SERVER_IP } from '../context/Constants';
import Endgame from "../game/Endgame";

const FIND_MATCH_URL = "http://"+SERVER_IP+":8080/rest/matchmaking/findCustomMatch";
const GET_CURRENT = "http://"+SERVER_IP+":8080/rest/users/current";

const X = 1;
const O = 2;
let test = 0
function GameBoard(props) {
    const {user, setUser} = useContext(UserContext);
    const [clientRef, setClientRef] = useState(null);

    // const [test, setTest] = useState(0)

    const [game, setGame] = useState(null);
    const [gameActive, setGameActive] = useState(false);
    const [player, setPlayer] = useState(null)
    const [opponent, setOpponent] = useState(null)
    const [canPlay, setCanPlay] = useState(null);
    const [symbol, setSymbol] = useState(null);

    const [isEnd, setIsEnd] = useState(false);
    const [hasWon, setHasWon] = useState(null);
    const [playerNum, setPlayerNum] = useState(null);

    const toggleEndPopUp = () => {
        setIsEnd(true)
    }

    const gameBoardStyle = {
        display: "flex",
        justifyContent: "center",
        top: "100px"
    }

    const findOpponent = () => {
        console.log("find game")
        try{
            const gameResponse = axios.get(FIND_MATCH_URL,{
                withCredentials: true,
            })
                .then((resp) => {
                    console.log("game found with data: " + resp.data)
                    console.log(resp)
                    setGame(resp.data)
                })
                .catch(e => {
                    console.log("Get find match not working.")
                    console.log(e)
                });


        }catch (e){
            console.log("opponent not found")
        }
    }

    const initPlayers = () => {
        axios.get(GET_CURRENT, {
            withCredentials: true,
        })
            .then((resp) => {
                if (resp.data === ""){
                    // setPlayer(null)
                }else{
                    setPlayer(resp.data)
                }
                console.log(resp.data)
            })
            .catch((e) => console.error(e));
        console.log(player)

    }

    const getPlayerName = () =>{
        if (player !== null){
            return player.username
        }
        else return "guest"
    }

    const getOpponentName = () => {
        if (opponent !== null){
            return opponent.username
        }
        else return "guest"
    }

    useEffect(() => {
        if (!gameActive){
            initPlayers();
            findOpponent();

            setGameActive(true)

            test += 1
            console.log(test)
        }
    },[gameActive])


    useEffect(() => {
        if (game !== null && player !== null){
            if (player.username === game.player1.username){
                // setPlayerNum(1)
                setOpponent(game.player2)
                if (game.player1IsX){
                    setSymbol(X)
                    setPlayerNum(X)
                    if (game.xTurn){
                        setCanPlay(true)
                    }else {
                        setCanPlay(false)
                    }
                }else{
                    setSymbol(O)
                    setPlayerNum(O)
                    if (game.xTurn){
                        setCanPlay(false)
                    }else {
                        setCanPlay(true)
                    }
                }
            }else{
                // setPlayerNum(2)
                setOpponent(game.player1)
                if (game.player1IsX){
                    setSymbol(O)
                    setPlayerNum(O)
                    if (game.xTurn){
                        setCanPlay(false)
                    }else {
                        setCanPlay(true)
                    }
                }else{
                    setSymbol(X)
                    setPlayerNum(X)
                    if (game.xTurn){
                        setCanPlay(true)
                    }else {
                        setCanPlay(false)
                    }
                }
            }
        }


        renderGame()
    }, [player, game])

    useEffect(() => {
        renderGame()

    }, [game, canPlay, symbol, player])

    const renderGame = () => {
        if (game == null) {
            return(
                <Matchmaking />
            )
        }
        else{
            return (
            <div style={gameBoardStyle} >

                {/*<Button onClick={() => {*/}
                {/*    toggleEndPopUp()*/}

                {/*    // renderGame()*/}
                {/*}}>*/}
                {/*    test button*/}
                {/*</Button>*/}

                <Row>
                    <Col >
                        {isEnd && <Endgame hasWon={hasWon} playerName={getPlayerName()}/>}
                        <Board
                            player={getPlayerName()} opponent={getOpponentName()}
                            // player={"getPlayerName()"} opponent={"getOpponentName"}
                            gameUID={game.gameUID}
                            canPlay={canPlay} setCanPlay={setCanPlay}
                            symbol={symbol}

                            toggleEndPopUp={toggleEndPopUp} setHasWon={setHasWon} playerNum={playerNum}
                            // render={renderGame}
                        />
                    </Col>

                    {/*<Col className={"chatCol"}>*/}
                    {/*    <Chat/>*/}
                    {/*</Col>*/}
                </Row>
            </div>)
        }
    }

    return renderGame()

}

export default GameBoard;