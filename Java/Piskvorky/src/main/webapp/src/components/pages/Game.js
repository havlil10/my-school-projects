import React from "react";
import "../../App.css";
import Square from "../game/Square";
import Board from "../game/Board";
import "./Game.css";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import Container from "react-bootstrap/Container";
import { NavLink, useNavigate } from "react-router-dom";
import GameBoard from "./GameBoard";
import SockJsClient from "react-stomp";
import axios from "axios";

function Game() {
  const headerStyle = {
    padding: "1em",

    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  };

  const buttonStyle = {
    padding: "0.5em",
    color: "white",
    fontSize: "2em",
    margin: "2px",
  };

  const findMatch = async e => {
    await axios.get("/rest/matchmaking/findMatch")
        .then(() => {
          console.log("finding match")
        })
        .catch(() => {
          console.log("error with post for finding match")
        })
  }


  return (
    <Container>
      <h1 style={headerStyle}>Play match</h1>
      <div className="colorBack">
        <Row className="d-grid col-9 gap-2">
          <div className="paddingButton">
            <NavLink
              type="button"
              className="btn btn-outline-dark btn-secondary"
              style={(buttonStyle, headerStyle)}
              to="/match"
            >
              <div className="flexando">
                <div className="mainText">Quick Match </div>
                <div>Play against random person in unranked match</div>
              </div>
            </NavLink>
          </div>
        </Row>

        <Row className="d-grid col-9 gap-2">
          <div className="paddingButton">
            <NavLink
              type="button"
              className="btn btn-outline-dark btn-secondary"
              style={(buttonStyle, headerStyle)}
              to="/lobby"
            >
              <div className="flexando">
                <div className="mainText">Computer </div>
                <div>Play against computer bot</div>
              </div>
            </NavLink>
          </div>
        </Row>
        <Row className="d-grid col-9 gap-4">
          <div className="paddingButton">
            <NavLink
              type="button"
              className="btn btn-outline-dark btn-secondary"
              style={(buttonStyle, headerStyle)}
              to="/lobby"
            >
              <div className="flexando">
                <div className="mainText">Ranked match </div>
                <div>
                  Play against a person with similar skill in ranked match
                </div>
              </div>
            </NavLink>
          </div>
        </Row>
        <Row className="d-grid col-9 gap-4">
          <div className="paddingButton">
            <NavLink
              type="button"
              className="btn btn-outline-dark btn-secondary"
              style={(buttonStyle, headerStyle)}
              to="/lobby"
            >
              <div className="flexando">
                <div className="mainText">Tournament </div>
                <div>
                  Join a tournament with multiple players and try to win trophy
                </div>
              </div>
            </NavLink>
          </div>
        </Row>
        <Row className="d-grid col-9 gap-4">
          <div className="paddingButton">
            <NavLink
              type="button"
              className="btn btn-outline-dark btn-secondary"
              style={(buttonStyle, headerStyle)}
              to="/lobby"
            >
              <div className="flexando">
                <div className="mainText">Create match </div>
                Create your own match with your own rules
              </div>
            </NavLink>
          </div>
        </Row>
      </div>
    </Container>

    //<Board/>
  );
}

export default Game;
