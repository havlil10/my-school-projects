import React from 'react';

function ChatMessage(props) {
    return (
        <div className={"chatMessage"}>
            {props.name}: {props.msg}
        </div>
    );
}

export default ChatMessage;