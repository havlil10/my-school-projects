import React, {useState} from 'react';
import { Modal, Button, Form } from "react-bootstrap/";
import "./Chat.css"
import ChatMessage from "./ChatMessage";
import GameRow from "../game/GameRow";
import {FormGroup} from "react-bootstrap";

function Chat(props) {
    const buttonStyle = {
        color: "white",
        marginTop: "5px"
    };

    const [messages, setMessages] = useState([])

    const msgs = [
        {name: "pavel", text: "ahoj"}
    ]

    let chatValue = React.createRef();

    const setChatValue = () => {
        const value = chatValue.current.value
        console.log(value)
    }

    const renderMsg = () => {
      return messages.map((msg) =>
          <ChatMessage name={msg.name} msg={msg.text}/>
      )
    }

    const addMessage = (msg) => {
        let newMessages = [];
        for (let message of messages){
            newMessages.push(message)
        }
        newMessages.push(msg); // todo pridat name jako prihlasenyho usera
        setMessages(newMessages);
    }

    const sendMessage = () => {
        const message = {name: "Pavel", text: chatValue} // todo pridat name jako prihlasenyho usera
        addMessage(message)
    }


    return (
        <div className={"chat"}>
            <div className={"messages"}>
                {renderMsg()}
            </div>
            <Form>
                <FormGroup>

                    <Form.Control as="textarea" rows={2} placeholder={"Write message"} />

                    <Button
                        variant="primary-secondary"
                        // type="submit"
                        className="btn btn-success btn-secondary"
                        style={buttonStyle}
                        ref={chatValue}
                        onChange={() => setChatValue()}
                        onClick={() => setChatValue()}
                    >
                        Send
                    </Button>
                </FormGroup>
            </Form>
        </div>
    );
}

export default Chat;