import React, { useState } from 'react';

import ListGroup from 'react-bootstrap/ListGroup';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { Container } from 'react-bootstrap';
import { getFriends } from '../../MockApi';
import { OnlineIndicator } from "../icons/OnlineIndicator";
import { ChatIcon } from "../icons/ChatIcon";
import { CrossIcon } from "../icons/CrossIcon";
import { FriendsIcon } from '../icons/FriendsIcon';
import './FriendList.css';

const formStyle = {
    padding: "1.5%",
    backgroundColor: "#585858",
    borderRadius : "15px",
    marginTop: "25px",
    maxWidth:"65%"
}

function FriendList() {
    const [friends, setFriends] = useState(getFriends().sort((a,b) => Number(b.isOnline) - Number(a.isOnline)));

    return (
        <Container style={formStyle} className={"align-items-center justify-content-center"}>
        <Row className="friends-title sticky-top">                                      
            <Col xs={2} className="my-auto">
              <FriendsIcon/>
            </Col>
            <Col xs={8} className="title"> 
                Friends
            </Col>
            <Col xs={2}></Col>
        </Row>
        <ListGroup className="friendlist">
        

        {friends.map(friend => { 
            return (
            <ListGroup.Item key={friend.username} style={{backgroundColor: friend.isOnline ? "" : "#A3A3A3"}}>
                <Row>
                    <Col xs={2} className="my-auto">
                        {friend.isOnline ?  <OnlineIndicator/> : ""}
                    </Col>            
                    <Col xs={8}>
                        {friend.username}
                    </Col>
                    <Col xs={1}>
                    <ChatIcon/>
                    </Col>
                    <Col xs={1}>
                    <CrossIcon/>
                    </Col>
                </Row>
            </ListGroup.Item>    
        )})}
        </ListGroup>  
        </Container>
    )
}

export default FriendList;