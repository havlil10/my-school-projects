import React, {useContext, useEffect, useState} from "react";
import Square from "../game/Square"
import "./Game.css"
import PlayerIcon from "./PlayerIcon";
import GameRow from "./GameRow";
import PlayerRow from "./PlayerRow";
import SockJsClient from "react-stomp";
import {UserContext} from "../context/UserContext";
import {Redirect} from "react-router-dom";




function Board (props){
    const [clientRef, setClientRef] = useState(null);
    const {user} = useContext(UserContext);
    const [turnHistory, setTurnHistory] = useState([])


    const doTurn = (x, y, winner, symbol) => {
        sendTurn(x, y, winner, symbol)
    }

    const sendTurn = (x, y, winner, symbol) => {
        console.log(x + " " + y + symbol);
        clientRef.sendMessage('/app/doTurn/'+ props.gameUID, JSON.stringify({
            symbol: symbol,
            coordX: x,
            coordY: y,
            winner: winner
        }));
    }

    const timerRef = React.useRef(null);

    // const stopTimer = () => {
    //     timerRef.current.stopTimer();
    // }

    const getBoard = () => {
        let content = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14];
        return content.map((r_num) =>
            <GameRow key={"row_num:"+r_num.toString()}
                     row={r_num}
                     // func={stopTimer}
                     symbol={props.symbol}
                     turnHistory={turnHistory}
                     canPlay={props.canPlay} setCanPlay={props.setCanPlay}
                     doTurn={doTurn}

                     render={props.render}
            />
        );
    }

    useEffect(() => {
        console.log("Turn history: ")
        console.log(turnHistory)
        getBoard()
    }, [props.canPlay, turnHistory])

    let onConnected = () => {
        console.log("Connected game!!");
    };

    let onMessageReceived = (msg) => {
        //todo based on symbol (if same as mine, i cannot place another symbol until opponent does)
        console.log(msg);

        let coordX = msg.coordX
        let coordY = msg.coordY
        let symbolNew = msg.symbol
        let winner = msg.winner


        turnHistory.push({
            x: coordX,
            y: coordY,
            symbol: symbolNew
        })
        setTurnHistory(turnHistory)


        // todo konec hry implementovat

        if (winner !== 0){
            getBoard()
            // alert("konec")
            if (winner === props.playerNum){
                props.setHasWon(true)
            }else{
                props.setHasWon(false)
            }
            props.toggleEndPopUp()
            return
        }

        if (symbolNew !== props.symbol){
            props.setCanPlay(true)
        }
        getBoard()
    };

    return (
        <>
            <SockJsClient
                url={"http://localhost:8080/doTurn/" + props.gameUID}
                topics={["/topic/games/" + props.gameUID, "/user/queue/errors"]}
                onConnect={onConnected}
                onDisconnect={() => {console.log("Disconnected game!")}}
                onMessage={msg => onMessageReceived(msg)}
                ref={(client) => {
                    setClientRef(client);
                }}
                debug={false}
            />

            <div className={"board"}>

                <PlayerRow start={false}
                    playerContext = {props.player}
                    ref={timerRef} // todo vyber toho jakej timer patri komu
                    canPlay={props.canPlay}
                />

                {getBoard()}

                <PlayerRow
                    start={true}
                    playerContext={props.opponent}
                    canPlay={!props.canPlay}
                />
            </div>
        </>
    );
}

export default Board;