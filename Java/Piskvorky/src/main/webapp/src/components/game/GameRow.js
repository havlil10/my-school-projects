import React from "react";
import Square from "./Square";
import "./Game.css";

function GameRow(props) {
  const turnHistory = props.turnHistory

  const columns = () => {

    let columns = []
    for (let num = 0; num < 15; num++){

      let skip = false
      for (let i =0; i < turnHistory.length; i++){
        let turn = turnHistory[i]
        if (turn.y === props.row && turn.x === num){
        // if (0 === props.row && 0 === num){
        //   console.log(turnHistory)
        //   console.log(turn)
        //   console.log(turn.x)
        //   console.log(turn.y)
        //   console.log(turn.symbol)

          columns.push(
              <Square
                  key={"row:" + props.row.toString() + "_col:" + num.toString()}
                  col={num}
                  row={props.row}
                  value={turn.symbol}
                  // func={props.func}
                  symbol={props.symbol}
                  canPlay={props.canPlay} setCanPlay={props.setCanPlay}
                  doTurn={props.doTurn}
                  render={props.render}
              />
          )
          skip = true
          break
        }
      }


      if (!skip){
        columns.push(
            <Square
                key={"row:" + props.row.toString() + "_col:" + num.toString()}
                col={num}
                row={props.row}
                value={0}
                // func={props.func}
                symbol={props.symbol}
                canPlay={props.canPlay} setCanPlay={props.setCanPlay}
                doTurn={props.doTurn}
                render={props.render}
            />
        )
      }
    }
    return columns

    // return columns_num.map((num) => (
    //
    //     <Square
    //       key={"row:" + props.row.toString() + "_col:" + num.toString()}
    //       col={num}
    //       row={props.row}
    //       value={props.value}
    //       func={props.func}
    //       symbol={props.symbol}
    //       canPlay={props.canPlay} setCanPlay={props.setCanPlay}
    //       doTurn={props.doTurn}
    //       render={props.render}
    //     />
    //   ));
  };

  return <div className={"gameRow"}>{columns()}</div>;
}

export default GameRow;
