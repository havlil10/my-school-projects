import React, {createRef, forwardRef, useRef} from 'react';
import PlayerIcon from "./PlayerIcon";
import TimeClock from "./TimeClock";


function PlayerRow(props) {
    return (
        <div className={"playerRow"}>
            <div className={props.canPlay ? "canPlay": "cannotPlay"}/>
            <PlayerIcon playerContext = {props.playerContext}/>
        </div>
    );
}

export default PlayerRow;