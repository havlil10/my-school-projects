import React from "react";
import {ProfileIcon} from "../icons/ProfileIcon"

import "./Game.css"
import TimeClock from "./TimeClock";

function PlayerIcon(props){
    const playerContext = props.playerContext;
    return (
        <div className={"playerContent"}>
            <ProfileIcon/>
            {playerContext}
        </div>
    );
}

export default PlayerIcon;