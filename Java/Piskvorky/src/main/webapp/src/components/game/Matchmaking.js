import React from 'react';
import "./Matchmaking.css"
import {Button} from "react-bootstrap";
import {NavLink} from "react-router-dom";

function Matchmaking(props) {
    return (
        <div className={"matchmaking"}>
            <h1>Finding opponent</h1>
            <div className={"lds-roller"}>
                <div/>
                <div/>
                <div/>
                <div/>
                <div/>
                <div/>
                <div/>
                <div/>
            </div>
            <NavLink
                type="button"
                variant="primary-danger"
                to="/game"
                className="btn btn-danger"
                style={{
                    color: "white",
                    width: "150px",
                    height: "50px",
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center"
                }}
            >
                Cancel
            </NavLink>
        </div>
    );
}

export default Matchmaking;