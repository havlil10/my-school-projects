import React from 'react';
import {NavLink} from "react-router-dom";
import "./Game.css"

function Endgame(props) {
    const win = [(<p className={"endGame-info"}>{props.playerName} - You won!</p>), <p className={"endGame-info"}>Congrats, you are a TicTacMaster :)</p>]
    const lose = [<p className={"endGame-info"}>{props.playerName} - You lost!</p>, <p className={"endGame-info"}>You are gonna get him next time.</p>]

    const popupBoxStyle = {
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        position: "fixed",
        background: "#00000050",
        width: "100%",
        height: "100vh",
        // top: "0",
        left: "0"
    }

    const boxStyle = {
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#333",
        width: "50%",
        height: "50vh",
        borderRadius: "15px",
        fontSize: "30px"
    }

    return (
        <div className={"popup-box"} style={popupBoxStyle}>
            <div className={"box"} style={boxStyle}>
                {props.hasWon? win: lose}
                <NavLink
                    type="button"
                    variant="primary-danger"
                    to="/game"
                    className="btn btn-danger"
                    style={{
                        color: "white",
                        width: "150px",
                        height: "50px",
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center"
                    }}
                >
                    Close
                </NavLink>
            </div>
        </div>
    );
}

export default Endgame;