import React, {useEffect, useState} from "react";
import "./Game.css"
import {click} from "@testing-library/user-event/dist/click";

const values = ["", "X", "O"]

function Square(props){
    const col = props.col;
    const row = props.row;
    // const setTimerIsActive = props.func;

    const symbol = values[props.symbol]

    const [value, setValue] = useState(values[props.value]);

    const [clientRef, setClientRef] = useState(null);

    const isEmpty = () => {
        return props.value === 0
    }

    const isX = () =>{
        // return !isEmpty() && props.value === 1 || props.symbol === 1
        return props.value === 1 || props.symbol === 1 && isEmpty()
    }

    const clickEvent = () => {

        if (props.canPlay){
            if (isEmpty()){

            placeSymbol();
            printTurnPos();
            props.doTurn(col, row, 0, props.symbol)

            props.setCanPlay(false)

            // stopTime()
            }else{
                console.log("this position has been already played")
            }
        }else{
            console.log("It is not your turn")
        }
        // props.render()
    }

    const placeSymbol = () => {
        // todo validace,zda neni zapsany
        setValue(symbol);
    }

    // const stopTime = () => {
    //     setTimerIsActive(false);
    // }

    const printTurnPos = () => {
        // let xhr = new XMLHttpRequest();

        let msg = JSON.stringify(
            {
                x: col.toString(),
                y: row.toString(),
            }
        )
        console.log(msg);
        // xhr.open();
        // xhr.send('POST',"localhost:3000");
    }

    // useEffect(()=> {
    //     renderSquare()
    // },[value, isEmpty])

    const renderSquare = () => {
        return (
            // <div className={"square " }
            <div className={isX() ? "square X": "square O"}
                 onClick={clickEvent}>
                {isEmpty() ? value: values[props.value]}
            </div>
        )
    }

    return renderSquare()
}


export default Square;