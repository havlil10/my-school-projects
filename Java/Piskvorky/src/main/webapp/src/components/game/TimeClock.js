import React, {useEffect, useState, forwardRef, useImperativeHandle} from 'react';
import "./Game.css"

const TimeClock = forwardRef((props,ref) => {
    const [isActive, setIsActive] = useState(props.start);
    const [sec, setSeconds] = useState(5);
    const [min, setMinutes] = useState(5);

    useImperativeHandle(ref, () =>({
        stopTimer(){
            stop()
        }

    }))

    let timer;
    useEffect(() =>{
        if (isActive){
            timer = setInterval(() =>{
                if (min === 0 && sec === 0){
                    setMinutes(0)
                    stop()
                    return clearInterval(timer)
                }

                setSeconds(sec - 1);
                if(sec === 0){
                    setMinutes(min - 1);
                    setSeconds(59)
                }
            }, 1000)

            return () => clearInterval(timer)
        }else{
            return clearInterval(timer)
        }
    });

    const stop = () => {
        setIsActive(false)
        console.log("stop casu")
    }

    const resume = () => {
        setIsActive(true)
    }

    return (
        <div className={"timeClock"}>
            {min < 10? "0"+min: min} : {sec < 10? "0"+sec:sec}
        </div>
    );
});

export default TimeClock;