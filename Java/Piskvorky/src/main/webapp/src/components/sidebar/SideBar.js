/* eslint-disable spaced-comment */
/* eslint-disable capitalized-comments */
/* eslint-disable no-unused-expressions */
import React, { useContext, useState } from "react";
import {
	CDBSidebar,
	CDBSidebarContent,
	CDBSidebarFooter,
	CDBSidebarHeader,
	CDBSidebarMenu,
	CDBSidebarMenuItem,
} from "cdbreact";
import { NavLink } from "react-router-dom";
import "./Sidebar.css";
import Login from "../forms/Login";
import Register from "../forms/Register";
import BugReport from "../forms/BugReport";
import { UserContext } from "../context/UserContext";
import axios from "../../api/axios";

const SideBar = () => {
	const [menuCollapse, setMenuCollapse] = useState(false);
	const { user, setUser } = useContext(UserContext);

	const menuIconClick = () => {
		menuCollapse ? setMenuCollapse(false) : setMenuCollapse(true);
	};

	const [loginModalShow, setLoginModalShow] = React.useState(false); //login
	const [bugReportModalShow, setBugReportModalShow] = React.useState(false); //bugreport

	const [registerModalShow, setRegisterModalShow] = React.useState(false); //register

	return (
		<div
			style={{
				minHeight: "100vh",
			}}
		>
			<CDBSidebar textColor="#fff" backgroundColor="#333">
				<CDBSidebarHeader
					prefix={
						<i
							onClick={menuIconClick}
							className={
								menuCollapse
									? "fa fa-arrow-left fa-fw"
									: "fa fa-bars fa-large fa-fw"
							}
						/>
					}
				>
					Menu - TicTacBro
				</CDBSidebarHeader>
				<CDBSidebarContent className="sidebar-content">
					<CDBSidebarMenu>
						<NavLink to="/">
							<CDBSidebarMenuItem icon="home">Home</CDBSidebarMenuItem>
						</NavLink>

						{user ? (
							<NavLink to="/profile">
								<CDBSidebarMenuItem icon="user">Profile</CDBSidebarMenuItem>
							</NavLink>
						) : (
							<NavLink to="/">
								<CDBSidebarMenuItem
									icon="user fa-fw"
									onClick={() => setLoginModalShow(true)}
								>
									Login
								</CDBSidebarMenuItem>
							</NavLink>
						)}

						<NavLink to="/game">
							<CDBSidebarMenuItem icon="gamepad">Game</CDBSidebarMenuItem>
						</NavLink>

						<NavLink to="/shop">
							<CDBSidebarMenuItem icon="shopping-cart">Shop</CDBSidebarMenuItem>
						</NavLink>

						{user ? (
							<NavLink to="/">
								<CDBSidebarMenuItem
									icon="sign-out-alt"
									onClick={() => {
										sessionStorage.clear();
                    setUser(null);
										axios
											.get("rest/users/logout", { withCredentials: true })
											.then(setUser(null));
									}}
								>
									Logout
								</CDBSidebarMenuItem>
							</NavLink>
						) : (
							<></>
						)}
					</CDBSidebarMenu>
				</CDBSidebarContent>

				<CDBSidebarFooter>
					<CDBSidebarContent>
						<CDBSidebarMenu>
							<NavLink to="/settings">
								<CDBSidebarMenuItem icon="cog">Settings</CDBSidebarMenuItem>
							</NavLink>
							<NavLink to="/about">
								<CDBSidebarMenuItem icon="question-circle">
									Questions
								</CDBSidebarMenuItem>
							</NavLink>
							<CDBSidebarMenuItem
								icon="bug"
								onClick={() => setBugReportModalShow(true)}
							>
								Bug report
							</CDBSidebarMenuItem>
							<BugReport
								show={bugReportModalShow}
								onHide={() => setBugReportModalShow(false)}
							/>
						</CDBSidebarMenu>
					</CDBSidebarContent>
				</CDBSidebarFooter>
			</CDBSidebar>
			<Login
				show={loginModalShow}
				onHide={() => setLoginModalShow(false)}
				onSwap={() => {
					setRegisterModalShow(true);
				}}
			/>
			<Register
				show={registerModalShow}
				onHide={() => {
					setRegisterModalShow(false);
				}}
				onSwap={() => {
					setLoginModalShow(true);
				}}
			/>
		</div>
	);
};

export default SideBar;
