import axios from "axios";
import "../components/context/Constants.js"
import {SERVER_IP} from "../components/context/Constants";

export default axios.create({
	baseURL: "http://"+SERVER_IP+":8080",
});
