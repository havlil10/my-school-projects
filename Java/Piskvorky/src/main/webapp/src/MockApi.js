export function login() {
	return {
		username: "mock-user",
		email: "test@fel.cz",
	};
}

export function getFriends() {
	return [
		{
			username: "friend-online",
			isOnline: true,
		},
		{
			username: "friend-not-online",
			isOnline: false,
		},
		{
			username: "friend-online3",
			isOnline: true,
		},
		{
			username: "friend1",
			isOnline: true,
		},
		{
			username: "friend12",
			isOnline: true,
		},
		{
			username: "friend2",
			isOnline: true,
		},
		{
			username: "friend13",
			isOnline: false,
		},
		{
			username: "friend3",
			isOnline: false,
		},
		{
			username: "friend4",
			isOnline: false,
		},
		{
			username: "friend5",
			isOnline: true,
		},
	];
}

export function getMatchHistory() {
	return [
		{
			id: 1,
			isWin: true,
		},
		{
			id: 2,
			isWin: false,
		},
		{
			id: 2,
			isWin: true,
		},
		{
			id: 2,
			isWin: false,
		},
		{
			id: 2,
			isWin: false,
		},
		{
			id: 2,
			isWin: false,
		},
	];
}
