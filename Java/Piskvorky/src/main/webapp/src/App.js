import React, { useState, useMemo } from "react";
import "./App.css";
import SideBar from "./components/sidebar/SideBar";
import Home from "./components/pages/Home";
import Game from "./components/pages/Game";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { UserContext } from "./components/context/UserContext";
import ProfilePage from "./components/pages/Profile";
import ShopPage from "./components/pages/Shop";
import TournamentPage from "./components/pages/Tournament";
import LobbyPage from "./components/pages/Lobby";
import GameBoard from "./components/pages/GameBoard";
import Matchmaking from "./components/game/Matchmaking";

function App() {
	const [user, setUser] = useState(
		sessionStorage.getItem("user") === null ? null : UserContext
	);

	const userValue = useMemo(() => ({ user, setUser }), [user, setUser]);

	return (
		<div className="App">
			<Router>
				<UserContext.Provider value={userValue}>
					<Row>
						<Col xs={1} style={{ display: "flex" }}>
							<SideBar />
						</Col>
						<Col xs={11}>
							<Routes>
								<Route path="/" element={<Home />} />
								<Route path="/game" element={<Game />} />
								<Route path="/profile" element={<ProfilePage />} />
								<Route path="/shop" element={<ShopPage />} />
								<Route path="/tournament" element={<TournamentPage />} />
								<Route path="/lobby" element={<LobbyPage />} />
								<Route path="/match" element={<GameBoard />} />
								<Route path="/matchmaking" element={<Matchmaking />} />
							</Routes>
						</Col>
					</Row>
				</UserContext.Provider>
			</Router>
		</div>
	);
}

export default App;
