# Piškvorky

Aplikaci jsme vytvářeli v 7 lidech v průběhu semestru

Jedná se o enterprise aplikaci "piškvorky" s možnostmi:
- vytváření účtů
- získávání bodů za výhry
- zobrazení statistik
- jednoduchý matchmaking
- a více

Pro mě nové technologie:
- spring websocket
- long-polling