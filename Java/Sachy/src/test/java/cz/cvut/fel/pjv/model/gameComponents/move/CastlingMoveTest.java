package cz.cvut.fel.pjv.model.gameComponents.move;

import cz.cvut.fel.pjv.model.gameComponents.Army;
import cz.cvut.fel.pjv.model.gameComponents.GameBoard;
import cz.cvut.fel.pjv.model.gameComponents.Tile;
import cz.cvut.fel.pjv.model.pieces.King;
import cz.cvut.fel.pjv.model.pieces.Rook;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CastlingMoveTest {
    @Test
    public void makeMoveTest() {
        //arrange
        CastlingMove castlingMove = new CastlingMove(0, 3, 4, 2);
        GameBoard testingGameBoard = new GameBoard();

        testingGameBoard.board.get(0).setOccupier(new Rook(Army.BLACK, 0));
        testingGameBoard.board.get(4).setOccupier(new King(Army.BLACK, 4));

        //act
        castlingMove.makeMove(testingGameBoard);

        Tile tileFromWhereMoved = testingGameBoard.board.get(0);
        Tile tileWhereMoved = testingGameBoard.board.get(3);
        Tile tileFromWhereSecondMoved = testingGameBoard.board.get(4);
        Tile tileWhereSecondMoved = testingGameBoard.board.get(2);

        //assert
        assertTrue(tileFromWhereMoved.getOccupier()==null);
        assertTrue(tileWhereMoved.getOccupier() instanceof Rook);
        assertEquals(tileWhereMoved.getOccupier().getCoordinate(), 3);

        assertTrue(tileFromWhereSecondMoved.getOccupier()==null);
        assertTrue(tileWhereSecondMoved.getOccupier() instanceof King);
        assertEquals(tileWhereSecondMoved.getOccupier().getCoordinate(), 2);
    }
}
