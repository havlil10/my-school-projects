package cz.cvut.fel.pjv.model.pieces;

import cz.cvut.fel.pjv.model.gameComponents.Army;
import cz.cvut.fel.pjv.model.gameComponents.GameBoard;
import cz.cvut.fel.pjv.model.gameComponents.Tile;
import cz.cvut.fel.pjv.model.gameComponents.move.BasicMove;
import cz.cvut.fel.pjv.model.gameComponents.move.CastlingMove;
import cz.cvut.fel.pjv.model.gameComponents.move.Move;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class RookTest {
    @Test
    public void calculatePossibleMoves_onNewBoard_returnsEmptyArray() {
        GameBoard gameBoard = new GameBoard();
        Piece rook = gameBoard.board.get(0).getOccupier();
        ArrayList<Move> actualMoves = rook.calculatePossibleMoves(gameBoard);

        assertEquals(actualMoves.size(), 0);
    }

    @Test
    public void calculatePossibleMoves_onModifiedBoard_returnsCorrectArray() {
        GameBoard gameBoard = new GameBoard();
        Piece rook = new Rook(Army.BLACK, 27);
        gameBoard.board.get(27).setOccupier(rook);

        ArrayList<Move> expectedMoves = new ArrayList<>();
        expectedMoves.add(new BasicMove(27, 19));
        expectedMoves.add(new BasicMove(27, 26));
        expectedMoves.add(new BasicMove(27, 25));
        expectedMoves.add(new BasicMove(27, 24));
        expectedMoves.add(new BasicMove(27, 28));
        expectedMoves.add(new BasicMove(27, 29));
        expectedMoves.add(new BasicMove(27, 30));
        expectedMoves.add(new BasicMove(27, 31));
        expectedMoves.add(new BasicMove(27, 35));
        expectedMoves.add(new BasicMove(27, 43));
        expectedMoves.add(new BasicMove(27, 51));

        ArrayList<Move> actualMoves = rook.calculatePossibleMoves(gameBoard);

        if (expectedMoves.size()==actualMoves.size()) {
            for (int i = 0; i < expectedMoves.size(); i++) {
                assertEquals(expectedMoves.get(i).getFromCoordinate(), actualMoves.get(i).getFromCoordinate());
                assertEquals(expectedMoves.get(i).getToCoordinate(), actualMoves.get(i).getToCoordinate());
            }
        } else {
            fail();
        }
    }

    @Test
    public void calculatePossibleMoves_onBoardWithCastlingMoves_returnsCorrectArray() {
        GameBoard gameBoard = new GameBoard();
        Piece rook = gameBoard.board.get(0).getOccupier();
        gameBoard.board.get(1).setOccupier(null);
        gameBoard.board.get(2).setOccupier(null);
        gameBoard.board.get(3).setOccupier(null);

        ArrayList<Move> expectedMoves = new ArrayList<>();
        expectedMoves.add(new BasicMove(0, 1));
        expectedMoves.add(new BasicMove(0, 2));
        expectedMoves.add(new BasicMove(0, 3));
        expectedMoves.add(new CastlingMove(4, 2, 0, 3));;

        ArrayList<Move> actualMoves = rook.calculatePossibleMoves(gameBoard);

        if (expectedMoves.size()==actualMoves.size()) {
            for (int i = 0; i < expectedMoves.size(); i++) {
                assertEquals(expectedMoves.get(i).getFromCoordinate(), actualMoves.get(i).getFromCoordinate());
                assertEquals(expectedMoves.get(i).getToCoordinate(), actualMoves.get(i).getToCoordinate());
            }
            assertEquals(((CastlingMove) expectedMoves.get(expectedMoves.size()-1)).getRookFromCoordinate(),
                    ((CastlingMove) actualMoves.get(actualMoves.size()-1)).getRookFromCoordinate());
            assertEquals(((CastlingMove) expectedMoves.get(expectedMoves.size()-1)).getRookToCoordinate(),
                    ((CastlingMove) actualMoves.get(actualMoves.size()-1)).getRookToCoordinate());

        } else {
            fail();
        }
    }
    
}
