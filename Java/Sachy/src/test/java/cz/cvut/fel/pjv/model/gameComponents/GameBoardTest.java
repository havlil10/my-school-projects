package cz.cvut.fel.pjv.model.gameComponents;

import cz.cvut.fel.pjv.model.gameComponents.move.BasicMove;
import cz.cvut.fel.pjv.model.gameComponents.move.Move;
import cz.cvut.fel.pjv.model.pieces.*;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class GameBoardTest {
    @Test
    public void constructorCalled_prepareNewGameBoard_returnsCorrectArray() {
        GameBoard gameBoard = new GameBoard();
        int expectedSize = 64;
        Piece blackRook = gameBoard.board.get(7).getOccupier();
        Piece noPiece = gameBoard.board.get(16).getOccupier();
        Piece whiteRook = gameBoard.board.get(63).getOccupier();

        assertEquals(expectedSize, gameBoard.board.size());
        assertTrue(blackRook instanceof Rook);
        assertTrue(noPiece==null);
        assertTrue(whiteRook instanceof Rook);
    }

    @Test
    public void getMovesWithoutCheckIssue_pawnThreaten_returnsCorrectArray() {
        GameBoard gameBoard = new GameBoard();
        new BasicMove(60, 28).makeMove(gameBoard);


        ArrayList<Move> arrayToRemoveFrom = new ArrayList<>();
        arrayToRemoveFrom.add(new BasicMove(28, 19));
        arrayToRemoveFrom.add(new BasicMove(28, 20));
        arrayToRemoveFrom.add(new BasicMove(28, 21));
        arrayToRemoveFrom.add(new BasicMove(28, 27));
        arrayToRemoveFrom.add(new BasicMove(28, 29));
        arrayToRemoveFrom.add(new BasicMove(28, 35));
        arrayToRemoveFrom.add(new BasicMove(28, 36));
        arrayToRemoveFrom.add(new BasicMove(28, 37));

        ArrayList<Move> expectedResult = new ArrayList<>();
        expectedResult.add(new BasicMove(28, 27));
        expectedResult.add(new BasicMove(28, 29));
        expectedResult.add(new BasicMove(28, 35));
        expectedResult.add(new BasicMove(28, 36));
        expectedResult.add(new BasicMove(28, 37));

        ArrayList<Move> actualArray = gameBoard.getMovesWithoutCheckIssue(arrayToRemoveFrom, Army.WHITE);


        if (expectedResult.size()==actualArray.size()) {
            for (int i = 0; i < expectedResult.size(); i++) {
                assertEquals(expectedResult.get(i).getFromCoordinate(), actualArray.get(i).getFromCoordinate());
                assertEquals(expectedResult.get(i).getToCoordinate(), actualArray.get(i).getToCoordinate());
            }
        } else {
            fail();
        }

    }



    @Test
    public void getMovesWithoutCheckIssue_KnightThreaten_returnsCorrectArray() {
        GameBoard gameBoard = new GameBoard();
        new BasicMove(60, 28).makeMove(gameBoard);
        gameBoard.board.get(26).setOccupier(new Knight(Army.BLACK, 26));
        gameBoard.board.get(44).setOccupier(new Knight(Army.WHITE, 44));

        ArrayList<Move> arrayToRemoveFrom = new ArrayList<>();
        arrayToRemoveFrom.add(new BasicMove(28, 19));
        arrayToRemoveFrom.add(new BasicMove(28, 20));
        arrayToRemoveFrom.add(new BasicMove(28, 21));
        arrayToRemoveFrom.add(new BasicMove(28, 27));
        arrayToRemoveFrom.add(new BasicMove(28, 29));
        arrayToRemoveFrom.add(new BasicMove(28, 35));
        arrayToRemoveFrom.add(new BasicMove(28, 36));
        arrayToRemoveFrom.add(new BasicMove(28, 37));

        ArrayList<Move> expectedResult = new ArrayList<>();
        expectedResult.add(new BasicMove(28, 27));
        expectedResult.add(new BasicMove(28, 29));
        expectedResult.add(new BasicMove(28, 35));
        expectedResult.add(new BasicMove(28, 37));

        ArrayList<Move> actualArray = gameBoard.getMovesWithoutCheckIssue(arrayToRemoveFrom, Army.WHITE);

        if (expectedResult.size()==actualArray.size()) {
            for (int i = 0; i < expectedResult.size(); i++) {
                assertEquals(expectedResult.get(i).getFromCoordinate(), actualArray.get(i).getFromCoordinate());
                assertEquals(expectedResult.get(i).getToCoordinate(), actualArray.get(i).getToCoordinate());
            }
        } else {
            fail();
        }
    }

    @Test
    public void getMovesWithoutCheckIssue_KingThreaten_returnsCorrectArray() {
        GameBoard gameBoard = new GameBoard();
        new BasicMove(60, 28).makeMove(gameBoard);
        new BasicMove(4, 26).makeMove(gameBoard);

        ArrayList<Move> arrayToRemoveFrom = new ArrayList<>();
        arrayToRemoveFrom.add(new BasicMove(28, 19));
        arrayToRemoveFrom.add(new BasicMove(28, 20));
        arrayToRemoveFrom.add(new BasicMove(28, 21));
        arrayToRemoveFrom.add(new BasicMove(28, 27));
        arrayToRemoveFrom.add(new BasicMove(28, 29));
        arrayToRemoveFrom.add(new BasicMove(28, 35));
        arrayToRemoveFrom.add(new BasicMove(28, 36));
        arrayToRemoveFrom.add(new BasicMove(28, 37));

        ArrayList<Move> expectedResult = new ArrayList<>();
        expectedResult.add(new BasicMove(28, 29));
        expectedResult.add(new BasicMove(28, 36));
        expectedResult.add(new BasicMove(28, 37));

        ArrayList<Move> actualArray = gameBoard.getMovesWithoutCheckIssue(arrayToRemoveFrom, Army.WHITE);

        if (expectedResult.size()==actualArray.size()) {
            for (int i = 0; i < expectedResult.size(); i++) {
                assertEquals(expectedResult.get(i).getFromCoordinate(), actualArray.get(i).getFromCoordinate());
                assertEquals(expectedResult.get(i).getToCoordinate(), actualArray.get(i).getToCoordinate());
            }
        } else {
            fail();
        }
    }

    @Test
    public void getMovesWithoutCheckIssue_OnLineThreaten_returnsCorrectArray() {
        GameBoard gameBoard = new GameBoard();
        new BasicMove(60, 28).makeMove(gameBoard);
        new BasicMove(0, 24).makeMove(gameBoard);

        ArrayList<Move> arrayToRemoveFrom = new ArrayList<>();
        arrayToRemoveFrom.add(new BasicMove(28, 19));
        arrayToRemoveFrom.add(new BasicMove(28, 20));
        arrayToRemoveFrom.add(new BasicMove(28, 21));
        arrayToRemoveFrom.add(new BasicMove(28, 27));
        arrayToRemoveFrom.add(new BasicMove(28, 29));
        arrayToRemoveFrom.add(new BasicMove(28, 35));
        arrayToRemoveFrom.add(new BasicMove(28, 36));
        arrayToRemoveFrom.add(new BasicMove(28, 37));

        ArrayList<Move> expectedResult = new ArrayList<>();
        expectedResult.add(new BasicMove(28, 35));
        expectedResult.add(new BasicMove(28, 36));
        expectedResult.add(new BasicMove(28, 37));

        ArrayList<Move> actualArray = gameBoard.getMovesWithoutCheckIssue(arrayToRemoveFrom, Army.WHITE);

        if (expectedResult.size()==actualArray.size()) {
            for (int i = 0; i < expectedResult.size(); i++) {
                assertEquals(expectedResult.get(i).getFromCoordinate(), actualArray.get(i).getFromCoordinate());
                assertEquals(expectedResult.get(i).getToCoordinate(), actualArray.get(i).getToCoordinate());
            }
        } else {
            fail();
        }
    }

    @Test
    public void getMovesWithoutCheckIssue_OnDiagonalThreaten_returnsCorrectArray() {
        GameBoard gameBoard = new GameBoard();
        new BasicMove(60, 28).makeMove(gameBoard);
        new BasicMove(5, 29).makeMove(gameBoard);

        ArrayList<Move> arrayToRemoveFrom = new ArrayList<>();
        arrayToRemoveFrom.add(new BasicMove(28, 19));
        arrayToRemoveFrom.add(new BasicMove(28, 20));
        arrayToRemoveFrom.add(new BasicMove(28, 21));
        arrayToRemoveFrom.add(new BasicMove(28, 27));
        arrayToRemoveFrom.add(new BasicMove(28, 29));
        arrayToRemoveFrom.add(new BasicMove(28, 35));
        arrayToRemoveFrom.add(new BasicMove(28, 36));
        arrayToRemoveFrom.add(new BasicMove(28, 37));

        ArrayList<Move> expectedResult = new ArrayList<>();
        expectedResult.add(new BasicMove(28, 27));
        expectedResult.add(new BasicMove(28, 29));
        expectedResult.add(new BasicMove(28, 35));
        expectedResult.add(new BasicMove(28, 37));

        ArrayList<Move> actualArray = gameBoard.getMovesWithoutCheckIssue(arrayToRemoveFrom, Army.WHITE);

        if (expectedResult.size()==actualArray.size()) {
            for (int i = 0; i < expectedResult.size(); i++) {
                assertEquals(expectedResult.get(i).getFromCoordinate(), actualArray.get(i).getFromCoordinate());
                assertEquals(expectedResult.get(i).getToCoordinate(), actualArray.get(i).getToCoordinate());
            }
        } else {
            fail();
        }
    }

    @Test
    public void isArmyInCheck_OnBoardWithNoKingThreat_returnsFalse() {
        GameBoard gameBoard = new GameBoard();
        boolean actualResult = gameBoard.isArmyInCheck(Army.WHITE);
        assertFalse(actualResult);
    }

    @Test
    public void isArmyInCheck_OnBoardWithKingThreat_returnsTrue() {
        GameBoard gameBoard = new GameBoard();
        BasicMove basicMove = new BasicMove(1, 43);
        basicMove.makeMove(gameBoard);
        boolean actualResult = gameBoard.isArmyInCheck(Army.WHITE);
        assertTrue(actualResult);
    }

    @Test
    public void canArmyMakeMove_OnStarterBoard_returnsTrue() {
        GameBoard gameBoard = new GameBoard();
        boolean actualResult = gameBoard.canArmyMakeMove(Army.WHITE);
        assertTrue(actualResult);
    }

    @Test
    public void canArmyMakeMove_OnBoardWithNoPossibleMoves_returnsFalse() {
        GameBoard gameBoard = new GameBoard();
        for (int i = 0; i<64; i++) {
            gameBoard.board.set(i, new Tile(i));
        }
        gameBoard.board.get(7).setOccupier(new King(Army.BLACK, 7));
        gameBoard.board.get(8).setOccupier(new Rook(Army.WHITE, 8));
        gameBoard.board.get(62).setOccupier(new Queen(Army.WHITE, 62));

        boolean actualResult = gameBoard.canArmyMakeMove(Army.BLACK);
        assertFalse(actualResult);
    }

    @Test
    public void convertPGNTest() {
        String stringToSplit = "1. e4 e5 2. Nf3 Nc6 3. Bb5 a6 4. Ba4 Nf6 5. O-O Be7 6. Re1 b5 7. Bb3 d6 8. c3 O-O 9. h3 Nb8 10. d4 Nbd7 11. c4 c6 12. cxb5 axb5 13. Nc3 Bb7 14. Bg5 b4 15. Nb1 h6 16. Bh4 c5 17. dxe5 Nxe4 18. Bxe7 Qxe7 19. exd6 Qf6 20. Nbd2 Nxd6 21. Nc4 Nxc4 22. Bxc4 Nb6 23. Ne5 Rae8 24. Bxf7+ Rxf7 25. Nxf7 Rxe1+ 26. Qxe1 Kxf7 27. Qe3 Qg5 28. Qxg5 hxg5 29. b3 Ke6 30. a3 Kd6 31. axb4 cxb4 32. Ra5 Nd5 33. f3 Bc8 34. Kf2 Bf5 35. Ra7 g6 36. Ra6+ Kc5 37. Ke1 Nf4 38. g3 Nxh3 39. Kd2 Kb5 40. Rd6 Kc5 41. Ra6 Nf2 42. g4 Bd3 43. Re6 1/2-1/2";

        String[] stringArrayList = stringToSplit.split(" ");
        ArrayList<String> PGNMoves = new ArrayList<>();
        for(int i=0; i<stringArrayList.length; i++) {
            if(i%3==0) {
                continue;
            }
            PGNMoves.add(stringArrayList[i]);
        }
        System.out.println(PGNMoves);
    }


}