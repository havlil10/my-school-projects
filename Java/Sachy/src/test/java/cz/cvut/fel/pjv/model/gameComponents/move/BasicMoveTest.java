package cz.cvut.fel.pjv.model.gameComponents.move;

import cz.cvut.fel.pjv.model.gameComponents.Army;
import cz.cvut.fel.pjv.model.gameComponents.GameBoard;
import cz.cvut.fel.pjv.model.gameComponents.Tile;
import cz.cvut.fel.pjv.model.pieces.Pawn;
import cz.cvut.fel.pjv.model.pieces.Rook;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class BasicMoveTest {
    @Test
    public void makeMoveTest() {
        //arrange
        BasicMove basicMove = new BasicMove(7, 5);
        GameBoard testingGameBoard = new GameBoard();

        testingGameBoard.board.get(7).setOccupier(new Rook(Army.BLACK, 7));

        //act
        basicMove.makeMove(testingGameBoard);

        Tile tileFromWhereMoved = testingGameBoard.board.get(7);
        Tile tileWhereMoved = testingGameBoard.board.get(5);

        //assert
        assertTrue(tileFromWhereMoved.getOccupier()==null);
        assertTrue(tileWhereMoved.getOccupier() instanceof Rook);
        assertTrue(((Rook) tileWhereMoved.getOccupier()).isAlreadyMoved());
        assertEquals(tileWhereMoved.getOccupier().getCoordinate(), 5);
    }



}
