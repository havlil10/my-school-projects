package cz.cvut.fel.pjv.model.gameComponents;

import org.junit.jupiter.api.Test;


public class DBManagerTest {
    @Test
    public void getGames_functionCalled_returnsNoError() {
        DBManager dbManager = new DBManager();
        dbManager.getGames();
    }

    @Test
    public void clearAllGames() {
        DBManager dbManager = new DBManager();
        dbManager.clearAllGames();
    }

}
