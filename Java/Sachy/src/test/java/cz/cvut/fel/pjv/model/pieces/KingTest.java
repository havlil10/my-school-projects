package cz.cvut.fel.pjv.model.pieces;

import cz.cvut.fel.pjv.model.gameComponents.Army;
import cz.cvut.fel.pjv.model.gameComponents.GameBoard;
import cz.cvut.fel.pjv.model.gameComponents.move.BasicMove;
import cz.cvut.fel.pjv.model.gameComponents.move.CastlingMove;
import cz.cvut.fel.pjv.model.gameComponents.move.Move;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class KingTest {
    @Test
    public void calculatePossibleMoves_onStarterBoard_returnsEmptyArray() {
        GameBoard gameBoard = new GameBoard();
        Piece king = gameBoard.board.get(60).getOccupier();
        ArrayList<Move> actualMoves = king.calculatePossibleMoves(gameBoard);

        assertEquals(actualMoves.size(), 0);
    }

    @Test
    public void calculatePossibleMoves_onModifiedBoard_returnsCorrectArray() {
        GameBoard gameBoard = new GameBoard();
        Piece king = new King(Army.BLACK, 29);
        gameBoard.board.get(29).setOccupier(king);

        ArrayList<Move> expectedMoves = new ArrayList<>();
        expectedMoves.add(new BasicMove(29, 20));
        expectedMoves.add(new BasicMove(29, 21));
        expectedMoves.add(new BasicMove(29, 22));
        expectedMoves.add(new BasicMove(29, 28));
        expectedMoves.add(new BasicMove(29, 30));
        expectedMoves.add(new BasicMove(29, 36));
        expectedMoves.add(new BasicMove(29, 37));
        expectedMoves.add(new BasicMove(29, 38));

        ArrayList<Move> actualMoves = king.calculatePossibleMoves(gameBoard);

        if (expectedMoves.size()==actualMoves.size()) {
            for (int i = 0; i < expectedMoves.size(); i++) {
                assertEquals(expectedMoves.get(i).getFromCoordinate(), actualMoves.get(i).getFromCoordinate());
                assertEquals(expectedMoves.get(i).getToCoordinate(), actualMoves.get(i).getToCoordinate());
            }
        } else {
            fail();
        }
    }

    @Test
    public void calculatePossibleMoves_onBoardWithCastlingMoves_returnsCorrectArray() {
        GameBoard gameBoard = new GameBoard();
        Piece king = gameBoard.board.get(60).getOccupier();
        gameBoard.board.get(61).setOccupier(null);
        gameBoard.board.get(62).setOccupier(null);

        ArrayList<Move> expectedMoves = new ArrayList<>();
        expectedMoves.add(new BasicMove(60, 61));
        expectedMoves.add(new CastlingMove(60, 62, 63, 61));;

        ArrayList<Move> actualMoves = king.calculatePossibleMoves(gameBoard);

        if (expectedMoves.size()==actualMoves.size()) {
            for (int i = 0; i < expectedMoves.size(); i++) {
                assertEquals(expectedMoves.get(i).getFromCoordinate(), actualMoves.get(i).getFromCoordinate());
                assertEquals(expectedMoves.get(i).getToCoordinate(), actualMoves.get(i).getToCoordinate());
            }
            assertEquals(((CastlingMove) expectedMoves.get(expectedMoves.size()-1)).getRookFromCoordinate(),
                    ((CastlingMove) actualMoves.get(actualMoves.size()-1)).getRookFromCoordinate());
            assertEquals(((CastlingMove) expectedMoves.get(expectedMoves.size()-1)).getRookToCoordinate(),
                    ((CastlingMove) actualMoves.get(actualMoves.size()-1)).getRookToCoordinate());

        } else {
            fail();
        }

    }
}
