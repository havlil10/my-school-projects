package cz.cvut.fel.pjv.model.pieces;

import cz.cvut.fel.pjv.model.gameComponents.Army;
import cz.cvut.fel.pjv.model.gameComponents.GameBoard;
import cz.cvut.fel.pjv.model.gameComponents.move.BasicMove;
import cz.cvut.fel.pjv.model.gameComponents.move.EnPassantMove;
import cz.cvut.fel.pjv.model.gameComponents.move.Move;
import cz.cvut.fel.pjv.model.gameComponents.move.PromotingMove;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class PawnTest {
    @Test
    public void calculatePossibleMoves_onStarterBoard_returnsCorrectArray() {
        //arrange
        GameBoard gameBoard = new GameBoard();
        Piece pawn = gameBoard.board.get(14).getOccupier();

        ArrayList<Move> expectedMoves = new ArrayList<>();
        expectedMoves.add(new BasicMove(14, 22));
        expectedMoves.add(new BasicMove(14, 30));

        //act
        ArrayList<Move> actualMoves = pawn.calculatePossibleMoves(gameBoard);

        //assert
        if (expectedMoves.size()==actualMoves.size()) {
            for (int i = 0; i < expectedMoves.size(); i++) {
                assertEquals(expectedMoves.get(i).getFromCoordinate(), actualMoves.get(i).getFromCoordinate());
                assertEquals(expectedMoves.get(i).getToCoordinate(), actualMoves.get(i).getToCoordinate());
            }
        } else {
            fail();
        }
    }

    @Test
    public void calculatePossibleMoves_onModifiedBoardWithEnPassant_returnsCorrectArray() {
        //arrange
        GameBoard gameBoard = new GameBoard();

        new BasicMove(51, 35).makeMove(gameBoard);
        new BasicMove(8, 16).makeMove(gameBoard);
        new BasicMove(35, 27).makeMove(gameBoard);
        new BasicMove(10, 26).makeMove(gameBoard);

        ArrayList<Move> expectedMoves = new ArrayList<>();
        expectedMoves.add(new BasicMove(27, 19));
        expectedMoves.add(new EnPassantMove(27, 18, 26));

        Piece pawn = gameBoard.board.get(27).getOccupier();
        //act
        ArrayList<Move> actualMoves = pawn.calculatePossibleMoves(gameBoard);

        //assert
        if (expectedMoves.size()==actualMoves.size()) {
            for (int i = 0; i < expectedMoves.size(); i++) {
                assertEquals(expectedMoves.get(i).getFromCoordinate(), actualMoves.get(i).getFromCoordinate());
                assertEquals(expectedMoves.get(i).getToCoordinate(), actualMoves.get(i).getToCoordinate());
            }
        } else {
            fail();
        }
        assertTrue(actualMoves.get(1) instanceof EnPassantMove);
        assertEquals(((EnPassantMove) actualMoves.get(1)).getCoordinateToRemove(), 26);
    }

    @Test
    public void calculatePossibleMoves_onModifiedBoardWithPawnPromotion_returnsCorrectArray() {
        //arrange
        GameBoard gameBoard = new GameBoard();
        gameBoard.board.get(8).setOccupier(new Pawn(Army.WHITE, 8));
        gameBoard.board.get(0).setOccupier(null);
        Piece pawn = gameBoard.board.get(8).getOccupier();

        ArrayList<Move> expectedMoves = new ArrayList<>();
        expectedMoves.add(new BasicMove(8, 1));
        expectedMoves.add(new PromotingMove(8, 0));

        //act
        ArrayList<Move> actualMoves = pawn.calculatePossibleMoves(gameBoard);

        //assert
        if (expectedMoves.size()==actualMoves.size()) {
            for (int i = 0; i < expectedMoves.size(); i++) {
                assertEquals(expectedMoves.get(i).getFromCoordinate(), actualMoves.get(i).getFromCoordinate());
                assertEquals(expectedMoves.get(i).getToCoordinate(), actualMoves.get(i).getToCoordinate());
            }
        } else {
            fail();
        }
        assertTrue(expectedMoves.get(1) instanceof PromotingMove);
    }
}
