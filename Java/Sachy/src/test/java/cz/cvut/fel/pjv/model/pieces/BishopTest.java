package cz.cvut.fel.pjv.model.pieces;

import cz.cvut.fel.pjv.model.gameComponents.Army;
import cz.cvut.fel.pjv.model.gameComponents.GameBoard;
import cz.cvut.fel.pjv.model.gameComponents.move.BasicMove;
import cz.cvut.fel.pjv.model.gameComponents.move.Move;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class BishopTest {
    @Test
    public void calculatePossibleMoves_onStarterBoard_returnsEmptyArray() {
        GameBoard gameBoard = new GameBoard();
        Piece bishop = gameBoard.board.get(2).getOccupier();
        ArrayList<Move> actualMoves = bishop.calculatePossibleMoves(gameBoard);

        assertEquals(actualMoves.size(), 0);
    }

    @Test
    public void calculatePossibleMoves_onModifiedBoard_returnsCorrectArray() {
        GameBoard gameBoard = new GameBoard();
        Piece bishop = new Bishop(Army.BLACK, 35);
        gameBoard.board.get(35).setOccupier(bishop);

        ArrayList<Move> expectedMoves = new ArrayList<>();
        expectedMoves.add(new BasicMove(35, 26));
        expectedMoves.add(new BasicMove(35, 17));
        expectedMoves.add(new BasicMove(35, 28));
        expectedMoves.add(new BasicMove(35, 21));
        expectedMoves.add(new BasicMove(35, 42));
        expectedMoves.add(new BasicMove(35, 49));
        expectedMoves.add(new BasicMove(35, 44));
        expectedMoves.add(new BasicMove(35, 53));

        ArrayList<Move> actualMoves = bishop.calculatePossibleMoves(gameBoard);

        if (expectedMoves.size()==actualMoves.size()) {
            for (int i = 0; i < expectedMoves.size(); i++) {
                assertEquals(expectedMoves.get(i).getFromCoordinate(), actualMoves.get(i).getFromCoordinate());
                assertEquals(expectedMoves.get(i).getToCoordinate(), actualMoves.get(i).getToCoordinate());
            }
        } else {
            fail();
        }

    }


}
