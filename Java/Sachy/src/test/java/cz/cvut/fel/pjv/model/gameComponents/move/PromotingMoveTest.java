package cz.cvut.fel.pjv.model.gameComponents.move;

import cz.cvut.fel.pjv.model.gameComponents.Army;
import cz.cvut.fel.pjv.model.gameComponents.GameBoard;
import cz.cvut.fel.pjv.model.gameComponents.Tile;
import cz.cvut.fel.pjv.model.pieces.Pawn;
import cz.cvut.fel.pjv.model.pieces.Queen;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PromotingMoveTest {
    @Test
    public void makeMoveTest() {
        //arrange
        PromotingMove promotingMove = new PromotingMove(12, 4);
        GameBoard testingGameBoard = new GameBoard();
        testingGameBoard.board.get(12).setOccupier(new Pawn(Army.WHITE, 12));

        //act
        promotingMove.makeMove(testingGameBoard);

        Tile tileFromWhereMoved = testingGameBoard.board.get(12);
        Tile tileWhereMoved = testingGameBoard.board.get(4);

        //assert
        assertTrue(tileFromWhereMoved.getOccupier()==null);
        assertTrue(tileWhereMoved.getOccupier() instanceof Queen);
        assertEquals(tileWhereMoved.getOccupier().getCoordinate(), 4);
    }
}
