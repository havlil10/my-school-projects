package cz.cvut.fel.pjv.model.pieces;

import cz.cvut.fel.pjv.model.gameComponents.Army;
import cz.cvut.fel.pjv.model.gameComponents.GameBoard;
import cz.cvut.fel.pjv.model.gameComponents.move.BasicMove;
import cz.cvut.fel.pjv.model.gameComponents.move.Move;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class QueenTest {
    @Test
    public void calculatePossibleMoves_onStarterBoard_returnsEmptyArray() {
        GameBoard gameBoard = new GameBoard();
        Piece queen = gameBoard.board.get(3).getOccupier();
        ArrayList<Move> actualMoves = queen.calculatePossibleMoves(gameBoard);

        assertEquals(actualMoves.size(), 0);
    }

    @Test
    public void calculatePossibleMoves_onModifiedBoard_returnsCorrectArray() {
        GameBoard gameBoard = new GameBoard();
        Piece queen = new Queen(Army.BLACK, 35);
        gameBoard.board.get(35).setOccupier(queen);

        ArrayList<Move> expectedMoves = new ArrayList<>();
        expectedMoves.add(new BasicMove(35, 26));
        expectedMoves.add(new BasicMove(35, 17));
        expectedMoves.add(new BasicMove(35, 27));
        expectedMoves.add(new BasicMove(35, 19));
        expectedMoves.add(new BasicMove(35, 28));
        expectedMoves.add(new BasicMove(35, 21));
        expectedMoves.add(new BasicMove(35, 34));
        expectedMoves.add(new BasicMove(35, 33));
        expectedMoves.add(new BasicMove(35, 32));
        expectedMoves.add(new BasicMove(35, 36));
        expectedMoves.add(new BasicMove(35, 37));
        expectedMoves.add(new BasicMove(35, 38));
        expectedMoves.add(new BasicMove(35, 39));
        expectedMoves.add(new BasicMove(35, 42));
        expectedMoves.add(new BasicMove(35, 49));
        expectedMoves.add(new BasicMove(35, 43));
        expectedMoves.add(new BasicMove(35, 51));
        expectedMoves.add(new BasicMove(35, 44));
        expectedMoves.add(new BasicMove(35, 53));

        ArrayList<Move> actualMoves = queen.calculatePossibleMoves(gameBoard);

        if (expectedMoves.size()==actualMoves.size()) {
            for (int i = 0; i < expectedMoves.size(); i++) {
                assertEquals(expectedMoves.get(i).getFromCoordinate(), actualMoves.get(i).getFromCoordinate());
                assertEquals(expectedMoves.get(i).getToCoordinate(), actualMoves.get(i).getToCoordinate());
            }
        } else {
            fail();
        }

    }
}
