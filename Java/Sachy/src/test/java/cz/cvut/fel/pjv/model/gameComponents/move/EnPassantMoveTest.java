package cz.cvut.fel.pjv.model.gameComponents.move;

import cz.cvut.fel.pjv.model.gameComponents.Army;
import cz.cvut.fel.pjv.model.gameComponents.GameBoard;
import cz.cvut.fel.pjv.model.gameComponents.Tile;
import cz.cvut.fel.pjv.model.pieces.Pawn;
import cz.cvut.fel.pjv.model.pieces.Rook;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class EnPassantMoveTest {
    @Test
    public void makeMoveTest() {
        //arrange
        EnPassantMove enPassantMove = new EnPassantMove(33, 40, 32);
        GameBoard testingGameBoard = new GameBoard();

        testingGameBoard.board.get(33).setOccupier(new Pawn(Army.BLACK, 33));

        //act
        enPassantMove.makeMove(testingGameBoard);

        Tile tileFromWhereMoved = testingGameBoard.board.get(33);
        Tile tileWhereMoved = testingGameBoard.board.get(40);
        Tile tileWherePawnRemoved = testingGameBoard.board.get(32);

        //assert
        assertTrue(tileFromWhereMoved.getOccupier()==null);
        assertTrue(tileWhereMoved.getOccupier() instanceof Pawn);
        assertEquals(tileWhereMoved.getOccupier().getCoordinate(), 40);
        assertTrue(tileWherePawnRemoved.getOccupier()==null);
    }
}
