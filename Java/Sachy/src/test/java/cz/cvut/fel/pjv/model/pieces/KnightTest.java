package cz.cvut.fel.pjv.model.pieces;

import cz.cvut.fel.pjv.model.gameComponents.GameBoard;
import cz.cvut.fel.pjv.model.gameComponents.move.BasicMove;
import cz.cvut.fel.pjv.model.gameComponents.move.Move;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class KnightTest {
    @Test
    public void calculatePossibleMoves_onStarterBoard_returnsCorrectCandidates() {
        GameBoard gameBoard = new GameBoard();
        Piece knight = gameBoard.board.get(6).getOccupier();
        ArrayList<Move> expectedMoves = new ArrayList<>();
        expectedMoves.add(new BasicMove(6, 21));
        expectedMoves.add(new BasicMove(6, 23));

        ArrayList<Move> actualMoves = knight.calculatePossibleMoves(gameBoard);

        if (expectedMoves.size()==actualMoves.size()) {
            for (int i = 0; i < expectedMoves.size(); i++) {
                assertEquals(expectedMoves.get(i).getFromCoordinate(), actualMoves.get(i).getFromCoordinate());
                assertEquals(expectedMoves.get(i).getToCoordinate(), actualMoves.get(i).getToCoordinate());
            }
        } else {
            fail();
        }
    }

}
