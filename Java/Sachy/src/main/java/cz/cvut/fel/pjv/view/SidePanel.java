package cz.cvut.fel.pjv.view;

import cz.cvut.fel.pjv.controller.GameManager;

import javax.swing.*;
import javax.swing.border.EtchedBorder;
import java.awt.*;

public class SidePanel extends JPanel {
    private final JPanel headerPanel;
    private final JPanel startGamePanel;
    private final JPanel PGNPanel;
    private final JLabel headerLabel = new JLabel("");
    private final VisibleInterface visibleInterface;

    private final GameManager gameManager;

    private static final Dimension PANEL_DIMENSION = new Dimension(120, 100);
    private static final EtchedBorder PANEL_BORDER = new EtchedBorder(EtchedBorder.RAISED);

    /**
     * creates new sidePanel, sets its properties such as size, color, etc.
     * creates headerPanel, startGamePanel and PGNPanel in it
     * @param gameManager - representing current game mode
     *                    (for the purpose of changing game mode from custom mode to new game)
     * @param visibleInterface - the visible interface it is part of
     */
    public SidePanel(GameManager gameManager, VisibleInterface visibleInterface) {
        super(new BorderLayout());
        this.gameManager = gameManager;
        this.visibleInterface = visibleInterface;
        setBackground(Color.decode("#f5f7d8"));
        setBorder(PANEL_BORDER);
        setPreferredSize(PANEL_DIMENSION);
        this.headerPanel = new JPanel(new GridLayout(2, 0));
        this.startGamePanel = new JPanel(new GridLayout(2, 1));
        this.PGNPanel = new JPanel(new GridLayout(4, 1));
        setButtonPanel();
        setPGNPanel();

        headerPanel.add(this.headerLabel);
        add(this.headerPanel, BorderLayout.NORTH);
        add(this.startGamePanel, BorderLayout.EAST);
        add(this.PGNPanel, BorderLayout.SOUTH);
        startGamePanel.setVisible(false);
        PGNPanel.setVisible(false);
    }

    private void setButtonPanel() {
        JButton button = new JButton("Start game");
        button.addActionListener(e -> {
            gameManager.setInCustomMode(false);
            gameManager.startChessClock(headerLabel);
            hideStartGameButton();
        });
        startGamePanel.add(button);
    }

    private void setPGNPanel() {
        JButton previous = new JButton("previous");
        previous.addActionListener(e -> visibleInterface.previousPGN());

        JButton next = new JButton("next");
        next.addActionListener(e -> visibleInterface.nextPGN());

        JButton firstPgn = new JButton("Jump to start");
        firstPgn.addActionListener(e -> visibleInterface.firstPGN());

        JButton lastPgn = new JButton("Jump to end");
        lastPgn.addActionListener(e -> visibleInterface.lastPGN());

        this.PGNPanel.add(next);
        this.PGNPanel.add(previous);
        this.PGNPanel.add(firstPgn);
        this.PGNPanel.add(lastPgn);
    }

    /**
     * shows the start game button
     */
    public void showStartGameButton() {
        startGamePanel.setVisible(true);
    }

    /**
     * hides the start game button
     */
    public void hideStartGameButton() {
        startGamePanel.setVisible(false);
    }

    /**
     * shows PGN panel
     */
    public void showPGNPanel() {
        PGNPanel.setVisible(true);
    }

    /**
     * hides the PGN panel
     */
    public void hidePGNPanel() {
        PGNPanel.setVisible(false);
    }

    /**
     * This method is for purpose of changing text of header label
     * @return header label
     */
    public JLabel getHeaderLabel() {
        return headerLabel;
    }
}
