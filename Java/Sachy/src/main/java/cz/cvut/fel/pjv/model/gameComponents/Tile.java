package cz.cvut.fel.pjv.model.gameComponents;

import cz.cvut.fel.pjv.model.pieces.Piece;

import java.io.Serializable;

public class Tile implements Serializable {
    private final int coordinate;
    private Piece occupier;

    public Tile(int coordinate) {
        this.coordinate = coordinate;
    }

    public Piece getOccupier() {
        return occupier;
    }

    public void setOccupier(Piece occupier) {
        this.occupier = occupier;
    }

    public int getCoordinate() {
        return coordinate;
    }
}
