package cz.cvut.fel.pjv.controller;

import cz.cvut.fel.pjv.model.gameComponents.Army;
import cz.cvut.fel.pjv.model.gameComponents.ChessTimer;

import javax.swing.*;

public class GameManager {

    private boolean isInCustomMode;
    private boolean isInPGNMode = false;
    private boolean isGameFinished = false;

    private ChessTimer chessTimer;

    public GameManager(boolean isCustomGame) {
        this.isInCustomMode = isCustomGame;
    }

    public boolean isInCustomMode() {
        return isInCustomMode;
    }

    public void startChessClock(JLabel timer) {
        if(chessTimer!=null) {
            chessTimer.stopTimer();
        }
        chessTimer = new ChessTimer(timer, this);
        //start new thread
        chessTimer.start();
    }

    public void stopChessClock() {
        if(chessTimer!=null) {
            chessTimer.stopTimer();
        }
    }

    public void processEndOfGame(Army loser) {
        this.isGameFinished = true;
        JOptionPane.showMessageDialog(null, "END OF TIME! " + changeArmy(loser) + " WINS!");
    }

    private Army changeArmy(Army army) {
        return army==Army.WHITE ? Army.BLACK : Army.WHITE;
    }

    public void changeArmyStopWatch() {
        chessTimer.changeCurrentPlayer();
    }


    public void setInCustomMode(boolean inCustomMode) {
        isInCustomMode = inCustomMode;
    }

    public boolean isGameFinished() {
        return isGameFinished;
    }

    public void setGameFinished(boolean gameFinished) {
        isGameFinished = gameFinished;
    }

    public boolean isInPGNMode() {
        return isInPGNMode;
    }

    public void setInPGNMode(boolean inPGNMode) {
        isInPGNMode = inPGNMode;
    }
}
