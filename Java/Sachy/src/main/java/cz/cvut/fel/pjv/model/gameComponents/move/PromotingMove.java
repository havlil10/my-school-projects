package cz.cvut.fel.pjv.model.gameComponents.move;

import cz.cvut.fel.pjv.model.gameComponents.GameBoard;
import cz.cvut.fel.pjv.model.gameComponents.Tile;
import cz.cvut.fel.pjv.model.pieces.Piece;
import cz.cvut.fel.pjv.model.pieces.Queen;


public class PromotingMove extends Move{
    private final int fromCoordinate;
    private final int toCoordinate;

    public PromotingMove(int fromCoordinate, int toCoordinate) {
        this.fromCoordinate = fromCoordinate;
        this.toCoordinate = toCoordinate;
    }

    @Override
    public void makeMove(GameBoard gameBoard) {
        Piece pieceToMove = gameBoard.board.get(fromCoordinate).getOccupier();
        Piece promotedPiece = new Queen(pieceToMove.getArmy(), toCoordinate);

        Tile targetTile = gameBoard.board.get(toCoordinate);

        gameBoard.board.get(fromCoordinate).setOccupier(null);
        targetTile.setOccupier(promotedPiece);

        gameBoard.madeMoves.add(this);
    }

    @Override
    public int getFromCoordinate() {
        return fromCoordinate;
    }

    @Override
    public int getToCoordinate() {
        return toCoordinate;
    }
}
