package cz.cvut.fel.pjv.model.gameComponents;

import cz.cvut.fel.pjv.model.gameComponents.move.Move;
import cz.cvut.fel.pjv.model.pieces.*;
import org.apache.commons.lang3.SerializationUtils;

import java.util.ArrayList;
import java.util.Random;


public class ComputerPlayer {
    private final Army army;
    private Difficulty difficulty;

    public Difficulty getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(Difficulty difficulty) {
        this.difficulty = difficulty;
    }

    public ComputerPlayer(Army army, Difficulty difficulty) {
        this.army = army;
        this.difficulty = difficulty;
    }

    /**
     * Makes move that army can make over a given gameBoard
     * if the ComputerPlayer difficulty is set to EASY, it makes random moves from all possible moves
     * if the difficulty is MEDIUM, it also tries to attack the most valuable piece, if possible
     * if the difficulty is SEMIHARD, it also tries to make move, after which it can attack, if possible
     * @param gameBoard - gameBoard over which can be processed a move
     */
    public void makeBestMove(GameBoard gameBoard) {
        if(this.difficulty==Difficulty.MEDIUM || this.difficulty==Difficulty.SEMIHARD) {
            Move candidateMoveDepthOne = findBestMoveDepthOne(gameBoard);
            if (candidateMoveDepthOne!=null) {
                candidateMoveDepthOne.makeMove(gameBoard);
                return;
            }
        }

        if(this.difficulty==Difficulty.SEMIHARD) {
            Move candidateMoveDepthTwo = findBestMoveDepthTwo(gameBoard);
            if(candidateMoveDepthTwo!= null) {
                candidateMoveDepthTwo.makeMove(gameBoard);
                return;
            }
        }

        ArrayList<Move> movesToChooseFrom = getAllPossibleMoves(gameBoard);
        movesToChooseFrom.get(new Random().nextInt(movesToChooseFrom.size())).makeMove(gameBoard);
    }

    private ArrayList<Move> getAllPossibleMoves(GameBoard gameBoard) {
        ArrayList<Move> possibleMoves = new ArrayList<>();
        for (Tile tile : gameBoard.board) {
            Piece occupier = tile.getOccupier();
            if (occupier != null && occupier.getArmy()==gameBoard.currentPlayer
                    && occupier.calculatePossibleMoves(gameBoard).size()>0) {
                possibleMoves.addAll(occupier.calculatePossibleMoves(gameBoard));
            }
        }
        return possibleMoves;
    }

    public Army getArmy() {
        return army;
    }

    private Move findBestMoveDepthOne(GameBoard gameBoard) {
        ArrayList<Move> candidateMoves = new ArrayList<>();
        ArrayList<Move> allPossibleMoves = getAllPossibleMoves(gameBoard);
        for (Move move : allPossibleMoves) {
            if (gameBoard.board.get(move.getToCoordinate()).getOccupier()!=null) {
                candidateMoves.add(move);
            }
        }
        if(candidateMoves.size()==0) {
            return null;
        }
        // didnt know how to simplify those
        for (Move move : candidateMoves) {
            if (gameBoard.board.get(move.getToCoordinate()).getOccupier() instanceof Queen) {
                return move;
            }
        }
        for (Move move : candidateMoves) {
            if (gameBoard.board.get(move.getToCoordinate()).getOccupier() instanceof Rook) {
                return move;
            }
        }
        for (Move move : candidateMoves) {
            if (gameBoard.board.get(move.getToCoordinate()).getOccupier() instanceof Bishop) {
                return move;
            }
        }
        for (Move move : candidateMoves) {
            if (gameBoard.board.get(move.getToCoordinate()).getOccupier() instanceof Knight) {
                return move;
            }
        }
        return candidateMoves.get(new Random().nextInt(candidateMoves.size()));
    }

    private Move findBestMoveDepthTwo(GameBoard gameBoard) {
        ArrayList<Move> firstMoves = new ArrayList<>();
        ArrayList<Move> secondMoves = new ArrayList<>();
        for (Move move : getAllPossibleMoves(gameBoard)) {
            GameBoard testingBoard = SerializationUtils.clone(gameBoard);
            move.makeMove(testingBoard);
            Move bestMove = findBestMoveDepthOne(testingBoard);
            if(bestMove!=null) {
                secondMoves.add(bestMove);
                firstMoves.add(move);
            }
        }
        if(secondMoves.size()==0) {
            return null;
        }

        return firstMoves.get(new Random().nextInt(firstMoves.size()));
    }

}
