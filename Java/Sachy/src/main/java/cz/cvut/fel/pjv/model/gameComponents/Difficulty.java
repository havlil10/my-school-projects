package cz.cvut.fel.pjv.model.gameComponents;

public enum Difficulty {
    EASY,
    MEDIUM,
    SEMIHARD
}
