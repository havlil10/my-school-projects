package cz.cvut.fel.pjv.model.gameComponents;

import cz.cvut.fel.pjv.controller.GameManager;

import javax.swing.*;

public class ChessTimer extends Thread {
    private int whitePlayerTime = 900;
    private int blackPlayerTime = 900;
    private Army currentPlayer = Army.WHITE;
    private final GameManager gameManager;

    private Timer timer;

    private final JLabel headerLabel;

    public ChessTimer(JLabel headerLabel, GameManager gameManager) {
        this.headerLabel = headerLabel;
        this.gameManager = gameManager;
    }

    /**
     * changes army from white to black or from black to white
     */
    public void changeCurrentPlayer() {
        if(currentPlayer==Army.WHITE) {
            currentPlayer = Army.BLACK;
        } else {
            currentPlayer = Army.WHITE;
        }
    }

    /**
     * starts new chess timer
     * every second it updates timer and text on GUI in format mm:ss
     * and checks whether it is the end of game - if it is, then it calls processEndOfGame method
     */
    @Override
    public void run() {
        timer = new Timer(1000, e -> {
            headerLabel.setText(
                    "<html><body>Human vs Human<br>Timer<br>w: "
                            + String.format("%d:%02d", whitePlayerTime / 60, whitePlayerTime % 60)
                            + "<br>b: " + String.format("%d:%02d", blackPlayerTime / 60, blackPlayerTime % 60)
                    + "</body></html>");
            if(currentPlayer==Army.WHITE) {
                whitePlayerTime--;
            } else {
                blackPlayerTime--;
            }
            if(whitePlayerTime<=0 || blackPlayerTime<=0) {
                timer.stop();
                gameManager.processEndOfGame(currentPlayer);
            }
        });
        this.timer.start();
    }

    /**
     * stops the timer
     */
    public void stopTimer() {
        this.timer.stop();
    }
}
