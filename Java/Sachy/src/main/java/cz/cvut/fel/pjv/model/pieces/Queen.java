package cz.cvut.fel.pjv.model.pieces;

import cz.cvut.fel.pjv.model.gameComponents.Army;
import cz.cvut.fel.pjv.model.gameComponents.GameBoard;
import cz.cvut.fel.pjv.model.gameComponents.Tile;
import cz.cvut.fel.pjv.model.gameComponents.move.BasicMove;
import cz.cvut.fel.pjv.model.gameComponents.move.Move;

import java.util.ArrayList;

public class Queen extends Piece{
    private final Army army;
    private int coordinate;
    private final int[] candidateMoves = {-9, -8, -7, -1, 1, 7, 8, 9};

    @Override
    public int getCoordinate() {
        return coordinate;
    }

    @Override
    public void setCoordinate(int coordinate) {
        this.coordinate = coordinate;
    }

    public Queen(Army army, int coordinate) {
        this.army = army;
        this.coordinate = coordinate;
    }

    @Override
    public ArrayList<Move> calculatePossibleMoves(GameBoard gameBoard) {
        ArrayList<Move> possibleMoves = new ArrayList<>();
        ArrayList<Tile> board = gameBoard.board;

        for (int candidateMove : candidateMoves) {
            int coordinateToResearch = this.coordinate + candidateMove;
            if (isQueenOnEdgeOfBoard(this.coordinate, candidateMove)) {
                continue;
            }
            while (true) {
                Piece occupierOnCandidateTile = board.get(coordinateToResearch).getOccupier();
                if (occupierOnCandidateTile!=null) {
                    if (occupierOnCandidateTile.getArmy() != this.army && !(occupierOnCandidateTile instanceof King)) {
                        possibleMoves.add(new BasicMove(this.coordinate, coordinateToResearch));
                    }
                    break;
                }
                if (isQueenOnEdgeOfBoard(coordinateToResearch, candidateMove)) {
                    possibleMoves.add(new BasicMove(this.coordinate, coordinateToResearch));
                    break;
                }
                possibleMoves.add(new BasicMove(this.coordinate, coordinateToResearch));
                coordinateToResearch+=candidateMove;
            }
        }
        possibleMoves = gameBoard.getMovesWithoutCheckIssue(possibleMoves, this.army);
        return possibleMoves;
    }

    private boolean isQueenOnEdgeOfBoard(int coordinateToResearch, int candidateMove) {
        if (coordinateToResearch+candidateMove > 63 || coordinateToResearch+candidateMove < 0) {
            return true;
        } else if (coordinateToResearch % 8 == 0 && (candidateMove == -9 || candidateMove == 7 || candidateMove == -1)) {
            return true;
        } else if (coordinateToResearch % 8 == 7 && (candidateMove == -7 || candidateMove == 9 || candidateMove == 1)) {
            return true;
        }
        return false;
    }

    @Override
    public Army getArmy() {
        return army;
    }

    @Override
    public String toString() {
        return this.army.toString().substring(0, 1).toLowerCase() + "q";
    }

    @Override
    public String toPGNString() {
        return "Q";
    }
}
