package cz.cvut.fel.pjv.model.pieces;

import cz.cvut.fel.pjv.model.gameComponents.Army;
import cz.cvut.fel.pjv.model.gameComponents.GameBoard;
import cz.cvut.fel.pjv.model.gameComponents.move.Move;

import java.io.Serializable;
import java.util.ArrayList;

public abstract class Piece implements Serializable {
    private Army army;
    private int coordinate;
    private int[] candidateMoves;

    /**
     * Returns all moves that the piece can do
     * @param gameBoard - gameBoard where the piece is
     * @return possible moves array
     */
    public abstract ArrayList<Move> calculatePossibleMoves(GameBoard gameBoard);

    public int getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(int coordinate) {
        this.coordinate = coordinate;
    }

    public Army getArmy() {
        return army;
    }

    /**
     * Returns name of a specific piece in PGN notation (i.e. N for knight)
     * @return string in PGN notation
     */
    public abstract String toPGNString();

}
