package cz.cvut.fel.pjv.model.gameComponents.move;

import cz.cvut.fel.pjv.model.gameComponents.GameBoard;
import cz.cvut.fel.pjv.model.pieces.Piece;


public class EnPassantMove extends Move {
    private final int fromCoordinate;
    private final int toCoordinate;
    private final int coordinateToRemove;

    public EnPassantMove(int fromCoordinate, int toCoordinate, int coordinateToRemove) {
        this.fromCoordinate = fromCoordinate;
        this.toCoordinate = toCoordinate;
        this.coordinateToRemove = coordinateToRemove;
    }

    @Override
    public void makeMove(GameBoard gameBoard) {
        Piece pieceToMove = gameBoard.board.get(fromCoordinate).getOccupier();

        gameBoard.board.get(fromCoordinate).setOccupier(null);
        gameBoard.board.get(toCoordinate).setOccupier(pieceToMove);
        pieceToMove.setCoordinate(toCoordinate);
        gameBoard.board.get(coordinateToRemove).setOccupier(null);

        gameBoard.madeMoves.add(this);
    }

    @Override
    public int getFromCoordinate() {
        return fromCoordinate;
    }

    @Override
    public int getToCoordinate() {
        return toCoordinate;
    }

    public int getCoordinateToRemove() {
        return coordinateToRemove;
    }
}
