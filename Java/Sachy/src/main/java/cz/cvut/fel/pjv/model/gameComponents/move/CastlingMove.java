package cz.cvut.fel.pjv.model.gameComponents.move;

import cz.cvut.fel.pjv.model.gameComponents.GameBoard;
import cz.cvut.fel.pjv.model.pieces.Piece;


public class CastlingMove extends Move {
    private final int fromCoordinate;
    private final int toCoordinate;
    private final int rookFromCoordinate;
    private final int rookToCoordinate;

    public CastlingMove(int fromCoordinate, int toCoordinate, int rookFromCoordinate, int rookToCoordinate) {
        this.fromCoordinate = fromCoordinate;
        this.toCoordinate = toCoordinate;
        this.rookFromCoordinate = rookFromCoordinate;
        this.rookToCoordinate = rookToCoordinate;
    }

    @Override
    public void makeMove(GameBoard gameBoard) {
        Piece kingToMove = gameBoard.board.get(fromCoordinate).getOccupier();
        Piece rookPieceToMove = gameBoard.board.get(rookFromCoordinate).getOccupier();

        gameBoard.board.get(fromCoordinate).setOccupier(null);
        gameBoard.board.get(toCoordinate).setOccupier(kingToMove);
        kingToMove.setCoordinate(toCoordinate);

        gameBoard.board.get(rookFromCoordinate).setOccupier(null);
        gameBoard.board.get(rookToCoordinate).setOccupier(rookPieceToMove);
        rookPieceToMove.setCoordinate(rookToCoordinate);

        gameBoard.madeMoves.add(this);
    }

    @Override
    public int getFromCoordinate() {
        return fromCoordinate;
    }

    @Override
    public int getToCoordinate() {
        return toCoordinate;
    }

    public int getRookFromCoordinate() {
        return rookFromCoordinate;
    }

    public int getRookToCoordinate() {
        return rookToCoordinate;
    }
}
