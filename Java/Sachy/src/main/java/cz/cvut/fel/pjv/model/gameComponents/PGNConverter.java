package cz.cvut.fel.pjv.model.gameComponents;

import cz.cvut.fel.pjv.model.gameComponents.move.*;
import cz.cvut.fel.pjv.model.pieces.Pawn;
import cz.cvut.fel.pjv.model.pieces.Piece;
import org.apache.commons.lang3.math.NumberUtils;

import java.util.ArrayList;

public class PGNConverter {
    /**
     * Converts the given input PGN string into array of moves
     * @param inputPGN - string in PGN format
     * @return array of moves
     */
    public ArrayList<Move> convertFromPGN(String inputPGN) {
        ArrayList<Move> resultMoves = new ArrayList<>();
        ArrayList<String> stringsArray = splitPGNStringToMoves(inputPGN);
        GameBoard testingGameBoard = new GameBoard();
        for(int i = 0; i<stringsArray.size(); i++) {
            stringsArray.set(i, removeUnnecessaryChars(stringsArray.get(i)));
            Move retrievedMove = constructMove(stringsArray.get(i), i%2, testingGameBoard);
            resultMoves.add(retrievedMove);
            retrievedMove.makeMove(testingGameBoard);
        }
        return resultMoves;
    }

    /**
     * splits PGN string to array of PGN moves (i.e. Qe4)
     * @param inputPGN - input PGN string
     * @return PGN string moves
     */
    public ArrayList<String> splitPGNStringToMoves(String inputPGN) {
        ArrayList<String> resultArray = new ArrayList<>();
        String[] PGNMoves = inputPGN.split(" ");
        for(int i = 0; i < PGNMoves.length; i++) {
            if(i%3==0) {
                continue;
            }
            resultArray.add(PGNMoves[i]);
        }
        return resultArray;
    }

    private String removeUnnecessaryChars(String stringToModify) {
        String resultString = stringToModify;
        if(resultString.charAt(resultString.length()-1)=='+' || resultString.charAt(resultString.length()-1)=='#') {
            resultString = resultString.substring(0, resultString.length()-1);
        } else if (resultString.charAt(resultString.length()-1)=='+'&&resultString.charAt(resultString.length()-2)=='+') {
            resultString = resultString.substring(0, resultString.length()-2);
        }
        resultString = resultString.replaceAll("=Q", "");
        resultString = resultString.replaceAll("x", "");
        return resultString;
    }

    private Move constructMove(String moveString, int currentPlayerIndex, GameBoard testingGameBoard) {
        if(constructCastlingMove(moveString, currentPlayerIndex)!=null) {
            return constructCastlingMove(moveString, currentPlayerIndex);
        }
        int fromCoordinate = calculateFromCoordinateOfMoveCandidate(moveString, currentPlayerIndex, testingGameBoard);
        int toCoordinate = calculateToCoordinateOfMoveCandidate(moveString);
        if((Math.abs(toCoordinate-fromCoordinate)==7 || Math.abs(toCoordinate-fromCoordinate)==9)
                && testingGameBoard.board.get(toCoordinate).getOccupier()==null
                && testingGameBoard.board.get(fromCoordinate).getOccupier() instanceof Pawn) {
            int armyCoefficient = currentPlayerIndex==0 ? 1 : -1;
            return new EnPassantMove(fromCoordinate, toCoordinate, toCoordinate-armyCoefficient*8);
        } else if (testingGameBoard.board.get(fromCoordinate).getOccupier() instanceof Pawn
                && ((toCoordinate<=7 && toCoordinate>=0) || (toCoordinate<=63 && toCoordinate>=56))) {
            System.out.println(testingGameBoard.board.get(fromCoordinate).getCoordinate());
            return new PromotingMove(fromCoordinate, toCoordinate);
        } else {
            return new BasicMove(fromCoordinate, toCoordinate);
        }
    }

    private int calculateFromCoordinateOfMoveCandidate(String moveString, int currentPlayerIndex, GameBoard testingGameBoard) {
        if(moveString.equals("O-O")||moveString.equals("O-O-O")) {
            return -1;
        }
        if((retrievePiecePGN(moveString).equals("")&&moveString.length()>2)||moveString.length()>3) {
            Piece firstCandidate = getFirstCandidate(moveString, currentPlayerIndex, testingGameBoard);
            Piece secondCandidate = getSecondCandidate(moveString, currentPlayerIndex, testingGameBoard, firstCandidate.getCoordinate());
            String differentialString = retrieveDifferentialString(moveString);
            char[] columnNames = new char[]{'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'};
            if(differentialString.length()==1 && secondCandidate!=null) {
                if(NumberUtils.isCreatable(differentialString)) {
                    //if the row matches
                    if(Math.floor(secondCandidate.getCoordinate()/8)==Integer.parseInt(differentialString)*(-1)+8) {
                        firstCandidate = secondCandidate;
                    }
                } else {
                    if(differentialString.equals(String.valueOf(columnNames[secondCandidate.getCoordinate()%8]))) {
                        firstCandidate = secondCandidate;
                    }
                }
            }
            return firstCandidate.getCoordinate();
        } else {
            Piece firstCandidate = getFirstCandidate(moveString, currentPlayerIndex, testingGameBoard);
            return firstCandidate.getCoordinate();
        }
    }


    private String retrieveDifferentialString(String moveString) {
        if(retrievePiecePGN(moveString).equals("")) {
            if(moveString.length()==3) {
                return moveString.substring(0, 1);
            } else {
                return moveString.substring(0, 2);
            }
        } else {
            if(moveString.length()==4) {
                return moveString.substring(1, 2);
            } else {
                return moveString.substring(1, 3);
            }
        }
    }

    private Piece getFirstCandidate(String moveString, int currentPlayerIndex, GameBoard testingGameBoard) {
        Army currentArmy = currentPlayerIndex == 0 ? Army.WHITE : Army.BLACK;
        int toCoordinate = calculateToCoordinateOfMoveCandidate(moveString);
        for (Tile tile : testingGameBoard.board) {
            if(tile.getOccupier()!=null && tile.getOccupier().getArmy()==currentArmy
                    && tile.getOccupier().toPGNString().equals(retrievePiecePGN(moveString))) {
                for(Move move : tile.getOccupier().calculatePossibleMoves(testingGameBoard)) {
                    if(move.getToCoordinate()==toCoordinate) {
                        return tile.getOccupier();
                    }
                }
            }
        }
        return null;
    }

    private Piece getSecondCandidate(String moveString, int currentPlayerIndex, GameBoard testingGameBoard, int firstCandidateCoordinate) {
        Army currentArmy = currentPlayerIndex == 0 ? Army.WHITE : Army.BLACK;
        int toCoordinate = calculateToCoordinateOfMoveCandidate(moveString);
        for (Tile tile : testingGameBoard.board) {
            if(tile.getOccupier()!=null && tile.getOccupier().getArmy()==currentArmy
                    && tile.getOccupier().toPGNString().equals(retrievePiecePGN(moveString))
                    && tile.getOccupier().getCoordinate()!=firstCandidateCoordinate) {
                for(Move move : tile.getOccupier().calculatePossibleMoves(testingGameBoard)) {
                    if(move.getToCoordinate()==toCoordinate) {
                        return tile.getOccupier();
                    }
                }
            }
        }
        return null;
    }

    private int calculateToCoordinateOfMoveCandidate(String moveString) {
        if(moveString.equals("O-O")||moveString.equals("O-O-O")) {
            return -1;
        }
        char[] columnNames = new char[]{'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'};
        int columnIndex = 0;
        for(int i = 0; i<columnNames.length; i++) {
            if(columnNames[i]==moveString.charAt(moveString.length()-2)) {
                columnIndex = i;
            }
        }
        int rowIndex = Character.getNumericValue(moveString.charAt(moveString.length()-1));
        return (-8*rowIndex + 64) + columnIndex;
    }

    private Move constructCastlingMove(String moveString, int currentPlayerIndex) {
        if(moveString.equals("O-O")) {
            if(currentPlayerIndex==0) {
                return new CastlingMove(60, 62, 63, 61);
            } else {
                return new CastlingMove(4, 6, 7, 5);
            }
        } else if(moveString.equals("O-O-O")) {
            if(currentPlayerIndex==0) {
                return new CastlingMove(60, 58, 56, 59);
            } else {
                return new CastlingMove(4, 2, 0, 3);
            }
        }
        return null;
    }

    private String retrievePiecePGN(String moveString) {
        char[] PGNCharacters = new char[]{'N', 'B', 'R', 'Q', 'K'};
        for (char character : PGNCharacters) {
            if(moveString.charAt(0)==character) {
                return String.valueOf(character);
            }
        }
        return "";
    }


    /**
     * Converts the given array of moves into PGN format
     * @param madeMoves - array of moves
     * @return string representing the PGN
     */
    public String convertToPGN(ArrayList<Move> madeMoves) {
        String resultString = "";
        GameBoard testingGameBoard = new GameBoard();
        for(int i=0; i<madeMoves.size(); i++) {
            Move moveToAnalyze = madeMoves.get(i);
            if(i%2==0) {
                resultString+=(i+2)/2 + ". ";
            }
            if(moveToAnalyze instanceof CastlingMove) {
                resultString+=stringifyCastleMove((CastlingMove) moveToAnalyze);
                moveToAnalyze.makeMove(testingGameBoard);
                if(testingGameBoard.isArmyInCheck(i%2==0 ? Army.BLACK : Army.WHITE)) {
                    resultString+="+";
                }
                continue;
            }
            Piece pieceToMove = testingGameBoard.board.get(moveToAnalyze.getFromCoordinate()).getOccupier();
            Piece secondCandidate = findSecondCandidate(testingGameBoard, moveToAnalyze);
            resultString+=pieceToMove.toPGNString();
            //specify piece position if 2 pieces can move on the tile
            if(secondCandidate!=null) {
                resultString+=differentialString(pieceToMove, secondCandidate);
            }
            if(testingGameBoard.board.get(moveToAnalyze.getToCoordinate()).getOccupier()!=null
                    || moveToAnalyze instanceof EnPassantMove) {
                resultString+="x";
            }
            resultString+= convertTileCoordinate(moveToAnalyze.getToCoordinate());
            if(moveToAnalyze instanceof PromotingMove) {
                resultString+="=Q";
            }
            moveToAnalyze.makeMove(testingGameBoard);
            resultString+= processCheck(testingGameBoard, i%2==0 ? Army.BLACK : Army.WHITE);
        }
        return resultString;
    }

    private String processCheck(GameBoard testingGameBoard, Army opponentPlayer) {
        String resultString = "";
        if(testingGameBoard.isArmyInCheck(opponentPlayer)) {
            if(testingGameBoard.canArmyMakeMove(opponentPlayer)) {
                resultString+="+ ";
            } else {
                resultString+="# ";
            }
        } else {
            resultString+=" ";
        }
        return resultString;
    }
    private String convertTileCoordinate(int tileCoordinate) {
        return getColumnOfCoordinate(tileCoordinate) + getRowOfCoordinate(tileCoordinate) + "";
    }

    private String stringifyCastleMove(CastlingMove castlingMove) {
        if((castlingMove).getRookFromCoordinate()==7
                ||(castlingMove).getRookFromCoordinate()==63) {
            return "O-O ";
        } else {
            return "O-O-O ";
        }
    }

    //i.e. tile with coordinate 25 is on b5 -> 5th row
    private int getRowOfCoordinate(int tileCoordinate) {
        return ((int)Math.floor(tileCoordinate/8)+1)*(-1) + 9;
    }

    private String getColumnOfCoordinate(int tileCoordinate) {
        char[] columnNames = new char[]{'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'};
        return String.valueOf(columnNames[tileCoordinate%8]);
    }

    private Piece findSecondCandidate(GameBoard testingGameBoard, Move moveToAnalyze) {
        Piece pieceToMove = testingGameBoard.board.get(moveToAnalyze.getFromCoordinate()).getOccupier();
        for (Tile tile : testingGameBoard.board) {
            Piece secondCandidate = tile.getOccupier();
            if(secondCandidate!=null
                    && secondCandidate.getArmy()==pieceToMove.getArmy()
                    && secondCandidate.getClass().equals(pieceToMove.getClass())
                    && secondCandidate.getCoordinate()!=pieceToMove.getCoordinate()) {
                for(Move otherPieceCandidateMove : secondCandidate.calculatePossibleMoves(testingGameBoard)) {
                    if(otherPieceCandidateMove.getToCoordinate()== moveToAnalyze.getToCoordinate()) {
                        return secondCandidate;
                    }
                }
            }
        }
        return null;
    }
    private String differentialString(Piece pieceToMove, Piece secondCandidate) {
        String resultString = "";
        if(pieceToMove.getCoordinate()%8==secondCandidate.getCoordinate()%8) {
            resultString+=String.valueOf(getRowOfCoordinate(pieceToMove.getCoordinate()));
        } else if(getRowOfCoordinate(pieceToMove.getCoordinate())==getRowOfCoordinate(secondCandidate.getCoordinate())) {
            resultString+=getColumnOfCoordinate(pieceToMove.getCoordinate());
        } else {
            resultString+=getColumnOfCoordinate(pieceToMove.getCoordinate());
        }
        return resultString;
    }
}
