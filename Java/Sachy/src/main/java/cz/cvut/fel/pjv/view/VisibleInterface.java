package cz.cvut.fel.pjv.view;

import cz.cvut.fel.pjv.controller.GameManager;
import cz.cvut.fel.pjv.model.gameComponents.*;
import cz.cvut.fel.pjv.model.gameComponents.move.BasicMove;
import cz.cvut.fel.pjv.model.gameComponents.move.Move;
import cz.cvut.fel.pjv.model.pieces.King;
import cz.cvut.fel.pjv.model.pieces.Piece;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import static javax.swing.SwingUtilities.*;

public class VisibleInterface extends javax.swing.JFrame {

    private final static Logger LOGGER = Logger.getLogger(VisibleInterface.class.getName());

    private final JFrame gameFrame;
    private final BoardPanel boardPanel;
    private final SidePanel sidePanel;
    private GameBoard gameboard;
    private final DBManager dbManager = new DBManager();
    private final ComputerPlayer computerPlayer = new ComputerPlayer(Army.BLACK, Difficulty.MEDIUM);
    private final GameManager gameManager = new GameManager(false);
    private int PGNPointer = 0;
    private String inputPGN;

    private Tile sourceTile;
    private Tile destinationTile;
    private Piece humanMovedPiece;

    private final static Dimension OUTER_FRAME_DIMENSION = new Dimension(660, 600);
    //private final static Dimension BOARD_PANEL_DIMENSION = new Dimension(400, 350);
    private final static Dimension TILE_PANEL_DIMENSION = new Dimension(10, 10);

    private final Color lightTileColor = Color.decode("#ede2e1");
    private final Color darkTileColor = Color.decode("#6c7456");
    private final Color lightMoveCandidateColor = Color.decode("#b9ce9b");
    private final Color darkMoveCandidateColor = Color.decode("#8f9c67");

    /**
     * creates chessboard and sets the important attributes such as
     * gameBoard which boardPanel paints
     * sidePanel, size of gameFrame, etc.
     * @throws HeadlessException
     */
    public VisibleInterface() throws HeadlessException {
        this.gameFrame = new JFrame("Chess");
        this.gameFrame.setLayout(new BorderLayout());
        this.gameFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        final JMenuBar jMenuBar = createMenuBar();
        this.gameFrame.setJMenuBar(jMenuBar);
        this.gameFrame.setSize(OUTER_FRAME_DIMENSION);
        this.gameboard = new GameBoard();

        this.boardPanel = new BoardPanel();
        this.sidePanel = new SidePanel(gameManager, this);
        this.sidePanel.getHeaderLabel().setText("<html><body>Human vs PC<br>Difficulty: "
                + computerPlayer.getDifficulty() + "</body></html>");
        this.gameFrame.add(this.boardPanel, BorderLayout.CENTER);
        this.gameFrame.add(this.sidePanel, BorderLayout.EAST);
        this.gameFrame.setVisible(true);
    }

    /**
     * processes a move in PGN mode
     */
    public void nextPGN() {
        if(PGNPointer<=gameboard.madeMoves.size()-1) {
            gameboard.madeMoves.get(PGNPointer).makeMove(gameboard);
            gameboard.madeMoves.remove(gameboard.madeMoves.size()-1);
            sidePanel.getHeaderLabel().setText("<html><body>PGN<br>current move:<br>" + (PGNPointer+1) + ". "
                    + new PGNConverter().splitPGNStringToMoves(inputPGN).get(PGNPointer) +" </body></html>");
            PGNPointer++;
            boardPanel.drawBoard(gameboard.board);
        }
    }

    /**
     * takes the state of game one move back in PGN mode
     */
    public void previousPGN() {
        if(PGNPointer>0) {
            PGNPointer--;
            gameboard.prepareNewBoard();
            for(int i = 0; i<PGNPointer; i++) {
                gameboard.madeMoves.get(i).makeMove(gameboard);
                gameboard.madeMoves.remove(gameboard.madeMoves.size()-1);
            }
            sidePanel.getHeaderLabel().setText("<html><body>PGN<br>current move:<br>" + (PGNPointer+1) + ". "
                    + new PGNConverter().splitPGNStringToMoves(inputPGN).get(PGNPointer) +" </body></html>");
            boardPanel.drawBoard(gameboard.board);
        }
    }

    /**
     * takes the state of game to the beginning in PGN mode
     */
    public void firstPGN() {
        gameboard.prepareNewBoard();
        PGNPointer = 0;
        sidePanel.getHeaderLabel().setText("<html><body>PGN<br>current move:<br>" + (PGNPointer+1) + ". "
                + new PGNConverter().splitPGNStringToMoves(inputPGN).get(PGNPointer) +" </body></html>");
        boardPanel.drawBoard(gameboard.board);
    }

    /**
     * takes the state of game to the end in PGN mode (makes all PGN moves)
     */
    public void lastPGN() {
        while (PGNPointer<=gameboard.madeMoves.size()-1) {
            gameboard.madeMoves.get(PGNPointer).makeMove(gameboard);
            gameboard.madeMoves.remove(gameboard.madeMoves.size()-1);
            PGNPointer++;
        }
        sidePanel.getHeaderLabel().setText("<html><body>PGN<br>current move:<br>" + (PGNPointer+1) + ". "
                + new PGNConverter().splitPGNStringToMoves(inputPGN).get(PGNPointer-1) +" </body></html>");
                boardPanel.drawBoard(gameboard.board);
    }

    private JMenuBar createMenuBar() {
        final JMenuBar jMenuBar = new JMenuBar();
        jMenuBar.add(createNewGameMenu());
        JMenu loadGameMenu = createLoadGameMenu();
        jMenuBar.add(loadGameMenu);
        jMenuBar.add(createOptionsMenu(loadGameMenu));

        return jMenuBar;
    }

    private JMenu createNewGameMenu() {
        final JMenu menuComponent = new JMenu("New game");
        final JMenuItem newGameVsComputer = new JMenuItem("New game - Human vs PC");
        final JMenuItem newGameVsHuman = new JMenuItem("New game - Human vs Human");
        final JMenuItem customGame = new JMenuItem("Custom game - Human vs Human");
        final JMenuItem loadPGN = new JMenuItem("Load PGN");
        newGameVsComputer.addActionListener(e -> {
            setNewGameProperties();
            sidePanel.hideStartGameButton();
            sidePanel.getHeaderLabel().setText("<html><body>Human vs PC<br>Difficulty: "
                    + computerPlayer.getDifficulty() + "</body></html>");
            gameManager.setInCustomMode(false);
            gameboard.setHumanVsComputer(true);
            gameManager.stopChessClock();
            boardPanel.drawBoard(gameboard.board);
        });
        newGameVsHuman.addActionListener(e -> {
            setNewGameProperties();
            sidePanel.hideStartGameButton();
            gameManager.setInCustomMode(false);
            gameboard.setHumanVsComputer(false);
            gameManager.startChessClock(sidePanel.getHeaderLabel());
            boardPanel.drawBoard(gameboard.board);
        });
        customGame.addActionListener(e -> {
            setNewGameProperties();
            sidePanel.showStartGameButton();
            sidePanel.getHeaderLabel().setText("Custom mode");
            gameManager.setInCustomMode(true);
            gameboard.setHumanVsComputer(false);
            gameManager.stopChessClock();
            boardPanel.drawBoard(gameboard.board);
        });
        loadPGN.addActionListener(e -> {
            String loadedPGN = JOptionPane.showInputDialog("Enter your PGN");
            if (loadedPGN!=null) {
                try {
                    PGNPointer = 0;
                    ArrayList<Move> moves = new PGNConverter().convertFromPGN(loadedPGN);
                    if(moves.size()==0) {
                        throw new NullPointerException();
                    }
                    gameboard = new GameBoard();
                    gameboard.madeMoves = moves;
                    sidePanel.showPGNPanel();
                    sidePanel.hideStartGameButton();
                    sidePanel.getHeaderLabel().setText("PGN");
                    inputPGN = loadedPGN;
                    gameManager.setInPGNMode(true);
                    boardPanel.drawBoard(gameboard.board);

                } catch (NullPointerException ex) {
                    JOptionPane.showMessageDialog(null, "Incorrect PGN");
                    LOGGER.info("Could not parse PGN input");
                }
            }

        });
        menuComponent.add(newGameVsComputer);
        menuComponent.add(newGameVsHuman);
        menuComponent.add(customGame);
        menuComponent.add(loadPGN);
        return menuComponent;
    }

    private void setNewGameProperties() {
        gameboard = new GameBoard();
        sidePanel.hidePGNPanel();
        gameManager.setGameFinished(false);
        gameManager.setInPGNMode(false);
    }

    private JMenu createLoadGameMenu() {
        final JMenu menuComponent = new JMenu("Load game");
        for (GameBoard game : dbManager.getGames()) {
            JMenuItem loadGame = new JMenuItem(game.getGameBoardName());
            loadGame.addActionListener(e -> {
                gameboard = game;
                setLoadGameSettings();
            });
            menuComponent.add(loadGame);
        }

        return menuComponent;
    }

    private JMenu createOptionsMenu(JMenu loadGameMenuComponent) {
        final JMenu menuComponent = new JMenu("Options");
        final JMenuItem saveGame = new JMenuItem("Save game");
        saveGame.addActionListener(e -> {
            while (true) {
                String nameOfGame = JOptionPane.showInputDialog("Give your game a name");
                if (nameOfGame==null) {
                    break;
                } else if(!nameOfGame.equals("")) {
                    gameboard.setGameBoardName(nameOfGame);
                    JOptionPane.showMessageDialog(null, "Game was successfully saved");
                    dbManager.saveGame(gameboard);
                    JMenuItem additionalLoadGameItem = new JMenuItem(nameOfGame);
                    additionalLoadGameItem.addActionListener(e1 -> {
                        gameboard = dbManager.getGames().get(dbManager.getGames().size()-1);
                        setLoadGameSettings();
                    });
                    loadGameMenuComponent.add(additionalLoadGameItem);
                    break;
                }
            }
        });
        final JMenuItem removeGamesFromFile = new JMenuItem("Remove existing games");
        removeGamesFromFile.addActionListener(e -> {
            dbManager.clearAllGames();
            loadGameMenuComponent.removeAll();
            JOptionPane.showMessageDialog(null, "All games have been removed");
        });
        final JMenuItem getPGN = new JMenuItem("Generate PGN moves of this game");
        getPGN.addActionListener(e -> JOptionPane.showInputDialog("Your PGN:", gameboard.getPGN()));
        menuComponent.add(saveGame);
        menuComponent.add(removeGamesFromFile);
        menuComponent.add(getPGN);
        menuComponent.add(createOptionsSubMenu());
        return menuComponent;
    }

    private void setLoadGameSettings() {
        gameManager.setInCustomMode(false);
        resetPicks();
        boardPanel.drawBoard(gameboard.board);
        sidePanel.hideStartGameButton();
        sidePanel.hidePGNPanel();
        gameManager.stopChessClock();
        gameManager.setGameFinished(false);
        gameManager.setInPGNMode(false);
        sidePanel.getHeaderLabel().setText(!gameboard.isHumanVsComputer() ? "Human vs Human" : "<html><body>" +
                "Human vs PC<br>Difficulty: " + computerPlayer.getDifficulty() + "</body></html>");
        if(!gameboard.isHumanVsComputer()) {
            gameManager.startChessClock(sidePanel.getHeaderLabel());
        }
    }

    private JMenu createOptionsSubMenu() {
        final JMenu chooseDifficulty = new JMenu("Choose computer difficulty");
        final JMenuItem easy = new JMenuItem("easy");
        easy.addActionListener(e -> {
            computerPlayer.setDifficulty(Difficulty.EASY);
            updateInfoText();
        });
        final JMenuItem medium = new JMenuItem("medium");
        medium.addActionListener(e -> {
            computerPlayer.setDifficulty(Difficulty.MEDIUM);
            updateInfoText();
        });
        final JMenuItem semiHard = new JMenuItem("semi-hard");
        semiHard.addActionListener(e -> {
            computerPlayer.setDifficulty(Difficulty.SEMIHARD);
            updateInfoText();
        });

        chooseDifficulty.add(easy);
        chooseDifficulty.add(medium);
        chooseDifficulty.add(semiHard);
        return chooseDifficulty;
    }

    private void updateInfoText() {
        if(gameboard.isHumanVsComputer()&&!gameManager.isInCustomMode()&&!gameManager.isGameFinished()) {
            sidePanel.getHeaderLabel().setText("<html><body>Human vs PC<br>Difficulty: "
                    + computerPlayer.getDifficulty() + "</body></html>");
        }
    }

    private void resetPicks() {
        sourceTile = null;
        destinationTile = null;
        humanMovedPiece = null;
    }


    //--------------------------------------------------------------------//

    private class BoardPanel extends JPanel {
        final List<TilePanel> boardTiles;

        BoardPanel() {
            super(new GridLayout(8, 8));
            this.boardTiles = new ArrayList<>();
            for (int i = 0; i < 64; i++) {
                final TilePanel tilePanel = new TilePanel(this, i);
                this.boardTiles.add(tilePanel);
                add(tilePanel);
            }
            setPreferredSize(OUTER_FRAME_DIMENSION);
            validate();
        }
        private void drawBoard(final ArrayList<Tile> board) {
            removeAll();
            for(final TilePanel tilePanel : boardTiles) {
                tilePanel.drawTile(board);
                add(tilePanel);
            }

            validate();
            repaint();
        }
    }

    //------------------------------------------------------------------------//

    private class TilePanel extends JPanel {
        private final int tileId;

        TilePanel(final BoardPanel boardPanel, final int tileId) {
            super(new GridLayout());
            this.tileId = tileId;
            setPreferredSize(TILE_PANEL_DIMENSION);
            assignTileColor(lightTileColor, darkTileColor);
            assignTilePieceIcon(gameboard.board);

            addMouseListener(new MouseListener() {
                @Override
                public void mouseClicked(final MouseEvent e) {
                    if (isRightMouseButton(e)) {
                        resetPicks();
                    } else if(isLeftMouseButton(e)) {
                        if(sourceTile == null) {
                            sourceTile = gameboard.board.get(tileId);
                            LOGGER.finest("tile clicked: " + tileId + ", current player: " + gameboard.currentPlayer);
                            humanMovedPiece = sourceTile.getOccupier();
                            if((humanMovedPiece == null || humanMovedPiece.getArmy()!=gameboard.currentPlayer)
                                    && !gameManager.isInCustomMode()
                                    || gameManager.isGameFinished() || gameManager.isInPGNMode()) {
                                sourceTile = null;
                            }
                        } else {
                            destinationTile = gameboard.board.get(tileId);
                            if(gameManager.isInCustomMode()) {
                                if(!(destinationTile.getOccupier() instanceof King)) {
                                    new BasicMove(sourceTile.getCoordinate()
                                            , destinationTile.getCoordinate()).makeMove(gameboard);
                                }
                                resetPicks();
                            } else {
                                processHumanMove(tileId);

                                if (!gameboard.canArmyMakeMove(gameboard.currentPlayer)) {
                                    processEndOfGame();
                                } else if (gameboard.isHumanVsComputer() && gameboard.currentPlayer == Army.BLACK) {
                                    computerPlayer.makeBestMove(gameboard);
                                    gameboard.currentPlayer = changeArmy(gameboard.currentPlayer);
                                    if (!gameboard.canArmyMakeMove(gameboard.currentPlayer)) {
                                        processEndOfGame();
                                    }
                                }
                            }
                        }
                    }
                    SwingUtilities.invokeLater(() -> boardPanel.drawBoard(gameboard.board));
                }

                @Override
                public void mousePressed(final MouseEvent e) {}

                @Override
                public void mouseReleased(final MouseEvent e) {}

                @Override
                public void mouseEntered(final MouseEvent e) {}

                @Override
                public void mouseExited(final MouseEvent e) {}
            });
            validate();
        }

        //Helping functions
        //----------------------------------------------------------//

        private Army changeArmy(Army army) {
            return army == Army.BLACK ? Army.WHITE : Army.BLACK;
        }

        private void drawTile(final ArrayList<Tile> board) {
            assignTileColor(lightTileColor, darkTileColor);
            assignTilePieceIcon(board);
            highlightPossibleMoves();
            validate();
            repaint();
        }

        private void assignTilePieceIcon(final ArrayList<Tile> board) {
            this.removeAll();
            if(board.get(this.tileId).getOccupier()!=null) {
                try {
                    final BufferedImage image = ImageIO.read(getClass().getResource("/"
                            + board.get(this.tileId).getOccupier().toString() + ".gif"));
                    add(new JLabel(new ImageIcon(image)));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }


        private void assignTileColor(Color lightTileColor, Color darkTileColor) {
            int rowCoefficient = (int) ((Math.floor(this.tileId/8))%2);
            if ((this.tileId+rowCoefficient) % 2 == 0) {
                setBackground(lightTileColor);
            }
            else {
                setBackground(darkTileColor);
            }
        }
        private Move findCorrespondingMove(ArrayList<Move> possibleMoves, int fromCoordinate, int toCoordinate) {
            for (Move move : possibleMoves) {
                if (move.getFromCoordinate()==fromCoordinate && move.getToCoordinate()==toCoordinate) {
                    return move;
                }
            }
            return null;
        }
        private void highlightPossibleMoves() {
            if (humanMovedPiece!=null) {
                if(gameManager.isInCustomMode()) {
                    for(TilePanel ignored : boardPanel.boardTiles) {
                        assignTileColor(lightMoveCandidateColor, darkMoveCandidateColor);
                    }
                } else {
                    if(humanMovedPiece.getArmy()==gameboard.currentPlayer) {
                        ArrayList<Move> movesToHighlight = humanMovedPiece.calculatePossibleMoves(gameboard);
                        for (Move move : movesToHighlight) {
                            if(move.getToCoordinate()==this.tileId) {
                                assignTileColor(lightMoveCandidateColor, darkMoveCandidateColor);
                            }
                        }
                    }
                }
            }
        }

        private void processHumanMove(final int tileId) {
            if(!gameManager.isGameFinished()) {
                LOGGER.info("tried to move from tile: " + sourceTile.getCoordinate() + " to: "
                        + destinationTile.getCoordinate());
            }
            ArrayList<Move> possibleMoves = humanMovedPiece.calculatePossibleMoves(gameboard);
            Move moveToMake = findCorrespondingMove(possibleMoves, sourceTile.getCoordinate()
                    , destinationTile.getCoordinate());
            if (moveToMake!=null) {
                moveToMake.makeMove(gameboard);
                gameboard.currentPlayer = changeArmy(gameboard.currentPlayer);
            }
            resetPicks();
            if(!gameboard.isHumanVsComputer()) {
                gameManager.changeArmyStopWatch();
            }
        }

        private void processEndOfGame() {
            gameManager.setGameFinished(true);
            gameManager.stopChessClock();
            boardPanel.drawBoard(gameboard.board);
            if(gameboard.isArmyInCheck(gameboard.currentPlayer)) {
                JOptionPane.showMessageDialog(null,
                        changeArmy(gameboard.currentPlayer) + " WINS!");
            } else {
                JOptionPane.showMessageDialog(null, "It is a draw");
            }
            resetPicks();
        }
    }
}
