package cz.cvut.fel.pjv.model.pieces;

import cz.cvut.fel.pjv.model.gameComponents.Army;
import cz.cvut.fel.pjv.model.gameComponents.GameBoard;
import cz.cvut.fel.pjv.model.gameComponents.Tile;
import cz.cvut.fel.pjv.model.gameComponents.move.BasicMove;
import cz.cvut.fel.pjv.model.gameComponents.move.EnPassantMove;
import cz.cvut.fel.pjv.model.gameComponents.move.Move;
import cz.cvut.fel.pjv.model.gameComponents.move.PromotingMove;

import java.util.ArrayList;

public class Pawn extends Piece{
    private final Army army;
    private int coordinate;
    private final int[] candidateMoves = {7, 8, 9, 16};

    @Override
    public int getCoordinate() {
        return coordinate;
    }

    @Override
    public void setCoordinate(int coordinate) {
        this.coordinate = coordinate;
    }

    public Pawn(Army army, int coordinate) {
        this.army = army;
        this.coordinate = coordinate;
    }

    @Override
    public ArrayList<Move> calculatePossibleMoves(GameBoard gameBoard) {
        ArrayList<Move> possibleMoves = new ArrayList<>();
        ArrayList<Tile> board = gameBoard.board;
        int armyCoefficient = this.army == Army.BLACK ? 1 : -1;

        for (int candidateMove : candidateMoves) {
            int coordinateToResearch = this.coordinate + armyCoefficient*candidateMove;
            if (coordinateToResearch < 0 || coordinateToResearch > 63) {
                continue;
            }
            //promoting moves
            if ((isPieceOn2ndRow() && this.army == Army.WHITE) || (isPieceOn7thRow() && this.army == Army.BLACK)) {
                if ((candidateMove==7 || candidateMove==9) && isAttackableCoordinate(coordinateToResearch, gameBoard)) {
                    possibleMoves.add(new PromotingMove(this.coordinate, coordinateToResearch));
                } else if (candidateMove==8 && board.get(coordinateToResearch).getOccupier()==null) {
                    possibleMoves.add(new PromotingMove(this.coordinate, coordinateToResearch));
                }
                continue;
            }
            //attacking moves
            if ((candidateMove == 7 || candidateMove == 9)) {
                if (isAttackableCoordinate(coordinateToResearch, gameBoard)) {
                    possibleMoves.add(new BasicMove(this.coordinate, coordinateToResearch));
                } else if (isEnPassantableCoordinate(coordinateToResearch, armyCoefficient, gameBoard)) {
                    possibleMoves.add(new EnPassantMove(this.coordinate, coordinateToResearch,
                            coordinateToResearch - armyCoefficient*8));
                }
                continue;
            } //moves forward
            else if (candidateMove == 16
                    && (isPieceOn2ndRow() || isPieceOn7thRow())
                    && board.get(this.coordinate+armyCoefficient*8).getOccupier()==null
                    && board.get(coordinateToResearch).getOccupier()==null) {
                possibleMoves.add(new BasicMove(this.coordinate, coordinateToResearch));
            } else if (candidateMove == 8 && board.get(coordinateToResearch).getOccupier()==null) {
                possibleMoves.add(new BasicMove(this.coordinate, coordinateToResearch));
            }
        }
        possibleMoves = gameBoard.getMovesWithoutCheckIssue(possibleMoves, this.army);
        return possibleMoves;
    }

    private boolean isPieceOn2ndRow() {
        return this.coordinate >= 8 && this.coordinate <=15;
    }
    private boolean isPieceOn7thRow() {
        return this.coordinate>=48 && this.coordinate<=55;
    }
    private boolean isCandidateNotOutOfBoard(int coordinateToResearch) {
        return !(this.coordinate%8==0&&coordinateToResearch%8==7 || this.coordinate%8==7&&coordinateToResearch%8==0);
    }
    private boolean isAttackableCoordinate(int coordinateToResearch, GameBoard gameBoard) {
        Piece occupierOnCandidateTile = gameBoard.board.get(coordinateToResearch).getOccupier();
        return occupierOnCandidateTile!=null
                && occupierOnCandidateTile.getArmy()!=this.army
                && isCandidateNotOutOfBoard(coordinateToResearch)
                && !(occupierOnCandidateTile instanceof King);
    }

    private boolean isEnPassantableCoordinate(int coordinateToResearch, int armyCoefficient, GameBoard gameBoard) {
        Piece enPassantVictim = gameBoard.board.get(coordinateToResearch - armyCoefficient*8).getOccupier();
        Move lastGameBoardMove = null;
        if(gameBoard.madeMoves.size()!=0) {
            lastGameBoardMove = gameBoard.madeMoves.get(gameBoard.madeMoves.size()-1);
        }
        return gameBoard.board.get(coordinateToResearch).getOccupier() == null
                && enPassantVictim instanceof Pawn
                && enPassantVictim.getArmy() != this.army
                && lastGameBoardMove!=null
                && lastGameBoardMove.getFromCoordinate()==enPassantVictim.getCoordinate()+armyCoefficient*16
                && lastGameBoardMove.getToCoordinate()==enPassantVictim.getCoordinate();
    }

    @Override
    public Army getArmy() {
        return army;
    }


    @Override
    public String toString() {
        return this.army.toString().substring(0, 1).toLowerCase() + "p";
    }

    @Override
    public String toPGNString() {
        return "";
    }
}
