package cz.cvut.fel.pjv.model.pieces;

import cz.cvut.fel.pjv.model.gameComponents.Army;
import cz.cvut.fel.pjv.model.gameComponents.GameBoard;
import cz.cvut.fel.pjv.model.gameComponents.Tile;
import cz.cvut.fel.pjv.model.gameComponents.move.BasicMove;
import cz.cvut.fel.pjv.model.gameComponents.move.CastlingMove;
import cz.cvut.fel.pjv.model.gameComponents.move.Move;

import java.util.ArrayList;


public class King extends Piece{
    private final Army army;
    private int coordinate;
    private final int[] candidateMoves = {-9, -8, -7, -1, 1, 7, 8, 9};
    private boolean alreadyMoved;

    @Override
    public int getCoordinate() {
        return coordinate;
    }

    @Override
    public void setCoordinate(int coordinate) {
        this.coordinate = coordinate;
    }

    public King(Army army, int coordinate) {
        this.army = army;
        this.coordinate = coordinate;
        this.alreadyMoved = false;
    }

    @Override
    public ArrayList<Move> calculatePossibleMoves(GameBoard gameBoard) {
        ArrayList<Move> possibleMoves = new ArrayList<>();
        ArrayList<Tile> board = gameBoard.board;

        for (int candidateMove : candidateMoves) {
            int coordinateToResearch = this.coordinate + candidateMove;
            if (isCandidateOutOfBoard(coordinateToResearch, candidateMove)) {
                continue;
            }
            if (board.get(coordinateToResearch).getOccupier()!= null &&
                    board.get(coordinateToResearch).getOccupier().getArmy()==this.army) {
                continue;
            }
            possibleMoves.add(new BasicMove(this.coordinate, coordinateToResearch));
        }
        possibleMoves.addAll(calculateCastlingMoves(board));
        possibleMoves = gameBoard.getMovesWithoutCheckIssue(possibleMoves, this.army);
        return possibleMoves;
    }

    private boolean isCandidateOutOfBoard(int coordinateToResearch, int candidate) {
        if (coordinateToResearch < 0 || coordinateToResearch > 63) {
            return true;
        }

        if (this.coordinate % 8 == 0 && (candidate==-9 || candidate==-1 || candidate==7)) {
            return true;
        } else if (this.coordinate % 8 == 7 && (candidate==-7 || candidate==1 || candidate==9)) {
            return true;
        }
        return false;
    }

    private ArrayList<Move> calculateCastlingMoves(ArrayList<Tile> board) {
        ArrayList<Move> castlingMoves = new ArrayList<>();

        if (this.army==Army.BLACK && !this.alreadyMoved) {
            if (board.get(0).getOccupier() instanceof Rook
                    && !((Rook) board.get(0).getOccupier()).isAlreadyMoved()
                    && areEmptyTilesBetweenCoordinates(0, 4, board)) {
                castlingMoves.add(new CastlingMove(4, 2, 0, 3));
            }
            if (board.get(7).getOccupier() instanceof Rook
                    && !((Rook) board.get(7).getOccupier()).isAlreadyMoved()
                    && areEmptyTilesBetweenCoordinates(4, 7, board)) {
                castlingMoves.add(new CastlingMove(4, 6, 7, 5));
            }
        }

        if (this.army==Army.WHITE && !this.alreadyMoved) {
            if (board.get(56).getOccupier() instanceof Rook
                    && !((Rook) board.get(56).getOccupier()).isAlreadyMoved()
                    && areEmptyTilesBetweenCoordinates(56, 60, board)) {
                castlingMoves.add(new CastlingMove(60, 58, 56, 59));
            }
            if (board.get(63).getOccupier() instanceof Rook
                    && !((Rook) board.get(63).getOccupier()).isAlreadyMoved()
                    && areEmptyTilesBetweenCoordinates(60, 63, board)) {
                castlingMoves.add(new CastlingMove(60, 62, 63, 61));
            }
        }
        return castlingMoves;
    }

    private boolean areEmptyTilesBetweenCoordinates(int coordinate1, int coordinate2, ArrayList<Tile> board) {
        for (int i = coordinate1+1; i<coordinate2; i++) {
            if (board.get(i).getOccupier()!=null) {
                return false;
            }
        }
        return true;
    }

    @Override
    public Army getArmy() {
        return army;
    }

    public boolean isAlreadyMoved() {
        return alreadyMoved;
    }

    public void setAlreadyMoved(boolean alreadyMoved) {
        this.alreadyMoved = alreadyMoved;
    }

    @Override
    public String toString() {
        return this.army.toString().substring(0, 1).toLowerCase() + "k";
    }

    @Override
    public String toPGNString() {
        return "K";
    }
}
