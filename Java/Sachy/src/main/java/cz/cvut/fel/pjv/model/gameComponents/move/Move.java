package cz.cvut.fel.pjv.model.gameComponents.move;

import cz.cvut.fel.pjv.model.gameComponents.GameBoard;

import java.io.Serializable;


public abstract class Move implements Serializable {
    private int fromCoordinate;
    private int toCoordinate;

    /**
     * Makes a move on a given gameBoard by overwriting tiles’ occupiers
     * and adds the move on gameBoard history of moves (arrayList madeMoves)
     * In case of PromotingMove changes pawn into queen
     * @param gameBoard
     */
    public abstract void makeMove(GameBoard gameBoard);

    public int getFromCoordinate() {
        return fromCoordinate;
    }

    public int getToCoordinate() {
        return toCoordinate;
    }
}
