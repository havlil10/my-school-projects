package cz.cvut.fel.pjv.model.pieces;

import cz.cvut.fel.pjv.model.gameComponents.Army;
import cz.cvut.fel.pjv.model.gameComponents.GameBoard;
import cz.cvut.fel.pjv.model.gameComponents.Tile;
import cz.cvut.fel.pjv.model.gameComponents.move.BasicMove;
import cz.cvut.fel.pjv.model.gameComponents.move.Move;

import java.util.ArrayList;

public class Knight extends Piece{
    private final Army army;
    private int coordinate;
    private final int[] candidateMoves = {-17, -15, -10, -6, 6, 10, 15, 17};

    @Override
    public int getCoordinate() {
        return coordinate;
    }

    @Override
    public void setCoordinate(int coordinate) {
        this.coordinate = coordinate;
    }

    public Knight(Army army, int coordinate) {
        this.army = army;
        this.coordinate = coordinate;
    }

    @Override
    public ArrayList<Move> calculatePossibleMoves(GameBoard gameBoard) {
        ArrayList<Move> possibleMoves = new ArrayList<>();
        ArrayList<Tile> board = gameBoard.board;


        for (int candidateMove : candidateMoves) {
            int coordinateToResearch = this.coordinate + candidateMove;
            if (isCandidateOutOfBoard(coordinateToResearch, candidateMove)) {
                continue;
            }
            Piece occupierOnCandidateTile = board.get(coordinateToResearch).getOccupier();
            if (occupierOnCandidateTile!= null
                    && (occupierOnCandidateTile.getArmy()==this.army || occupierOnCandidateTile instanceof King)) {
                continue;
            }
            possibleMoves.add(new BasicMove(this.coordinate, coordinateToResearch));
            possibleMoves = gameBoard.getMovesWithoutCheckIssue(possibleMoves, this.army);
        }
        return possibleMoves;
    }

    private boolean isCandidateOutOfBoard(int coordinateToResearch, int candidate) {
        if (coordinateToResearch < 0 || coordinateToResearch > 63) {
            return true;
        }

        if (this.coordinate % 8 == 0 && (candidate==-17 || candidate==-10 || candidate==6 || candidate==15)) {
            return true;
        } else if (this.coordinate % 8 == 1 && (candidate==-10 || candidate==6)) {
            return true;
        } else if (this.coordinate % 8 == 6 && (candidate==-6 || candidate==10)) {
            return true;
        } else if (this.coordinate % 8 == 7 && (candidate==-15 || candidate==-6 || candidate==10 || candidate==17)) {
            return true;
        }
        return false;
    }

    @Override
    public Army getArmy() {
        return army;
    }

    @Override
    public String toString() {
        return this.army.toString().substring(0, 1).toLowerCase() + "n";
    }

    @Override
    public String toPGNString() {
        return "N";
    }
}
