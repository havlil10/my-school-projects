package cz.cvut.fel.pjv.model.pieces;

import cz.cvut.fel.pjv.model.gameComponents.Army;
import cz.cvut.fel.pjv.model.gameComponents.GameBoard;
import cz.cvut.fel.pjv.model.gameComponents.Tile;
import cz.cvut.fel.pjv.model.gameComponents.move.BasicMove;
import cz.cvut.fel.pjv.model.gameComponents.move.CastlingMove;
import cz.cvut.fel.pjv.model.gameComponents.move.Move;

import java.util.ArrayList;

public class Rook extends Piece{
    private final Army army;
    private int coordinate;
    private final int[] candidateMoves = {-8, -1, 1, 8};
    private boolean alreadyMoved;

    @Override
    public int getCoordinate() {
        return coordinate;
    }

    @Override
    public void setCoordinate(int coordinate) {
        this.coordinate = coordinate;
    }

    public Rook(Army army, int coordinate) {
        this.army = army;
        this.coordinate = coordinate;
        this.alreadyMoved = false;
    }

    @Override
    public ArrayList<Move> calculatePossibleMoves(GameBoard gameBoard) {
        ArrayList<Move> possibleMoves = new ArrayList<>();
        ArrayList<Tile> board = gameBoard.board;

        for (int candidateMove : candidateMoves) {
            int endPointCoordinate = calculateEndPointCoordinate(candidateMove);
            int coordinateToResearch = this.coordinate;

            while (coordinateToResearch!=endPointCoordinate) {
                coordinateToResearch+=candidateMove;
                Piece occupierOnCandidateTile = board.get(coordinateToResearch).getOccupier();
                if (occupierOnCandidateTile!=null) {
                    if (occupierOnCandidateTile.getArmy() != this.army && !(occupierOnCandidateTile instanceof King)) {
                        possibleMoves.add(new BasicMove(this.coordinate, coordinateToResearch));
                    }
                    break;
                }
                possibleMoves.add(new BasicMove(this.coordinate, coordinateToResearch));
            }
        }
        possibleMoves.addAll(calculateCastlingMoves(board));
        possibleMoves = gameBoard.getMovesWithoutCheckIssue(possibleMoves, this.army);
        return possibleMoves;
    }

    private int calculateEndPointCoordinate(int candidate) {
        if (candidate==-8) {
            return this.coordinate % 8;
        } else if (candidate==-1) {
            return this.coordinate - (this.coordinate % 8);
        } else if (candidate==1) {
            return this.coordinate - (this.coordinate % 8) + 7;
        } else {
            return (this.coordinate % 8) + 56;
        }
    }

    private ArrayList<Move> calculateCastlingMoves(ArrayList<Tile> board) {
        ArrayList<Move> castlingMoves = new ArrayList<>();

        if (this.army==Army.BLACK
                && board.get(4).getOccupier() instanceof King
                && !((King) board.get(4).getOccupier()).isAlreadyMoved()
                && !this.alreadyMoved) {
            if (this.coordinate == 0 && areEmptyTilesBetweenCoordinates(0, 4, board)) {
                castlingMoves.add(new CastlingMove(4, 2, 0, 3));
            } else if (this.coordinate == 7 && areEmptyTilesBetweenCoordinates(4, 7, board)) {
                castlingMoves.add(new CastlingMove(4, 6, 7, 5));
            }
        }

        if (this.army==Army.WHITE
                && board.get(60).getOccupier() instanceof King
                && !((King) board.get(60).getOccupier()).isAlreadyMoved()
                && !this.alreadyMoved) {
            if (this.coordinate == 56 && areEmptyTilesBetweenCoordinates(56, 60, board)) {
                castlingMoves.add(new CastlingMove(60, 58, 56, 59));
            } else if (this.coordinate == 63 && areEmptyTilesBetweenCoordinates(60, 63, board)) {
                castlingMoves.add(new CastlingMove(60, 62, 63, 61));
            }
        }
        return castlingMoves;
    }

    private boolean areEmptyTilesBetweenCoordinates(int coordinate1, int coordinate2, ArrayList<Tile> board) {
        for (int i = coordinate1+1; i<coordinate2; i++) {
            if (board.get(i).getOccupier()!=null) {
                return false;
            }
        }
        return true;
    }

    @Override
    public Army getArmy() {
        return army;
    }

    public boolean isAlreadyMoved() {
        return alreadyMoved;
    }

    public void setAlreadyMoved(boolean alreadyMoved) {
        this.alreadyMoved = alreadyMoved;
    }

    @Override
    public String toString() {
        return this.army.toString().substring(0, 1).toLowerCase() + "r";
    }

    @Override
    public String toPGNString() {
        return "R";
    }
}
