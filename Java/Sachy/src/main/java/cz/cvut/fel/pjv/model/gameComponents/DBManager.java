package cz.cvut.fel.pjv.model.gameComponents;


import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.SerializationUtils;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Logger;

public class DBManager {

    private final static Logger LOGGER = Logger.getLogger(DBManager.class.getName());

    /**
     * Tries to save the given gameBoard into a json file
     * gets all games in the file, adds the gameBoard
     * and writes the new array into the file
     * @param gameBoard
     */
    public void saveGame(GameBoard gameBoard) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            ArrayList<GameBoard> games = getGames();
            games.add(gameBoard);
            byte[] gamesInBytes = SerializationUtils.serialize(games);
            objectMapper.writeValue(new File("saved-games.json"), gamesInBytes);
        } catch (IOException ex) {
            ex.printStackTrace();
            LOGGER.severe("cannot write into file");
        }
        LOGGER.info("DBManager saved the game");
    }

    /**
     * Tries to read from saved-games json file
     * if the file is empty, makes new empty array
     * if the file is not empty, deserializes it and returns it
     * @return gameBoards array
     */
    public ArrayList<GameBoard> getGames() {
        ObjectMapper objectMapper = new ObjectMapper();
        ArrayList<GameBoard> games = new ArrayList<>();
        try {
            FileReader fr = new FileReader("saved-games.json");
            if(fr.read()==-1) {
                games = new ArrayList<>();
            } else {
                byte[] gamesInBytes = objectMapper.readValue(new File("saved-games.json"), byte[].class);
                games = SerializationUtils.deserialize(gamesInBytes);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            LOGGER.severe("cannot read from file");
        }
        return games;
    }

    /**
     * tries to write an empty array into the file
     */
    public void clearAllGames() {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            ArrayList<GameBoard> games = new ArrayList<>();
            byte[] gamesInBytes = SerializationUtils.serialize(games);
            objectMapper.writeValue(new File("saved-games.json"), gamesInBytes);
        } catch (IOException ex) {
            ex.printStackTrace();
            LOGGER.severe("cannot clear the file, because writing into file is not possible");
        }
    }

}
