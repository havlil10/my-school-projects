package cz.cvut.fel.pjv.model.gameComponents.move;

import cz.cvut.fel.pjv.model.gameComponents.GameBoard;
import cz.cvut.fel.pjv.model.gameComponents.Tile;
import cz.cvut.fel.pjv.model.pieces.King;
import cz.cvut.fel.pjv.model.pieces.Piece;
import cz.cvut.fel.pjv.model.pieces.Rook;


public class BasicMove extends Move {
    private final int fromCoordinate;
    private final int toCoordinate;

    public BasicMove(int fromCoordinate, int toCoordinate) {
        this.fromCoordinate = fromCoordinate;
        this.toCoordinate = toCoordinate;
    }

    @Override
    public void makeMove(GameBoard gameBoard) {
        Piece pieceToMove = gameBoard.board.get(fromCoordinate).getOccupier();

        gameBoard.board.get(fromCoordinate).setOccupier(null);
        Tile targetTile = gameBoard.board.get(toCoordinate);

        targetTile.setOccupier(pieceToMove);
        pieceToMove.setCoordinate(toCoordinate);

        if (pieceToMove instanceof Rook && !((Rook) pieceToMove).isAlreadyMoved()) {
            ((Rook) pieceToMove).setAlreadyMoved(true);
        } else if (pieceToMove instanceof King && !((King) pieceToMove).isAlreadyMoved()) {
            ((King) pieceToMove).setAlreadyMoved(true);
        }

        gameBoard.madeMoves.add(this);
    }

    @Override
    public int getFromCoordinate() {
        return fromCoordinate;
    }

    @Override
    public int getToCoordinate() {
        return toCoordinate;
    }
}
