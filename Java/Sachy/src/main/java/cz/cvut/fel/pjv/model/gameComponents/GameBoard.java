package cz.cvut.fel.pjv.model.gameComponents;

import cz.cvut.fel.pjv.model.gameComponents.move.Move;
import cz.cvut.fel.pjv.model.pieces.*;

import org.apache.commons.lang3.SerializationUtils;

import java.io.Serializable;
import java.util.ArrayList;

public class GameBoard implements Serializable {
    public ArrayList<Tile> board;
    public ArrayList<Move> madeMoves = new ArrayList<>();
    public Army currentPlayer = Army.WHITE;

    private boolean isHumanVsComputer = true;
    private String gameBoardName = "Unknown game";

    /**
     * prepares new gameBoard and places the pieces in it
     */
    public GameBoard() {
        prepareNewBoard();
    }

    public boolean isHumanVsComputer() {
        return isHumanVsComputer;
    }

    public String getGameBoardName() {
        return gameBoardName;
    }

    public void setHumanVsComputer(boolean humanVsComputer) {
        isHumanVsComputer = humanVsComputer;
    }

    public void setGameBoardName(String gameBoardName) {
        this.gameBoardName = gameBoardName;
    }

    /**
     * rewrites the board to new array and places pieces on the chessboard
     */
    public void prepareNewBoard() {
        board = new ArrayList<>();
        for (int i=0; i<64; i++) {
            board.add(new Tile(i));
        }
        board.get(0).setOccupier(new Rook(Army.BLACK, 0));
        board.get(1).setOccupier(new Knight(Army.BLACK, 1));
        board.get(2).setOccupier(new Bishop(Army.BLACK, 2));
        board.get(3).setOccupier(new Queen(Army.BLACK, 3));
        board.get(4).setOccupier(new King(Army.BLACK, 4));
        board.get(5).setOccupier(new Bishop(Army.BLACK, 5));
        board.get(6).setOccupier(new Knight(Army.BLACK, 6));
        board.get(7).setOccupier(new Rook(Army.BLACK, 7));
        for (int i=8; i<16; i++) {
            board.get(i).setOccupier(new Pawn(Army.BLACK, i));
        }
        for (int i=48; i<56; i++) {
            board.get(i).setOccupier(new Pawn(Army.WHITE, i));
        }
        board.get(56).setOccupier(new Rook(Army.WHITE, 56));
        board.get(57).setOccupier(new Knight(Army.WHITE, 57));
        board.get(58).setOccupier(new Bishop(Army.WHITE, 58));
        board.get(59).setOccupier(new Queen(Army.WHITE, 59));
        board.get(60).setOccupier(new King(Army.WHITE, 60));
        board.get(61).setOccupier(new Bishop(Army.WHITE, 61));
        board.get(62).setOccupier(new Knight(Army.WHITE, 62));
        board.get(63).setOccupier(new Rook(Army.WHITE, 63));

    }

    /**
     * Checks, whether the army is in check
     * @param army - army to examine (Army.BLACK or Army.WHITE)
     * @return true if the army is in check
     */
    public boolean isArmyInCheck(Army army) {
        Piece king = findKing(army, this.board);
        return isThreatenedByPawn(king.getCoordinate(), this.board, king.getArmy())
                || isThreatenedByKnight(king.getCoordinate(), this.board, king.getArmy())
                || isThreatenedByKing(king.getCoordinate(), this.board)
                || isThreatenedOnLine(king.getCoordinate(), this.board, king.getArmy())
                || isThreatenedOnDiagonal(king.getCoordinate(), this.board, king.getArmy());
    }

    /**
     * Checks, whether the army can make move on the gameBoard
     * @param army - army to examine (Army.BLACK or Army.WHITE)
     * @return - true if a possible move is found
     */
    public boolean canArmyMakeMove(Army army) {
        for (Tile tile : this.board) {
            Piece occupier = tile.getOccupier();
            if (occupier != null && occupier.getArmy()==army
                    && occupier.calculatePossibleMoves(this).size()>0) {
                return true;
            }
        }
        return false;
    }

    /**
     * Retrieves name of a specific piece in PGN notation (i.e. "N" for knight, "" for pawn)
     * @return string in PGN notation
     */
    public String getPGN() {
        return new PGNConverter().convertToPGN(this.madeMoves);
    }

    /**
     * Removes all the moves from candidateMoves, that cause the army to get in check
     * @param candidateMoves - array of moves to examine
     * @param army - Army.BLACK or Army.WHITE
     * @return moves array
     */
    public ArrayList<Move> getMovesWithoutCheckIssue(ArrayList<Move> candidateMoves, Army army) {
        ArrayList<Move> resultMoves = new ArrayList<>();

        for (Move move : candidateMoves) {
            GameBoard testingGameBoard = SerializationUtils.clone(this);
            move.makeMove(testingGameBoard);
            Piece king = findKing(army, testingGameBoard.board);
            if (!isThreatenedByPawn(king.getCoordinate(), testingGameBoard.board, king.getArmy())
                    && !isThreatenedByKnight(king.getCoordinate(), testingGameBoard.board, king.getArmy())
                    && !isThreatenedByKing(king.getCoordinate(), testingGameBoard.board)
                    && !isThreatenedOnLine(king.getCoordinate(), testingGameBoard.board, king.getArmy())
                    && !isThreatenedOnDiagonal(king.getCoordinate(), testingGameBoard.board, king.getArmy())) {
                resultMoves.add(move);
            }
        }
        return resultMoves;
    }

    private Piece findKing(Army army, ArrayList<Tile> board) {
        for (Tile tile : board) {
            Piece occupier = tile.getOccupier();
            if (occupier instanceof King && occupier.getArmy()==army) {
                return occupier;
            }
        }
        throw new RuntimeException("There are not 2 kings on the board");
    }

    private boolean isThreatenedByPawn(int kingCoordinate, ArrayList<Tile> board, Army army) {
        int armyCoefficient = army == Army.BLACK ? 1 : -1;
        int firstPossiblePawnCoordinate = kingCoordinate + armyCoefficient*7;
        int secondPossiblePawnCoordinate = kingCoordinate + armyCoefficient*9;
        return (!isCoordinateOutOfBoard(firstPossiblePawnCoordinate)
                && board.get(firstPossiblePawnCoordinate).getOccupier() instanceof Pawn
                && board.get(firstPossiblePawnCoordinate).getOccupier().getArmy()!=army)
                || (!isCoordinateOutOfBoard(secondPossiblePawnCoordinate)
                && board.get(secondPossiblePawnCoordinate).getOccupier() instanceof Pawn
                && board.get(secondPossiblePawnCoordinate).getOccupier().getArmy()!=army);
    }

    private boolean isThreatenedByKnight(int kingCoordinate, ArrayList<Tile> board, Army army) {
        int[] candidateMoves = {-17, -15, -10, -6, 6, 10, 15, 17};
        for (int candidateMove : candidateMoves) {
            int coordinateToResearch = kingCoordinate + candidateMove;
            if (!isKnightCandidateOutOfBoard(kingCoordinate, coordinateToResearch, candidateMove)
                    && board.get(coordinateToResearch).getOccupier() instanceof Knight
                    && board.get(coordinateToResearch).getOccupier().getArmy()!=army) {
                return true;
            }
        }
        return false;
    }

    private boolean isThreatenedByKing(int kingCoordinate, ArrayList<Tile> board) {
        int[] candidateMoves = {-9, -8, -7, -1, 1, 7, 8, 9};
        for (int candidateMove : candidateMoves) {
            int coordinateToResearch = kingCoordinate + candidateMove;
            if (!isKingCandidateOutOfBoard(kingCoordinate, coordinateToResearch, candidateMove)
                    && board.get(coordinateToResearch).getOccupier() instanceof King) {
                return true;
            }
        }
        return false;
    }

    private boolean isThreatenedOnLine(int kingCoordinate, ArrayList<Tile> board, Army army) {
        int[] candidateMoves = {-8, -1, 1, 8};
        for (int candidateMove : candidateMoves) {
            int endPointCoordinate = calculateLineEndPointCoordinate(kingCoordinate, candidateMove);
            int coordinateToResearch = kingCoordinate;

            while (coordinateToResearch!=endPointCoordinate) {
                coordinateToResearch+=candidateMove;
                Piece occupierOnCandidateTile = board.get(coordinateToResearch).getOccupier();
                if (occupierOnCandidateTile!=null) {
                    if (occupierOnCandidateTile.getArmy() != army
                            && (occupierOnCandidateTile instanceof Rook || occupierOnCandidateTile instanceof Queen)) {
                        return true;
                    }
                    break;
                }
            }
        }
        return false;
    }

    private boolean isThreatenedOnDiagonal(int kingCoordinate, ArrayList<Tile> board, Army army) {
        int[] candidateMoves = {-9, -7, 7, 9};
        for (int candidateMove : candidateMoves) {
            int coordinateToResearch = kingCoordinate + candidateMove;
            if (isPieceOnEdgeOfBoard(kingCoordinate, candidateMove)) {
                continue;
            }
            while (true) {
                Piece occupierOnCandidateTile = board.get(coordinateToResearch).getOccupier();
                if (occupierOnCandidateTile!=null) {
                    if (occupierOnCandidateTile.getArmy() != army
                            && (occupierOnCandidateTile instanceof Bishop || occupierOnCandidateTile instanceof Queen)) {
                        return true;
                    }
                    break;
                }
                if (isPieceOnEdgeOfBoard(coordinateToResearch, candidateMove)) {
                    break;
                }
                coordinateToResearch+=candidateMove;
            }
        }
        return false;
    }

    private boolean isCoordinateOutOfBoard(int coordinate) {
        return coordinate < 0 || coordinate > 63;
    }

    private boolean isKnightCandidateOutOfBoard(int kingCoordinate, int coordinateToResearch, int candidate) {
        if (coordinateToResearch < 0 || coordinateToResearch > 63) {
            return true;
        }

        if (kingCoordinate % 8 == 0 && (candidate==-17 || candidate==-10 || candidate==6 || candidate==15)) {
            return true;
        } else if (kingCoordinate % 8 == 1 && (candidate==-10 || candidate==6)) {
            return true;
        } else if (kingCoordinate % 8 == 6 && (candidate==-6 || candidate==10)) {
            return true;
        } else if (kingCoordinate % 8 == 7 && (candidate==-15 || candidate==-6 || candidate==10 || candidate==17)) {
            return true;
        }
        return false;
    }

    private boolean isKingCandidateOutOfBoard(int kingCoordinate, int coordinateToResearch, int candidate) {
        if (coordinateToResearch < 0 || coordinateToResearch > 63) {
            return true;
        }
        if (kingCoordinate % 8 == 0 && (candidate==-9 || candidate==-1 || candidate==7)) {
            return true;
        } else if (kingCoordinate % 8 == 7 && (candidate==-7 || candidate==1 || candidate==9)) {
            return true;
        }
        return false;
    }

    private int calculateLineEndPointCoordinate(int kingCoordinate, int candidate) {
        if (candidate==-8) {
            return kingCoordinate % 8;
        } else if (candidate==-1) {
            return kingCoordinate - (kingCoordinate % 8);
        } else if (candidate==1) {
            return kingCoordinate - (kingCoordinate % 8) + 7;
        } else {
            return (kingCoordinate % 8) + 56;
        }
    }

    private boolean isPieceOnEdgeOfBoard(int coordinateToResearch, int candidateMove) {
        if (coordinateToResearch+candidateMove > 63 || coordinateToResearch+candidateMove < 0) {
            return true;
        } else if (coordinateToResearch % 8 == 0 && (candidateMove == -9 || candidateMove == 7)) {
            return true;
        } else if (coordinateToResearch % 8 == 7 && (candidateMove == -7 || candidateMove == 9)) {
            return true;
        }
        return false;
    }

}
