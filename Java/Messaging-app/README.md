# Informace o aplikaci
Aplikace podobná např. Messengeru nebo Discordu kladla důraz na využití technologií.
Použité technologie:
- Elasticsearch
- Cache
- Kafka
- Java,Spring,REST,PostgreSQL
- OAuth (google gmail autorizace)


# CheatChat

As a team of students from CTU in Prague we decided to create an app for proper exam preparations (or other activities). The app consists of multiple chat rooms for individual subjects that are taught at our university. In the best case scenario the app will serve as repository of study materials like notes from lectures or our own materials. We do not accept plagiarism or exams from previous years. WE ARE AGAINST CHEATING but we must help each other.

## Authors

- Arťom Ňorba | [@norbaart](https://www.github.com/norbaart)
- Jakub Švorc | [@svorcjak](https://www.github.com/svorcjak)
- Vojtěch Plocica | [@plocivoj](https://www.github.com/plocivoj)
- Lukáš Havlíček | [@havlil10](https://www.github.com/havlil10)

## Info for setup
1. Clone project <br> 
2. Set database - dbname: cheatchat, username: nss, password: nss, port:5432 <br> 
3. Start zookeeper, Start Kafka, Start elasticSearch <br> 
4. Run mvn clean install (that should build backend and frontend) <br> 



## Back-end Installation and Setup
### Kafka/Zookeper Installation
1. Download [Kafka](https://www.apache.org/dyn/closer.cgi?path=/kafka/3.1.0/kafka_2.13-3.1.0.tgz)
2. Extract the compressed (make sure the path does not contain any spaces :D).
3. Go to the `bin` directory and open the terminal.
4. Start Zookeeper `./zookeeper-server-start.sh ../config/zookeeper.properties`
5. Start Kafka broker `./kafka-server-start.sh ../config/server.properties`
6. Create a topic `./kafka-topics.sh --create --topic chat`

> TIP:
> Windows users can use the `.bat` scripts in the `/bin/windows` folder

## Front-end Installation and Setup
Install `ui` dependencies with npm

```bash
cd ui
npm install
```

## Run Locally
### Run the Back-end server

In your Kafka directory run
```bash
cd bin
./zookeeper-server-start.sh ../config/zookeeper.properties
./kafka-server-start.sh ../config/server.properties
```

Then run the `NssProjectAvjlApplication` Spring Application

### Run the Front-end dev server 
```bash
  npm start
```

## Features

- Instant messaging
- File Uploads
- Multiple channels
- Google auth
- Deleting messages
- Search messages

## Full documentation
[click here for magic](https://docs.google.com/document/d/1h20TwISiXOAs_L8OPr5Aqd5-B89Ojhatw-ZbE5G1o8Q/edit)


## What is completed

- Database - postgresql
- Cache - native - O(1) get, O(1) insert, LRU algorithm - done with hashmap+doublyLinkedList
- Logging (i.e. in Kafka, ElasticsearchService,..)
- REST + websocket (controllers, MessageController)
- Kafka
- Heroku deploy
- OAuth2 for authentication (turned on in config, used in UserService)
- Architecture - specific client-server approach that we consulted
- Elasticsearch (located in service -> ElasticsearchService), used in message search - submessage is passed -> returns the whole message (i.e. “el” finds “hello”). Not implemented in Heroku because the feature costs money
- S3 AWS for file upload (service -> BucketService)

## Design patterns

Design patterns would not make our particular code better in any way their purpose is (cleaner code, ...) so we did not focus on implementing them. Design patterns that are present in app are:

- Singleton (cache)
- DTO
- Builder (StringBuilder - ElasticsearchService->buildResponseContent)
- Facade (BucketService)
- Interceptor + strategy


