package jsme.nejlepsi.na.svete.nssprojectavjl.model;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * User entity
 */
@Entity
@AllArgsConstructor
@Getter
@Setter
@ToString
@Table(name = "SYSTEM_USER")
@NamedQueries({
        @NamedQuery(name = "User.findByEmail", query = "SELECT u FROM User u WHERE u.email = :email")
})
public class User extends AbstractEntity {

    @NotNull
    @Column(unique = true)
    private String email;

    @NotNull
    private String name;

    @Enumerated
    private Role role;

    private String avatarUrl;


    public User() {
        role = Role.USER;
    }
}

