package jsme.nejlepsi.na.svete.nssprojectavjl.dto;

import jsme.nejlepsi.na.svete.nssprojectavjl.model.File;

public class FileDto {
    private final String displayName;
    private final String fileUrl;

    public FileDto(File file) {
        this.displayName = file.getDisplayName();
        this.fileUrl = file.getFileUrl();
    }
}
