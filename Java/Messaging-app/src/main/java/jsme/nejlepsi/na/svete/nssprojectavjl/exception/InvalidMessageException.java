package jsme.nejlepsi.na.svete.nssprojectavjl.exception;

public class InvalidMessageException extends CheatChatException {

    public InvalidMessageException() {
    }


    public InvalidMessageException(String message) {
        super(message);
    }


    public InvalidMessageException(String message, Throwable cause) {
        super(message, cause);
    }


    public InvalidMessageException(Throwable cause) {
        super(cause);
    }
}
