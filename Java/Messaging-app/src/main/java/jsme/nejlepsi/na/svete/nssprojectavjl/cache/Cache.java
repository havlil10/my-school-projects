package jsme.nejlepsi.na.svete.nssprojectavjl.cache;

import jsme.nejlepsi.na.svete.nssprojectavjl.model.Channel;
import jsme.nejlepsi.na.svete.nssprojectavjl.model.Message;

import java.util.*;

/**
 * Class for cache
 */
public class Cache {

    private int size;
    private Map<Long, LinkedListNode> linkedListNodeMap;
    private DoublyLinkedList doublyLinkedList;


    public Cache(int size) {
        this.size = size;
        this.linkedListNodeMap = new HashMap<>(size);
        this.doublyLinkedList = new DoublyLinkedList();
    }


    /**
     * Put something in cache
     * @param key of written
     * @param value what we want to write
     * @return result
     */
    public boolean put(Long key, List<Channel> value) {
        CacheElement item = new CacheElement(key, value);
        LinkedListNode newNode;
        if (this.linkedListNodeMap.containsKey(key)) {
            LinkedListNode node = this.linkedListNodeMap.get(key);
            newNode = doublyLinkedList.updateAndMoveToFront(node, item);
        } else {

            newNode = this.doublyLinkedList.add(item);
        }
        if (newNode.isEmpty()) {
            return false;
        }
        this.linkedListNodeMap.put(key, newNode);
        return true;
    }


    /**
     * Get something from cache
     * @param key id of what we want to get
     * @return null or entity from cache
     */
    public Optional<List<Channel>> get(Long key) {
        LinkedListNode linkedListNode = this.linkedListNodeMap.get(key);
        if (linkedListNode != null && !linkedListNode.isEmpty()) {
            linkedListNodeMap.put(key, this.doublyLinkedList.moveToFront(linkedListNode));
            return Optional.of(linkedListNode.getElement().getChannels());
        }
        return Optional.empty();
    }


    /**
     * Add channel to everyone
     * @param channel what channel we want to add
     */
    public void addChannelToEveryone(Channel channel) {
        for(LinkedListNode linkedListNode : linkedListNodeMap.values()) {
            linkedListNode.getElement().addChannel(channel);
        }
    }
}
