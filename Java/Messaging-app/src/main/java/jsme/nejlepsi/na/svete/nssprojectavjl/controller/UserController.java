package jsme.nejlepsi.na.svete.nssprojectavjl.controller;

import java.util.stream.Collectors;
import jsme.nejlepsi.na.svete.nssprojectavjl.dto.ChannelDto;
import jsme.nejlepsi.na.svete.nssprojectavjl.dto.UserDto;
import jsme.nejlepsi.na.svete.nssprojectavjl.model.User;
import jsme.nejlepsi.na.svete.nssprojectavjl.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;


import java.util.List;


/**
 * Rest controller to manage user
 */
@RestController
@RequestMapping("/rest/v1/user")
@PreAuthorize("isAuthenticated()")
public class UserController {


    private final UserService userService;


    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }


    /**
     * Endpoint to get all channels for user - public and secret that he is invited to
     *
     * @return List of channels that user can use
     */
    @GetMapping(value = "/channels")
    public List<ChannelDto> getAllChannels() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        DefaultOidcUser oidcUser = (DefaultOidcUser) auth.getPrincipal();
        User user = userService.findUserByEmail(oidcUser.getEmail());
        return userService.findAllChannelsForUser(user).stream()
            .map(ChannelDto::new)
            .collect(Collectors.toList());
    }


    /**
     * Endpoint to find user by email
     *
     * @return user that is currently logged
     */
    @GetMapping(value = "/me")
    public UserDto getMe() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        DefaultOidcUser oidcUser = (DefaultOidcUser) auth.getPrincipal();
        return new UserDto(userService.findUserByEmail(oidcUser.getEmail()));
    }
}
