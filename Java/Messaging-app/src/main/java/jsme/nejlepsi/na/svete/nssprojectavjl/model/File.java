package jsme.nejlepsi.na.svete.nssprojectavjl.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;
import java.time.Instant;

/**
 * File entity
 */
@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@Table(name = "FILES")
public class File extends AbstractEntity {

    public File(String name, Channel channel, User sender, String displayName) {
        this.name = name;
        this.channel = channel;
        this.timestamp = Date.from(Instant.now());
        this.sender = sender;
        this.displayName = displayName;
    }


    @NotNull
    @Column(unique = true)
    private String name;

    @NotNull
    private String displayName;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    private Date timestamp;

    @Column(unique = true)
    private String fileUrl;

    @ManyToOne
    @JoinColumn(name = "channel_id")
    private Channel channel;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User sender;

    @NotNull
    @JsonIgnore
    private boolean deleted;
}
