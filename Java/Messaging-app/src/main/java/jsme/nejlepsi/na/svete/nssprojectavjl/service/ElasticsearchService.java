package jsme.nejlepsi.na.svete.nssprojectavjl.service;

import lombok.extern.java.Log;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;


@Service
@Log
public class ElasticsearchService {

    public final Logger logger = LoggerFactory.getLogger(ElasticsearchService.class);


    /**
     * inserts message to elasticsearch database
     * creates http post request and sends it
     * synchronously programmed, better to remake to asynchronous in the future
     *
     * @param messageId - id of message
     * @param messageContent - content of message
     * @param channelId - id of channel we are sending message in
     * @throws IOException - error with sending http post request
     */
    public void saveMessageToElastic(long messageId, String messageContent, long channelId) {
        try {
            URL url = new URL("http://localhost:9200/messages4/message");
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json");
            con.setRequestProperty("Accept", "application/json");

            con.setDoOutput(true);
            String jsonInputString = "{" +
                    "\"id\": " + messageId + "," +
                    "\"content\": \"" + messageContent.toLowerCase() + "\"," +
                    "\"timestamp\": null," +
                    "\"sender\": null," +
                    "\"channel\": {" +
                    "\"id\": " + channelId + "," +
                    "\"topic\": null" +
                    "}," +
                    "\"deleted\": false" +
                    "}";

            con.setConnectTimeout(5000);
            con.setReadTimeout(5000);
            try (OutputStream os = con.getOutputStream()) {
                byte[] input = jsonInputString.getBytes("utf-8");
                os.write(input, 0, input.length);
            }
            logger.info("successfully sent request to save with response code: " + con.getResponseCode());
        } catch (IOException e) {
            log.warning(e.getMessage());
            logger.error("tried to save message to elastic but request was unsuccessful");
        }
    }


    private HttpURLConnection prepareGetConnection() throws IOException {
        URL url = new URL("http://localhost:9200/messages4/message/_search");
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");
        con.setConnectTimeout(5000);
        con.setReadTimeout(5000);
        con.setRequestProperty("Content-Type", "application/json");
        con.setRequestProperty("Accept", "application/json");
        con.setDoOutput(true);
        return con;

    }


    /**
     * finds id of message given message content and channel id
     * @param content - given substring that will match the content of result message
     * @param channelId - id of the channel from where to search
     * @return - id of message
     * @throws IOException - error with sending http get request
     */

    public long getMessageIdByContentAndChannel(String content, long channelId) throws IOException {
        HttpURLConnection con = prepareGetConnection();
        String jsonInputString = "{" +
                "  \"query\": {" +
                "    \"bool\": {" +
                "      \"must\": [" +
                "        {" +
                "          \"match\": {" +
                "            \"channel.id\": \"" + channelId + "\"" +
                "          }" +
                "        }," +
                "        {\"wildcard\": {" +
                "       \"content\": {" +
                "          \"value\": \"*" + content.toLowerCase() + "*\"" +
                "       }" +
                "    }}" +
                "      ]" +
                "    }" +
                "  }" +
                "}";

        logger.info("successfully sent request to find message");
        con.disconnect();

        StringBuilder responseContent = buildResponseContent(con, jsonInputString);
        String jsonInput = responseContent.toString();

        JSONObject json = new JSONObject(jsonInput);
        JSONObject hits = json.getJSONObject("hits");
        JSONArray hitsArray = hits.getJSONArray("hits");
        long id = -1;

        //can be changed so that each request returns different Message
        if (hitsArray.length() > 0) {
            JSONObject h = hitsArray.getJSONObject(0);
            JSONObject jsonSource = h.getJSONObject("_source");
            id = jsonSource.getInt("id");
        }

        return id;
    }

    private StringBuilder buildResponseContent(HttpURLConnection con, String jsonInputString) throws IOException {
        try (OutputStream os = con.getOutputStream()) {
            byte[] input = jsonInputString.getBytes("utf-8");
            os.write(input, 0, input.length);
        }

        StringBuilder responseContent = new StringBuilder();
        BufferedReader reader;
        String line;

        if (con.getResponseCode() < 300) {
            reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
            while ((line = reader.readLine()) != null) {
                responseContent.append(line);
            }
            reader.close();
        }
        return responseContent;
    }


    private String retrieveElasticId(long messageId) throws IOException {
        HttpURLConnection con = prepareGetConnection();
        String jsonInputString = "{\"query\": { \"match\": {\"id\" : " + messageId + " } }}";

        StringBuilder responseContent = buildResponseContent(con, jsonInputString);

        con.disconnect();

        String jsonInput = responseContent.toString();

        JSONObject json = new JSONObject(jsonInput);
        JSONObject hits = json.getJSONObject("hits");
        JSONArray hitsArray = hits.getJSONArray("hits");
        String id = null;

        //can be changed so that each request returns different Message
        if (hitsArray.length() > 0) {
            JSONObject h = hitsArray.getJSONObject(0);
            id = h.getString("_id");
        }

        return id;
    }

    /**
     * deletes message from elasticsearch database
     * @param messageId - id of message to remove
     * @throws IOException - error with sending http delete request
     */

    public void deleteMessageFromElastic(long messageId) throws IOException {
        String elasticId = retrieveElasticId(messageId);
        if (elasticId == null) {
            logger.info("tried to delete, but did not find the message");
            return;
        }
        String urlString = "http://localhost:9200/messages4/message/" + elasticId;
        URL url = new URL(urlString);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();

        con.setRequestMethod("DELETE");
        con.setRequestProperty("Accept", "*/*");
        logger.info("successfully deleted a message. Response code: " + con.getResponseCode());
        con.disconnect();

    }


}
