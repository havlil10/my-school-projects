package jsme.nejlepsi.na.svete.nssprojectavjl.repository;

import jsme.nejlepsi.na.svete.nssprojectavjl.model.Channel;
import jsme.nejlepsi.na.svete.nssprojectavjl.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


/**
 * Repository for channels
 */
@Repository
public interface ChannelRepository extends JpaRepository<Channel, Long> {

    /**
     * Method to get all public or all secret channels
     * @param secret
     * @return
     */
    List<Channel> getChannelsBySecret(boolean secret);

    /**
     * Method to get all channels where user is invited
     * @param user logged user
     * @return List of channels
     */
    @Query("SELECT c from Channel c WHERE ?1 member of c.users")
    List<Channel> getChannelsByUsersInvited(User user);
}
