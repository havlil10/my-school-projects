package jsme.nejlepsi.na.svete.nssprojectavjl.controller;

import java.util.stream.Collectors;

import jsme.nejlepsi.na.svete.nssprojectavjl.dto.ChannelDto;
import jsme.nejlepsi.na.svete.nssprojectavjl.dto.OutboundMessageDto;
import jsme.nejlepsi.na.svete.nssprojectavjl.model.Message;
import jsme.nejlepsi.na.svete.nssprojectavjl.service.ChannelService;
import jsme.nejlepsi.na.svete.nssprojectavjl.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

/**
 * Channel rest controller with endpoints
 */
@RestController
@RequestMapping("/rest/v1/channel")
public class ChannelController {

    private final ChannelService channelService;
    private final MessageService messageService;


    @Autowired
    public ChannelController(ChannelService channelService, MessageService messageService) {
        this.channelService = channelService;
        this.messageService = messageService;
    }


    /**
     * Basic endpoint
     *
     * @return all channels in DB
     */
    @GetMapping(value = "/")
    public List<ChannelDto> getAllChannels() {
        return channelService.findAll().stream()
            .map(ChannelDto::new)
            .collect(Collectors.toList());
    }


    /**
     * Only authorized user can create channel
     *
     * @param topic name of the created channel
     */
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    @PostMapping(value = "/create")
    public void createChannel(@RequestBody String topic) {
        channelService.createChannel(topic);
    }


    /**
     * Endpoint using elastic search for finding messages
     *
     * @param message   - message to find
     * @param channelId - channel in which we want to find message
     * @return - messages found
     * @throws IOException - if bad things happen
     */
    @GetMapping(value = "/{channelId}/findMessage")
    public List<OutboundMessageDto> findMessagesMatch(@RequestParam String message, @PathVariable Long channelId) throws IOException {
        return channelService.findMessagesMatch(message, channelId).stream()
            .map(OutboundMessageDto::new)
            .collect(Collectors.toList());
    }


    /**
     * Endpoint to get channel by id
     *
     * @param channelId - id of channel we want to get
     * @return - channel
     */
    @GetMapping(value = "/{channelId}")
    public ChannelDto getChannelById(@PathVariable Long channelId) {
        return new ChannelDto(channelService.findChannel(channelId));
    }


    /**
     * Endpoint to get all messages for some channel
     *
     * @param channelId - channel we want to get messages from
     * @return - List of messagesDTO
     */
    @GetMapping(value = "/{channelId}/all-messages")
    public List<OutboundMessageDto> findMessagesForChannel(@PathVariable Long channelId) {
        return channelService.findMessagesForChannel(channelService.findChannel(channelId)).stream()
                .map(OutboundMessageDto::new)
                .collect(Collectors.toList());
    }

    /**
     * Endpoint of pagination of messages
     *
     * @param channelId = channel we want to find messages for
     * @param size      - how many messages wqe want to get
     * @return - return Page of messagesDTO
     */
    @GetMapping(value = "/{channelId}/messages", params = {"size"})
    public List<OutboundMessageDto> findMessagesByPage(@PathVariable Long channelId, @RequestParam int size) {
        return channelService.firstLatestMessages(channelService.findChannel(channelId), size).stream()
            .map(OutboundMessageDto::new)
            .collect(Collectors.toList());
    }


    /**
     * Endpoint of pagination of messages
     *
     * @param channelId     - channel we want to get messages for
     * @param size          - number of messages
     * @param lastMessageId - last message
     * @return List of messages of our size
     */
    @GetMapping(value = "/{channelId}/messages", params = {"size", "lastMessageId"})
    public List<OutboundMessageDto> findMessagesByPage(@PathVariable Long channelId, @RequestParam int size, @RequestParam Long lastMessageId) {
        Message lastMessage = messageService.findMessage(lastMessageId);
        return channelService.findNextLatestMessages(channelService.findChannel(channelId), size, lastMessage).stream()
            .map(OutboundMessageDto::new)
            .collect(Collectors.toList());
    }
}
