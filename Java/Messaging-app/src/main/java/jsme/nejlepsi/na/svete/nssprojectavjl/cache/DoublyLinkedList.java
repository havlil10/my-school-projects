package jsme.nejlepsi.na.svete.nssprojectavjl.cache;

public class DoublyLinkedList {

    private LinkedListNode head;


    public DoublyLinkedList() {

    }


    /**
     * Method to update list and move new value to top
     * @param node linked list
     * @param newValue value we want to move to the top
     * @return new LinkedListNode
     */
    public LinkedListNode updateAndMoveToFront(LinkedListNode node, CacheElement newValue) {
        detach(node);
        add(newValue);
        return head;
    }


    /**
     * Method to mode node to front
     * @param node we want to move
     * @return LinkedListNode
     */
    public LinkedListNode moveToFront(LinkedListNode node) {
        return node.isEmpty() ? new LinkedListNode() : updateAndMoveToFront(node, node.getElement());
    }


    /**
     * Method to add value
     * @param value we want to add
     * @return LinkedListNode
     */
    public LinkedListNode add(CacheElement value) {
        LinkedListNode newNode = new LinkedListNode(value);
        newNode.setNext(head);
        newNode.setPrevious(null);
        if (head != null) {
            head.setPrevious(newNode);
        }
        head = newNode;
        return newNode;
    }


    /**
     * Method to detach
     * @param node to detach
     */
    public void detach(LinkedListNode node) {
        if (head == null || node == null) {
            return;
        }

        if (node.getPrevious() == null) {
            head = node.getNext();
        }

        if (node.getNext() != null) {
            node.getNext().setPrevious(node.getPrevious());
        }

        if (node.getPrevious() != null) {
            node.getPrevious().setNext(node.getNext());
        }
    }


}
