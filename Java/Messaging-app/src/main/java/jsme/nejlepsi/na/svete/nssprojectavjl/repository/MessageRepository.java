package jsme.nejlepsi.na.svete.nssprojectavjl.repository;

import jsme.nejlepsi.na.svete.nssprojectavjl.repository.interfaces.CustomMessageRepository;
import jsme.nejlepsi.na.svete.nssprojectavjl.model.Channel;
import jsme.nejlepsi.na.svete.nssprojectavjl.model.Message;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repository for Messages with pagination
 */
@Repository
public interface MessageRepository extends PagingAndSortingRepository<Message, Long>,
    CustomMessageRepository {

    List<Message> getAllByChannel(Channel channel);

    Page<Message> findByChannel(Channel channel, Pageable pageable);
}
