package jsme.nejlepsi.na.svete.nssprojectavjl.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;


@Getter
@ToString
public class InboundMessageDto {

    private String content;
}
