package jsme.nejlepsi.na.svete.nssprojectavjl.dto;

import jsme.nejlepsi.na.svete.nssprojectavjl.model.Message;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;


@Getter
@Setter
@ToString
public class OutboundMessageDto {

    private Long Id;
    private String content;
    private SenderDto sender;
    private Date timeStamp;
    private Long id;


    public OutboundMessageDto(Message message) {
        this.Id = message.getId();
        this.content = message.getContent();
        this.sender = new SenderDto(message.getSender());
        this.timeStamp = message.getTimestamp();
        this.id = message.getId();
    }
}
