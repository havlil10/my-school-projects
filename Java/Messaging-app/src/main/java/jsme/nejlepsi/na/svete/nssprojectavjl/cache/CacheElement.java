package jsme.nejlepsi.na.svete.nssprojectavjl.cache;

import jsme.nejlepsi.na.svete.nssprojectavjl.model.Channel;
import jsme.nejlepsi.na.svete.nssprojectavjl.model.Message;

import java.util.List;


public class CacheElement {

    private final Long request;
    private final List<Channel> channels;


    public CacheElement(Long request, List<Channel> channels) {
        this.request = request;
        this.channels = channels;
    }


    /**
     * Method to get request
     * @return request
     */
    public Long getRequest() {
        return request;
    }


    /**
     * Method to get all channels
     * @return all channels
     */
    public List<Channel> getChannels() {
        return channels;
    }


    /**
     * Method to add channel
     * @param channel we want to add
     */
    public void addChannel(Channel channel) {
        channels.add(channel);
    }
}
