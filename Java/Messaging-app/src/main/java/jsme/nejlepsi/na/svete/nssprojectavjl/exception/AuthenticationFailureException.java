package jsme.nejlepsi.na.svete.nssprojectavjl.exception;

public class AuthenticationFailureException extends CheatChatException{
    public AuthenticationFailureException() {
    }

    public AuthenticationFailureException(String message) {
        super(message);
    }

    public AuthenticationFailureException(String message, Throwable cause) {
        super(message, cause);
    }

    public AuthenticationFailureException(Throwable cause) {
        super(cause);
    }
}
