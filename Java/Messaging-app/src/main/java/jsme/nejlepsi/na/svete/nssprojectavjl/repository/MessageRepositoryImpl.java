package jsme.nejlepsi.na.svete.nssprojectavjl.repository;

import com.blazebit.persistence.CriteriaBuilder;
import com.blazebit.persistence.CriteriaBuilderFactory;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import jsme.nejlepsi.na.svete.nssprojectavjl.repository.interfaces.CustomMessageRepository;
import jsme.nejlepsi.na.svete.nssprojectavjl.model.Channel;
import jsme.nejlepsi.na.svete.nssprojectavjl.model.Message;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Message Repository
 */
public class MessageRepositoryImpl implements CustomMessageRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private CriteriaBuilderFactory criteriaBuilderFactory;


    /**
     * Find messages with defined number at top
     * @param channel our channel we want to find messages
     * @param pageSize = how many messages we want to show
     * @return List of messages
     */
    @Override
    public List<Message> findTopNByChannel(Channel channel, int pageSize) {
        return getCriteriaBuilder(channel, false)
                .setMaxResults(pageSize)
                .getResultList();
    }

    /**
     * Find messages with defined number
     * @param channel our channel we want to find messages
     * @param pageSize = how many messages we want to show
     * @return List of messages
     */
    @Override
    public List<Message> findNAroundByChannel(Channel channel, int pageSize, Message currentMessage) {
        List<Message> previousN = findNextNByChannel(channel, pageSize / 2, currentMessage, true);
        List<Message> nextN = findNextNByChannel(channel, pageSize - previousN.size(), currentMessage, false);
        Collections.reverse(previousN);
        return Stream.of(previousN, Collections.singletonList(currentMessage), nextN)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }


    /**
     * Find messages with defined number
     * @param channel our channel we want to find messages
     * @param pageSize = how many messages we want to show
     * @param lastMessage = last show message
     * @param reverse true if we want to reverse it
     * @return List of messages
     */
    @Override
    public List<Message> findNextNByChannel(Channel channel, int pageSize, Message lastMessage, boolean reverse) {
        return getCriteriaBuilder(channel, reverse)
                .afterKeyset(lastMessage.getTimestamp(), lastMessage.getId())
                .setMaxResults(pageSize)
                .getResultList();
    }


    /**
     * get Criteria builder
     * @param channel we want to make builder for
     * @param reverse true if reversed
     * @return Criteria builder
     */
    private CriteriaBuilder<Message> getCriteriaBuilder(Channel channel, boolean reverse) {
        CriteriaBuilder<Message> criteriaBuilder = criteriaBuilderFactory
                .create(entityManager, Message.class);

        return criteriaBuilder
                .where("channel").eqExpression(":channel")
                .where("deleted").eqExpression("false")
                .orderBy("timestamp", reverse)
                .orderBy("id", reverse)
                .setParameter("channel", channel);
    }
}
