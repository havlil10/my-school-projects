package jsme.nejlepsi.na.svete.nssprojectavjl.exception;

public class PersistenceException extends CheatChatException {

    public PersistenceException(String message, Throwable cause) {
        super(message, cause);
    }


    public PersistenceException(Throwable cause) {
        super(cause);
    }
}
