package jsme.nejlepsi.na.svete.nssprojectavjl.model;

/**
 * Role manager
 */
public enum Role {
    USER("ROLE_USER"), ADMIN("ROLE_ADMIN");
    private final String name;


    Role(String name) {
        this.name = name;
    }
}
