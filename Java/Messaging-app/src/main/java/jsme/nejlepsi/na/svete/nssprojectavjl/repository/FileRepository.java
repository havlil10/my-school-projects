package jsme.nejlepsi.na.svete.nssprojectavjl.repository;

import jsme.nejlepsi.na.svete.nssprojectavjl.model.Channel;
import jsme.nejlepsi.na.svete.nssprojectavjl.model.File;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repository for files
 */
@Repository
public interface FileRepository extends JpaRepository<File, Long> {
    File findFileByName(String name);

    List<File> getFilesByChannel(Channel channel);
}
