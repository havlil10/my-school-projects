package jsme.nejlepsi.na.svete.nssprojectavjl.service;

import jsme.nejlepsi.na.svete.nssprojectavjl.repository.ChannelRepository;
import jsme.nejlepsi.na.svete.nssprojectavjl.repository.MessageRepository;
import jsme.nejlepsi.na.svete.nssprojectavjl.repository.UserRepository;
import jsme.nejlepsi.na.svete.nssprojectavjl.exception.AuthenticationFailureException;
import jsme.nejlepsi.na.svete.nssprojectavjl.exception.CheatChatException;
import jsme.nejlepsi.na.svete.nssprojectavjl.exception.InvalidMessageException;
import jsme.nejlepsi.na.svete.nssprojectavjl.exception.InvalidRequestException;
import jsme.nejlepsi.na.svete.nssprojectavjl.model.Channel;
import jsme.nejlepsi.na.svete.nssprojectavjl.model.Message;
import jsme.nejlepsi.na.svete.nssprojectavjl.model.Role;
import jsme.nejlepsi.na.svete.nssprojectavjl.model.User;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
@Log
public class MessageService {

    private final UserRepository userRepository;

    private final MessageRepository messageRepository;

    private final ChannelRepository channelRepository;

    private final ElasticsearchService elasticsearchService;


    @Autowired
    public MessageService(UserRepository userRepository, MessageRepository messageRepository, ChannelRepository channelRepository, ElasticsearchService elasticsearchService) {
        this.userRepository = userRepository;
        this.messageRepository = messageRepository;
        this.channelRepository = channelRepository;
        this.elasticsearchService = elasticsearchService;
    }


    public Message handleInboundMessage(String content, OidcUser sender, Long channelId) throws CheatChatException, IOException {
        Channel channel = channelRepository.findById(channelId).get();
        DefaultOidcUser oidcUser = (DefaultOidcUser) sender;
        User user = userRepository.findUserByEmail(oidcUser.getEmail());
        validateMessage(content, user, channel);
        Message message = new Message(content, user, channel);
        messageRepository.save(message);
        try {
            elasticsearchService.saveMessageToElastic(message.getId(), message.getContent(), channelId);
        } catch (Exception e) {
            log.warning("Elastic is off.");
        }
        return message;
    }


    private void validateMessage(String content, User user, Channel channel) throws CheatChatException {
        if (content.length() < 1 || content.length() > 500) {
            throw new InvalidMessageException("Message is empty or longer than 500 characters.");
        }
        if (channel.isSecret() && !channel.getUsers().contains(user)) {
            throw new InvalidMessageException("Message sent into a channel which the user isn't a part of.");
        }
    }


    public Long deleteMessage(Long messageId, String email) throws CheatChatException {
        User user = userRepository.findUserByEmail(email);
        Message toDelete = messageRepository.findById(messageId).get();

        if (toDelete.isDeleted()) {
            throw new InvalidRequestException("Couldn't delete message.");
        }

        if (!toDelete.getSender().equals(user)) {
            if (!user.getRole().equals(Role.ADMIN)) {
                throw new AuthenticationFailureException("Message can't be deleted, user is not admin.");
            }
        }
        toDelete.setDeleted(true);
        messageRepository.save(toDelete);
        try {
            elasticsearchService.deleteMessageFromElastic(messageId);
        } catch (Exception e) {
            log.warning("Elastic is off.");
        }
        return toDelete.getId();
    }


    public List<Message> getAllMessages() {
        return (List<Message>) messageRepository.findAll();
    }


    public Message findMessage(Long messageId) {
        return messageRepository.findById(messageId).get();
    }
}
