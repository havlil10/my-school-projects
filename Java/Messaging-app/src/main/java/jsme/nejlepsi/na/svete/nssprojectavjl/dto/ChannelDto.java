package jsme.nejlepsi.na.svete.nssprojectavjl.dto;

import jsme.nejlepsi.na.svete.nssprojectavjl.model.Channel;

public class ChannelDto {

    private final Long id;
    private final String topic;

    public ChannelDto(Channel channel) {
        this.id = channel.getId();
        this.topic = channel.getTopic();
    }
}
