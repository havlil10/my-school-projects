package jsme.nejlepsi.na.svete.nssprojectavjl.security;

import jsme.nejlepsi.na.svete.nssprojectavjl.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Service;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * Class for handling Auth
 */
@Service
public class AuthenticationSuccess implements AuthenticationSuccessHandler, LogoutSuccessHandler {
    @Autowired
    private UserService userService;


    /**
     * Handle auth
     * @param request someone wants to auth
     * @param response of authorization
     * @param authentication auth
     * @throws IOException if something bad happened
     * @throws ServletException if something bad happened
     */
    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        DefaultOidcUser oidcUser = (DefaultOidcUser) authentication.getPrincipal();
        userService.processOAuthRegistration(oidcUser);

        response.sendRedirect("/");
    }


    /**
     * Handle logout
     * @param request someone wants to auth
     * @param response of authorization
     * @param authentication auth
     * @throws IOException if something bad happened
     * @throws ServletException if something bad happened
     */
    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {

    }
}
