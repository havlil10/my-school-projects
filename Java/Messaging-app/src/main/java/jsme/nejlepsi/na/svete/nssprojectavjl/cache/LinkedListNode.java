package jsme.nejlepsi.na.svete.nssprojectavjl.cache;

public class LinkedListNode {

    private CacheElement cacheElement = null;
    private LinkedListNode next;
    private LinkedListNode previous;


    public LinkedListNode() {

    }

    public LinkedListNode(CacheElement cacheElement) {
        this.cacheElement = cacheElement;
    }


    /**
     *
     * @return if LinkedListNode is Empty
     */
    public boolean isEmpty() {
        return cacheElement == null;
    }


    /**
     *
     * @return CacheElement
     */
    public CacheElement getElement() {
        return cacheElement;
    }


    /**
     *
     * @return next value
     */
    public LinkedListNode getNext() {
        return next;
    }


    /**
     *
     * @return previous value
     */
    public LinkedListNode getPrevious() {
        return previous;
    }


    /**
     *
     * @param next value we want to set
     */
    public void setNext(LinkedListNode next) {
        this.next = next;
    }


    /**
     *
     * @param previous value we want to set
     */
    public void setPrevious(LinkedListNode previous) {
        this.previous = previous;
    }

}
