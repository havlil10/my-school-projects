package jsme.nejlepsi.na.svete.nssprojectavjl.dto;

import lombok.*;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class DeleteMessageKafkaDto {
    private String email;
    private Long messageId;
    private Long channelId;
}
