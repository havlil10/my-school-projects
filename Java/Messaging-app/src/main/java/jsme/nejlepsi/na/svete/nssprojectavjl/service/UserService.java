package jsme.nejlepsi.na.svete.nssprojectavjl.service;

import jsme.nejlepsi.na.svete.nssprojectavjl.cache.SingletonCache;
import jsme.nejlepsi.na.svete.nssprojectavjl.repository.ChannelRepository;
import jsme.nejlepsi.na.svete.nssprojectavjl.repository.UserRepository;
import jsme.nejlepsi.na.svete.nssprojectavjl.model.Channel;
import jsme.nejlepsi.na.svete.nssprojectavjl.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ChannelRepository channelRepository;


    public User findUserById(Long id) {
        return userRepository.getById(id);
    }


    @Transactional
    public User findUserByEmail(String mail) {
        return userRepository.findUserByEmail(mail);
    }


    @Transactional
    public void processOAuthRegistration(DefaultOidcUser user) {
        User existingUser = userRepository.findUserByEmail(user.getEmail());

        if (existingUser == null) {
            User createdUser = new User();
            createdUser.setEmail(user.getEmail());
            createdUser.setName(user.getFullName());
            createdUser.setAvatarUrl(user.getPicture());
            userRepository.save(createdUser);
        }
    }


    @Transactional
    public List<Channel> findAllChannelsForUser(User user) {
        if (SingletonCache.getInstance().get(user.getId()).isPresent()) {
            return SingletonCache.getInstance().get(user.getId()).get();
        }
        List<Channel> result = channelRepository.getChannelsBySecret(false);
        result.addAll(channelRepository.getChannelsByUsersInvited(user));
        SingletonCache.getInstance().put(user.getId(), result);
        return result;
    }
}
