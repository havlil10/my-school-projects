package jsme.nejlepsi.na.svete.nssprojectavjl.controller;

import java.util.stream.Collectors;
import jsme.nejlepsi.na.svete.nssprojectavjl.dto.FileDto;
import jsme.nejlepsi.na.svete.nssprojectavjl.model.File;
import jsme.nejlepsi.na.svete.nssprojectavjl.service.BucketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
import org.springframework.web.bind.annotation.*;

import java.net.URL;
import java.util.List;

/**
 * Rest controller for files - only logged user can access
 */
@RestController
@RequestMapping("/file")
@PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
public class FileController {
    private final BucketService bucketService;


    @Autowired
    public FileController(BucketService bucketService) {
        this.bucketService = bucketService;
    }


    /**
     * Endpoint to get URL of uploaded file
     *
     * @param channelId   channel we want to upload file
     * @param displayName name we want to display
     * @return crested url of file
     */
    @PostMapping(value = "/upload/{channelId}")
    public URL getUrlForUpload(@PathVariable Long channelId, @RequestBody String displayName) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        DefaultOidcUser oidcUser = (DefaultOidcUser) auth.getPrincipal();
        return bucketService.prepareFileUpload(oidcUser, channelId, displayName);
    }


    /**
     * Endpoint to save created url to db
     *
     * @param fileUrl url of file
     */
    @PostMapping(value = "/upload")
    public void saveUrlOfUploadedFile(@RequestBody String fileUrl) {
        bucketService.putSavedFileToDB(fileUrl);
    }


    /**
     * Endpoint to get files for channel
     *
     * @param channelId channel we want to show files for
     * @return List of files of that channel
     */
    @GetMapping(value = "/{channelId}")
    public List<FileDto> getChannelFiles(@PathVariable Long channelId) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        DefaultOidcUser oidcUser = (DefaultOidcUser) auth.getPrincipal();
        return bucketService.getFilesFromChannel(channelId, oidcUser).stream()
            .map(FileDto::new)
            .collect(Collectors.toList());
    }
}