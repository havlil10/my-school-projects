package jsme.nejlepsi.na.svete.nssprojectavjl.exception;

public class InvalidRequestException extends CheatChatException{
    public InvalidRequestException() {
    }

    public InvalidRequestException(String message) {
        super(message);
    }

    public InvalidRequestException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidRequestException(Throwable cause) {
        super(cause);
    }
}
