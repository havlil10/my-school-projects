package jsme.nejlepsi.na.svete.nssprojectavjl.dto;

import jsme.nejlepsi.na.svete.nssprojectavjl.model.User;

public class UserDto {
    private final String avatarUrl;
    private final String name;
    private final String role;
    private final String email;

    public UserDto(User user) {
        this.avatarUrl = user.getAvatarUrl();
        this.name = user.getName();
        this.role = user.getRole().toString();
        this.email = user.getEmail();
    }
}
