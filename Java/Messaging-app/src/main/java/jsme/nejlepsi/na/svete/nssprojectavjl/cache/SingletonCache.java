package jsme.nejlepsi.na.svete.nssprojectavjl.cache;

import jsme.nejlepsi.na.svete.nssprojectavjl.model.Channel;

import java.util.List;
import java.util.Optional;

public class SingletonCache {
    private Cache cache = new Cache(20);
    private static SingletonCache singletonCache = null;

    private SingletonCache() {
    }


    /**
     * Method to get Singleton instance of cache
     * @return the only one instance of cache
     */
    public static SingletonCache getInstance() {
        if(singletonCache==null) {
            synchronized (SingletonCache.class) {
                if(singletonCache==null) {
                    singletonCache = new SingletonCache();
                }
            }
        }
        return singletonCache;
    }


    /**
     * Method to put sm in cache
     * @param key of the new node
     * @param value of the new node
     * @return if it was successful
     */
    public boolean put(Long key, List<Channel> value) {
        return cache.put(key, value);
    }


    /**
     * Method to get sm from cache
     * @param key we want to get value for
     * @return Null if nothing was found or value
     */
    public Optional<List<Channel>> get(Long key) {
        return cache.get(key);
    }


    /**
     * Method to add channel to everyone
     * @param channel we want to add
     */
    public void addChannelToEveryone(Channel channel) {
        cache.addChannelToEveryone(channel);
    }


}
