package jsme.nejlepsi.na.svete.nssprojectavjl.repository;

import jsme.nejlepsi.na.svete.nssprojectavjl.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * Repository for Users
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    User findUserByEmail(String email);
}
