package jsme.nejlepsi.na.svete.nssprojectavjl.service;


import jsme.nejlepsi.na.svete.nssprojectavjl.repository.ChannelRepository;
import jsme.nejlepsi.na.svete.nssprojectavjl.repository.MessageRepository;
import jsme.nejlepsi.na.svete.nssprojectavjl.model.Channel;
import jsme.nejlepsi.na.svete.nssprojectavjl.model.Message;
import jsme.nejlepsi.na.svete.nssprojectavjl.cache.SingletonCache;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.List;
import java.util.Optional;


@Service
@Log
public class ChannelService {

    private final ChannelRepository channelRepository;

    private final MessageRepository messageRepository;

    private final ElasticsearchService elasticsearchService;


    @Autowired
    public ChannelService(ChannelRepository channelRepository, MessageRepository messageRepository, ElasticsearchService elasticsearchService) {
        this.channelRepository = channelRepository;
        this.messageRepository = messageRepository;
        this.elasticsearchService = elasticsearchService;
    }


    public List<Channel> findAll() {
        return channelRepository.findAll();
    }


    @Transactional
    public void createChannel(String topic) {
        Channel newChannel = new Channel();
        newChannel.setTopic(topic);
        channelRepository.save(newChannel);
        SingletonCache.getInstance().addChannelToEveryone(newChannel);
    }


    public List<Message> findMessagesMatch(String message, long channelId) throws IOException {
        long messageId = elasticsearchService.getMessageIdByContentAndChannel(message, channelId);
        if (messageId == -1) {
            return null;
        }

        Optional<Channel> channel = channelRepository.findById(channelId);
        if (channel.isEmpty()) {
            log.warning("could not find channel with this ID");
            return null;
        }

        Optional<Message> foundMessage = messageRepository.findById(messageId);
        if (foundMessage.isEmpty()) {
            log.warning("could not find message with this ID");
            return null;
        }

        return messageRepository.findNAroundByChannel(channel.get(), 20, foundMessage.get());
    }


    public Channel findChannel(Long channelId) {
        return channelRepository.findById(channelId).get();
    }


    public List<Message> findMessagesForChannel(Channel channel) {
        return messageRepository.getAllByChannel(channel);
    }


    public Page<Message> findMessagesByPage(Channel channel, Pageable pageable) {
        return messageRepository.findByChannel(channel, pageable);
    }


    public List<Message> firstLatestMessages(Channel channel, int pageSize) {
        return messageRepository.findTopNByChannel(channel, pageSize);
    }


    public List<Message> findNextLatestMessages(Channel channel, int pageSize, Message lastMessage) {
        return messageRepository.findNextNByChannel(channel, pageSize, lastMessage, false);
    }
}
