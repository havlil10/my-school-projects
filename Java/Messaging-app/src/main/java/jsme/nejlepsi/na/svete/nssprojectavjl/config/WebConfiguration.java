package jsme.nejlepsi.na.svete.nssprojectavjl.config;

import jsme.nejlepsi.na.svete.nssprojectavjl.interceptors.RequestProcessingLengthInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfiguration implements WebMvcConfigurer {

    private final RequestProcessingLengthInterceptor requestProcessingLengthInterceptor;

    @Autowired
    public WebConfiguration(RequestProcessingLengthInterceptor requestProcessingLengthInterceptor) {
        this.requestProcessingLengthInterceptor = requestProcessingLengthInterceptor;
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/{spring:\\w+}")
            .setViewName("forward:/");
        registry.addViewController("/**/{spring:\\w+}")
            .setViewName("forward:/");
        registry.addViewController("/{spring:\\w+}/**{spring:?!(\\.js|\\.css)$}")
            .setViewName("forward:/");
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(requestProcessingLengthInterceptor);
    }
}
