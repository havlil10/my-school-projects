package jsme.nejlepsi.na.svete.nssprojectavjl.kafka.producer;

import jsme.nejlepsi.na.svete.nssprojectavjl.dto.DeleteMessageKafkaDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;


@Component
public class MessageSender {

    private static final Logger LOGGER = LoggerFactory.getLogger(MessageSender.class);

    private final KafkaTemplate<String, DeleteMessageKafkaDto> kafkaTemplate;

    @Autowired
    public MessageSender(KafkaTemplate<String, DeleteMessageKafkaDto> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }


    /**
     * Send something
     * @param kafkaDto kafka
     */
    public void send(DeleteMessageKafkaDto kafkaDto) {
        LOGGER.info("Deleting message with ID: '{}' made by user with email: '{}'", kafkaDto.getMessageId(), kafkaDto.getEmail());
        kafkaTemplate.send("chat", kafkaDto);
    }
}
