package jsme.nejlepsi.na.svete.nssprojectavjl.kafka.consumer;

import jsme.nejlepsi.na.svete.nssprojectavjl.dto.DeleteMessageKafkaDto;
import jsme.nejlepsi.na.svete.nssprojectavjl.service.MessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.io.IOException;


@Service
public class MessageReceiver {

    public static final Logger LOGGER = LoggerFactory.getLogger(MessageReceiver.class);

    private final SimpMessagingTemplate template;

    private final MessageService messageService;

    @Autowired
    public MessageReceiver(SimpMessagingTemplate template, MessageService messageService) {
        this.template = template;
        this.messageService = messageService;
    }


    /**
     * Receive messages
     * @param kafkaDto - kafka
     * @throws IOException if something bad happened
     */
    @KafkaListener(topics = "chat", autoStartup = "${kafka.enabled}")
    public void receive(DeleteMessageKafkaDto kafkaDto) throws IOException {
        LOGGER.info("Received data='{}'", kafkaDto.toString());
        Long toSend = messageService.deleteMessage(kafkaDto.getMessageId(), kafkaDto.getEmail());
        this.template.convertAndSend("/topic/chat/"+ kafkaDto.getChannelId() + "/delete", toSend);
    }
}
