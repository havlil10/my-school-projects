package jsme.nejlepsi.na.svete.nssprojectavjl.interceptors;

import lombok.extern.java.Log;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.Instant;

@Component
@Log
public class RequestProcessingLengthInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        long start = Instant.now().toEpochMilli();

        request.setAttribute("startOfProcessing", start);

        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        long start = (long) request.getAttribute("startOfProcessing");
        long end = Instant.now().toEpochMilli();
        long duration = end - start;

        request.setAttribute("proccessingLength", duration);

        log.info("[" + handler + "] processing took: " + duration + " ms");
    }
}
