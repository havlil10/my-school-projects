package jsme.nejlepsi.na.svete.nssprojectavjl.repository.interfaces;

import java.util.List;
import jsme.nejlepsi.na.svete.nssprojectavjl.model.Channel;
import jsme.nejlepsi.na.svete.nssprojectavjl.model.Message;

public interface CustomMessageRepository {
    List<Message> findTopNByChannel(Channel channel, int pageSize);

    List<Message> findNextNByChannel(Channel channel, int pageSize, Message lastMessage, boolean reverse);

    public List<Message> findNAroundByChannel(Channel channel, int pageSize, Message currentMessage);
}
