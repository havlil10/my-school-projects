package jsme.nejlepsi.na.svete.nssprojectavjl.controller;

import jsme.nejlepsi.na.svete.nssprojectavjl.dto.DeleteMessageKafkaDto;
import jsme.nejlepsi.na.svete.nssprojectavjl.dto.InboundMessageDto;
import jsme.nejlepsi.na.svete.nssprojectavjl.dto.OutboundMessageDto;
import jsme.nejlepsi.na.svete.nssprojectavjl.exception.CheatChatException;
import jsme.nejlepsi.na.svete.nssprojectavjl.kafka.producer.MessageSender;
import jsme.nejlepsi.na.svete.nssprojectavjl.service.MessageService;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageExceptionHandler;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;

import java.io.IOException;

/**
 * Controller for messages
 */
@Controller
@Log
public class MessageController {
    private final MessageService messageService;
    private final MessageSender sender;


    @Autowired
    public MessageController(MessageService messageService, MessageSender sender) {
        this.messageService = messageService;
        this.sender = sender;
    }


    /**
     * Method to send message
     *
     * @param message   we want to send
     * @param auth      check if we can do that
     * @param channelId channel we want to send message to
     * @return OutboundMessageDto
     * @throws IOException if something bad happens
     */
    @MessageMapping("/message/{channelId}")
    @SendTo("/topic/chat/{channelId}")
    public OutboundMessageDto sendMessage(@Payload InboundMessageDto message, Authentication auth, @DestinationVariable Long channelId) throws IOException {
        log.info(message.toString());
        OidcUser oidcUser = (OidcUser) auth.getPrincipal();
        return new OutboundMessageDto(messageService.handleInboundMessage(message.getContent(), oidcUser, channelId));
    }


    /**
     * Method to handle errors
     *
     * @param e error that occurred
     * @return String of error
     */
    @MessageExceptionHandler
    @SendToUser("/queue/chat/errors")
    public String socketExceptionHandler(CheatChatException e) {
        return "Error: " + e.getMessage();
    }


    /**
     * Method to delete message
     *
     * @param messageId message we want to delete
     * @param auth      check if we can do that
     * @param channelId channel we want to delete message in
     */
    @MessageMapping("/message/{channelId}/delete")
    public void deleteMessage(@RequestBody Long messageId, Authentication auth, @DestinationVariable Long channelId) {
        OidcUser oidcUser = (OidcUser) auth.getPrincipal();
        log.info("Delete: " + messageId.toString());
        sender.send(new DeleteMessageKafkaDto(oidcUser.getEmail(), messageId, channelId));
    }
}
