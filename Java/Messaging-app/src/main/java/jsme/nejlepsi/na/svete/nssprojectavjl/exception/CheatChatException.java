package jsme.nejlepsi.na.svete.nssprojectavjl.exception;

/**
 * Base for all application-specific exceptions.
 */
public class CheatChatException extends RuntimeException {

    public CheatChatException() {
    }


    public CheatChatException(String message) {
        super(message);
    }


    public CheatChatException(String message, Throwable cause) {
        super(message, cause);
    }


    public CheatChatException(Throwable cause) {
        super(cause);
    }
}
