package jsme.nejlepsi.na.svete.nssprojectavjl.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Controller for home page
 */
@Controller
public class IndexController {

    /**
     * Endpoint for homepage
     *
     * @return home page
     */
    @GetMapping("/")
    public ModelAndView home() {
        return new ModelAndView("index");
    }

}
