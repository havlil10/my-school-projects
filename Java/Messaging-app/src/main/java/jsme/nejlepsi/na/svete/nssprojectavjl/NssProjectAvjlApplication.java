package jsme.nejlepsi.na.svete.nssprojectavjl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class NssProjectAvjlApplication {

    public static void main(String[] args) {
        SpringApplication.run(NssProjectAvjlApplication.class, args);
    }

}
