package jsme.nejlepsi.na.svete.nssprojectavjl.service;

import com.amazonaws.HttpMethod;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import jsme.nejlepsi.na.svete.nssprojectavjl.repository.ChannelRepository;
import jsme.nejlepsi.na.svete.nssprojectavjl.repository.FileRepository;
import jsme.nejlepsi.na.svete.nssprojectavjl.repository.UserRepository;
import jsme.nejlepsi.na.svete.nssprojectavjl.exception.*;
import jsme.nejlepsi.na.svete.nssprojectavjl.model.Channel;
import jsme.nejlepsi.na.svete.nssprojectavjl.model.File;
import jsme.nejlepsi.na.svete.nssprojectavjl.model.User;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.stereotype.Service;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@Log
public class BucketService {
    private final AmazonS3 amazonS3Client;

    @Value("${application.bucket.name}")
    private String bucketName;

    private final FileRepository fileRepository;

    private final ChannelRepository channelRepository;

    private final UserRepository userRepository;

    @Autowired
    public BucketService(AmazonS3 amazonS3Client, FileRepository fileRepository, ChannelRepository channelRepository, UserRepository userRepository) {
        this.amazonS3Client = amazonS3Client;
        this.fileRepository = fileRepository;
        this.channelRepository = channelRepository;
        this.userRepository = userRepository;
    }

    /**
     * Generates a URL for file upload and validates if user can upload a file.
     *
     * @param oidcUser user who wants to upload the file.
     * @param channelId Channel id to which the file is being uploaded.
     * @param displayName original name of the file.
     * @return URL to which the file is uploaded to.
     * @throws CheatChatException
     */
    public URL prepareFileUpload(OidcUser oidcUser, Long channelId, String displayName) throws CheatChatException {
        String objectKey = generateObjectKey();
        User user = userRepository.findUserByEmail(oidcUser.getEmail());
        Channel channel = findChannelAndValidate(channelId, user);
        createFileObjectInDB(objectKey, channel, user, displayName);
        return genereateUrl(objectKey);
    }

    private String generateObjectKey(){
        return UUID.randomUUID().toString();
    }

    /**
     * Generates a URL signed by an IAM user that allows file upload to a bucket.
     *
     * @param objectKey generated name for the file. This is the name used by the S3 bucket.
     * @return URL to which a file can be uploaded.
     */
    private URL genereateUrl(String objectKey){
        Date expiration = Date.from(Instant.now());
        expiration.setTime(expiration.toInstant().toEpochMilli() + 1000 * 60);

        GeneratePresignedUrlRequest request = new GeneratePresignedUrlRequest(bucketName, objectKey)
                .withMethod(HttpMethod.PUT)
                .withExpiration(expiration);
        return amazonS3Client.generatePresignedUrl(request);
    }

    private void createFileObjectInDB(String name, Channel channel, User user, String displayName){
        File file = new File(name, channel, user, displayName);
        fileRepository.save(file);
    }

    /**
     * Validates if user is able to send stuff to a channel and also finds the channel.
     *
     * @param channelId
     * @param user
     * @return found channel.
     * @throws CheatChatException
     */
    private Channel findChannelAndValidate(Long channelId, User user) throws CheatChatException{
        Optional<Channel> channel = channelRepository.findById(channelId);
        if (channel.isEmpty()){
            throw new InvalidRequestException("No such channel found.");
        }

        Channel ch = channel.get();

        if(!ch.isSecret() || ch.getUsers().contains(user)){
            return ch;
        }
        else{
            throw new AuthenticationFailureException("User is not part of this channel.");
        }
    }

    /**
     * Updates DB file object with URL where the file is located.
     *
     * @param fileUrl URL where the file is located in cloud.
     */
    public void putSavedFileToDB(String fileUrl){
        String name = getNameFromURL(fileUrl);
        File file = fileRepository.findFileByName(name);
        if (file == null) {
            throw new InvalidRequestException("No such file upload was requested.");
        }
        file.setFileUrl(fileUrl);
        fileRepository.save(file);
    }

    /**
     * Extracts file name from an S3 URL
     *
     * @param fileUrl S3 URL where the file is saved
     * @return name of the file. (UUID)
     * @throws CheatChatException exception for invalid URLs.
     */
    private String getNameFromURL(String fileUrl) throws CheatChatException {
        try {
            URL url = new URL(fileUrl);
            String path = url.getPath();
            return path.replaceFirst("/", "");
        }
        catch (MalformedURLException e){
            log.info("Cannot parse URL because fuck you.");
        }
        throw new InvalidRequestException("File cannot be saved because URL is invalid.");
    }


    /**
     * Returns all files that were uploaded to the channel.
     *
     * @param channelId
     * @param oidcUser
     * @return
     */
    public List<File> getFilesFromChannel(Long channelId, OidcUser oidcUser) {
        User user = userRepository.findUserByEmail(oidcUser.getEmail());

        Channel channel = findChannelAndValidate(channelId, user);

        return fileRepository.getFilesByChannel(channel);
    }
}
