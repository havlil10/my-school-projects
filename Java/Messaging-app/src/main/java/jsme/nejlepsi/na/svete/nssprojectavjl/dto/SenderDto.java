package jsme.nejlepsi.na.svete.nssprojectavjl.dto;

import jsme.nejlepsi.na.svete.nssprojectavjl.model.User;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class SenderDto {

    private final Long Id;
    private final String name;
    private final String avatarUrl;

    public SenderDto(User sender) {
        this.Id = sender.getId();
        this.name = sender.getName();
        this.avatarUrl = sender.getAvatarUrl();
    }
}
