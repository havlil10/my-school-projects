package jsme.nejlepsi.na.svete.nssprojectavjl.service;

import jsme.nejlepsi.na.svete.nssprojectavjl.model.Channel;

import jsme.nejlepsi.na.svete.nssprojectavjl.model.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
@Transactional
public class ChannelServiceTest {

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private ChannelService channelService;


    @Test
    public void createChannelAddsChannel() {

        Channel channel = new Channel();
        em.persist(channel);

        Channel resultChannel = em.find(Channel.class, channel.getId());

        assertNotNull(resultChannel);
        assertEquals(channel.getId(), resultChannel.getId());
    }
}
