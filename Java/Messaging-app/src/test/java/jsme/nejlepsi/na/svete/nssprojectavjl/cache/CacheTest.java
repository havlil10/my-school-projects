package jsme.nejlepsi.na.svete.nssprojectavjl.cache;

import jsme.nejlepsi.na.svete.nssprojectavjl.model.Channel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

public class CacheTest {
    private Cache cache;

    @BeforeEach
    public void setUp() {
        cache = new Cache(3);
    }

    @Test
    public void cacheGetTest_withNoContent_returnsNothing() {
        Optional<List<Channel>> users = cache.get(1L);
        assertFalse(users.isPresent());
    }

    @Test
    public void putTest_addRecord_shouldReturnRecord() {
        List<Channel> channels = new ArrayList<>();
        channels.add(new Channel());
        cache.put(4L, channels);

        Optional<List<Channel>> result = cache.get(4L);
        Optional<List<Channel>> resultThatShouldBeEmpty = cache.get(1L);

        assertTrue(result.isPresent());
        assertSame(channels, result.get());

        assertFalse(resultThatShouldBeEmpty.isPresent());
    }

    @Test
    public void cacheExtendingInitialSize_stillWorks() {
        cache.put(2L, new ArrayList<>());
        cache.put(3L, new ArrayList<>());
        cache.put(4L, new ArrayList<>());
        cache.put(5L, new ArrayList<>());


        Optional<List<Channel>> resultThatShouldNotBeEmpty1 = cache.get(3L);
        Optional<List<Channel>> resultThatShouldNotBeEmpty2 = cache.get(4L);
        Optional<List<Channel>> resultThatShouldNotBeEmpty3 = cache.get(5L);
        Optional<List<Channel>> resultThatShouldNotBeEmpty4 = cache.get(2L);

        assertTrue(resultThatShouldNotBeEmpty1.isPresent());
        assertTrue(resultThatShouldNotBeEmpty2.isPresent());
        assertTrue(resultThatShouldNotBeEmpty3.isPresent());
        assertTrue(resultThatShouldNotBeEmpty4.isPresent());
    }

    @Test
    public void putSameRecord_returnsCorrectRecord() {
        List<Channel> channels = new ArrayList<>();
        channels.add(new Channel());

        cache.put(2L, channels);
        cache.put(2L, new ArrayList<>());

        Optional<List<Channel>> result = cache.get(2L);

        assertTrue(result.isPresent());
        assertEquals(0, result.get().size());

    }

}
