import {BrowserRouter, Routes, Route} from "react-router-dom";
import Chat from "./pages/Chat";
import Login from "./pages/Login";
import Channel from "./components/Channel";
import {createContext, useMemo, useState} from "react";
import Search from "./components/Search";
import Messages from "./components/Messages";

export const UserContext = createContext({
  user: null,
  setUser: () => {},
});


function App() {
  const [user, setUser] = useState(null);
  const value = useMemo(
    () => ({ user, setUser }),
    [user]
  );

  return (
    <UserContext.Provider value={value}>
      <BrowserRouter>
        <Routes>
          <Route path="/login" element={<Login/>}/>
          <Route path="/" element={<Chat/>}>
            <Route path="/chat/:channelId" element={<Channel/>}>
              <Route index element={<Messages/>}/>
              <Route path="/chat/:channelId/find" element={<Search/>}/>
            </Route>
          </Route>
        </Routes>
      </BrowserRouter>
    </UserContext.Provider>
  );
}

export default App;
