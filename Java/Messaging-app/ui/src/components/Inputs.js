import {MessageInput} from "@chatscope/chat-ui-kit-react";
import {useStompClient} from "react-stomp-hooks";
import {useParams} from "react-router-dom";
import {useState} from "react";
import { Dialog } from '@headlessui/react'
import { FileIcon } from 'react-file-icon';
import axios from "axios";

function Inputs() {
  const { channelId } = useParams();
  const stompClient = useStompClient();
  const [isOpen, setIsOpen] = useState(false);
  const [file, setFile] = useState(null);
  const input = document.createElement('input');
  input.type = 'file';

  input.onchange = (e) => {
    setFile(e.target.files[0]);
    setIsOpen(true);
  }

  const handleSendFile = async () => {
    const { data: url } = await axios.post(`/file/upload/${channelId}`, file.name, {
      headers: {
        'Content-Type': 'text/plain',
      }
    });
    await axios.put(url, file);
    const imageUrl = url.split('?')[0]
    await axios.post('/file/upload/', imageUrl, {
      headers: {
        'Content-Type': 'text/plain',
      }
    });
    setIsOpen(false);
  };

  const handleSend = (_, textContent) => {
    if (stompClient) {
      stompClient.publish({
        destination: `/ws/message/${channelId}`,
        body: JSON.stringify({ content: textContent.trim() }),
      });
    }
  };

  return (
    <div>
      {file &&
        <Dialog
          open={isOpen}
          onClose={() => setIsOpen(false)}
          className="relative z-50"
        >
          <div className="fixed inset-0 bg-black/30" aria-hidden="true" />
          <div className="fixed inset-0 flex items-center justify-center p-4">
            <Dialog.Panel className="w-full max-w-sm rounded bg-white p-10">
              <Dialog.Title
                  as="h3"
                  className="text-lg font-bold leading-6 text-center">
                Send file
              </Dialog.Title>
              <div className="flex flex-col justify-center items-center my-5 text-center">
                <div className="w-20 mb-3">
                  <FileIcon extension={file.name.split('.').pop()}/>
                </div>
                {file.name}
              </div>

              <div className="mt-4 flex justify-center space-x-4">
                <button
                  type="button"
                  className="inline-flex justify-center rounded-md border border-transparent bg-blue-100 px-4 py-2 text-sm font-medium text-blue-900 hover:bg-blue-200 focus:outline-none focus-visible:ring-2 focus-visible:ring-blue-500 focus-visible:ring-offset-2"
                  onClick={handleSendFile}
                >
                  Send
                </button>
                <button
                    type="button"
                    className="inline-flex justify-center rounded-md border border-transparent bg-red-100 px-4 py-2 text-sm font-medium text-red-900 hover:bg-red-200 focus:outline-none focus-visible:ring-2 focus-visible:ring-red-500 focus-visible:ring-offset-2"
                    onClick={() => setIsOpen(false)}
                >
                  Cancel
                </button>
              </div>
            </Dialog.Panel>
          </div>
        </Dialog>
      }
      <MessageInput placeholder="Type message here" onSend={handleSend} onAttachClick={() => input.click()}/>
    </div>
  );
}

export default Inputs;
