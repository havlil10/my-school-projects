import {useParams} from "react-router-dom";
import {
  useCallback,
  useEffect,
  useRef,
  useState
} from "react";
import {useSubscription} from "react-stomp-hooks";
import InfiniteScroll from 'react-infinite-scroll-component';
import axios from "axios";
import CustomMessageList from "./CustomMessageList";
import {Loader} from "@chatscope/chat-ui-kit-react";

const MESSAGES_PER_PAGE = 20;

function Messages() {
  const { channelId } = useParams();
  const [messages, setMessages] = useState([]);
  const lastMessageIdRef = useRef(null);
  const [hasMore, setHasMore] = useState(false);

  useSubscription(`/topic/chat/${channelId}`, (payload) => {
    const message = JSON.parse(payload.body);
    setMessages((messages) => [message, ...messages])
  });

  useSubscription(`/topic/chat/${channelId}/delete`, (payload) => {
    const toDeleteId = parseInt(payload.body);
    setMessages((messages) => {
      return messages.filter((message) => message.id !== toDeleteId);
    })
  });

  useEffect(() => {
    axios.get(`/rest/v1/channel/${channelId}/messages`, {
      params: {size: MESSAGES_PER_PAGE},
    }).then(({data}) => {
      setMessages(data);
      setHasMore(data.length === MESSAGES_PER_PAGE);
      lastMessageIdRef.current = data[data.length - 1]?.id;
    });
  }, [channelId]);

  const loadMore = useCallback(async () => {
    const { data } = await axios.get(`/rest/v1/channel/${channelId}/messages`, {
      params: {lastMessageId: lastMessageIdRef.current, size: MESSAGES_PER_PAGE},
    });
    setMessages((items) => [...items, ...data]);
    setHasMore(data.length === MESSAGES_PER_PAGE);
    lastMessageIdRef.current = data[data.length - 1].id;
  }, [channelId]);

  return (
    <div
      id="scrollableDiv"
      className="overflow-auto flex flex-col-reverse h-full px-3 pb-5"
    >
      <InfiniteScroll
        dataLength={messages.length}
        next={loadMore}
        className="flex flex-col-reverse"
        inverse={true}
        hasMore={hasMore}
        loader={<div className="w-full flex justify-center py-5"><Loader /></div>}
        scrollableTarget="scrollableDiv"
      >
        <CustomMessageList
          messages={messages}
        />
      </InfiniteScroll>
    </div>
  );
}

export default Messages;