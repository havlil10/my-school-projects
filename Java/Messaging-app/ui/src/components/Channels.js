import {
  Conversation,
  ConversationList,
} from "@chatscope/chat-ui-kit-react";
import useAxios from "axios-hooks";
import {PlusIcon} from '@heroicons/react/solid';
import {
  NavLink,
} from "react-router-dom";
import {useState} from "react";

function Channels() {
  const [{data: channels, loading, error}, execute] = useAxios('/rest/v1/user/channels')
  const [{loading: createLoading, error: createError}, executeCreate] = useAxios({
    url: '/rest/v1/channel/create',
    method: 'POST',
    headers: {
      'Content-Type': 'text/plain',
    }
  }, {manual: true});
  const [topic, setTopic] = useState("");
  const [expanded, setExpanded] = useState(false);

  const handleCreate = () => {
    executeCreate({
      data: topic,
    }).then(() => {
      setExpanded(false);
      execute();
    }).catch(() => {});
  };

  if (error) {
    return (
      <div className="grid place-items-center h-full">
        <div className="text-center">
          <p>Something went wrong.</p>
          <button className="text-blue-700" onClick={() => execute()}>
            Try again
          </button>
        </div>
      </div>
    );
  }

  return (
    loading ?
      <ConversationList className="mt-3" loading={loading}/>
    :
      <ConversationList className="mt-3" loading={loading}>
        <div as="Conversation" className="uppercase font-bold mx-3 text-sm justify-between hidden md:flex mb-3">
          Channels
          <button onClick={() => setExpanded(true)}>
            <PlusIcon className="h-5 w-5"/>
          </button>
        </div>
        {expanded &&
          <div as="Conversation" className="px-3 my-4 flex flex-col">
            <div className="flex space-x-3">
              <input
                  type="text"
                  placeholder="Topic"
                  className="rounded-md bg-blue-100 shrink min-w-0 py-2 px-3 text-sm"
                  onInput={(e) => setTopic(e.target.value)}
                  disabled={createLoading}
              />
              <button
                  disabled={createLoading}
                  className="bg-green-200 rounded-md px-3"
                  onClick={handleCreate}
              >
                Create
              </button>
            </div>
            {createError && <span className="text-red-500 mt-1 ml-1">Failed to create channel</span>}
          </div>
        }
        {channels && channels.map((channel) => (
          <NavLink as="Conversation" to={`/chat/${channel.id}`} key={channel.id}>
            {({ isActive }) => (
              <Conversation
                active={isActive}
              >
                <div as="Avatar">
                  {channel.topic}
                </div>
              </Conversation>
            )}

          </NavLink>
        ))}
      </ConversationList>
  );
}

export default Channels;