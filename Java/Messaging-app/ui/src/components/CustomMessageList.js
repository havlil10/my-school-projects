import {Message} from "@chatscope/chat-ui-kit-react";
import {UserContext} from "../App";
import {useCallback, useContext, useMemo} from "react";
import Linkify from 'react-linkify';
import {TrashIcon} from '@heroicons/react/outline';
import {useStompClient} from "react-stomp-hooks";
import {useParams} from "react-router-dom";

function CustomMessageList({messages}) {
  const { channelId } = useParams();
  const { user } = useContext(UserContext);
  const stompClient = useStompClient();

  const addMessageByGroup = useCallback((messages, message) => {
    const messageToAdd = {
      ...message,
      direction: message.sender.id === user.id ? 'outgoing' : 'incoming',
    };

    if (messages.length > 0) {
      const lastMessage = messages[messages.length - 1];

      if (lastMessage.sender.id === message.sender.id) {
        if (lastMessage.position === 'first') lastMessage.position = 'normal';
        if (lastMessage.position === 'single') lastMessage.position = 'last';
        messageToAdd.position = 'first';
        return [...messages, messageToAdd];
      }
    }

    messageToAdd.position = 'single';

    return [...messages, messageToAdd];
  }, [user]);

  const messageGroups = useMemo(() => {
    return messages ? messages.reduce(addMessageByGroup, []) : [];
  }, [messages, addMessageByGroup]);

  const handleDelete = (message) => {
    if (stompClient) {
      stompClient.publish({
        destination: `/ws/message/${channelId}/delete`,
        body: message.id,
      });
    }
  }

  return (messageGroups.map((m) => (
    <div
      as="Message"
      key={m.id}
      className={`flex justify-end ${m.direction === 'incoming' ? 'pr-12 flex-row-reverse' : 'pl-12'}`}
    >
      <div className={`flex items-end group ${m.direction === 'incoming' && 'flex-row-reverse'}`}>
        {(m.direction === 'outgoing' || user.role === 'ADMIN') &&
          <button
            className="p-2.5 hidden group-hover:block"
            onClick={() => handleDelete(m)}
          >
            <TrashIcon className="h-5 w-5 text-gray-400 hover:text-red-500"/>
          </button>
        }
        <Message
          model={{
            type: "text",
            payload: m.content,
            direction: m.direction,
            position: m.position,
          }}
          avatarSpacer={m.direction === 'incoming' && ['normal', 'first'].includes(m.position)}
        >
          {['single', 'first'].includes(m.position) &&
            <Message.Header sender={m.sender.name} />
          }
          <Message.CustomContent>
            <Linkify componentDecorator={(decoratedHref, decoratedText, key) => <a target="blank" rel="noopener" href={decoratedHref} key={key}>
              {decoratedText}
            </a>}>
              {m.content}
            </Linkify>
          </Message.CustomContent>
          {['single', 'last'].includes(m.position) && m.direction === 'incoming' &&
            <img
              as="Avatar"
              src={m.sender.avatarUrl}
              className="rounded-full"
              referrerPolicy="no-referrer"
              alt={m.sender.name}
            />
          }
        </Message>
      </div>
    </div>
  )));
}

export default CustomMessageList;