import {
  ChatContainer,
  ConversationHeader, ExpansionPanel,
  MessageInput, MessageList, Sidebar,
} from "@chatscope/chat-ui-kit-react";
import {
  createSearchParams,
  Navigate,
  useNavigate,
  useParams,
  Outlet, useMatch
} from "react-router-dom";
import useAxios from "axios-hooks";
import Inputs from "./Inputs";
import {StompSessionProvider} from "react-stomp-hooks";
import axios from "axios";
import fileDownload from "js-file-download";
import {FileIcon} from "react-file-icon";
import {DotsHorizontalIcon, SearchIcon} from "@heroicons/react/solid";
import {useEffect, useMemo, useState} from "react";
import {useLocation} from "react-use";

function Channel() {
  const { channelId } = useParams();
  const location = useLocation();
  const [{ data: channel, loading, error }] = useAxios(`/rest/v1/channel/${channelId}`)
  const [{ data: dataFiles, loadingFiles }, execute] = useAxios(`/file/${channelId}`);
  const [searchExpanded, setSearchExpanded] = useState(false);
  const [panelExpanded, setPanelExpanded] = useState(false);
  const [searchString, setSearchString] = useState('');
  const navigate = useNavigate();
  const match = useMatch('/chat/:channelId');
  const inputsVisible = !!match;

  useEffect(() => {
    setSearchString('');
    setSearchExpanded(false);
  }, [location]);

  const wsUrl = useMemo(() => {
    const protocol = location.protocol === 'https:' ? 'wss:' : 'ws:';
    return `${protocol}//${process.env.REACT_APP_API_BASE_URL}/ws/chat/${channelId}`;
  }, [location, channelId])

  const handleDownload = async (fileUrl, displayName) => {
    const { data } = await axios.get(fileUrl, {
      responseType: 'blob',
    });
    fileDownload(data, displayName);
  }

  if (error) {
    return <Navigate to="/" replace={true} />;
  }

  if (loading) {
    return <ChatContainer><MessageList loading={true}/></ChatContainer>
  }

  const handleSearch = (e) => {
    if (e.key === "Enter") {
      navigate({
        pathname: 'find',
        search: createSearchParams({
          message: searchString,
        }).toString(),
        replace: true,
      });
    }
  }

  return (channel &&
      <StompSessionProvider
        url={wsUrl}
      >
        <ChatContainer>
          <ConversationHeader>
            <ConversationHeader.Content>
              <span className="h-8 flex items-center">{channel.topic}</span>
            </ConversationHeader.Content>
            <ConversationHeader.Actions className="flex space-x-4">
              {searchExpanded ?
                <div className="rounded-md bg-blue-100 shrink min-w-0 py-1 px-3 text-sm flex items-center space-x-2">
                  <SearchIcon className="w-4 h-4 text-cyan-500 hover:text-cyan-600"/>
                  <input
                    type="text"
                    value={searchString}
                    onInput={({target: { value }}) => {setSearchString(value)}}
                    className="bg-blue-100 outline-0"
                    placeholder="Find message..."
                    onKeyDown={handleSearch}
                  />
                </div>
              :
                <button onClick={() => setSearchExpanded(true)}>
                  <SearchIcon className="w-5 h-5 text-cyan-500 hover:text-cyan-600"/>
                </button>
              }
              <button onClick={() => setPanelExpanded((expanded) => !expanded)}>
                <DotsHorizontalIcon className="w-5 h-5 text-cyan-500 hover:text-cyan-600"/>
              </button>
            </ConversationHeader.Actions>
          </ConversationHeader>
          <Outlet as={MessageList}/>
          {inputsVisible &&
            <Inputs as={MessageInput}/>
          }
        </ChatContainer>
        <Sidebar position="right" className={panelExpanded ? '!flex' : '!hidden'}>
          <ExpansionPanel open title="FILES">
            {
              loadingFiles ?
                <div>Loading...</div>
              :
                <div>
                  {
                    dataFiles?.length > 0 ?
                      <div className="flex flex-col space-y-2">
                        {dataFiles.map(({fileUrl, displayName}, index) =>
                          <button
                            key={index}
                            onClick={() => handleDownload(fileUrl, displayName)}
                            className="flex items-center"
                          >
                            <div className="h-5 inline-flex mr-2 shrink-0">
                              <FileIcon extension={displayName.split('.').pop()}/>
                            </div>
                            <span className="truncate">
                              {displayName}
                            </span>
                          </button>
                        )}
                      </div>
                      :
                      <p className="text-center text-gray-500 text-xs my-5">There are no files in this channel yet.</p>
                  }
                  <div className="text-center mt-3">
                    <button
                      type="button"
                      className="inline-flex justify-center rounded-md border border-transparent bg-blue-100 px-3 py-1 text-sm font-medium text-blue-900 hover:bg-blue-200 focus:outline-none focus-visible:ring-2 focus-visible:ring-blue-500 focus-visible:ring-offset-2"
                      onClick={() => execute()}
                    >
                      Refresh
                    </button>
                  </div>
                </div>
            }
          </ExpansionPanel>
        </Sidebar>
      </StompSessionProvider>
  );
}

export default Channel;