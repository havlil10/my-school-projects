import {
  ChatContainer,
  MessageList,
} from "@chatscope/chat-ui-kit-react";
import {Navigate, useParams, useSearchParams} from "react-router-dom";
import useAxios from "axios-hooks";
import {useMemo} from "react";
import CustomMessageList from "./CustomMessageList";

function Channel() {
  const { channelId } = useParams();
  const [{ data: channel, loading, error }] = useAxios(`/rest/v1/channel/${channelId}`);
  const [searchParams] = useSearchParams();
  const search = useMemo(() => {
    return searchParams.get("message") || ''
  }, [searchParams]);
  const [{ data: messages, messagesLoading, messagesError}] = useAxios({
    url: `/rest/v1/channel/${channelId}/findMessage`,
    params: {
      message: search,
    }
  })

  if (error) {
    return <Navigate to="/" replace={true} />;
  }

  if (loading) {
    return <ChatContainer><MessageList loading={true}/></ChatContainer>
  }

  return (channel &&
    <div className="overflow-auto flex flex-col-reverse h-full px-3 pb-5">
      <div className="flex flex-col-reverse">
        <CustomMessageList
          messages={messages}
        />
      </div>
    </div>
  );
}

export default Channel;