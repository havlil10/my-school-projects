import "@chatscope/chat-ui-kit-styles/dist/default/styles.min.css";
import {
  MainContainer,
  Sidebar,
} from "@chatscope/chat-ui-kit-react";

import {LogoutIcon} from '@heroicons/react/solid';
import {Navigate, Outlet, useNavigate} from "react-router-dom";
import useAxios from "axios-hooks";
import Channels from "../components/Channels";
import {useContext, useEffect} from "react";
import {UserContext} from "../App";
import axios from "axios";
import {ShieldCheckIcon} from "@heroicons/react/outline";

function Chat() {
  const [{ data, loading, error }] = useAxios('/rest/v1/user/me')
  const { user, setUser } = useContext(UserContext);
  const navigate = useNavigate();

  useEffect(() => {
    data && setUser(data);
  }, [data, setUser]);

  if (error) {
    const status = error.response.status
    if (status === 403)
      return <Navigate to="/login" replace={true} />;
    return <div>{JSON.stringify(error.response.statusText)}</div>
  }

  const handleLogout = async () => {
    await axios.post('/logout');
    navigate('/login');
  }

  return (
    <div className="h-screen">
      <MainContainer responsive>
        {
          loading ?
          <Sidebar position="left" loading={loading} scrollable>
            <div className="flex items-center gap-4 p-4">
              <div className="flex flex-col">
                <span className="h-5 w-32 bg-gray-200 my-0.5"/>
                <span className="h-4 w-24 bg-gray-200 my-0.5"/>
              </div>
            </div>
          </Sidebar>
          :
          <Sidebar className="!flex"  position="left" scrollable>
            {
              user &&
              <div className="flex gap-2 items-center gap-4 p-2 md:p-4">
                <img
                  src={user.avatarUrl}
                  className="rounded-full w-12"
                  referrerPolicy="no-referrer"
                  alt={user.name}
                />
                <div className="flex flex-col hidden md:block flex-shrink min-w-0">
                  <span className="font-bold break-all">
                    {user.name}
                    {user.role === 'ROLE_ADMIN' &&
                      <span title="Admin">
                        <ShieldCheckIcon
                          className="w-5 h-5 text-green-700 inline ml-2 align-text-bottom"
                        />
                      </span>
                    }
                  </span>
                  <span className="text-sm text-gray-600 truncate">{user.email}</span>
                </div>
                <LogoutIcon
                    className="h-6 w-6 text-red-500 ml-auto cursor-pointer flex-shrink-0 hidden md:block"
                    onClick={handleLogout}
                />
              </div>
            }
            <Channels />
          </Sidebar>
        }

        <Outlet/>
      </MainContainer>
    </div>
  );
}

export default Chat;