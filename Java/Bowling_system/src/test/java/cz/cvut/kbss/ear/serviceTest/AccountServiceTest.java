package cz.cvut.kbss.ear.serviceTest;

import cz.cvut.kbss.ear.model.BowlingTeam;
import cz.cvut.kbss.ear.model.account.Account;
import cz.cvut.kbss.ear.service.AccountService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Transactional
@TestPropertySource(locations = "classpath:application-test.properties")
public class AccountServiceTest {
    @Autowired
    private EntityManager em;

    @Autowired
    private AccountService accountService;

    private Account userAccount;

    private BowlingTeam bowlingTeam;

    @BeforeEach
    public void setUp() {
        this.userAccount = new Account("heslo1234", "UserTest", "fel@fel.cvut.cz", 756232545, "Franta", "Omacka");
        em.persist(userAccount);
        bowlingTeam = new BowlingTeam();
        bowlingTeam.setName("TestTeam");
        em.persist(bowlingTeam);
    }

    @Test
    public void addTeamAddsTeamToAccountAndUpdatesBowlingTeam(){
        assertEquals(new ArrayList<>(), bowlingTeam.getAccounts());
        accountService.addTeam(userAccount, bowlingTeam);

        final Account resultAcc = em.find(Account.class, userAccount.getId());
        final BowlingTeam resultTeam = em.find(BowlingTeam.class, bowlingTeam.getId());
        ArrayList<BowlingTeam> expectedTeam = new ArrayList<>();
        expectedTeam.add(bowlingTeam);
        ArrayList<Account> expectedAccounts = new ArrayList<>();
        expectedAccounts.add(userAccount);
        assertEquals(expectedTeam, resultAcc.getBowlingTeams());
        assertEquals(expectedAccounts, resultTeam.getAccounts());
    }

    @Test
    public void removeTeamRemovesTeamAndUpdatesTeam(){
        bowlingTeam.addAccount(userAccount);
        userAccount.addTeam(bowlingTeam);

        accountService.removeTeam(userAccount, bowlingTeam);

        final Account resultAcc = em.find(Account.class, userAccount.getId());
        final BowlingTeam resultTeam = em.find(BowlingTeam.class, bowlingTeam.getId());
        assertEquals(new ArrayList<>(), resultAcc.getBowlingTeams());
        assertEquals(new ArrayList<>(), resultTeam.getAccounts());
    }

    @Test
    public void banAccountTest(){
        accountService.banAccount(userAccount);

        final Account result = em.find(Account.class, userAccount.getId());
        assertTrue(result.isBanned());
    }

    @Test
    public void unbanAccountTest(){
        userAccount.setBanned(true);
        accountService.unbanAccount(userAccount);

        final Account result = em.find(Account.class, userAccount.getId());
        assertFalse(result.isBanned());
    }
}
