package cz.cvut.kbss.ear.DAOtest;

import cz.cvut.kbss.ear.BowlingApp;
import cz.cvut.kbss.ear.DAO.AccountDao;
import cz.cvut.kbss.ear.model.account.Account;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@ComponentScan(basePackageClasses = BowlingApp.class)
public class AccountDaoTest {

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private AccountDao userDao;

    @Test
    public void findUserByPhoneNumberTest(){
        final Account acc = new Account("heslo1234", "UserTest", "fel@fel.cvut.cz", 756232545, "Franta", "Omacka");
        assertFalse(acc.isBanned());
        em.persist(acc);

        final Account result = userDao.findUserByPhoneNumber(756232545);
        assertNotNull(result);
        assertEquals(acc.getPhoneNumber(), result.getPhoneNumber());
        assertEquals(acc.getUsername(), result.getUsername());

    }

    @Test
    public void findUserByUsernameTest(){
        final Account acc = new Account("heslo1234", "UserTest", "fel@fel.cvut.cz", 756232545, "Franta", "Omacka");
        assertFalse(acc.isBanned());
        em.persist(acc);

        final Account result = userDao.findByUsername("UserTest");
        assertNotNull(result);
        assertEquals(acc.getPhoneNumber(), result.getPhoneNumber());
        assertEquals(acc.getUsername(), result.getUsername());

    }
}
