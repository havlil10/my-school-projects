package cz.cvut.kbss.ear.serviceTest;

import cz.cvut.kbss.ear.model.*;
import cz.cvut.kbss.ear.model.account.Account;
import cz.cvut.kbss.ear.model.account.ManagerAccount;
import cz.cvut.kbss.ear.service.ReservationService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Transactional
@TestPropertySource(locations = "classpath:application-test.properties")
public class ReservationServiceTest {
    @PersistenceContext
    private EntityManager em;

    @Autowired
    private ReservationService sut;


    @Disabled
    @Test
    public void createReservationTest() {
        List<TimeSlot> timeSlots = new ArrayList<>();
        TimeSlot timeSlot1 = new TimeSlot(new Date(), 12, 150);
        TimeSlot timeSlot2 = new TimeSlot(new Date(), 14, 150);
        timeSlots.add(timeSlot1);
        timeSlots.add(timeSlot2);
        em.persist(timeSlot1);
        em.persist(timeSlot2);
        Account account = new Account("heslo1234", "UserTest", "fel@fel.cvut.cz", 756232545, "Franta", "Omacka");
        em.persist(account);
        try {
            sut.createReservation(timeSlots, account);
        }
        catch (Exception e){
            fail("Bad exception thrown.");
        }

        for(TimeSlot timeSlot : timeSlots) {
            assertTrue(timeSlot.isReserved());
        }

        assertEquals(sut.findAll().size(), 1);
    }

    @Test
    public void cancelReservation_cancelsReservation() {
        List<TimeSlot> timeSlots = new ArrayList<>();
        timeSlots.add(new TimeSlot(new Date(), 12, 150));
        timeSlots.add(new TimeSlot(new Date(), 14, 150));
        Reservation reservation = new Reservation(timeSlots, new ManagerAccount("heslo1234", "UserTest", "fel@fel.cvut.cz", 756232545, "Franta", "Omacka"));
        em.persist(reservation);
        for(TimeSlot timeSlot : reservation.getTimeSlots()) {
            timeSlot.setReserved(true);
        }

        sut.cancelReservation(reservation);
        Reservation myReservation = em.find(Reservation.class, reservation.getId());

        for(TimeSlot timeSlot : myReservation.getTimeSlots()) {
            assertFalse(timeSlot.isReserved());
        }
        assertTrue(myReservation.isCanceled());
    }

    @Test
    public void findAllReservationsByPhoneNumber_works() {
        List<TimeSlot> timeSlots = new ArrayList<>();
        TimeSlot testTimeSlot1 = new TimeSlot(new Date(), 12, 150);
        TimeSlot testTimeSlot2 = new TimeSlot(new Date(), 14, 150);
        timeSlots.add(testTimeSlot1);
        timeSlots.add(testTimeSlot2);
        em.persist(testTimeSlot1);
        em.persist(testTimeSlot2);
        List<TimeSlot> timeSlots2 = new ArrayList<>();

        Account account = new Account("heslo1234", "UserTest", "fel@fel.cvut.cz", 123456789, "Franta", "Omacka");
        em.persist(account);

        Reservation reservation1 = new Reservation(timeSlots, account);
        Reservation reservation2 = new Reservation(timeSlots2, account);
        em.persist(reservation1);
        em.persist(reservation2);

        List<Reservation> reservations = sut.findAllReservationsByPhoneNumber(account.getPhoneNumber());
        assertEquals(2, reservations.size());
        assertTrue(reservations.stream().anyMatch(c -> c.getId().equals(reservation1.getId())));
        assertTrue(reservations.stream().anyMatch(c -> c.getId().equals(reservation2.getId())));
    }
}
