package cz.cvut.kbss.ear.serviceTest;

import cz.cvut.kbss.ear.model.account.ManagerAccount;
import cz.cvut.kbss.ear.service.ManagerService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Transactional
@TestPropertySource(locations = "classpath:application-test.properties")
public class ManagerServiceTest {
    @Autowired
    private EntityManager em;

    @Autowired
    private ManagerService adminService;

    private ManagerAccount managerAccount1;
    private ManagerAccount managerAccount2;
    private ManagerAccount managerAccount3;

    @BeforeEach
    public void setUp(){
        this.managerAccount1 = new ManagerAccount("heslo1234", "UserTest", "fel@fel.cvut.cz", 756232545, "Franta", "Omacka");
        this.managerAccount2 = new ManagerAccount("heslo", "UserTest", "fel@fel.cvut.cz", 756232545, "Franta", "Omacka");
        this.managerAccount3 = new ManagerAccount("heslo1234", "Us", "fel@fel.cvut.cz", 756232545, "Franta", "Omacka");
    }

    @Test
    public void persistLogicDoesntPersistInvalidAccounts(){
        adminService.persist(managerAccount1);
        adminService.persist(managerAccount2);
        adminService.persist(managerAccount3);

        assertTrue(em.contains(managerAccount1));
        final ManagerAccount result1 = em.find(ManagerAccount.class, managerAccount1.getId());
        assertFalse(em.contains(managerAccount2));
        assertFalse(em.contains(managerAccount3));
        assertEquals(managerAccount1, result1);
    }
}
