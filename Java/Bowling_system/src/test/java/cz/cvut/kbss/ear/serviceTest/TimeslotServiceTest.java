package cz.cvut.kbss.ear.serviceTest;

import cz.cvut.kbss.ear.model.Reservation;
import cz.cvut.kbss.ear.model.TimeSlot;
import cz.cvut.kbss.ear.service.TimeslotService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Transactional
@TestPropertySource(locations = "classpath:application-test.properties")
public class TimeslotServiceTest {
    @Autowired
    private EntityManager em;

    @Autowired
    private TimeslotService timeslotService;

    private TimeSlot timeSlot;

    private Reservation reservation;

    @BeforeEach
    public void setUp(){
        this.timeSlot = new TimeSlot(new Date(), 18, 250);
        em.persist(timeSlot);
        this.reservation = new Reservation();
        reservation.setCanceled(false);
        em.persist(reservation);
    }

    @Test
    public void reserveTimeSlotUpdatesTimeSlot(){
        timeslotService.reserveTimeslot(timeSlot, reservation);

        final TimeSlot result = em.find(TimeSlot.class, timeSlot.getId());
        assertTrue(result.isReserved());
    }

    @Test
    public void cancelTimeslotReservationRemovesReservaton(){
        timeSlot.setReserved(true);

        timeslotService.cancelTimeslotReservation(timeSlot);

        final TimeSlot result = em.find(TimeSlot.class, timeSlot.getId());
        assertFalse(result.isReserved());
    }

    @Test
    public void changePriceChangesTimeslotPrice(){
        timeslotService.changeTimeslotPrice(timeSlot, 564);

        final TimeSlot result = em.find(TimeSlot.class, timeSlot.getId());
        assertEquals(564, result.getPrice());
    }
}
