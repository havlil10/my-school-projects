package cz.cvut.kbss.ear.serviceTest;

import cz.cvut.kbss.ear.model.BowlingAlley;
import cz.cvut.kbss.ear.model.Lane;
import cz.cvut.kbss.ear.model.PriceModification;
import cz.cvut.kbss.ear.model.TimeSlot;
import cz.cvut.kbss.ear.service.BowlingAlleyService;
import cz.cvut.kbss.ear.service.LaneService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Transactional
@TestPropertySource(locations = "classpath:application-test.properties")
public class LaneServiceTest {
    @PersistenceContext
    private EntityManager em;

    @Autowired
    private BowlingAlleyService bowlingAlleyService;

    @Autowired
    private LaneService sut;

    @Test
    public void getAvailableTimeSlots_timeslotsFromSameDay_returnsCorrectArray() {
        Lane lane = new Lane();
        List<TimeSlot> laneTimeslots = new ArrayList<>();
        laneTimeslots.add(new TimeSlot(new Date(), 11, 150));
        laneTimeslots.add(new TimeSlot(new Date(), 12, 150));
        lane.setTimeSlots(laneTimeslots);
        assertEquals(2, sut.getAvailableTimeSlots(new Date(), lane).size());
        assertEquals(0, (sut.getAvailableTimeSlots(new Date(System.currentTimeMillis()-24*60*60*1000), lane).size()));
    }

    @Test
    public void getAvailableTimeSlots_timeslotsFromDifferentDays_returnsCorrectArray() {
        Lane lane = new Lane();
        List<TimeSlot> laneTimeslots = new ArrayList<>();
        laneTimeslots.add(new TimeSlot(new Date(), 11, 150));
        laneTimeslots.add(new TimeSlot(new Date(), 12, 150));
        laneTimeslots.add(new TimeSlot(new Date(System.currentTimeMillis()-24*60*60*1000), 11, 150));
        lane.setTimeSlots(laneTimeslots);
        assertEquals(2, sut.getAvailableTimeSlots(new Date(), lane).size());
        assertEquals(1, (sut.getAvailableTimeSlots(new Date(System.currentTimeMillis()-24*60*60*1000), lane).size()));
    }

    @Test
    public void createNewPriceModification_withConflicts_doesntAddPriceModificationToArray(){
        Lane lane = new Lane();
        List<PriceModification> priceModifications = new ArrayList<>();
        priceModifications.add(new PriceModification(18, 22, 200));
        lane.setPriceModifications(priceModifications);
        sut.createNewPriceModification(17, 19, 250, lane);
        sut.createNewPriceModification(21, 23, 250, lane);
        sut.createNewPriceModification(19, 20, 250, lane);
        sut.createNewPriceModification(17, 23, 250, lane);
        assertEquals(1, lane.getPriceModifications().size());
    }

    @Test
    public void createNewPriceModification_withoutConflicts_addsPriceModificationToArray(){
        Lane lane = new Lane();
        List<PriceModification> priceModifications = new ArrayList<>();
        priceModifications.add(new PriceModification(18, 22, 200));
        lane.setPriceModifications(priceModifications);
        sut.createNewPriceModification(17, 18, 250, lane);
        sut.createNewPriceModification(22, 24, 250, lane);
        assertEquals(3, lane.getPriceModifications().size());
    }

    @Test
    public void changeLaneState_changesLaneState() {
        Lane lane = new Lane();
        em.persist(lane);
        sut.changeLaneState(lane);
        assertTrue(lane.isOpened());
        assertTrue(em.find(Lane.class, lane.getId()).isOpened());
    }

    @Test
    public void changeInitialPriceForLane_works() {
        Lane lane = new Lane();
        lane.setInitialPricerPerHour(150);
        em.persist(lane);
        sut.changeInitialPriceForLane(lane, 200);
        assertEquals(200, lane.getInitialPricerPerHour());
        assertEquals(200, em.find(Lane.class, lane.getId()).getInitialPricerPerHour());
    }

    @Test
    public void generateTimeSlotsByAllLanes_addsTimeSlots() {
        BowlingAlley bowlingAlley = new BowlingAlley();
        Lane lane1 = new Lane(bowlingAlley, 1, 150);
        Lane lane2 = new Lane(bowlingAlley, 2, 150);
        List<Lane> lanes = new ArrayList<>();
        lanes.add(lane1);
        lanes.add(lane2);
        lane1.setBowlingAlley(bowlingAlley);
        lane2.setBowlingAlley(bowlingAlley);
        //bowlingAlley.setLanes(lanes);

        em.persist(bowlingAlley);
        em.persist(lane1);
        em.persist(lane2);
        bowlingAlleyService.changeOpeningHour(10);
        bowlingAlleyService.changeClosingHour(20);
        sut.generateTimeSlotsByAllLanes(bowlingAlley, new Date(System.currentTimeMillis()+24*60*60*1000));

        Lane resultLane1 = em.find(Lane.class, lane1.getId());
        Lane resultLane2 = em.find(Lane.class, lane2.getId());

        assertEquals(10, resultLane1.getTimeSlots().size());
        assertEquals(10, resultLane2.getTimeSlots().size());

    }

}
