package cz.cvut.kbss.ear.serviceTest;

import cz.cvut.kbss.ear.model.BowlingAlley;
import cz.cvut.kbss.ear.service.BowlingAlleyService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Transactional
@TestPropertySource(locations = "classpath:application-test.properties")
public class BowlingAlleySeviceTest {
    @Autowired
    private EntityManager em;

    @Autowired
    private BowlingAlleyService bowlingAlleyService;

    private BowlingAlley bowlingAlley = new BowlingAlley();

    @Test
    public void changeHourWorks(){
        bowlingAlley.setOpeningHour(10);
        bowlingAlley.setClosingHour(22);
        em.persist(bowlingAlley);

        bowlingAlleyService.changeOpeningHour(12);
        bowlingAlleyService.changeClosingHour(21);

        BowlingAlley result = em.find(BowlingAlley.class, bowlingAlley.getId());
        assertEquals(12, result.getOpeningHour());
        assertEquals(21, result.getClosingHour());

        bowlingAlleyService.changeOpeningHour(25);
        bowlingAlleyService.changeClosingHour(25);

        result = em.find(BowlingAlley.class, bowlingAlley.getId());
        assertEquals(12, result.getOpeningHour());
        assertEquals(21, result.getClosingHour());

    }
}
