package cz.cvut.kbss.ear.DAOtest;

import cz.cvut.kbss.ear.BowlingApp;
import cz.cvut.kbss.ear.DAO.LaneDao;
import cz.cvut.kbss.ear.model.BowlingAlley;
import cz.cvut.kbss.ear.model.Lane;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.annotation.ComponentScan;

import javax.persistence.PersistenceException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@ComponentScan(basePackageClasses = BowlingApp.class)
public class BaseDaoTest {
    @Autowired
    private TestEntityManager em;

    @Autowired
    private LaneDao laneDao;

    Lane lane = new Lane(null, 1, 150);
    Lane lane2 = new Lane(null, 2, 150);

    @Test
    public void persistSavesInstance(){
        laneDao.persist(lane);

        final Lane result = em.find(Lane.class, lane.getId());
        assertEquals(lane.getId(), result.getId());
        assertEquals(lane.getLaneNumber(), result.getLaneNumber());
    }

    @Test
    @Cascade(CascadeType.ALL)
    public void findByIdTest(){
        em.persist(lane);

        final Lane result = laneDao.find(lane.getId());
        em.clear();
        assertEquals(lane.getId(), result.getId());
        assertEquals(lane.getLaneNumber(), result.getLaneNumber());
    }

    @Test
    public void findAllTest(){
        em.persist(lane);
        em.persist(lane2);

        final List<Lane> result = laneDao.findAll();
        assertEquals(2, result.size());
        assertTrue(result.stream().anyMatch(l -> l.getId().equals(lane.getId())));
        assertTrue(result.stream().anyMatch(l -> l.getId().equals(lane2.getId())));
    }

    @Test
    public void removeRemovesInstance(){
        em.persist(lane);

        laneDao.remove(lane);
        assertNull(em.find(Lane.class, lane.getId()));
    }

    @Test
    public void updateThrowsExceptionWhenClassDoesntExist(){
        em.persist(lane);
        em.remove(lane);
        assertThrows(PersistenceException.class, () -> laneDao.update(lane));
    }

    @Test
    public void existReturnsTrueForExistingId() {
        em.persist(lane);
        assertTrue(laneDao.exists(lane.getId()));
        assertFalse(laneDao.exists(-6));
    }
}
