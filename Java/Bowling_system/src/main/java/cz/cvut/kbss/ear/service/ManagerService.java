package cz.cvut.kbss.ear.service;

import cz.cvut.kbss.ear.DAO.ManagerDao;
import cz.cvut.kbss.ear.model.account.ManagerAccount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;

@Service
public class ManagerService extends BaseService<ManagerAccount>{
    private PasswordEncoder passwordEncoder;

    @Autowired
    public ManagerService(ManagerDao managerDao, PasswordEncoder passwordEncoder) {
        super(ManagerAccount.class, managerDao);
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    @Transactional
    public void persist(ManagerAccount account){
        Objects.requireNonNull(account);
        if(account.getPassword().length() < 8 || account.getPassword().length() > 30){
            return;
        }
        if(account.getUsername().length() < 3 || account.getUsername().length() > 30){
            return;
        }
        account.encodePassword(passwordEncoder);
        dao.persist(account);
    }
}
