package cz.cvut.kbss.ear.service;

import cz.cvut.kbss.ear.DAO.BaseDao;
import cz.cvut.kbss.ear.DAO.BowlingTeamDao;
import cz.cvut.kbss.ear.model.BowlingTeam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BowlingTeamService extends BaseService<BowlingTeam>{
    private final BowlingTeamDao bowlingTeamDao;

    @Autowired
    public BowlingTeamService(BaseDao<BowlingTeam> dao, BowlingTeamDao bowlingTeamDao) {
        super(BowlingTeam.class, dao);
        this.bowlingTeamDao = bowlingTeamDao;
    }
}
