package cz.cvut.kbss.ear.exceptions;

public class TimeSlotNotReservedException extends Exception{
    public TimeSlotNotReservedException(String message) {
        super(message);
    }
}
