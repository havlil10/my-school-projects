package cz.cvut.kbss.ear.DAO;

import cz.cvut.kbss.ear.model.account.ManagerAccount;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;

@Repository
public class ManagerDao extends BaseDao<ManagerAccount>{
    public ManagerDao() {
        super(ManagerAccount.class);
    }

    public ManagerAccount findByUsername(String username) {
        try {
            return em.createNamedQuery("ManagerAccount.findByUsername", ManagerAccount.class).setParameter("username", username)
                    .getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
}
