package cz.cvut.kbss.ear.model.account;

import cz.cvut.kbss.ear.model.account.Account;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.*;
@Entity
@DiscriminatorValue("ADMIN")
@NamedQueries({
        @NamedQuery(name = "ManagerAccount.findByUsername", query = "SELECT u FROM ManagerAccount u WHERE u.username = :username")
})

public class ManagerAccount extends Account {
    public ManagerAccount(String password, String username, String email, int phoneNumber, String firstName, String lastName) {
        super(password, username, email, phoneNumber, firstName, lastName);
        this.banned = false;
        this.isVIP = false;
        this.isCurrentManager = false;
    }

    @Basic(optional = false)
    @Column(nullable = false)
    private boolean isCurrentManager;

    public ManagerAccount() {

    }

    @Override
    public String getRole() {
        return "ROLE_MANAGER";
    }

    public boolean isCurrentManager() {
        return isCurrentManager;
    }

    public void setCurrentManager(boolean currentManager) {
        isCurrentManager = currentManager;
    }

    @Override
    public boolean isAdmin() {
        return true;
    }
}
