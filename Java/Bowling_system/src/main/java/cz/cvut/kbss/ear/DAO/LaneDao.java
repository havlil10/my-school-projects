package cz.cvut.kbss.ear.DAO;

import cz.cvut.kbss.ear.model.Lane;
import org.springframework.stereotype.Repository;

@Repository
public class LaneDao extends BaseDao<Lane>{
    public LaneDao() {
        super(Lane.class);
    }
}
