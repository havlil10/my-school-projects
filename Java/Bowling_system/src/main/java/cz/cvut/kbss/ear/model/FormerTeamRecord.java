package cz.cvut.kbss.ear.model;

import javax.persistence.*;
import java.util.Date;

@Entity
public class FormerTeamRecord extends AbstractEntity{
    @ManyToOne
    @JoinColumn(name = "bowlingTeam_id")
    private BowlingTeam bowlingTeam;

    @Basic(optional = false)
    @Column(nullable = false)
    private Date fromDate;

    private Date toDate;

    public FormerTeamRecord(BowlingTeam bowlingTeam, Date fromDate) {
        this.bowlingTeam = bowlingTeam;
        this.fromDate = fromDate;
    }

    public FormerTeamRecord() {

    }

    public BowlingTeam getBowlingTeam() {
        return bowlingTeam;
    }

    public Date getFrom() {
        return fromDate;
    }

    public Date getTo() {
        return toDate;
    }

    public void setBowlingTeam(BowlingTeam bowlingTeam) {
        this.bowlingTeam = bowlingTeam;
    }

    public void setFrom(Date fromDate) {
        this.fromDate = fromDate;
    }

    public void setTo(Date toDate) {
        this.toDate = toDate;
    }
}
