package cz.cvut.kbss.ear.exceptions;

public class EntitiesDontMatchException extends Exception{
    public EntitiesDontMatchException(String message) {
        super(message);
    }
}
