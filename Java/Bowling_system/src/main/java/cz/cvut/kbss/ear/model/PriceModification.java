package cz.cvut.kbss.ear.model;

import javax.persistence.*;

@Entity
public class    PriceModification extends AbstractEntity{
    @Basic(optional = false)
    @Column(nullable = false)
    private int fromTime;
    @Basic(optional = false)
    @Column(nullable = false)
    private int toTime;
    @Basic(optional = false)
    @Column(nullable = false)
    private int price;

    @ManyToOne
    @JoinColumn(name = "lane_id")
    private Lane lane;

    public PriceModification(int fromTime, int toTime, int price) {
        this.fromTime = fromTime;
        this.toTime = toTime;
        this.price = price;
    }

    public PriceModification() {
    }

    public Lane getLane() {
        return lane;
    }

    public void setLane(Lane lane) {
        this.lane = lane;
    }

    public int getFrom() {
        return fromTime;
    }

    public void setFrom(int from) {
        this.fromTime = from;
    }

    public int getTo() {
        return toTime;
    }

    public void setTo(int to) {
        this.toTime = to;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}