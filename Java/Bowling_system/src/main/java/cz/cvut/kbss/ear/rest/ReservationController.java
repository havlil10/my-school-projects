package cz.cvut.kbss.ear.rest;

import cz.cvut.kbss.ear.exceptions.NotFoundException;
import cz.cvut.kbss.ear.model.Reservation;
import cz.cvut.kbss.ear.model.account.Account;
import cz.cvut.kbss.ear.security.model.AuthenticationToken;
import cz.cvut.kbss.ear.service.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/reservations")
public class ReservationController {
    private final ReservationService reservationService;

    @Autowired
    public ReservationController(ReservationService reservationService) {
        this.reservationService = reservationService;
    }

    @PreAuthorize("hasAnyRole('ROLE_MANAGER', 'ROLE_USER')")
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Reservation> getReservations(Principal principal){
        final AuthenticationToken auth = (AuthenticationToken) principal;
        Account account = auth.getPrincipal().getUser();
        if (account==null) {
            throw new AccessDeniedException("You have to be signed in in order to see reservations");
        }

        return reservationService.findAll();
    }


    //@ResponseStatus(HttpStatus.NO_CONTENT)
    @PreAuthorize("hasAnyRole('ROLE_MANAGER', 'ROLE_USER')")
    @PutMapping(value = "/cancel/{id}")
    public void cancelReservation(@PathVariable Integer id){
        final Reservation toRemove = reservationService.find(id);
        if(toRemove == null){
            return;
        }
        reservationService.cancelReservation(toRemove);
    }

    @PreAuthorize("hasAnyRole('ROLE_MANAGER', 'ROLE_USER')")
    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    //@ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateReservation(@PathVariable Integer id, Reservation reservation){
        final Reservation toUpdate = reservationService.find(id);
        if(toUpdate == null){
            return;
        }
        reservationService.update(toUpdate);
    }




    @PreAuthorize("hasRole('ROLE_MANAGER')")
    @PutMapping(value = "/approve/{id}")
    public void approveReservation(@PathVariable int id) throws NotFoundException {
        Reservation reservation = reservationService.find(id);
        if(reservation == null){
            throw new NotFoundException("Reservation with id: " + id + " not found. Cannot approve.");
        }
        reservationService.approveReservation(reservation);
    }

    //for testing purposes
    @DeleteMapping
    public void removeAllReservations() {
        List<Reservation> allReservations = reservationService.findAll();
        for(Reservation reservation : allReservations) {
            reservationService.remove(reservation);
        }
    }
    //for testing purposes
    //@PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping(value = "/{id}")
    public void removeReservation(@PathVariable int id) throws NotFoundException {
        Reservation reservation = reservationService.find(id);
        if(reservation == null){throw new NotFoundException("Reservation with id: " + id + " not found. Cannot remove.");}
        reservationService.remove(reservation);
    }

}
