package cz.cvut.kbss.ear.rest;

import cz.cvut.kbss.ear.model.BowlingAlley;
import cz.cvut.kbss.ear.model.Lane;
import cz.cvut.kbss.ear.service.BowlingAlleyService;
import cz.cvut.kbss.ear.service.LaneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;

import java.time.Instant;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/lane")
public class LaneController {
    private final LaneService laneService;
    private final BowlingAlleyService bowlingAlleyService;

    @Autowired
    public LaneController(LaneService laneService, BowlingAlleyService bowlingAlleyService) {
        this.laneService = laneService;
        this.bowlingAlleyService = bowlingAlleyService;
    }

    @Scheduled(cron = "0 1 1 * * *")
    public void generateTimeSlots(){
        List<BowlingAlley> alleys = bowlingAlleyService.findAll();
        for(BowlingAlley alley : alleys) {
            laneService.generateTimeSlotsByAllLanes(alley, Date.from(Instant.now()));
        }
    }

    //for testing purposes
    @GetMapping(value = "/generateTimeslots")
    public void generateTimeSlotsInit(){
        BowlingAlley bowlingAlley = new BowlingAlley(10, 18);
        bowlingAlleyService.persist(bowlingAlley);
        Lane lane1 = new Lane(bowlingAlley, 1, 300);
        laneService.persist(lane1);
        List<Lane> lanes = laneService.findAll();


        laneService.generateTimeSlotsForLane(lanes.get(0), Date.from(Instant.now().plus(1, ChronoUnit.DAYS)));

    }
}
