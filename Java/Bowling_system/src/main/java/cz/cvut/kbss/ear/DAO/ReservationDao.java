package cz.cvut.kbss.ear.DAO;

import cz.cvut.kbss.ear.model.Reservation;
import cz.cvut.kbss.ear.model.account.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;

@Repository
public class ReservationDao extends BaseDao<Reservation> {
    @Autowired
    private AccountDao accountDao;

    public ReservationDao() {
        super(Reservation.class);
    }
}
