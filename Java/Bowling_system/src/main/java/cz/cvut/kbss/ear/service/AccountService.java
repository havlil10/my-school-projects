package cz.cvut.kbss.ear.service;

import cz.cvut.kbss.ear.DAO.BowlingTeamDao;
import cz.cvut.kbss.ear.DAO.AccountDao;
import cz.cvut.kbss.ear.model.BowlingTeam;
import cz.cvut.kbss.ear.model.account.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;

@Service
public class AccountService extends BaseService<Account>{
    private BowlingTeamDao bowlingTeamDao;
    private PasswordEncoder passwordEncoder;
    private final AccountDao accountDao;

    @Autowired
    public AccountService(AccountDao userDao, BowlingTeamDao bowlingTeamDao, PasswordEncoder passwordEncoder) {
        super(Account.class, userDao);
        this.bowlingTeamDao = bowlingTeamDao;
        this.passwordEncoder = passwordEncoder;
        this.accountDao = userDao;
    }

    @Override
    @Transactional
    public void persist(Account account){
        Objects.requireNonNull(account);
        if(account.getPassword().length() < 8 || account.getPassword().length() > 30){
            return;
        }
        if(account.getUsername().length() < 3 || account.getUsername().length() > 30){
            return;
        }
        account.encodePassword(passwordEncoder);
        dao.persist(account);
    }

    @Transactional
    public void addTeam(Account account, BowlingTeam bowlingTeam){
        Objects.requireNonNull(bowlingTeam);
        Objects.requireNonNull(account);
        bowlingTeam.addAccount(account);
        bowlingTeamDao.update(bowlingTeam);
        account.addTeam(bowlingTeam);
        dao.update(account);
    }

    @Transactional
    public void removeTeam(Account account, BowlingTeam bowlingTeam){
        Objects.requireNonNull(bowlingTeam);
        Objects.requireNonNull(account);
        Objects.requireNonNull(bowlingTeamDao.find(bowlingTeam.getId()));
        Objects.requireNonNull(dao.find(account.getId()));
        if(bowlingTeam.getAccounts().contains(account) && account.getBowlingTeams().contains(bowlingTeam)){
            bowlingTeam.removeAccount(account);
            account.removeTeam(bowlingTeam);
            bowlingTeamDao.update(bowlingTeam);
            dao.update(account);
        }
    }


    @Transactional
    public void banAccount(Account account){
        Objects.requireNonNull(account);
        account.setBanned(true);
        dao.update(account);
    }

    @Transactional
    public void unbanAccount(Account account){
        Objects.requireNonNull(account);
        account.setBanned(false);
        dao.update(account);
    }

    @Transactional
    public Account findByUsername(String username){
        return accountDao.findByUsername(username);
    }

    @Transactional
    public Account findByPhone(int phoneNr){
        return accountDao.findUserByPhoneNumber(phoneNr);
    }
}
