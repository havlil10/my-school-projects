package cz.cvut.kbss.ear.exceptions;

public class ReservationTooLargeException extends Exception{
    public ReservationTooLargeException(String message) {
        super(message);
    }
}
