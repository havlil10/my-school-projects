package cz.cvut.kbss.ear.service;

import cz.cvut.kbss.ear.DAO.BaseDao;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;

public abstract class BaseService<T> {
    protected final Class<T> type;

    protected final BaseDao<T> dao;

    protected BaseService(Class<T> type, BaseDao<T> dao){
        this.type = type;
        this.dao = dao;
    }

    @Transactional
    public void persist(T t){dao.persist(t);}

    @Transactional
    public List<T> findAll() {
        return dao.findAll();
    }

    @Transactional
    public void update(T entity) {
        Objects.requireNonNull(entity);
        dao.update(entity);
    }

    @Transactional
    public void remove(T entity) {
        Objects.requireNonNull(entity);
        dao.remove(entity);
    }

    @Transactional
    public boolean exists(int id) {
        return dao.exists(id);
    }

    @Transactional
    public T find(int id){
        return dao.find(id);
    }

}
