package cz.cvut.kbss.ear.service;

import cz.cvut.kbss.ear.DAO.TimeSlotDao;
import cz.cvut.kbss.ear.model.Reservation;
import cz.cvut.kbss.ear.model.TimeSlot;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;

@Service
public class TimeslotService extends BaseService<TimeSlot>{

    @Autowired
    public TimeslotService(TimeSlotDao timeSlotDao) {
        super(TimeSlot.class, timeSlotDao);
    }

    @Transactional
    public void persist(TimeSlot timeSlot){
        Objects.requireNonNull(timeSlot);
        dao.persist(timeSlot);
    }

    @Transactional
    public void reserveTimeslot(TimeSlot timeSlot, Reservation reservation){
        Objects.requireNonNull(timeSlot);
        Objects.requireNonNull(reservation);
        if(!timeSlot.isReserved()){
            timeSlot.setReserved(true);
            dao.update(timeSlot);
        }
    }

    @Transactional
    public void cancelTimeslotReservation(TimeSlot timeSlot){
        Objects.requireNonNull(timeSlot);
        if(timeSlot.isReserved()){
            timeSlot.setReserved(false);
            dao.update(timeSlot);
        }
    }

    @Transactional
    public void changeTimeslotPrice(TimeSlot timeSlot, int newPrice) {
        Objects.requireNonNull(timeSlot);
        timeSlot.setPrice(newPrice);
        dao.update(timeSlot);
    }
}
