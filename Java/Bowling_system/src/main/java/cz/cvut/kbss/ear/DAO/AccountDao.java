package cz.cvut.kbss.ear.DAO;

import cz.cvut.kbss.ear.model.account.Account;
import cz.cvut.kbss.ear.model.account.ManagerAccount;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;

@Repository
public class AccountDao extends BaseDao<Account>{
    public AccountDao() {
        super(Account.class);
    }

    public Account findByUsername(String username) {
        try {
            return em.createNamedQuery("Account.findByUsername", Account.class).setParameter("username", username)
                    .getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    public Account findUserByPhoneNumber(int phoneNumber) {
        try {
            return em.createNamedQuery("Account.findByPhoneNumber", Account.class).setParameter("phoneNumber", phoneNumber)
                    .getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
}
