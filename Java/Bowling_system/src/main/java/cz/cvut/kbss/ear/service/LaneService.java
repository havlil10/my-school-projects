package cz.cvut.kbss.ear.service;

import cz.cvut.kbss.ear.DAO.BowlingAlleyDao;
import cz.cvut.kbss.ear.DAO.LaneDao;
import cz.cvut.kbss.ear.DAO.PriceModificationDao;
import cz.cvut.kbss.ear.DAO.TimeSlotDao;
import cz.cvut.kbss.ear.model.BowlingAlley;
import cz.cvut.kbss.ear.model.Lane;
import cz.cvut.kbss.ear.model.PriceModification;
import cz.cvut.kbss.ear.model.TimeSlot;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class LaneService extends BaseService<Lane>{
    private final PriceModificationDao priceModificationDao;
    private final TimeSlotDao timeSlotDao;
    private final LaneDao laneDao;

    @Autowired
    public LaneService(LaneDao laneDao, PriceModificationDao priceModificationDao, TimeSlotDao timeSlotDao) {
        super(Lane.class, laneDao);
        this.priceModificationDao = priceModificationDao;
        this.timeSlotDao = timeSlotDao;
        this.laneDao = laneDao;
    }

    @Transactional
    public void changeLaneState(Lane lane) {
        lane.setOpened(!lane.isOpened());
        dao.update(lane);
    }

    @Transactional
    public void changeInitialPriceForLane(Lane lane, int newPrice) {
        lane.setInitialPricerPerHour(newPrice);
        dao.update(lane);
    }

    @Transactional
    public void generateTimeSlotsByAllLanes(BowlingAlley bowlingAlley, Date date) {
        List<Lane> lanes = laneDao.findAll();
        for(Lane lane : lanes) {
            generateTimeSlotsForLane(lane, date);
        }
    }

    @Transactional
    public void generateTimeSlotsForLane(Lane lane, Date date) {
        Objects.requireNonNull(lane);
        removeOutdatedTimeSlots(lane);
        Calendar calendar = Calendar.getInstance();
        BowlingAlley bowlingAlley = lane.getBowlingAlley();
        int timeslotStartingHour = bowlingAlley.getOpeningHour();
        while (timeslotStartingHour<bowlingAlley.getClosingHour()) {
            calendar.setTime(date);
            calendar.add(Calendar.HOUR_OF_DAY, timeslotStartingHour-bowlingAlley.getOpeningHour());
            TimeSlot timeSlot = new TimeSlot(calendar.getTime(), timeslotStartingHour, countTimeslotPriceAccordingToPriceModification(lane.getPriceModifications(), timeslotStartingHour, lane.getInitialPricerPerHour()));
            lane.getTimeSlots().add(timeSlot);
            timeSlotDao.persist(timeSlot);
            timeslotStartingHour+=1;
        }
        dao.update(lane);
    }

    private int countTimeslotPriceAccordingToPriceModification(List<PriceModification> priceModifications, int from, int initialPricePerHour) {
        for(PriceModification priceModification : priceModifications) {
            if(priceModification.getFrom()<=from && priceModification.getTo()<=from+1) {
                return priceModification.getPrice();
            }
        }
        return initialPricePerHour;
    }

    private void removeOutdatedTimeSlots(Lane lane) {
        lane.getTimeSlots().removeIf(timeSlot -> timeSlot.getDate().before(Calendar.getInstance().getTime()));
    }

    @Transactional
    public List<TimeSlot> getAvailableTimeSlots(Date date, Lane lane) {
        Objects.requireNonNull(lane);
        List<TimeSlot> resultTimeSlots = new ArrayList<>();
        SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
        for(TimeSlot timeSlot : lane.getTimeSlots()) {
            if(fmt.format(date).equals(fmt.format(timeSlot.getDate()))) {
                resultTimeSlots.add(timeSlot);
            }
        }
        return resultTimeSlots;
    }

    @Transactional
    public void createNewPriceModification(int from, int to, int price, Lane lane) {
        if(lane==null || hasPriceModificationConflict(lane.getPriceModifications(), from, to)) {
            return;
        }
        if(from < to) {
            PriceModification priceModification = new PriceModification(from, to, price);
            lane.getPriceModifications().add(priceModification);
            priceModificationDao.persist(priceModification);
            dao.update(lane);
        }
    }

    private boolean hasPriceModificationConflict(List<PriceModification> priceModifications, int from,int to) {
        for(PriceModification priceModification : priceModifications) {
            if(conflicts(priceModification, from, to)) {
                return true;
            }
        }
        return false;
    }

    private boolean conflicts(PriceModification priceModification, int from,int to){
        return ((priceModification.getFrom() <= from || priceModification.getFrom() < to) && (priceModification.getTo() >= to || priceModification.getTo() > from));

    }

    @Transactional
    public void removePriceModification(PriceModification priceModification) {
        priceModificationDao.remove(priceModification);
    }

    @Transactional
    public void removeAllPriceModifications() {
        List<PriceModification> priceModifications = priceModificationDao.findAll();
        for(PriceModification priceModification : priceModifications) {
            priceModificationDao.remove(priceModification);
        }
    }
}
