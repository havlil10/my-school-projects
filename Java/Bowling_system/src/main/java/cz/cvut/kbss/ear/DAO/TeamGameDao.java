package cz.cvut.kbss.ear.DAO;

import cz.cvut.kbss.ear.model.TeamGame;
import org.springframework.stereotype.Repository;

@Repository
public class TeamGameDao extends BaseDao<TeamGame>{
    public TeamGameDao() {
        super(TeamGame.class);
    }
}
