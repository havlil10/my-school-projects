package cz.cvut.kbss.ear.rest.handler;

import cz.cvut.kbss.ear.exceptions.EntitiesDontMatchException;
import cz.cvut.kbss.ear.exceptions.NotFoundException;
import cz.cvut.kbss.ear.exceptions.ReservationTooLargeException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.persistence.PersistenceException;
import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class MyExceptionHandler {
    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<ErrorInfo> notFound(HttpServletRequest request, NotFoundException e){
        return new ResponseEntity<>(new ErrorInfo(e.getMessage(), request.getRequestURI()), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(PersistenceException.class)
    public ResponseEntity<ErrorInfo> persistenceException(HttpServletRequest request, PersistenceException e){
        return new ResponseEntity<>(new ErrorInfo(e.getMessage(), request.getRequestURI()), HttpStatus.INTERNAL_SERVER_ERROR);
    }


    @ExceptionHandler(ReservationTooLargeException.class)
    public ResponseEntity<ErrorInfo> reservationSizeException(HttpServletRequest request, ReservationTooLargeException e){
        return new ResponseEntity<>(new ErrorInfo(e.getMessage(), request.getRequestURI()), HttpStatus.CONFLICT);
    }

    @ExceptionHandler(EntitiesDontMatchException.class)
    public ResponseEntity<ErrorInfo> entitiesDontMatch(HttpServletRequest request, EntitiesDontMatchException e){
        return new ResponseEntity<>(new ErrorInfo(e.getMessage(), request.getRequestURI()), HttpStatus.CONFLICT);
    }
/*
    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorInfo> genericException(HttpServletRequest request, Exception e){
        return new ResponseEntity<>(new ErrorInfo(e.getMessage(), request.getRequestURI()), HttpStatus.INTERNAL_SERVER_ERROR);
    }
 */

}
