package cz.cvut.kbss.ear.model;

import org.springframework.context.annotation.Bean;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
public class BowlingAlley extends AbstractEntity{

    @Basic(optional = false)
    @Column(nullable = false)
    private int closingHour;

    @Basic(optional = false)
    @Column(nullable = false)
    private int openingHour;

    /*
    @OneToMany(orphanRemoval = true)
    private List<Lane> lanes;
*/

    public BowlingAlley(int openingHour, int closingHour) {
        this.openingHour = openingHour;
        this.closingHour = closingHour;

    }

    public BowlingAlley() {
    }



    public int getOpeningHour() {
        return openingHour;
    }

    public void setOpeningHour(int openingHour) {
        this.openingHour = openingHour;
    }

    public int getClosingHour() {
        return closingHour;
    }

    public void setClosingHour(int closingHour) {
        this.closingHour = closingHour;
    }

}
