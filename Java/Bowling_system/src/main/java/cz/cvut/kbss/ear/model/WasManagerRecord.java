package cz.cvut.kbss.ear.model;

import cz.cvut.kbss.ear.model.account.ManagerAccount;

import javax.persistence.*;
import java.util.Date;

@Entity
public class WasManagerRecord extends AbstractEntity {

    @ManyToOne
    @JoinColumn(name = "managerAccount_id")
    private ManagerAccount managerAccount;

    @Basic(optional = false)
    @Column(nullable = false)
    private Date fromDate;

    private Date toDate;

    public WasManagerRecord(ManagerAccount managerAccount, Date fromDate) {
        this.managerAccount = managerAccount;
        this.fromDate = fromDate;
    }

    public WasManagerRecord() {

    }

    public ManagerAccount getAdminAccount() {
        return managerAccount;
    }

    public Date getFrom() {
        return fromDate;
    }

    public Date getTo() {
        return toDate;
    }

    public void setAdminAccount(ManagerAccount managerAccount) {
        this.managerAccount = managerAccount;
    }

    public void setFrom(Date fromDate) {
        this.fromDate = fromDate;
    }

    public void setTo(Date toDate) {
        this.toDate = toDate;
    }
}
