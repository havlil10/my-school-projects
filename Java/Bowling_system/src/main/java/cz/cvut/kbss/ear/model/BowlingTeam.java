package cz.cvut.kbss.ear.model;

import cz.cvut.kbss.ear.model.account.Account;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
public class BowlingTeam extends AbstractEntity{
    @Basic(optional = false)
    @Column(nullable = false)
    private String name;

    @ManyToMany
    @JoinTable(name = "bowling_team_accounts",
            joinColumns = @JoinColumn(name = "bowling_team_id"),
            inverseJoinColumns = @JoinColumn(name = "accounts_id"))
    private List<Account> accounts = new ArrayList<>();

    public BowlingTeam(String name, List<Account> accounts) {
        this.name = name;
        this.accounts = accounts;
    }

    public BowlingTeam() {
    }

    public List<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }

    public void addAccount(Account account){
        Objects.requireNonNull(account);
        accounts.add(account);
    }

    public void removeAccount(Account account){
        Objects.requireNonNull(account);
        accounts.remove(account);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
