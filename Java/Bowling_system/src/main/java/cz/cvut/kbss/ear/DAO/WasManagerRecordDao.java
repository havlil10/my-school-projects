package cz.cvut.kbss.ear.DAO;

import cz.cvut.kbss.ear.model.WasManagerRecord;
import org.springframework.stereotype.Repository;

@Repository
public class WasManagerRecordDao extends BaseDao<WasManagerRecord>{
    public WasManagerRecordDao() {
        super(WasManagerRecord.class);
    }
}
