package cz.cvut.kbss.ear.DAO;

import cz.cvut.kbss.ear.model.BowlingTeam;
import org.springframework.stereotype.Repository;

@Repository
public class BowlingTeamDao extends BaseDao<BowlingTeam>{
    public BowlingTeamDao() {
        super(BowlingTeam.class);
    }
}
