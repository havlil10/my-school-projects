package cz.cvut.kbss.ear.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.Date;

@Entity
public class TeamGame extends AbstractEntity{
    @Basic(optional = false)
    @Column(nullable = false)
    private Date date;

    @Basic(optional = false)
    @Column(nullable = false)
    private int teamPoints;

    @Basic(optional = false)
    @Column(nullable = false)
    private boolean won;

    @ManyToOne
    private BowlingTeam bowlingTeam;

    //TODO - decide if change is needed
    //private BowlingTeam opponentTeam;

    public TeamGame(int teamPoints, boolean won, BowlingTeam bowlingTeam, Date date) {
        this.teamPoints = teamPoints;
        this.won = won;
        this.bowlingTeam = bowlingTeam;
        this.date = date;
    }

    public TeamGame() {
    }

    public int getTeamPoints() {
        return teamPoints;
    }

    public void setTeamPoints(int teamPoints) {
        this.teamPoints = teamPoints;
    }

    public boolean isWon() {
        return won;
    }

    public void setWon(boolean won) {
        this.won = won;
    }

    public BowlingTeam getBowlingTeam() {
        return bowlingTeam;
    }

    public void setBowlingTeam(BowlingTeam bowlingTeam) {
        this.bowlingTeam = bowlingTeam;
    }

    public Date getDate() {
        return date;
    }
}
