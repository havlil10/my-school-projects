package cz.cvut.kbss.ear.service;

import cz.cvut.kbss.ear.DAO.ManagerDao;
import cz.cvut.kbss.ear.DAO.BowlingAlleyDao;
import cz.cvut.kbss.ear.model.BowlingAlley;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class BowlingAlleyService extends BaseService<BowlingAlley> {


    private final ManagerDao managerDao;

    private final BowlingAlleyDao bowlingAlleyDao;

    @Autowired
    public BowlingAlleyService(ManagerDao managerDao, BowlingAlleyDao bowlingAlleyDao) {
        super(BowlingAlley.class, bowlingAlleyDao);
        this.managerDao = managerDao;
        this.bowlingAlleyDao = bowlingAlleyDao;
    }


    @Transactional
    public void changeOpeningHour(int newOpeningHour) {
        if(newOpeningHour < 0 || newOpeningHour > 24){
            return;
        }
        BowlingAlley bowlingAlley = bowlingAlleyDao.getBowlingAlley();
        bowlingAlley.setOpeningHour(newOpeningHour);
        bowlingAlleyDao.update(bowlingAlley);
    }

    @Transactional
    public void changeClosingHour(int newClosingHour) {
        if(newClosingHour < 0 || newClosingHour >24){
            return;
        }
        BowlingAlley bowlingAlley = bowlingAlleyDao.getBowlingAlley();
        bowlingAlley.setClosingHour(newClosingHour);
        bowlingAlleyDao.update(bowlingAlley);
    }
}
