package cz.cvut.kbss.ear.model;

public enum StateOfBill {
    PAID, UNPAID
}
