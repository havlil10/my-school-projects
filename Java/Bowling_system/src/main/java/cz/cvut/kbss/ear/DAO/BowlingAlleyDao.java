package cz.cvut.kbss.ear.DAO;

import cz.cvut.kbss.ear.model.BowlingAlley;
import javassist.NotFoundException;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class BowlingAlleyDao extends BaseDao<BowlingAlley> {
    public BowlingAlleyDao() {
        super(BowlingAlley.class);
    }

    public BowlingAlley getBowlingAlley() {
        List<BowlingAlley> bowlingAlleys = this.findAll();
        if(bowlingAlleys.size()==0) {
            //throw new NotFoundException("bowling alley not found");
            return null;
        }
        return bowlingAlleys.get(0);
    }
}
