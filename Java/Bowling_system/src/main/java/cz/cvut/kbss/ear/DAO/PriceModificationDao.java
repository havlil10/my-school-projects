package cz.cvut.kbss.ear.DAO;

import cz.cvut.kbss.ear.model.PriceModification;
import org.springframework.stereotype.Repository;

@Repository
public class PriceModificationDao extends BaseDao<PriceModification>{
    public PriceModificationDao() {
        super(PriceModification.class);
    }
}
