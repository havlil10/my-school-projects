package cz.cvut.kbss.ear.service.security;

import cz.cvut.kbss.ear.DAO.ManagerDao;
import cz.cvut.kbss.ear.DAO.AccountDao;
import cz.cvut.kbss.ear.model.account.Account;
import cz.cvut.kbss.ear.model.account.ManagerAccount;
import cz.cvut.kbss.ear.security.model.UserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {

    private final ManagerDao managerDao;
    private final AccountDao accountDao;

    @Autowired
    public UserDetailsService(ManagerDao managerDao, AccountDao accountDao) {
        this.accountDao = accountDao;
        this.managerDao = managerDao;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        final Account account = accountDao.findByUsername(username);
        if (account != null) {
            return new cz.cvut.kbss.ear.security.model.UserDetails(account);
        }
        final ManagerAccount managerAccount = managerDao.findByUsername(username);
        if(managerAccount!=null) {
            return new cz.cvut.kbss.ear.security.model.UserDetails(managerAccount);
        }
        throw new UsernameNotFoundException("User with username " + username + " not found.");
    }
}
