package cz.cvut.kbss.ear.model;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
public class TimeSlot extends AbstractEntity{
    private Date date;
    private int hourFrom;
    private boolean reserved;
    private int price;


    @ManyToOne
    private Lane lane;


    public TimeSlot(Date date, int hourFrom, int price) {
        this.date = date;
        this.hourFrom = hourFrom;
        this.price = price;
        reserved = false;
    }

    public TimeSlot() {
        reserved = false;
    }

    public Lane getLane() {
        return lane;
    }

    public void setLane(Lane lane) {
        this.lane = lane;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getHourFrom() {
        return hourFrom;
    }

    public void setHourFrom(int hourFrom) {
        this.hourFrom = hourFrom;
    }

    public boolean isReserved() {
        return reserved;
    }

    public void setReserved(boolean reserved) {
        this.reserved = reserved;
    }


    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
}
