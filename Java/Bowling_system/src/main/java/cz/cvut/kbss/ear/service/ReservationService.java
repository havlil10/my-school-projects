package cz.cvut.kbss.ear.service;

import cz.cvut.kbss.ear.DAO.BillDao;
import cz.cvut.kbss.ear.DAO.ReservationDao;
import cz.cvut.kbss.ear.DAO.TimeSlotDao;
import cz.cvut.kbss.ear.exceptions.ReservationTooLargeException;
import cz.cvut.kbss.ear.exceptions.TimeSlotNotReservedException;
import cz.cvut.kbss.ear.model.Bill;
import cz.cvut.kbss.ear.model.account.Account;
import cz.cvut.kbss.ear.model.Reservation;
import cz.cvut.kbss.ear.model.TimeSlot;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PostFilter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.security.Principal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Service
public class ReservationService extends BaseService<Reservation>{
    private final TimeSlotDao timeSlotDao;
    private final BillDao billDao;
    private final ReservationDao reservationDao;

    @Autowired
    public ReservationService(ReservationDao reservationDao, TimeSlotDao timeSlotDao, BillDao billDao) {
        super(Reservation.class, reservationDao);
        this.timeSlotDao = timeSlotDao;
        this.billDao = billDao;
        this.reservationDao = reservationDao;
    }

    @Override
    @PostFilter("principal.username == filterObject.account.username or hasRole('ROLE_MANAGER')")
    public List<Reservation> findAll() {
        return reservationDao.findAll();
    }



    @Transactional(readOnly = true)
    public List<Reservation> findAllReservationsByPhoneNumber(int phoneNumber) {
        ReservationDao reservationDao = (ReservationDao) dao;
        List<Reservation> reservations = reservationDao.findAll();
        reservations.removeIf(reservation -> reservation.getAccount().getPhoneNumber() != phoneNumber);
        return reservations;
    }


    @Transactional
    public void cancelReservation(Reservation reservation) {
        Objects.requireNonNull(reservation);
        try {
            updateTimeSlotStatusOnRemoval(reservation);
        }
        catch (Exception e){
            e.printStackTrace();
        }
        reservation.setCanceled(true);
        dao.update(reservation);
    }

    @Transactional
    public void createReservation(List<TimeSlot> timeSlots, Account account) throws Exception {
        Objects.requireNonNull(timeSlots);
        Objects.requireNonNull(account);


        if(timeSlots.size() > 8 && !account.isVIP()){
            throw new ReservationTooLargeException("Reservation is larger than 8 timeslots.");
        }


        if(validateNumberOfReservations(account) > 1 && !account.isVIP()){
            throw new ReservationTooLargeException("User has too many reservations");
        }


        List<TimeSlot> fetchedTimeSlots = new ArrayList<>();
        for(TimeSlot timeSlot : timeSlots) {
            TimeSlot fetchedTimeSlot = timeSlotDao.find(timeSlot.getId());
            if(fetchedTimeSlot!=null) {
                fetchedTimeSlots.add(fetchedTimeSlot);
            }
        }

        Reservation reservation = new Reservation(fetchedTimeSlots, account);
        updateTimeSlotStatusWhenCreated(reservation);


        //create bill
        Bill bill = new Bill(reservation);
        billDao.persist(bill);

        reservation.setBill(bill);
        dao.persist(reservation);
    }

    private boolean isReservationOutDated(Reservation reservation) {
        for(TimeSlot timeSlot : reservation.getTimeSlots()){
            if(timeSlot.getDate().after(Date.from(Instant.now()))){
                return false;
            }
        }
        return true;
    }

    private int validateNumberOfReservations(Account acc){
        List<Reservation> userReservations = findAllReservationsByPhoneNumber(acc.getPhoneNumber());
        int numberOfReservations = 0;
        for(Reservation reservation : userReservations){
            if(!isReservationOutDated(reservation)&&!reservation.isCanceled()) {
                numberOfReservations++;
            }
        }
        return numberOfReservations;
    }

    private void updateTimeSlotStatusOnRemoval(Reservation reservation) throws Exception{
        final List<TimeSlot> timeSlotList = reservation.getTimeSlots();
        for(TimeSlot timeSlot : timeSlotList){
            if(!timeSlot.isReserved()) throw new TimeSlotNotReservedException("Timeslot in reservation not reserved");

            timeSlot.setReserved(false);
            timeSlotDao.update(timeSlot);
        }
    }

    private void updateTimeSlotStatusWhenCreated(Reservation reservation) throws Exception{
        for(TimeSlot timeSlot : reservation.getTimeSlots()){
            if(timeSlot.isReserved()) throw new AccessDeniedException("Reservation has already reserved timeslots");

            timeSlot.setReserved(true);
            timeSlotDao.update(timeSlot);
        }
    }

    @Transactional
    public void approveReservation(Reservation reservation){
        reservation.setApproved(true);
        dao.persist(reservation);
    }
}
