package cz.cvut.kbss.ear.model;

import javax.persistence.*;

@Entity
public class Bill extends AbstractEntity {
    @OneToOne(mappedBy = "bill")
    private Reservation reservation;

    @Enumerated(EnumType.STRING)
    private StateOfBill stateOfBill;

    public Bill(Reservation reservation) {
        this.reservation = reservation;
        stateOfBill = StateOfBill.UNPAID;
    }

    public Bill() {

    }

    public StateOfBill getStateOfBill() {
        return stateOfBill;
    }

    public void setStateOfBill(StateOfBill stateOfBill) {
        this.stateOfBill = stateOfBill;
    }
}
