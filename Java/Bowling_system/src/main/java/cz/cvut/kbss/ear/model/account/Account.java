package cz.cvut.kbss.ear.model.account;

import cz.cvut.kbss.ear.model.AbstractEntity;
import cz.cvut.kbss.ear.model.BowlingTeam;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@DiscriminatorColumn(name = "ACCOUNT_TYPE")
@NamedQueries({
        @NamedQuery(name = "Account.findByUsername", query = "SELECT u FROM Account u WHERE u.username = :username"),
        @NamedQuery(name = "Account.findByPhoneNumber", query = "SELECT u FROM Account u WHERE u.phoneNumber = :phoneNumber")
})
public class Account extends AbstractEntity {
    @Basic(optional = false)
    @Column(nullable = false)
    protected String password;

    @Basic(optional = false)
    @Column(nullable = false, unique = true)
    protected String username;

    @Basic(optional = false)
    @Column(nullable = false, unique = true)
    protected String email;

    @Basic(optional = false)
    @Column(nullable = false, unique = true)
    protected int phoneNumber;

    @Basic(optional = false)
    @Column(nullable = false)
    protected String firstName;

    @Basic(optional = false)
    @Column(nullable = false)
    protected String lastName;

    @Column(nullable = false)
    protected boolean banned;

    @Basic(optional = false)
    @Column(nullable = false)
    protected boolean isVIP;


    @ManyToMany
    @JoinTable(name = "account_bowling_teams",
            joinColumns = @JoinColumn(name = "account_id"),
            inverseJoinColumns = @JoinColumn(name = "bowling_teams_id"))
    private List<BowlingTeam> bowlingTeams = new ArrayList<>();

    public Account(String password, String username, String email, int phoneNumber, String firstName, String lastName) {
        this.password = password;
        this.username = username;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.firstName = firstName;
        this.lastName = lastName;
        this.banned = false;
        this.isVIP = false;
    }

    public Account() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<BowlingTeam> getBowlingTeams() {
        return bowlingTeams;
    }

    public void setBowlingTeams(List<BowlingTeam> bowlingTeams) {
        this.bowlingTeams = bowlingTeams;
    }

    public void addTeam(BowlingTeam bowlingTeam){
        Objects.requireNonNull(bowlingTeam);
        bowlingTeams.add(bowlingTeam);
    }

    public void removeTeam(BowlingTeam bowlingTeam){
        Objects.requireNonNull(bowlingTeam);
        bowlingTeams.remove(bowlingTeam);
    }

    public int getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(int phoneNumber) {
        this.phoneNumber = phoneNumber;
    }


    public void encodePassword(PasswordEncoder encoder) {
        this.password = encoder.encode(password);
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public boolean isBanned() {
        return banned;
    }

    public void setBanned(boolean banned) {
        this.banned = banned;
    }

    public boolean isVIP() {
        return isVIP;
    }

    public void setVIP(boolean VIP) {
        isVIP = VIP;
    }

    public String getRole() {
        return "ROLE_USER";
    }

    public void erasePassword() {
        this.password = null;
    }

    public boolean isAdmin() {
        return false;
    }
}
