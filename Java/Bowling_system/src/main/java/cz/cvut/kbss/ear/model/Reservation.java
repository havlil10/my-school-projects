package cz.cvut.kbss.ear.model;

import cz.cvut.kbss.ear.model.account.Account;

import javax.persistence.*;
import java.util.List;

@Entity
public class Reservation extends AbstractEntity{
    @Basic(optional = false)
    @Column(nullable = false)
    private boolean isCanceled;


    @OneToMany(cascade = CascadeType.ALL)
    private List<TimeSlot> timeSlots;

    @ManyToOne
    private Account account;

    @OneToOne(cascade = CascadeType.ALL)
    private Bill bill;

    @Basic(optional = false)
    @Column(nullable = false)
    private boolean approved;

    public Reservation(List<TimeSlot> timeSlots, Account account) {
        this.timeSlots = timeSlots;
        this.account = account;
        this.isCanceled = false;
        this.approved = false;
    }

    public Reservation() {
    }

    public boolean isCanceled() {
        return isCanceled;
    }

    public void setCanceled(boolean canceled) {
        isCanceled = canceled;
    }


    public List<TimeSlot> getTimeSlots() {
        return timeSlots;
    }

    public void setTimeSlots(List<TimeSlot> timeSlots) {
        this.timeSlots = timeSlots;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Bill getBill() {
        return bill;
    }

    public void setBill(Bill bill) {
        this.bill = bill;
    }

    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }
}
