package cz.cvut.kbss.ear.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Entity
public class Lane extends AbstractEntity{
    @OneToMany(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "timeslot_id")
    @OrderBy("date ASC")
    private List<TimeSlot> timeSlots;

    @OneToMany(orphanRemoval = true)
    private List<PriceModification> priceModifications;

    @ManyToOne
    @JoinColumn(name = "bowling_alley_id")
    private BowlingAlley bowlingAlley;

    public Lane(BowlingAlley bowlingAlley, int laneNumber, int initialPricerPerHour) {
        this.bowlingAlley = bowlingAlley;
        this.laneNumber = laneNumber;
        this.initialPricerPerHour = initialPricerPerHour;
        this.opened = true;
        timeSlots = new ArrayList<>();
        priceModifications = new ArrayList<>();
    }

    public Lane() {
    }

    @Basic(optional = false)
    @Column(nullable = false)
    private int laneNumber;

    public Integer getLaneNumber() {
        return laneNumber;
    }

    public void setLaneNumber(Integer laneNumber) {
        this.laneNumber = laneNumber;
    }

    @Basic(optional = false)
    @Column(nullable = false)
    private int initialPricerPerHour;

    @Basic(optional = false)
    @Column(nullable = false)
    private boolean opened;

    public List<PriceModification> getPriceModifications() {
        return priceModifications;
    }

    public void setPriceModifications(List<PriceModification> priceModifications) {
        this.priceModifications = priceModifications;
    }

    public int getInitialPricerPerHour() {
        return initialPricerPerHour;
    }

    public boolean isOpened() {
        return opened;
    }

    public void setInitialPricerPerHour(int initialPricerPerHour) {
        this.initialPricerPerHour = initialPricerPerHour;
    }

    public void setOpened(boolean opened) {
        this.opened = opened;
    }

    public List<TimeSlot> getTimeSlots() {
        return timeSlots;
    }

    public void setTimeSlots(List<TimeSlot> timeSlots) {
        this.timeSlots = timeSlots;
    }

    public BowlingAlley getBowlingAlley() {
        return bowlingAlley;
    }

    public void setBowlingAlley(BowlingAlley bowlingAlley) {
        this.bowlingAlley = bowlingAlley;
    }

}
