package cz.cvut.kbss.ear.security.model;

import cz.cvut.kbss.ear.model.account.Account;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.*;

public class UserDetails implements org.springframework.security.core.userdetails.UserDetails {

    private Account account;

    private final Set<GrantedAuthority> authorities;

    public UserDetails(Account account) {
        Objects.requireNonNull(account);
        this.account = account;
        this.authorities = new HashSet<>();
        addUserRole();
    }

    public UserDetails(Account account, Collection<GrantedAuthority> authorities) {
        Objects.requireNonNull(account);
        Objects.requireNonNull(authorities);
        this.account = account;
        this.authorities = new HashSet<>();
        addUserRole();
        this.authorities.addAll(authorities);
    }

    private void addUserRole() {
        authorities.add(new SimpleGrantedAuthority(account.getRole()));
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.unmodifiableCollection(authorities);
    }

    @Override
    public String getPassword() {
        return account.getPassword();
    }

    @Override
    public String getUsername() {
        return account.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return false;
    }

    @Override
    public boolean isAccountNonLocked() {
        return !account.isBanned();
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public Account getUser() {
        return account;
    }

    public void eraseCredentials() {
        account.erasePassword();
    }
}

