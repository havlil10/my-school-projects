package cz.cvut.kbss.ear.rest;

import cz.cvut.kbss.ear.exceptions.NotFoundException;
import cz.cvut.kbss.ear.model.BowlingTeam;
import cz.cvut.kbss.ear.model.account.Account;
import cz.cvut.kbss.ear.model.account.ManagerAccount;
import cz.cvut.kbss.ear.service.AccountService;
import cz.cvut.kbss.ear.service.BowlingTeamService;
import cz.cvut.kbss.ear.service.ManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/users")
public class AccountController {
    private final AccountService accountService;
    private final BowlingTeamService bowlingTeamService;

    @Autowired
    public AccountController(AccountService accountService, BowlingTeamService bowlingTeamService) {
        this.accountService = accountService;
        this.bowlingTeamService = bowlingTeamService;
    }

    @PreAuthorize("(!#account.isAdmin() && anonymous) || hasRole('ROLE_MANAGER')")
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> createAccount(@RequestBody Account account){
        accountService.persist(account);
        final HttpHeaders headers = Utils.createLocationHeaderFromCurrentUri("/{id}", account.getId());
        return new ResponseEntity<>(headers, HttpStatus.CREATED);
    }


    @PreAuthorize("hasAnyRole('ROLE_MANAGER', 'ROLE_USER')")
    @PutMapping(value = "/{id}/team/{teamId}")
    public void addTeam(@PathVariable int id, @PathVariable int idTeam) throws NotFoundException {
        Account acc = accountService.find(id);
        BowlingTeam team = bowlingTeamService.find(idTeam);

        if(acc == null || team == null){
            throw new NotFoundException("Cannot add team with id: " + idTeam + "to account: " + id);
        }

        acc.addTeam(team);
        team.addAccount(acc);

        accountService.update(acc);
        bowlingTeamService.update(team);
    }


    @PreAuthorize("hasRole('ROLE_MANAGER')")
    @PutMapping(value = "/ban/{id}")
    public void banAccount(@PathVariable int id) throws NotFoundException {
        Account acc = accountService.find(id);
        if(acc == null){
            throw new NotFoundException("User with id: " + id + " not found.");
        }
        accountService.banAccount(acc);
    }


    @PreAuthorize("hasRole('ROLE_MANAGER')")
    @GetMapping(value = "/all")
    public List<Account> getAccounts(){
        return accountService.findAll();
    }

    /*
    @GetMapping(value = "/name/{username}")
    public Account getAccountByName(@PathVariable String name){
        return accountService.findByUsername(name);
    }

    @GetMapping(value = "/{id}")
    public Account getAccount(@PathVariable int id){
        return accountService.find(id);
    }

    @GetMapping(value = "/phone/{phoneNr}")
    public Account getAccountByPhone(@PathVariable int phoneNr){
        return accountService.findByPhone(phoneNr);
    }
     */
}
