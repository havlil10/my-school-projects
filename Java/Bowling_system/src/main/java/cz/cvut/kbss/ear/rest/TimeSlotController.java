package cz.cvut.kbss.ear.rest;

import cz.cvut.kbss.ear.model.TimeSlot;
import cz.cvut.kbss.ear.model.account.Account;
import cz.cvut.kbss.ear.security.model.AuthenticationToken;
import cz.cvut.kbss.ear.service.ReservationService;
import cz.cvut.kbss.ear.service.TimeslotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.AccessDeniedException;

import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/timeslots")
public class TimeSlotController {
    private final TimeslotService timeslotService;
    private final ReservationService reservationService;

    @Autowired
    public TimeSlotController(TimeslotService timeslotService, ReservationService reservationService) {
        this.timeslotService = timeslotService;
        this.reservationService = reservationService;
    }

    @GetMapping
    public List<TimeSlot> getAllTimeslots() {
        return timeslotService.findAll();
    }


    @PreAuthorize("hasAnyRole('ROLE_MANAGER', 'ROLE_USER')")
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> createReservation(Principal principal, @RequestBody List<TimeSlot> timeSlots) throws Exception {

        final AuthenticationToken auth = (AuthenticationToken) principal;
        Account account = auth.getPrincipal().getUser();
        if (account==null) {
            throw new AccessDeniedException("You have to be signed in in order to reserve");
        }

        reservationService.createReservation(timeSlots, account);

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    //for testing purposes
    @DeleteMapping
    public void removeAllTimeslots() {
        List<TimeSlot> allTimeslots = timeslotService.findAll();
        for(TimeSlot timeSlot : allTimeslots) {
            timeslotService.remove(timeSlot);
        }
    }

}
