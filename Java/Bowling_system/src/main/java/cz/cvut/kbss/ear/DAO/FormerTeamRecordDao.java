package cz.cvut.kbss.ear.DAO;

import cz.cvut.kbss.ear.model.FormerTeamRecord;
import org.springframework.stereotype.Repository;

@Repository
public class FormerTeamRecordDao extends BaseDao<FormerTeamRecord>{
    public FormerTeamRecordDao() {
        super(FormerTeamRecord.class);
    }
}
