package cz.cvut.kbss.ear.DAO;

import cz.cvut.kbss.ear.model.TimeSlot;
import org.springframework.stereotype.Repository;

@Repository
public class TimeSlotDao extends BaseDao<TimeSlot> {
    public TimeSlotDao() {
        super(TimeSlot.class);
    }
}
