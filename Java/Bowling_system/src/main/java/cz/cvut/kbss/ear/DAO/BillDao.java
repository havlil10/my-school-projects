package cz.cvut.kbss.ear.DAO;

import cz.cvut.kbss.ear.model.Bill;
import org.springframework.stereotype.Repository;

@Repository
public class BillDao extends BaseDao<Bill>{
    public BillDao() {
        super(Bill.class);
    }
}
