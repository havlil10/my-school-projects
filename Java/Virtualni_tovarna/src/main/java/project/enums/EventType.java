package project.enums;

public enum EventType {
    MACHINE_DESTRUCTION, MACHINE_FIX_START, MACHINE_FIX_FINISHED
}
