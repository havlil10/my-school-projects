package project.model.State;

import project.enums.EventType;
import project.model.Event.Event;
import project.model.ExternalEmployee.Repairer;
import project.model.FactoryLine.Factory;
import project.model.FactoryLine.Line;

import java.util.ArrayList;
import java.util.List;

public class WaitingForFix extends LineState{
    private List<Event> eventsToHandle = new ArrayList<>();
    private List<Integer> hoursNeededToRepair = new ArrayList<>();
    private List<Boolean> isRequestSent = new ArrayList<>();


    public WaitingForFix(Line line, int currentAmount, int amountToCreate) {
        super(line, currentAmount, amountToCreate);
        this.stateName = "WaitingForFix";
    }

    public void nextTact() {
        if(areAllMachinesFixed()) {
            line.setLineState(new InProgress(line, line.getLineState().getAmount(), this.amountToCreate));
            return;
        }
        for(int i = 0; i < eventsToHandle.size(); i++) {
            if(eventsToHandle.get(i).getEventType()==EventType.MACHINE_DESTRUCTION&&!isRequestSent.get(i)) {
                Factory.getInstance().addRepairerRequest(line);
                isRequestSent.set(i, true);

            } else if(eventsToHandle.get(i).getEventType()==EventType.MACHINE_FIX_START) {
                if(hoursNeededToRepair.get(i) > 1) {
                    hoursNeededToRepair.set(i, hoursNeededToRepair.get(i)-1);
                } else {
                    finishFix(i);
                }
            }
        }

    }

    @Override
    public void acceptRepairer(Repairer repairer) {
        for(int i = 0; i<eventsToHandle.size(); i++) {
            if(eventsToHandle.get(i).getEventType()==EventType.MACHINE_DESTRUCTION) {
                Event machineDestruction = eventsToHandle.get(i);
                machineDestruction.setHandled(true);
                Event machineFix = new Event(EventType.MACHINE_FIX_START, machineDestruction.getRegisteredLineMember());
                machineFix.setRepairer(repairer);
                line.getEvents().add(machineFix);
                eventsToHandle.set(i, machineFix);
                hoursNeededToRepair.set(i, 5);
                return;
            }
        }

    }

    public void addEventToHandle(Event event) {
        this.eventsToHandle.add(event);
        this.hoursNeededToRepair.add(0);
        this.isRequestSent.add(false);
    }

    private void finishFix(int index) {
        Event handledEvent = eventsToHandle.get(index);
        handledEvent.getRegisteredLineMember().getFixed();
        handledEvent.setHandled(true);
        handledEvent.getRepairer().setAvailable(true);

        Event machineFixFinished = new Event(EventType.MACHINE_FIX_FINISHED, eventsToHandle.get(index).getRegisteredLineMember());
        machineFixFinished.setRepairer(handledEvent.getRepairer());
        line.getEvents().add(machineFixFinished);
        eventsToHandle.set(index, machineFixFinished);
    }

    private boolean areAllMachinesFixed() {
        for(Event event : eventsToHandle) {
            if(event.getEventType()!=EventType.MACHINE_FIX_FINISHED) {
                return false;
            }
        }
        return true;
    }

}
