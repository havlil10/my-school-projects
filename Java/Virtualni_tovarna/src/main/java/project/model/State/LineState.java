package project.model.State;

import project.model.ExternalEmployee.Repairer;
import project.model.FactoryLine.Line;

public abstract class LineState {

    protected String stateName;
    protected int currentAmount;
    protected int amountToCreate;
    protected int currentTact;
    protected Line line;


    public LineState(Line line, int currentAmount, int amountToCreate){
        this.currentAmount = currentAmount;
        this.currentTact = 0;
        this.line = line;
        this.amountToCreate = amountToCreate;
    }



    public abstract void nextTact();

    public int getAmount() {
        return currentAmount;
    }

    public void setAmount(int amount) {
        this.currentAmount = amount;
    }

    public int getAmountToCreate() {
        return amountToCreate;
    }

    public void setAmountToCreate(int amountToCreate) {
        this.amountToCreate = amountToCreate;
    }

    public int getCurrentTact() {
        return currentTact;
    }

    public void setCurrentTact(int currentTact) {
        this.currentTact = currentTact;
    }

    public String getStateName() {
        return stateName;
    }

    public abstract void acceptRepairer(Repairer repairer);



}
