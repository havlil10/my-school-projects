package project.model.State;


import project.model.ExternalEmployee.Repairer;
import project.model.FactoryLine.Line;
import project.model.LineMemeber.LineMember;
import project.model.LineMemeber.LineMemberVisitor;

public class InProgress extends LineState {


    public InProgress(Line line, int currentAmount, int amountToCreate) {
        super(line, currentAmount, amountToCreate);
        this.stateName = "InProgress";
    }
    public void nextTact() {
        if(this.currentAmount < this.amountToCreate) {
            this.currentAmount++;
            accept(new LineMemberVisitor(this.line));
        } else {
            this.line.setLineState(new Finished(this.line, this.currentAmount, this.amountToCreate));
        }
    }

    @Override
    public void acceptRepairer(Repairer repairer) {
        repairer.setAvailable(true);
        System.out.println("Repairer " + repairer.getName() + " was brought back because line needs no repairer right now");
    }

    public void accept(LineMemberVisitor visitor){
        for(LineMember l : line.getLineMemberList()){
            l.accept(visitor);
        }
    }


}
