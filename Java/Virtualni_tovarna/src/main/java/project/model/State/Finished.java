package project.model.State;

import project.model.ExternalEmployee.Repairer;
import project.model.FactoryLine.Line;

public class Finished extends LineState{
    public Finished(Line line, int currentAmount, int amountToCreate) {
        super(line, currentAmount, amountToCreate);
        this.stateName = "Finished";
    }

    public void nextTact() {

    }

    @Override
    public void acceptRepairer(Repairer repairer) {
        repairer.setAvailable(true);
        System.out.println("Repairer " + repairer.getName() + " was brought back because line needs no repairer right now");
    }

}
