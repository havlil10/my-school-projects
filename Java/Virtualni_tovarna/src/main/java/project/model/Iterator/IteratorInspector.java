package project.model.Iterator;

import project.main.Init;
import project.model.FactoryLine.Factory;
import project.model.FactoryLine.Line;
import project.model.LineMemeber.*;

import java.util.*;

public class IteratorInspector implements Iterator{

    private int posLine = 0;
    private int lineLength = 0;

    List<LineMember> list = new LinkedList<>();

    @Override
    public boolean hasNext() {
        if(posLine == 0){
            writeFactor();
            lineLength = Init.factory.getLineList().get(0).getLineMemberList().size();
            for(Line l : Init.factory.getLineList()){
                list.addAll(l.getLineMemberList());
            }
        }
        list.sort(Comparator.comparing(LineMember::getAbility));
        if(posLine + 1 < list.size()){
            return true;
        }
        return false;
    }

    @Override
    public LineMember next() {
        LineMember lineMember = list.get(posLine);
        writeLineMember(lineMember);
        posLine++;
        return lineMember;
    }

    private void writeLineMember(LineMember lineMember) {
        System.out.println("Line Member " + lineMember.getName() + " " + lineMember.getAbility());
    }



    private void writeFactor(){
        System.out.println();
        System.out.println("Factory Inspector start");
        System.out.println("-----------------");
    }

}
