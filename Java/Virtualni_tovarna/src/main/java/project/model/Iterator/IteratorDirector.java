package project.model.Iterator;

import project.main.Init;
import project.model.FactoryLine.Factory;
import project.model.FactoryLine.Line;
import project.model.LineMemeber.LineMember;

public class IteratorDirector implements Iterator{

    private int posLine = 0;
    private int posFactory = 0;


    @Override
    public boolean hasNext() {
        if(posLine >= Init.factory.getLineList().get(posFactory).getLineMemberList().size()){
            if(posFactory + 1 < Init.factory.getLineList().size()){
                posLine = 0;
                posFactory++;
                writeLine(Init.factory.getLineList().get(posFactory));
                return true;
            }else {
                return false;
            }
        }
        return true;
    }

    @Override
    public LineMember next() {
        if(posLine == 0 && posFactory == 0){
            writeFactor();
            writeLine(Init.factory.getLineList().get(0));
        }
        LineMember lineMember = Init.factory.getLineList().get(posFactory).getLineMemberList().get(posLine);
        posLine++;
        writeLineMember(lineMember);
        return lineMember;
    }

    private void writeLineMember(LineMember lineMember){
        System.out.println("Line Member " + lineMember.getName() + " " + lineMember.getAbility());
    }

    private void writeLine(Line line){
        System.out.println();
        System.out.println("Line " + line.getName() + " priority: " + line.getLineImportance() +
                " need to produce: " + line.getLineState().getAmountToCreate());
        System.out.println();
    }

    private void writeFactor(){
        System.out.println();
        System.out.println("Factory Director start");
        System.out.println("-----------------");
    }
}
