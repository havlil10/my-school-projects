package project.model.Iterator;

import project.model.LineMemeber.LineMember;

public interface Iterator {

    public boolean hasNext();

    public LineMember next();
}
