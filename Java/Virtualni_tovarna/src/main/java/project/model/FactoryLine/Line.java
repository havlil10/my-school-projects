package project.model.FactoryLine;

import project.enums.Ability;
import project.model.Event.Event;
import project.model.LineMemeber.GetDataVisitor;
import project.model.LineMemeber.LineMember;
import project.model.LineMemeber.LineMemberVisitor;
import project.model.Manual.Manual;
import project.model.Pool.LineMemberPool;
import project.model.State.LineState;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Line {


    private List<LineMember> lineMemberList;

    private  int lineImportance = 0;


    private String name;

    private List<Event> events;

    private LineState lineState;

    Line(){
        lineMemberList = new LinkedList<>();
        events = new ArrayList<>();
    }

    public void generateLine(Manual manual){
        name = manual.getName();
        lineImportance = manual.getLinePriority();
        for(Ability a : manual.getManualOfLine()){
            lineMemberList.add(LineMemberPool.getObject(a));
        }
    }

    public void accept(GetDataVisitor visitor) {
        visitor.visit(this);
    }

    public int getLineImportance() {
        return lineImportance;
    }

    public void setLineImportance(int lineImportance) {
        this.lineImportance = lineImportance;
    }

    public LineState getLineState() {
        return this.lineState;
    }

    public void setLineState(LineState lineState) {
        this.lineState = lineState;
    }

    public List<Event> getEvents() {
        return events;
    }

    public String getName() {
        return name;
    }

    public List<LineMember> getLineMemberList() {
        return lineMemberList;
    }
}
