package project.model.FactoryLine;

import project.enums.EventType;
import project.model.Event.Event;
import project.model.ExternalEmployee.Repairer;
import project.model.Iterator.IteratorDirector;
import project.model.Iterator.IteratorInspector;
import project.model.LineMemeber.FactoryData;
import project.model.LineMemeber.GetDataVisitor;
import project.model.LineMemeber.LineMember;
import project.model.Manual.Manual;
import project.model.State.Finished;
import project.model.State.InProgress;

import java.util.*;
import java.util.stream.Collectors;

public class Factory {

    private int tact = 0;

    private static Factory instance;


    private List<Line> lineList;
    private List<Repairer> repairers;
    private List<Line> requestsForRepairer;

    private Factory(){
        lineList = new LinkedList<>();
        repairers = new ArrayList<>();
        requestsForRepairer = new ArrayList<>();
        createSomeRepairers();
    }

    public synchronized static Factory getInstance(){
        if(instance == null){
            instance = new Factory();
        }
        return instance;
    }

    public IteratorDirector getDirectorIterator(){
        return new IteratorDirector();
    }

    public IteratorInspector getInspectorIterator(){
        return new IteratorInspector();
    }

    public void createLines(List<Manual> manuals){
        for(int x = 0; x < manuals.size();x++){
            Line line = new Line();
            line.setLineState(new InProgress(line, 0, manuals.get(x).getNeedToProduce()));
            lineList.add(line);
            assemblyLine(manuals.get(x),line);
        }
    }

    public void assemblyLine(Manual manual, Line line){
        line.generateLine(manual);
    }

    private boolean allLinesFinished(List<Line> lineList) {

        for(Line line : lineList) {

            if(!line.getLineState().getStateName().equals("Finished")) {
                return false;
            }
        }
        return true;
    }

    public void startProduction(){
        while (!allLinesFinished(lineList)){
            for(Line line : lineList) {
                line.getLineState().nextTact();
            }
            processRepairerRequests();
            System.out.println("tact number: " + tact);
            tact++;

        }
        getReport();
        getEventReport();
        getOutagesReport();
    }

    private void getOutagesReport() {
        List<Event> allEvents = mergeAllLineEvents();
        List<Integer> allOutageRecords = getAllOutageRecords(allEvents);
        System.out.println();
        System.out.println("-----------------------");
        System.out.println("Outages report");
        System.out.println("-----------------------");
        System.out.println("Minimal outage time: " + allOutageRecords.stream().min(Comparator.naturalOrder()).get());
        System.out.println("Maximal outage time: " + allOutageRecords.stream().max(Comparator.naturalOrder()).get());
        double averageTime = allOutageRecords.stream().mapToInt(Integer::intValue).summaryStatistics().getAverage();
        System.out.println("Average outage time: " + String.format("%.2f", averageTime));
        double averageWaitingTime = getAllWaitingTimesForRepairer(allEvents).stream().mapToInt(Integer::intValue).summaryStatistics().getAverage();
        System.out.println("Average waiting time for repairer: " + String.format("%.2f", averageWaitingTime));
    }

    private List<Integer> getAllWaitingTimesForRepairer(List<Event> allEvents) {
        List<Integer> waitingTimes = new ArrayList<>();
        Queue<Event> machineDestructions = new LinkedList<>();
        for(Event event : allEvents) {
            if(event.getEventType()==EventType.MACHINE_DESTRUCTION) {
                machineDestructions.add(event);
            } else if(event.getEventType()==EventType.MACHINE_FIX_START && !machineDestructions.isEmpty()) {
                waitingTimes.add(event.getTact()-machineDestructions.peek().getTact());
                machineDestructions.remove();
            }
        }
        return waitingTimes;
    }

    private List<Integer> getAllOutageRecords(List<Event> allEvents) {
        List<Integer> outageRecords = new ArrayList<>();
        Queue<Event> machineDestructions = new LinkedList<>();
        for(Event event : allEvents) {
            if(event.getEventType()==EventType.MACHINE_DESTRUCTION) {
                machineDestructions.add(event);
            } else if(event.getEventType()==EventType.MACHINE_FIX_FINISHED && !machineDestructions.isEmpty()) {
                outageRecords.add(event.getTact()-machineDestructions.peek().getTact());
                machineDestructions.remove();
            }
        }
        return outageRecords;
    }

    private void processRepairerRequests() {
        if(requestsForRepairer.isEmpty()) return;
        for(Repairer repairer : repairers) {
            if(repairer.isAvailable()) {
                Line lineToRepair = findMostImportantLine();
                lineToRepair.getLineState().acceptRepairer(repairer);
                repairer.setAvailable(false);
                System.out.println("Repairer " + repairer.getName() + " sent for repair");
                if(requestsForRepairer.isEmpty()) return;
            }
        }
    }

    private Line findMostImportantLine() {
        Line bestLine = requestsForRepairer.get(0);
        int index = 0;
        for(int i = 1; i < requestsForRepairer.size(); i++) {
            if(requestsForRepairer.get(i).getLineImportance() > bestLine.getLineImportance()) {
                bestLine = requestsForRepairer.get(i);
                index = i;
            }
        }
        requestsForRepairer.remove(index);
        return bestLine;
    }

    private List<Event> mergeAllLineEvents() {
        List<Event> allEvents = new ArrayList<>();
        for(Line line : lineList) {
            allEvents.addAll(line.getEvents());
        }
        return allEvents;
    }

    private void getEventReport() {
        List<Event> allEvents = mergeAllLineEvents();
        System.out.println();
        System.out.println("---------------");
        System.out.println("Event report");
        System.out.println("---------------");
        printReportSortedByEventType(allEvents);
        printReportSortedByRepairer(allEvents);

    }

    private List<LineMember> getUniqueEventSources(List<Event> events) {
        return events.stream()
                .map(Event::getRegisteredLineMember)
                .filter(Objects::nonNull)
                .distinct()
                .collect(Collectors.toList());
    }
/*
    private void printReportSortedByEventSource(List<Event> events) {
        for(LineMember lineMember : getUniqueEventSources(events)) {
            int size = events.stream().filter(e->e.getRegisteredLineMember()==lineMember).collect(Collectors.toList()).size();
            System.out.println("For device: " + lineMember.getName() + " is number of done fixes: " + size);
        }
    }
*/
    private List<EventType> getUniqueEventTypes(List<Event> events) {
        return events.stream()
                .map(Event::getEventType)
                .distinct()
                .collect(Collectors.toList());
    }

    private void printReportSortedByEventType(List<Event> events) {
        for(EventType eventType : getUniqueEventTypes(events)) {
            int size = events.stream().filter(e->e.getEventType()==eventType).collect(Collectors.toList()).size();
            System.out.println("For event: " + eventType + " (" + returnNameForEventType(eventType) + ")");
            System.out.println("Number of events: " + size);
            System.out.println("-----------------");
        }
    }

    private String returnNameForEventType(EventType eventType) {
        if(eventType==EventType.MACHINE_DESTRUCTION) {
            return "Destruction of machine or robot";
        } else if(eventType == EventType.MACHINE_FIX_START) {
            return "Start of fixing machine or robot";
        } else {
            return "Fix of machine or robot";
        }
    }

    private List<Repairer> getUniqueRepairers(List<Event> events) {
        return events.stream()
                .map(Event::getRepairer)
                .filter(Objects::nonNull)
                .distinct()
                .collect(Collectors.toList());
    }

    private void printReportSortedByRepairer(List<Event> events) {
        for(Repairer repairer : getUniqueRepairers(events)) {
            int size = events.stream().filter(e->e.getRepairer()==repairer).collect(Collectors.toList()).size();
            System.out.println("Number of equipments repaired by " + repairer.getName() + ": " + (size-size/2));
        }
    }


    public void getReport(){
        System.out.println();
        System.out.println("Factory consumption and configuration report");
        System.out.println("----------------------");
        for(Line line : lineList) {
            line.accept(new GetDataVisitor());
        }
        System.out.println("\n-----------------------");
        System.out.println("Factory totals energy: " + FactoryData.getTotalEnergy() + " oil:" + FactoryData.getTotalOil()
                + " cost:" + FactoryData.getTotalCost());
    }

    public List<Repairer> getRepairers() {
        return repairers;
    }

    private void createSomeRepairers() {
        repairers.add(new Repairer("Karel"));
        repairers.add(new Repairer("Pepa"));
    }

    public void addRepairerRequest(Line line) {
        requestsForRepairer.add(line);
    }

    public int getTact() {
        return tact;
    }

    public List<Line> getLineList() {
        return lineList;
    }
}
