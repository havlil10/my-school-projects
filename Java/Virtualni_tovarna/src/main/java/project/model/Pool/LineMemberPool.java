package project.model.Pool;

import project.enums.Ability;
import project.model.LineMemeber.LineMember;

import java.util.LinkedList;

public class LineMemberPool {

    private static LinkedList<LineMember> pool = new LinkedList<>();

    public static LineMember getObject(Ability ability){

        LineMember tmp = pool.stream()
                .filter(x -> x.getAbility() == ability)
                .findFirst()
                .orElse(null);

        pool.remove(tmp);
        return tmp;
    }

    public static void returnObject(LineMember lineMember){
        pool.add(lineMember);
    }

}
