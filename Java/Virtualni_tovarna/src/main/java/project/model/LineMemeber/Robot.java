package project.model.LineMemeber;

import project.enums.Ability;

import java.util.ArrayList;
import java.util.List;

public class Robot extends LineMember {

    private int depreciation = 100;
    private final int energyConsumption = 80;

    private int totalEnergyConsumption = 0;

    public Robot(Ability ability) {
        this.ability = ability;
    }

    public void oneTactDepreciation() {
        depreciation-= (int)(Math.random() * 2);
    }

    public int getDepreciation() {
        return depreciation;
    }

    @Override
    public void provideData() {
        System.out.println(getName()
                + " totalEnergyConsumption: " +totalEnergyConsumption + " deprecation: " + depreciation
                + " ability: " + ability + " energyConsumption: " + energyConsumption);
    }

    public void accept(LineMemberVisitor lineMemberVisitor) {
        lineMemberVisitor.visit(this);
    }

    @Override
    public void nextTact() {
        oneTactDepreciation();
        totalEnergyConsumption = totalEnergyConsumption + energyConsumption;
    }

    @Override
    public void getFixed() {
        this.depreciation = 100;
    }

    @Override
    public String getName() {
        return "Robot";
    }

    @Override
    public List<Integer> provideInfo() {
        List<Integer> ll = new ArrayList<>();
        ll.add(totalEnergyConsumption);
        ll.add(0);
        ll.add(0);
        return ll;

    }


}
