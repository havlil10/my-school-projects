package project.model.LineMemeber;

import project.enums.EventType;
import project.model.Event.Event;
import project.model.FactoryLine.Line;
import project.model.State.LineState;
import project.model.State.WaitingForFix;

public class LineMemberVisitor {

    private final Line line;

    public LineMemberVisitor(Line line) {
        this.line = line;
    }

    public void visit(Human human) {
        human.nextTact();
    }

    public void visit(Machine machine) {
        machine.nextTact();
        if(machine.getDepreciation()<=0) {
            Event machineDestruction = new Event(EventType.MACHINE_DESTRUCTION, machine);
            processEvent(machineDestruction);
        }
    }

    public void visit(Robot robot) {
        robot.nextTact();
        if(robot.getDepreciation()<=0) {
            Event machineDestruction = new Event(EventType.MACHINE_DESTRUCTION, robot);
            processEvent(machineDestruction);
        }
    }
    public void processEvent(Event machineDestruction) {
        line.getEvents().add(machineDestruction);
        WaitingForFix lineState;
        if(line.getLineState().getStateName().equals("WaitingForFix")) {
            lineState = (WaitingForFix)line.getLineState();
            //line.setLineState(new WaitingForFix(line, line.getLineState().getAmount(), line.getLineState().getAmountToCreate()));
        } else {
            lineState = new WaitingForFix(line, line.getLineState().getAmount(), line.getLineState().getAmountToCreate());
            line.setLineState(lineState);
        }
        lineState.addEventToHandle(machineDestruction);
    }
}
