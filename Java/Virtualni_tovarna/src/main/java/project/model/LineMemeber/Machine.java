package project.model.LineMemeber;

import project.enums.Ability;

import java.util.ArrayList;
import java.util.List;

public class Machine extends LineMember {

    private int depreciation = 100;
    private final int oilConsumption = 20;
    private final int energyConsumption = 50;

    private int totalOilConsumption = 0;
    private int totalEnergyConsumption = 0;

    public Machine(Ability ability) {
        this.ability = ability;
    }

    public void oneTactDepreciation() {
        depreciation-= (int)(Math.random() * 2);
    }

    public int getDepreciation() {
        return depreciation;
    }

    @Override
    public void provideData() {
        System.out.println(getName() + " totalOilConsumption: " + totalOilConsumption
                + " totalEnergyConsumption: " +totalEnergyConsumption + " deprecation: " + depreciation
        + " ability: " + ability + " oilConsumption: " + oilConsumption + " energyConsumption: " + energyConsumption);

    }

    public void accept(LineMemberVisitor lineMemberVisitor) {
        lineMemberVisitor.visit(this);
    }

    @Override
    public void nextTact() {
        oneTactDepreciation();
        totalOilConsumption = totalOilConsumption + oilConsumption;
        totalEnergyConsumption = totalEnergyConsumption + energyConsumption;
    }

    @Override
    public void getFixed() {
        this.depreciation = 100;
    }

    @Override
    public String getName() {
        return "Machine";
    }

    @Override
    public List<Integer> provideInfo() {
        List<Integer> ll = new ArrayList<>();
        ll.add(totalEnergyConsumption);
        ll.add(totalOilConsumption);
        ll.add(0);
        return ll;
    }


}
