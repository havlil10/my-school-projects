package project.model.LineMemeber;

import project.model.FactoryLine.Line;

import java.util.List;

public class GetDataVisitor {



    public void visit(Line line) {
        int totalEnergy  = 0;
        int totalOil = 0;
        int totalCost = 0;
        System.out.println();
        System.out.println("----------------------");
        System.out.println("Line " + line.getName() + " need to create: " + line.getLineState().getAmountToCreate()
                + " prioritization: " + line.getLineImportance());
        for(LineMember l : line.getLineMemberList()) {
            l.provideData();
            List<Integer> list = l.provideInfo();
            totalEnergy += list.get(0);
            totalOil += list.get(1);
            totalCost += list.get(2);
        }
        FactoryData.setTotalEnergy(totalEnergy);
        FactoryData.setTotalOil(totalOil);
        FactoryData.setTotalCost(totalCost);
        System.out.println("Line totals energy: " + totalEnergy + " oil: " + totalOil + " cost: " + totalCost);
    }


}
