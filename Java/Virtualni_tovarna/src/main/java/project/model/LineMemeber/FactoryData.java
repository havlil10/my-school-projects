package project.model.LineMemeber;

public class FactoryData {

    private static int totalEnergy = 0;
    private static int totalOil = 0;
    private static int totalCost = 0;

    public static int getTotalEnergy() {
        return totalEnergy;
    }

    public static void setTotalEnergy(int totalEnergy) {
        FactoryData.totalEnergy += totalEnergy;
    }

    public static int getTotalOil() {
        return totalOil;
    }

    public static void setTotalOil(int totalOil) {
        FactoryData.totalOil += totalOil;
    }

    public static int getTotalCost() {
        return totalCost;
    }

    public static void setTotalCost(int totalCost) {
        FactoryData.totalCost += totalCost;
    }
}
