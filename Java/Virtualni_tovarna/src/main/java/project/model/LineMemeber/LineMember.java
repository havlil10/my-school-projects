package project.model.LineMemeber;

import project.enums.Ability;
import project.model.FactoryLine.Line;

import java.util.List;

public abstract class LineMember {

    protected Ability ability;

    public Ability getAbility() {
        return ability;
    }

    public abstract void provideData();

    public abstract void accept(LineMemberVisitor lineMemberVisitor);

    public abstract void nextTact();

    public abstract void getFixed();

    public abstract String getName();

    //1 energy 2 iol 3 cost
    public abstract List<Integer> provideInfo();




}
