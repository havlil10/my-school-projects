package project.model.LineMemeber;

import project.enums.Ability;

import java.util.ArrayList;
import java.util.List;

public class Human extends LineMember{

    private int costPerHour = 100;
    private int totalCost = 0;



    public Human(Ability ability) {
        this.ability = ability;
    }


    @Override
    public void provideData() {
        System.out.println(getName() + " totalCost: " + totalCost
                + " ability: " + ability + " costPerHour: " + costPerHour);
    }

    public void accept(LineMemberVisitor lineMemberVisitor) {
        lineMemberVisitor.visit(this);
    }

    public void nextTact(){
        totalCost = totalCost + costPerHour;
    }

    @Override
    public void getFixed() {

    }

    @Override
    public String getName() {
        return "Human";
    }

    @Override
    public List<Integer> provideInfo() {
        List<Integer> ll = new ArrayList<>();
        ll.add(0);
        ll.add(0);
        ll.add(totalCost);
        return ll;
    }


}
