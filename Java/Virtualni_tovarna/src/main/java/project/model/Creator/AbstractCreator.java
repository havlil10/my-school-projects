package project.model.Creator;

import project.enums.Ability;
import project.model.LineMemeber.LineMember;

public abstract class AbstractCreator {

    protected abstract LineMember CreateMember(Ability ability);
}
