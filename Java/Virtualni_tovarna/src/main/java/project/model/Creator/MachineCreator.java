package project.model.Creator;

import project.enums.Ability;
import project.model.LineMemeber.LineMember;
import project.model.LineMemeber.Machine;

public class MachineCreator extends AbstractCreator{


    @Override
    public LineMember CreateMember(Ability ability) {
        return new Machine(ability);
    }
}
