package project.model.Creator;

import project.enums.Ability;
import project.model.LineMemeber.LineMember;
import project.model.LineMemeber.Machine;
import project.model.LineMemeber.Robot;

public class RobotCreator extends AbstractCreator {

    @Override
    public LineMember CreateMember(Ability ability) {
        return new Robot(ability);
    }
}
