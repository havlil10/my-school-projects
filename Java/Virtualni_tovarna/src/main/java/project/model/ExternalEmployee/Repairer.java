package project.model.ExternalEmployee;

public class Repairer {

    private final String name;
    private boolean isAvailable;

    public Repairer(String name) {
        this.isAvailable = true;
        this.name = name;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean available) {
        isAvailable = available;
    }

    public String getName() {
        return name;
    }
}
