package project.model.Event;

import project.enums.EventType;
import project.model.ExternalEmployee.Repairer;
import project.model.FactoryLine.Factory;
import project.model.LineMemeber.LineMember;

import java.util.ArrayList;
import java.util.List;

public class Event {

    private final EventType eventType;
    private boolean isHandled = false;
    private Repairer repairer;
    private LineMember registeredLineMember;
    private final int tact;

    public Event(EventType eventType, LineMember registeredLineMember) {
        this.eventType = eventType;
        this.registeredLineMember = registeredLineMember;
        this.tact = Factory.getInstance().getTact();
    }

    public LineMember getRegisteredLineMember() {
        return registeredLineMember;
    }

    public void setRegisteredLineMember(LineMember registeredLineMember) {
        this.registeredLineMember = registeredLineMember;
    }

    public EventType getEventType() {
        return eventType;
    }

    public boolean isHandled() {
        return isHandled;
    }


    public void setHandled(boolean handled) {
        isHandled = handled;
    }


    public Repairer getRepairer() {
        return repairer;
    }

    public void setRepairer(Repairer repairer) {
        this.repairer = repairer;
    }

    public int getTact() {
        return tact;
    }
}
