package project.model.Manual;

import project.enums.Ability;

import java.util.List;

public class Manual {


    private String name;

    public int getNeedToProduce() {
        return needToProduce;
    }

    private int needToProduce;

    private int linePriority;


    private List<Ability> manualOfLine;

    public Manual(String name, int needToProduce, List<Ability> manualOfLine, int linePriority){
        this.name = name;
        this.needToProduce = needToProduce;
        this.manualOfLine = manualOfLine;
        this.linePriority = linePriority;
    }

    public List<Ability> getManualOfLine() {
        return manualOfLine;
    }

    public void setManualOfLine(List<Ability> manualOfLine) {
        this.manualOfLine = manualOfLine;
    }

    public String getName() {
        return name;
    }

    public int getLinePriority() {
        return linePriority;
    }
}
