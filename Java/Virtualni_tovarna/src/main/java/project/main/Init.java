package project.main;

import project.enums.Ability;
import project.model.Creator.HumanCreator;
import project.model.Creator.MachineCreator;
import project.model.Creator.RobotCreator;
import project.model.FactoryLine.Factory;
import project.model.LineMemeber.LineMember;
import project.model.Manual.Manual;
import project.model.Pool.LineMemberPool;

import java.util.List;

public class Init {

    public static Factory factory;

    public static void createObjects(Manual manual){
        //creators
        MachineCreator machineCreator = new MachineCreator();
        HumanCreator humanCreator = new HumanCreator();
        RobotCreator robotCreator = new RobotCreator();

        for(Ability a : manual.getManualOfLine()){
            if(a == Ability.ADD_INTERIOR || a == Ability.CHECK_ELECTRONICS || a == Ability.CHECK_PAINT){
                LineMember human = humanCreator.CreateMember(a);
                LineMemberPool.returnObject(human);
            }
            if(a == Ability.CREATE_BODY || a == Ability.CREATE_CHASSIS || a == Ability.MERGE_CHASSIS_BODY){
                LineMember machine = machineCreator.CreateMember(a);
                LineMemberPool.returnObject(machine);
            }
            if(a == Ability.ADD_POWER_UNIT || a == Ability.PAINT_BODY || a == Ability.PAINT_CHASSIS){
                LineMember robot = robotCreator.CreateMember(a);
                LineMemberPool.returnObject(robot);
            }
        }
    }

    public static void createFactory(){
        factory = Factory.getInstance();
    }


}
