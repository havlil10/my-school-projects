package project.main;

import project.enums.Ability;
import project.model.Creator.HumanCreator;
import project.model.Creator.MachineCreator;
import project.model.Iterator.IteratorDirector;
import project.model.Iterator.IteratorInspector;
import project.model.LineMemeber.LineMember;
import project.model.LineMemeber.Machine;
import project.model.Manual.Manual;
import project.model.Pool.LineMemberPool;

import java.util.LinkedList;
import java.util.List;

public class Main {

    /**
     program argument @args
     for config, skoda -> config skoda, else config -> audi
     */
    public static void main(String[] args) {
        //creates workers, machines...
        //assembly line,factory
        //start

        //config 1 ---------------------------------------------------- skoda

        //manual config 1
        List<Ability> abilities = new LinkedList<>();
        abilities.add(Ability.CREATE_CHASSIS);
        abilities.add(Ability.CREATE_BODY);
        abilities.add(Ability.PAINT_CHASSIS);
        abilities.add(Ability.PAINT_BODY);
        abilities.add(Ability.ADD_POWER_UNIT);
        abilities.add(Ability.MERGE_CHASSIS_BODY);
        abilities.add(Ability.ADD_INTERIOR);
        abilities.add(Ability.CHECK_ELECTRONICS);
        abilities.add(Ability.CHECK_PAINT);
        Manual manual = new Manual("Octavia", 1500, abilities, 2);

        //manual config 2
        List<Ability> abilities2 = new LinkedList<>();
        abilities2.add(Ability.CREATE_CHASSIS);
        abilities2.add(Ability.CREATE_BODY);
        abilities2.add(Ability.PAINT_CHASSIS);
        abilities2.add(Ability.PAINT_BODY);
        abilities2.add(Ability.ADD_POWER_UNIT);
        abilities2.add(Ability.MERGE_CHASSIS_BODY);
        abilities2.add(Ability.ADD_INTERIOR);
        abilities2.add(Ability.CHECK_ELECTRONICS);
        abilities2.add(Ability.CHECK_PAINT);
        Manual manual2 = new Manual("Fabia", 1000, abilities2, 3);

        //manual config 3
        List<Ability> abilities3 = new LinkedList<>();
        abilities3.add(Ability.CREATE_CHASSIS);
        abilities3.add(Ability.CREATE_BODY);
        abilities3.add(Ability.PAINT_CHASSIS);
        abilities3.add(Ability.PAINT_BODY);
        abilities3.add(Ability.ADD_POWER_UNIT);
        abilities3.add(Ability.MERGE_CHASSIS_BODY);
        abilities3.add(Ability.ADD_INTERIOR);
        abilities3.add(Ability.CHECK_ELECTRONICS);
        abilities3.add(Ability.CHECK_PAINT);
        Manual manual3 = new Manual("Kodiaq", 750, abilities3, 1);

        List<Manual> manuals = new LinkedList<>();
        manuals.add(manual);
        manuals.add(manual2);
        manuals.add(manual3);


        //config 2 ---------------------------------------------------------------- audi

        //manual config 1
        List<Ability> abilities4 = new LinkedList<>();
        abilities4.add(Ability.CREATE_CHASSIS);
        abilities4.add(Ability.CREATE_BODY);
        abilities4.add(Ability.PAINT_CHASSIS);
        abilities4.add(Ability.PAINT_BODY);
        abilities4.add(Ability.ADD_POWER_UNIT);
        abilities4.add(Ability.MERGE_CHASSIS_BODY);
        abilities4.add(Ability.ADD_INTERIOR);
        abilities4.add(Ability.CHECK_ELECTRONICS);
        abilities4.add(Ability.CHECK_PAINT);
        Manual manual4 = new Manual("A6", 1200, abilities4, 2);

        //manual config 2
        List<Ability> abilities5 = new LinkedList<>();
        abilities5.add(Ability.CREATE_CHASSIS);
        abilities5.add(Ability.CREATE_BODY);
        abilities5.add(Ability.PAINT_CHASSIS);
        abilities5.add(Ability.PAINT_BODY);
        abilities5.add(Ability.ADD_POWER_UNIT);
        abilities5.add(Ability.MERGE_CHASSIS_BODY);
        abilities5.add(Ability.ADD_INTERIOR);
        abilities5.add(Ability.CHECK_ELECTRONICS);
        abilities5.add(Ability.CHECK_PAINT);
        Manual manual5 = new Manual("A4", 1000, abilities5, 3);

        //manual config 3
        List<Ability> abilities6 = new LinkedList<>();
        abilities6.add(Ability.CREATE_CHASSIS);
        abilities6.add(Ability.CREATE_BODY);
        abilities6.add(Ability.PAINT_CHASSIS);
        abilities6.add(Ability.PAINT_BODY);
        abilities6.add(Ability.ADD_POWER_UNIT);
        abilities6.add(Ability.MERGE_CHASSIS_BODY);
        abilities6.add(Ability.ADD_INTERIOR);
        abilities6.add(Ability.CHECK_ELECTRONICS);
        abilities6.add(Ability.CHECK_PAINT);
        Manual manual6 = new Manual("Q7", 750, abilities6, 1);

        List<Manual> manuals2 = new LinkedList<>();
        manuals2.add(manual4);
        manuals2.add(manual5);
        manuals2.add(manual6);

        if(args.length > 0){
            if(args[0].equals("skoda")){
                //run
                Init.createObjects(manual);
                Init.createObjects(manual2);
                Init.createObjects(manual3);
                //Init.createObjects(potential user manual)
                Init.createFactory();
                Init.factory.createLines(manuals);
                Init.factory.startProduction();
            }
        }else {
            //run
            Init.createObjects(manual4);
            Init.createObjects(manual5);
            Init.createObjects(manual6);
            //Init.createObjects(potential user manual)
            Init.createFactory();
            Init.factory.createLines(manuals2);
            Init.factory.startProduction();
        }


        IteratorDirector iteratorDirector = Init.factory.getDirectorIterator();
        while (iteratorDirector.hasNext()) {
            iteratorDirector.next();

        }
        IteratorInspector iteratorInspector = Init.factory.getInspectorIterator();
        while (iteratorInspector.hasNext()){
            iteratorInspector.next();
        }


    }
}
