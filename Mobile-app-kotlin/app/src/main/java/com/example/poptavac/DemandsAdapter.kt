package com.example.poptavac

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.ArrayAdapter
import android.widget.ImageView
import com.example.poptavac.model.DemandModel
import java.text.SimpleDateFormat
import java.util.*

class DemandsAdapter(private val context : Activity,private val arrayList: ArrayList<DemandModel>) : ArrayAdapter<DemandModel>(context,
                    R.layout.list_item,arrayList) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val inflater: LayoutInflater = LayoutInflater.from(context)
        val view : View = inflater.inflate(R.layout.list_item, null)

        val demandName : TextView = view.findViewById(R.id.personName)
        val description : TextView = view.findViewById(R.id.description)
        //val demandDate : TextView = view.findViewById(R.id.demandDate)

        demandName.text = arrayList[position].name
        if(arrayList[position].description.length > 25) {
            description.text = "" + arrayList[position].description.substring(0, 22) + "..."
        } else {
            description.text = arrayList[position].description
        }
        if(arrayList[position].latitude!=null && arrayList[position].longtitude!=null) {
            view.findViewById<ImageView>(R.id.profile_image).setImageResource(R.drawable.ic_baseline_my_location_24)
        }


        //val formatter =SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
        //demandDate.text = formatter.format(arrayList[position])
        //demandDate.text = arrayList[position].name

        return view
    }

}