package com.example.poptavac

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.example.poptavac.ui.UserViewModel
import com.google.android.material.textfield.TextInputLayout

class LoginActivity : AppCompatActivity() {

    //private lateinit var mUserViewModel: UserViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        supportActionBar!!.title = "Login"
        val button = findViewById<Button>(R.id.loginButton)
        button.setOnClickListener {
            //login validation
            val username = findViewById<TextInputLayout>(R.id.loginInputLayout).editText?.text.toString()
            val password = findViewById<TextInputLayout>(R.id.passwordInputLayout).editText?.text.toString()

            if(validateUser(username, password)) {

            } else {
                Toast.makeText(this, "Špatné jméno nebo heslo", Toast.LENGTH_SHORT).show()
            }

        //val intent = Intent(this, MainActivity::class.java)
            //startActivity(intent)
        }

        val register = findViewById<TextView>(R.id.loginRegister)
        register.setOnClickListener {

            val intent = Intent(this, CreateUser::class.java)
            startActivity(intent)
        }

    }

    fun validateUser(username: String, password: String): Boolean{
        val mUserViewModel = ViewModelProvider(this).get(UserViewModel::class.java)
        var isUserFound = false
        mUserViewModel.readAllData.observe(this, androidx.lifecycle.Observer { myUsers ->
            for (myUser in myUsers) {
                if(myUser.userName.equals(username) && myUser.password.equals(password)) {
                    isUserFound = true
                    UserSingleton.currentUser = myUser
                    val intent = Intent(this, MainActivity::class.java)
                    startActivity(intent)
                    Toast.makeText(this, "Úspěšně jste byl přihlášen", Toast.LENGTH_SHORT).show()
                }
            }

        })
        return isUserFound
    }
}