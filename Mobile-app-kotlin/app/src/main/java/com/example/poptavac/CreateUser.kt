package com.example.poptavac

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.widget.Button
import android.widget.Toast
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.example.poptavac.model.User
import com.example.poptavac.ui.UserViewModel
import com.google.android.gms.location.LocationServices
import com.google.android.material.textfield.TextInputLayout

class CreateUser : AppCompatActivity() {

    private lateinit var mUserViewModel: UserViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_user)

        supportActionBar!!.title = "Zaregistrovat se"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        mUserViewModel = ViewModelProvider(this).get(UserViewModel::class.java)


        val createDemand = findViewById<Button>(R.id.button)
        createDemand.setOnClickListener {
            insertDataToDatabase()
        }
    }
    private fun insertDataToDatabase() {
        val username =
            findViewById<TextInputLayout>(R.id.userName).editText?.text.toString()
        val firstName =
            findViewById<TextInputLayout>(R.id.firstName).editText?.text.toString()
        val lastName =
            findViewById<TextInputLayout>(R.id.lastName).editText?.text.toString()
        val phone = findViewById<TextInputLayout>(R.id.phone).editText?.text.toString()
        val email = findViewById<TextInputLayout>(R.id.email).editText?.text.toString()
        val password = findViewById<TextInputLayout>(R.id.password).editText?.text.toString()

        if( inputCheck(username,firstName, lastName, phone, email, password)){
            if(email=="" || username == "" || phone=="" || password == "") {
                Toast.makeText(this,"Zadejte prosím všechny povinné údaje", Toast.LENGTH_LONG).show()
            } else {
                //Create user object
                val user = User(0,username,firstName,lastName,phone,email,password)
                //Add Data To Database
                mUserViewModel.addUser(user)


//            mUserViewModel = ViewModelProvider(this).get(UserViewModel::class.java)
//
//            mUserViewModel.readAllData.observe(this, Observer { mUser ->
//                for (xUser in mUser) {
//                    println(xUser.userName)
//                }
//            })

                val intent = Intent(this, LoginActivity::class.java)
                startActivity(intent)
                Toast.makeText(this,"Úspěšně jste se zaregistroval", Toast.LENGTH_LONG).show()

            }


        }
        else{
            Toast.makeText(this,"Vyplňte prosím všechna pole", Toast.LENGTH_LONG).show()
        }
    }

    private fun inputCheck(userName: String, firstName: String, lastName: String, phone: String, email: String, password:String) : Boolean{
        return !(TextUtils.isEmpty(userName) && TextUtils.isEmpty(firstName) &&  TextUtils.isEmpty(lastName) && TextUtils.isEmpty(phone) && TextUtils.isEmpty(email) && TextUtils.isEmpty(password) )
    }

}