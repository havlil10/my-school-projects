package com.example.poptavac

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import com.example.poptavac.model.DemandModel
import com.example.poptavac.model.User
import com.example.poptavac.ui.DemandViewModel
import com.google.android.gms.location.*



import com.google.android.material.textfield.TextInputLayout
import java.util.ArrayList


class CreateDemand : AppCompatActivity(){

    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    var latitude = 0.0
    var longtitude = 0.0
    lateinit var saveLocationButton : Button
    private lateinit var mDemandViewModel: DemandViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_demand)
        supportActionBar!!.title = "Vytvořit poptávku"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)

        saveLocationButton = findViewById(R.id.saveLocation)

        saveLocationButton.setOnClickListener {
            getCurrentLocation()
        }

        val user: User? = UserSingleton.currentUser
        if (user != null){
            findViewById<TextInputLayout>(R.id.email).editText?.setText(user.email)
            findViewById<TextInputLayout>(R.id.phone).editText?.setText(user.phoneNumber)
        }



        val createDemand = findViewById<Button>(R.id.button)
        createDemand.setOnClickListener {
            val demandName =
                findViewById<TextInputLayout>(R.id.demandName).editText?.text.toString()
            val demandDescription =
                findViewById<TextInputLayout>(R.id.demandDescription).editText?.text.toString()
            val address =
                findViewById<TextInputLayout>(R.id.demandAddress).editText?.text.toString()
            val phone = findViewById<TextInputLayout>(R.id.phone).editText?.text.toString()
            val email = findViewById<TextInputLayout>(R.id.email).editText?.text.toString()
            var demandLatitude: Float?
            var demandLongtitude: Float?
            if(this.latitude==0.0 || this.longtitude==0.0) {
                demandLatitude = null
                demandLongtitude = null
            } else {
                demandLatitude = this.latitude.toFloat()
                demandLongtitude = this.longtitude.toFloat()
            }


            if(demandName.isEmpty() || address.isEmpty()) {
                Toast.makeText(this, "Vyplňte prosím všechny povinné pole", Toast.LENGTH_SHORT).show()
            } else if(phone.isEmpty() && email.isEmpty()) {
                Toast.makeText(this, "Zadejte prosím alespoň jeden způsob kontaktu", Toast.LENGTH_SHORT).show()
            }
            else {
                Toast.makeText(this, "Poptávku jste úspěšně vytvořili", Toast.LENGTH_SHORT).show()
                addDemand(DemandModel(0, address, demandName, phone, demandDescription,
                email, demandLatitude, demandLongtitude))
                val i = Intent(this, MainActivity::class.java)
                startActivity(i)
            }

        }


    }
    private fun getCurrentLocation() {
        if(checkPermissions()) {
            if(isLocationEnabled()) {

                if (ActivityCompat.checkSelfPermission(
                        this,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                        this,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return
                }
                fusedLocationProviderClient.lastLocation.addOnCompleteListener(this){ task->
                    val location: Location?=task.result
                    if(location==null) {
                        Toast.makeText(this, "Polohu se nedaří načíst, zapněte GPS na svém zařízení", Toast.LENGTH_SHORT).show()
                    } else {
                        latitude = location.latitude
                        longtitude = location.longitude
                        Toast.makeText(this, "Poloha byla uložena", Toast.LENGTH_SHORT).show()
                        saveLocationButton.text = "Poloha uložena"
                    }
                }

            } else {
                Toast.makeText(this, "Turn on location", Toast.LENGTH_SHORT).show()
                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(intent)
            }
        } else {
            requestPermission()
        }
    }

    private fun isLocationEnabled(): Boolean {
        val locationManager:LocationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)||locationManager.isProviderEnabled(
            LocationManager.NETWORK_PROVIDER
        )
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(
            this, arrayOf(android.Manifest.permission.ACCESS_COARSE_LOCATION,
                android.Manifest.permission.ACCESS_FINE_LOCATION),
            PERMISSION_REQUEST_ACCESS_LOCATION
        )
    }

    companion object {
        private const val PERMISSION_REQUEST_ACCESS_LOCATION=100
    }

    private fun checkPermissions(): Boolean {
        if(ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
            == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)==PackageManager.PERMISSION_GRANTED
        ) {
            return true
        }
        return false
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if(requestCode== PERMISSION_REQUEST_ACCESS_LOCATION) {
            if(grantResults.isNotEmpty() && grantResults[0]==PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(applicationContext, "Granted", Toast.LENGTH_SHORT).show()
                getCurrentLocation()
            } else {
                Toast.makeText(applicationContext, "Denied", Toast.LENGTH_SHORT).show()
            }
        }
    }
    fun addDemand(demandModel: DemandModel) {
        mDemandViewModel = ViewModelProvider(this).get(DemandViewModel::class.java)
        mDemandViewModel.addDemand(demandModel)
    }
}







