package com.example.poptavac.db.userdb

import androidx.lifecycle.LiveData
import com.example.poptavac.model.DemandModel
import com.example.poptavac.model.User

class UserRepository(private val userDao: UserDao) {
    val readAllData: LiveData<List<User>> = userDao.readAllData()

    suspend fun addUser(user: User){
        userDao.addUser(user)
    }

    suspend fun updateUser(user: User){
        userDao.updateUser(user)
    }
    suspend fun deleteUser(user: User){
        userDao.deleteUser(user)
    }

    suspend fun deleteAllUsers(){
        userDao.deleteAllUsers()
    }

    fun getUserById(id: Long) : LiveData<User>{
        return userDao.getUserById(id);
    }

}