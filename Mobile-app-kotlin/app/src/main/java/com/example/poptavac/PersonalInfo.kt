package com.example.poptavac

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import com.example.poptavac.model.User

class PersonalInfo : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_personal_info)
        supportActionBar!!.title = "Osobní údaje"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        val usernameField = findViewById<TextView>(R.id.personalUsername)
        val phoneField = findViewById<TextView>(R.id.personalPhone)
        val emailField = findViewById<TextView>(R.id.personalEmail)

        var currentUser: User? = UserSingleton.currentUser
        if(currentUser!=null) {
            usernameField.text = currentUser.userName
            if(currentUser.phoneNumber == "") {
                phoneField.text = "Telefon nebyl zadán"
            } else {
                phoneField.text = currentUser.phoneNumber
            }
            if(currentUser.email == "") {
                emailField.text = "Email nebyl zadán"
            } else {
                emailField.text = currentUser.email
            }

        }
    }
}