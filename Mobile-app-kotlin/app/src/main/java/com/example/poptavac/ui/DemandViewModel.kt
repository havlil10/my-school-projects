package com.example.poptavac.ui

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.example.poptavac.db.demanddb.DemandDatabase
import com.example.poptavac.db.demanddb.DemandRepository
import com.example.poptavac.model.DemandModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class DemandViewModel(application: Application): AndroidViewModel(application) {

    val readAllData: LiveData<List<DemandModel>>
    private val repository: DemandRepository
    init {
        val demandDao = DemandDatabase.getDatabase(application).demandDao()
        repository = DemandRepository(demandDao)
        readAllData = repository.readAllData
    }
    fun addDemand(demand: DemandModel){
        viewModelScope.launch(Dispatchers.IO) {
            repository.addDemand(demand)
        }
    }

    fun updateDemand(demand: DemandModel){
        viewModelScope.launch(Dispatchers.IO) {
            repository.updateDemand(demand)
        }
    }

    fun deleteDemand(demand: DemandModel){
        viewModelScope.launch(Dispatchers.IO) {
            repository.deleteDemand(demand)
        }
    }

    fun deleteAllDemands(){
        viewModelScope.launch(Dispatchers.IO) {
            repository.deleteAllDemands()
        }
    }

    suspend fun getDemandById(id: Long): LiveData<DemandModel> {
        return repository.getDemandById(id)
    }
}