package com.example.poptavac

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.provider.Settings
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.ViewModelProvider
import com.example.poptavac.databinding.ActivityMainBinding
import com.example.poptavac.model.DemandModel
import com.example.poptavac.ui.DemandViewModel
import com.example.poptavac.ui.UserViewModel
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.material.appbar.MaterialToolbar
import com.google.android.material.navigation.NavigationView
import java.util.*
import kotlin.collections.ArrayList

class MainActivity : AppCompatActivity() {

    private lateinit var binding : ActivityMainBinding
    private lateinit var demandsArrayList : ArrayList<DemandModel>
    private lateinit var mDemandViewModel: DemandViewModel
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient


    override fun onCreate(savedInstanceState: Bundle?) {
        //getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            //WindowManager.LayoutParams.FLAG_FULLSCREEN);

        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //supportActionBar?.hide()


        val topAppBar = findViewById<MaterialToolbar>(R.id.topAppBar)
        val drawerLayout = findViewById<DrawerLayout>(R.id.drawerLayout)
        val navigationView = findViewById<NavigationView>(R.id.navigationView)

        topAppBar.setNavigationOnClickListener {
            drawerLayout.open()
        }

        navigationView.setNavigationItemSelectedListener { menuItem ->
            // Handle menu item selected
            //menuItem.isChecked = true
            drawerLayout.close()


            if (menuItem.itemId == R.id.item1) {
                val intent = Intent(this, CreateDemand::class.java)
                startActivity(intent)
            } else if (menuItem.itemId == R.id.item2) {
                val intent = Intent(this, PersonalInfo::class.java)
                startActivity(intent)
            } else if (menuItem.itemId == R.id.item3) {
                val intent = Intent(this, AboutApp::class.java)
                startActivity(intent)
            } /*else if (menuItem.itemId == R.id.item4) {
                val intent = Intent(this, Map::class.java)
                startActivity(intent)
            }*/ else if (menuItem.itemId == R.id.item5) {
                val intent = Intent(this, LoginActivity::class.java)
                startActivity(intent)
            }
            true

        }

        getAllDemands()

        val locationFilter = findViewById<View>(R.id.locationFilter)
        locationFilter.setOnClickListener {
            filterByLocation()
        }

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
    }

    fun getAllDemands(){
        val result: ArrayList<DemandModel> = arrayListOf()
        mDemandViewModel = ViewModelProvider(this).get(DemandViewModel::class.java)

        mDemandViewModel.readAllData.observe(this, androidx.lifecycle.Observer { myDemand ->
            for (demand in myDemand) {
                result.add(demand);
            }
            binding.listView.isClickable = true
            binding.listView.adapter = DemandsAdapter(this, result)
            binding.listView.setOnItemClickListener { parent, view, position, id ->

                val demandName = result[position].name
                val description = result[position].description
                val address = result[position].address
                val phoneNumber = result[position].phoneNumber
                val email = result[position].email
                val demandId = result[position].id

                val i = Intent(this, DemandDetails::class.java)
                i.putExtra("id", demandId)
                i.putExtra("demandName", demandName)
                i.putExtra("description", description)
                i.putExtra("address", address)
                i.putExtra("phoneNumber", phoneNumber)
                i.putExtra("email", email)
                startActivity(i)

            }
        })
    }

    fun getAllFilteredDemands(currentLatitude: Double, currentLongtitude: Double) {
        var result: ArrayList<DemandModel> = arrayListOf()
        mDemandViewModel = ViewModelProvider(this).get(DemandViewModel::class.java)

        mDemandViewModel.readAllData.observe(this, androidx.lifecycle.Observer { myDemand ->
            for (demand in myDemand) {
                result.add(demand);
            }
            result = filterArrayByLocation(result, currentLatitude, currentLongtitude)
            binding.listView.isClickable = true
            binding.listView.adapter = DemandsAdapter(this, result)
            binding.listView.setOnItemClickListener { parent, view, position, id ->

                val demandName = result[position].name
                val description = result[position].description
                val address = result[position].address
                val phoneNumber = result[position].phoneNumber
                val email = result[position].email
                val demandId = result[position].id

                val i = Intent(this, DemandDetails::class.java)
                i.putExtra("id", demandId)
                i.putExtra("demandName", demandName)
                i.putExtra("description", description)
                i.putExtra("address", address)
                i.putExtra("phoneNumber", phoneNumber)
                i.putExtra("email", email)
                startActivity(i)

            }
        })
    }

    fun filterArrayByLocation(inputList: ArrayList<DemandModel>, currentLatitude: Double, currentLongtitude: Double): ArrayList<DemandModel> {
        var result: ArrayList<DemandModel> = ArrayList()
        var demandsWithoutLocation: ArrayList<DemandModel> = ArrayList()
        var demandsWithLocation: ArrayList<DemandModel> = ArrayList()
        var distances: ArrayList<Float> = ArrayList()
        var distanceHolder: FloatArray = FloatArray(1)

        for (demand in inputList) {
            if(demand.latitude==null || demand.longtitude==null) {
                demandsWithoutLocation.add(demand)
            } else {
                demandsWithLocation.add(demand)
                android.location.Location.distanceBetween(demand.latitude.toDouble(), demand.longtitude.toDouble(), currentLatitude, currentLongtitude, distanceHolder)
                distances.add(distanceHolder[0])
            }
        }

        for (i in distances.indices) {
            for(j in 0 until distances.size-i-1) {
                if(distances[j] > distances[j+1]) {
                    var temp = distances[j]
                    distances[j] = distances[j+1]
                    distances[j+1] = temp

                    var demandsTemp = demandsWithLocation[j]
                    demandsWithLocation[j] = demandsWithLocation[j+1]
                    demandsWithLocation[j+1] = demandsTemp
                }
            }
        }
        result = demandsWithLocation
        result.addAll(demandsWithoutLocation)
        return result


    }

    private fun filterByLocation() {
        if(checkPermissions()) {
            if(isLocationEnabled()) {

                if (ActivityCompat.checkSelfPermission(
                        this,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                        this,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return
                }
                fusedLocationProviderClient.lastLocation.addOnCompleteListener(this){ task->
                    val location: Location?=task.result
                    if(location==null) {
                        Toast.makeText(this, "Polohu se nedaří načíst, zapněte GPS na svém zařízení", Toast.LENGTH_SHORT).show()
                    } else {
                        //action after success
                        getAllFilteredDemands(location.latitude, location.longitude)
                        Toast.makeText(this,"Poptávky byly seřazeny podle vzdálenosti",Toast.LENGTH_SHORT).show()

                    }
                }

            } else {
                Toast.makeText(this, "Turn on location", Toast.LENGTH_SHORT).show()
                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(intent)
            }
        } else {
            requestPermission()
        }
    }

    private fun isLocationEnabled(): Boolean {
        val locationManager: LocationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)||locationManager.isProviderEnabled(
            LocationManager.NETWORK_PROVIDER
        )
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(
            this, arrayOf(android.Manifest.permission.ACCESS_COARSE_LOCATION,
                android.Manifest.permission.ACCESS_FINE_LOCATION),
            PERMISSION_REQUEST_ACCESS_LOCATION
        )
    }

    companion object {
        private const val PERMISSION_REQUEST_ACCESS_LOCATION=100
    }

    private fun checkPermissions(): Boolean {
        if(ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
            == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)== PackageManager.PERMISSION_GRANTED
        ) {
            return true
        }
        return false
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if(requestCode== PERMISSION_REQUEST_ACCESS_LOCATION) {
            if(grantResults.isNotEmpty() && grantResults[0]== PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(applicationContext, "Granted", Toast.LENGTH_SHORT).show()
                filterByLocation()
            } else {
                Toast.makeText(applicationContext, "Denied", Toast.LENGTH_SHORT).show()
            }
        }
    }

    fun getAllUsers(){
        val mUserViewModel = ViewModelProvider(this).get(UserViewModel::class.java)
        mUserViewModel.readAllData.observe(this, androidx.lifecycle.Observer { myUsers ->
            for (myUser in myUsers) {
                //here is list of all users
            }
        })
    }


}