package com.example.poptavac.db.userdb

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.poptavac.model.DemandModel
import com.example.poptavac.model.User

@Dao
interface UserDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addUser(user: User)

    @Query("SELECT * FROM user_table ORDER BY id ASC")
    fun readAllData(): LiveData<List<User>>

    @Update
    suspend fun updateUser(user: User)

    @Delete
    suspend fun deleteUser(user: User)

    @Query("Delete FROM user_table")
    suspend fun deleteAllUsers()

    @Query("SELECT * FROM user_table  WHERE id = :id")
    fun getUserById(id: Long): LiveData<User>
}