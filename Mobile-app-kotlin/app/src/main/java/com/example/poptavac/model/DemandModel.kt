package com.example.poptavac.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "demand_table")
data class DemandModel (
    @PrimaryKey(autoGenerate = true)
    val id: Int,
//    val user: User,
    val address: String,
    val name: String,
    val phoneNumber: String,
    val description: String,
    val email: String,
    val latitude: Float?,
    val longtitude: Float?

)