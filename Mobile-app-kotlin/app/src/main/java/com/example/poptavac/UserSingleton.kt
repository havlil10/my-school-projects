package com.example.poptavac

import com.example.poptavac.model.User

object UserSingleton {
    var currentUser: User? = null
    fun setUser(user: User) {
        currentUser = user
    }


}