package com.example.poptavac

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class AboutApp : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about_app)
        supportActionBar!!.title = "O aplikaci"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        val myTextView = findViewById<TextView>(R.id.testingTextView)
        myTextView.setOnClickListener {
            myTextView.text = "Something changed"
        }
    }
}