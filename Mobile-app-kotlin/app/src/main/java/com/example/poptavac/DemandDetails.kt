package com.example.poptavac

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.poptavac.databinding.ActivityDemandDetailsBinding
import com.example.poptavac.model.DemandModel
import com.example.poptavac.ui.DemandViewModel

class DemandDetails : AppCompatActivity() {

    private lateinit var binding : ActivityDemandDetailsBinding
    private lateinit var mDemandViewModel: DemandViewModel

    override fun onCreate(savedInstanceState: Bundle?) {

        mDemandViewModel = ViewModelProvider(this).get(DemandViewModel::class.java)

        super.onCreate(savedInstanceState)
        binding = ActivityDemandDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        supportActionBar!!.title = "Detail poptávky"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        //val id = intent.getIntExtra("id")
        val name = intent.getStringExtra("demandName")
        val description = intent.getStringExtra("description")
        val email = intent.getStringExtra("email")
        val phoneNumber = intent.getStringExtra("phoneNumber")
        val address = intent.getStringExtra("address")
        val latitude = intent.getStringExtra("latitude")
        val longtitude = intent.getStringExtra("longtitude")

        binding.nameTextField.text = name
        binding.descriptionTextField.text = description
        binding.address.text = address
        if(phoneNumber.equals("")) {
            binding.phoneTextField.text = "Zadavatel nevyplnil"
        } else {
            binding.phoneTextField.text = phoneNumber
        }
        if(email.equals("")) {
            binding.emailTextField.text = "Zadavatel nevyplnil"
        } else {
            binding.emailTextField.text = email
        }



        val button = findViewById<Button>(R.id.deleteButton)
        button.setOnClickListener {
            val demandId = intent.getIntExtra("id", 0)
            removeDemandById(demandId)
        }
    }
    private fun removeDemandById(id: Int) =
        mDemandViewModel.readAllData.observe(this, Observer { listOfDemands ->
            for (demand in listOfDemands) {
                if (id.compareTo(demand.id) == 0){
                    deleteDemand(demand)
                    break
                }
            }
        })
    private fun deleteDemand(demandModel: DemandModel){
        val builder = AlertDialog.Builder(this)
        builder.setPositiveButton("Yes") {_,_ ->
            mDemandViewModel.deleteDemand(demandModel)
            Toast.makeText(this,"Úspěšně jste smazali poptávku: ${demandModel.name}",Toast.LENGTH_SHORT).show()
            val i = Intent(this, MainActivity::class.java)
            startActivity(i)
        }
        builder.setNegativeButton("No"){_,_ ->

        }
        builder.setTitle("Delete ${demandModel.name}?")
        builder.setMessage("Jste si jisti že chcete smazat poptávku: ${demandModel.name}?")
        builder.create().show()
    }


}