package com.example.poptavac.db.demanddb

import androidx.lifecycle.LiveData
import com.example.poptavac.model.DemandModel

class DemandRepository(private val demandDao: DemandDao) {
    val readAllData: LiveData<List<DemandModel>> = demandDao.readAllData()

    suspend fun addDemand(demand: DemandModel){
        demandDao.addDemand(demand)
    }

    suspend fun updateDemand(demand: DemandModel){
        demandDao.updateDemand(demand)
    }

    suspend fun deleteDemand(demandModel: DemandModel){
        demandDao.deleteDemand(demandModel)
    }

    suspend fun deleteAllDemands(){
        demandDao.deleteAllDemands()
    }

    suspend fun getDemandById(id: Long) : LiveData<DemandModel>{
        return demandDao.getDemandById(id)
    }
}