package com.example.poptavac.db.demanddb

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.poptavac.model.DemandModel

@Database(entities = [DemandModel::class], version = 1, exportSchema = false)
abstract class DemandDatabase : RoomDatabase(){
    abstract fun demandDao(): DemandDao

    companion object {
        @Volatile
        private var INSTANCE: DemandDatabase? = null

        fun getDatabase(context: Context): DemandDatabase {
            val  tempInstance = INSTANCE
            if (tempInstance != null){
                return tempInstance
            }
            synchronized(this){
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    DemandDatabase::class.java,
                    "demand_database"
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }
}