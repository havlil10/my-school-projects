package com.example.poptavac.db.demanddb

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.poptavac.model.DemandModel

@Dao
interface DemandDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addDemand(demand: DemandModel)
    
    @Update
    suspend fun updateDemand(demand: DemandModel)

    @Delete
    suspend fun deleteDemand(demandModel: DemandModel)

    @Query("DELETE FROM demand_table")
    suspend fun deleteAllDemands()

    @Query("SELECT * FROM demand_table ORDER BY id ASC")
    fun readAllData(): LiveData<List<DemandModel>>

    @Query("SELECT * FROM demand_table  WHERE id = :id")
    fun getDemandById(id: Long): LiveData<DemandModel>
}