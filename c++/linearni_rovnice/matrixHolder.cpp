#include "matrixHolder.hpp"
#include "iostream"
#include <utility>
#include <algorithm>
#include <sstream>
#include <cstdlib>
#include <iomanip>

matrixHolder::matrixHolder(Matrix matrix) {
    this->matrix = std::move(matrix);
}

Matrix matrixHolder::getMatrix() {
    return this->matrix;
}

size_t countPivotPositionOfRow(const vector<double>& row) {
    for(size_t i = 0; i<row.size(); i++) {
        if(row[i]!=0) {
            return i;
        }
    }
    return row.size()+1;
}

size_t getRowIndexWithLowestPivot(vector<size_t>& pivotPositions, size_t rowSize) {
    size_t pivotPosition = pivotPositions[0];
    size_t pivotIndex = 0;
    for(size_t i=1; i<pivotPositions.size(); i++) {
        if(pivotPositions[i]<pivotPosition) {
            pivotPosition = pivotPositions[i];
            pivotIndex = i;
        }
    }
    pivotPositions[pivotIndex] = rowSize+2;
    return pivotIndex;
}

Matrix sortMatrix(const Matrix& matrix) {
    Matrix resultMatrix;
    vector<size_t> pivotPositions;
    for(auto& row : matrix) {
        pivotPositions.push_back(countPivotPositionOfRow(row));
    }

    for(size_t i = 0; i<matrix.size(); i++) {
        resultMatrix.push_back(matrix[getRowIndexWithLowestPivot(pivotPositions, matrix[0].size())]);
    }

    return resultMatrix;
}

void matrixHolder::applyGEM() {
    if(matrix.size()<=1||matrix[0].size()<2) {return;}
    int diagonalIndex = 0;
    int maxDiagonalIndex = min(matrix.size(), matrix[0].size());

    while(diagonalIndex<maxDiagonalIndex) {
        matrix = sortMatrix(matrix);
        for(int i = diagonalIndex+1; i<matrix.size(); i++) {
            if(matrix[i][diagonalIndex]==0) {continue;}
            double coeficient = matrix[diagonalIndex][diagonalIndex]/matrix[i][diagonalIndex];
            for(int j = diagonalIndex; j<matrix[0].size(); j++) {
                matrix[i][j] = coeficient*matrix[i][j]-matrix[diagonalIndex][j];
            }
        }

        diagonalIndex++;
    }

}


void addPartialResult(const vector<double>& matrixRow, int pivotIndex, vector<double>& resultVector) {
    if(matrixRow[pivotIndex]==0) {
        resultVector.push_back(0);
        return;
    }
    double result = 0;
    int index = matrixRow.size()-2;
    size_t resultVectorIndex = 0;
    while(index>pivotIndex) {
        result-=matrixRow[index]*resultVector[resultVectorIndex];
        resultVectorIndex++;
        index--;
    }
    result+=matrixRow[matrixRow.size()-1];
    result = result/matrixRow[pivotIndex];
    resultVector.push_back(result);
}

vector<double> matrixHolder::countSolution() {
    vector<double> result;
    int XIndex = matrix[0].size()-2;
    int YIndex = matrix.size()-1;

    while(XIndex>=0) {
        int pivotPositionOfRow = countPivotPositionOfRow(matrix[YIndex]);
        if(pivotPositionOfRow!=XIndex && pivotPositionOfRow!=matrix[YIndex].size()+1) {
            result.push_back(0);
        } else {
            addPartialResult(matrix[YIndex], XIndex, result);
            YIndex--;
        }
        XIndex--;
    }
    return result;
}


double countPartialResult(vector<double>& matrixRow, size_t pivotIndex, vector<double>& resultVector) {
    if(matrixRow[pivotIndex]==0) {
        return 0;
    }
    double result = 0;
    int index = matrixRow.size()-2;
    size_t resultVectorIndex = resultVector.size()-1;
    while(index>pivotIndex) {
        result-=matrixRow[index]*resultVector[resultVectorIndex];
        resultVectorIndex--;
        index--;
    }
    result = result/matrixRow[pivotIndex];
    return result;
}

vector<size_t> getVectorOfPivotPositions(const Matrix& matrix) {
    vector<size_t> result;
    for(auto& row : matrix) {
        for(size_t i = 0; i<row.size(); i++) {
            if(row[i]!=0) {
                result.push_back(i);
                break;
            }
        }
    }
    return result;
}

vector<vector<double> > matrixHolder::getSpanVectors() {
    vector<vector<double>> result;
    vector<size_t> pivotPositions = getVectorOfPivotPositions(matrix);
    vector<size_t> indexesFullOfNulls;
    //count and add to array all indexes without pivot
    for(size_t i = 0; i<matrix[0].size()-1; i++) {
        if(find(pivotPositions.begin(), pivotPositions.end(), i) == pivotPositions.end()) {
            indexesFullOfNulls.push_back(i);
        }
    }
    //assign "1" and "0" to vectors, so that the vectors are linearly independent
    for(size_t i = 0; i<indexesFullOfNulls.size(); i++) {
        vector<double> spanVector(matrix[0].size()-1);
        spanVector[indexesFullOfNulls[i]] = 1;
        result.push_back(spanVector);
    }


    for(auto& spanVector : result) {
        vector<size_t> pivotPositionsCopy = pivotPositions;
        while(!pivotPositionsCopy.empty()) {
            spanVector[pivotPositionsCopy.size()-1] = countPartialResult(matrix[pivotPositionsCopy.size()-1], pivotPositionsCopy[pivotPositionsCopy.size()-1], spanVector);
            pivotPositionsCopy.pop_back();
        }

    }
    return result;
}

std::vector<std::string> split(std::string input, char delimiter) {
    std::string line;
    std::vector<std::string> result;
    std::stringstream ss(input);
    while(std::getline(ss, line, delimiter)) {
        result.push_back(line);
    }
    return result;
}

bool isNumber(const string& input)
{
    long double ld;
    return((std::istringstream(input) >> ld >> std::ws).eof());
}

bool isVectorFullOfNumbers(vector<string>& vec) {
    if(vec.empty()) return false;
    for(auto& str : vec) {
        if(!isNumber(str)) {
            return false;
        }
    }

    return true;
}

void addRowToMatrix(Matrix& matrix, vector<string>& row) {
    vector<double> newRow;
    newRow.reserve(row.size());
    for(string& s : row) {
        newRow.push_back(stod(s));
    }
    matrix.push_back(newRow);
}

void printMatrix(Matrix& inputMatrix) {
    for(int i = 0; i < inputMatrix.size(); i++) {
        for(int j = 0; j < inputMatrix[0].size(); j++) {
            cout << inputMatrix[i][j]<<", ";
        }
        cout << endl;
    }
}

int countNeededWidth(const vector<double>& vec) {
    int longestLength = to_string((int)vec[0]).size();
    if((int)vec[0]==0 && vec[0]<0) longestLength++;
    for(auto& number : vec) {
        if(to_string((int)number).size()>longestLength) {
            longestLength = to_string((int)number).size();
        }
    }
    return longestLength+3;
}

void matrixHolder::printSolutionVector(const vector<double>& reversedSolution) {
    cout << "Solution vector is: " << endl;
    vector<double> solution = reversedSolution;
    reverse(solution.begin(), solution.end());
    int width = countNeededWidth(reversedSolution);
    for(auto& number : solution) {
        cout << "|" << fixed << setprecision(2) << setw(width) << number;
        cout << "| " <<endl;
    }
    cout << endl;
}

vector<size_t> vectorWidths(const Matrix& matrix) {
    vector<size_t> resultVector(matrix.size());
    for(size_t x = 0; x < matrix.size(); x++) {
        vector<double> columnToFindWidthIn(matrix[0].size());
        for(size_t i = 0; i < matrix[0].size(); i++) {
            columnToFindWidthIn[i] = matrix[x][i];
        }
        resultVector[x] = countNeededWidth(columnToFindWidthIn);
    }

    return resultVector;

}

void matrixHolder::printSpanVectors(const Matrix& matrixOfSpans) {
    if(matrixOfSpans.empty()) {
        return;
    }
    vector<size_t> vectorOfWidths = vectorWidths(matrixOfSpans);
    cout << "Span vectors are: " << endl;

    for(size_t i = 0; i < matrixOfSpans[0].size(); i++) {
        for(size_t x = 0; x < matrixOfSpans.size(); x++) {
            cout << "|" << setw(vectorOfWidths[x]) << matrixOfSpans[x][i] << "| ";
        }
        cout << endl;
    }

}

bool isRowInvalidEquation(vector<double>& row) {
    for(size_t i = 0; i<row.size()-1; i++) {
        if(row[i]!=0) {
            return false;
        }
    }
    if(row[row.size()-1]!=0) {
        return true;
    }
    return false;
}

bool hasMatrixSolution(Matrix& matrix) {
    for(auto& row : matrix) {
        if(isRowInvalidEquation(row)) {
            return false;
        }
    }
    return true;
}

void printHelp() {
    cout << "------------------------------" << endl;
    cout << "first, program will ask you to write number of rows and columns (type a valid number)" << endl;
    cout << "then, program will ask you for every single row of matrix. You will have to type numbers intended by space like: " << endl;
    cout << "1 5 -2 3" << endl;
    cout << "------------------------------" << endl;
}

void startOrHelp() {
    string userInput;
    cout << "Would you like to start the program or help? (press Enter if yes or type --help for help)" <<endl;
    while(true) {
        getline(cin, userInput);

        if(userInput=="--help") {
            printHelp();
        } else if(userInput.substr(0, 2)=="--") {
            cout << "I do not know this kind of switcher" << endl;
        } else {
            cout << "Starting program..." << endl;
            break;
        }
        cout << "press Enter for starting program or --help for help" << endl;
    }
}

//guides user to enter matrix properties
Matrix matrixHolder::getMatrixFromUser() {
    Matrix newMatrix;
    string numberOfRowsStr;
    string numberOfColumnsStr;
    string userInput;

    startOrHelp();
    cout << "This program will count system of linear equations represented by matrix" << endl;
    while (true) {
        cout << "How many rows does your matrix have?"<<endl;
        getline(cin, numberOfRowsStr);
        if(isNumber(numberOfRowsStr) && stoi(numberOfRowsStr)>=1) {
            break;
        } else {cout << "Wrong input! ";}
    }
    while (true) {
        cout << "How many columns does your matrix have?"<<endl;
        getline(cin, numberOfColumnsStr);
        if(isNumber(numberOfColumnsStr) && stoi(numberOfColumnsStr)>=2) {
            break;
        } else {cout << "Wrong input! ";}
    }
    size_t numberOfRow = 1;
    int numberOfRows = stoi(numberOfRowsStr);
    int numberOfColumns = stoi(numberOfColumnsStr);

    while(numberOfRow <= numberOfRows) {
        cout << "Write row number: " << numberOfRow << " intended by space (like:4 11 8)" <<endl;
        getline(cin, userInput);
        vector<string> inputNumbers = split(userInput, ' ');
        if(isVectorFullOfNumbers(inputNumbers) && inputNumbers.size()==numberOfColumns) {
            addRowToMatrix(newMatrix, inputNumbers);
            numberOfRow++;
        } else {
            cout << "Wrong! Make sure you write " << numberOfColumnsStr << " numbers intended by space bar" << endl;
        }
    }
    return newMatrix;
}

void matrixHolder::run() {

    matrix = getMatrixFromUser();
    cout << "Numbers of your matrix: " << endl;
    printMatrix(matrix);
    applyGEM();
    cout << "Numbers of your matrix after Gauss elimination method:" << endl;
    printMatrix(matrix);
    cout << "--------------------" << endl;
    if(!hasMatrixSolution(matrix)) {
        cout << "The matrix has no solution!";
        return;
    }
    printSolutionVector(this->countSolution());
    printSpanVectors(this->getSpanVectors());

}



