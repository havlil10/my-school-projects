# My-school-projects

V tomto repozitáři jsou školní projekty na kterých jsem buď sám nebo v týmu pracoval  
Důležité projekty:

- Messaging-app - enterprise aplikace podobná Messengeru/Discordu s důrazem na použití technologií
  - Odkaz: https://gitlab.fel.cvut.cz/havlil10/my-school-projects/-/tree/main/Java/Messaging-app
- Piskvorky - enterprise aplikace piškvorky s různými featurami, na které jsem pracoval se 6 lidmi
  - Odkaz: https://gitlab.fel.cvut.cz/havlil10/my-school-projects/-/tree/main/Java/Piskvorky
- Virtualni_tovarna - projekt s důrazem na čistý kód, návrh a využití design patternů
  - Odkaz: https://gitlab.fel.cvut.cz/havlil10/my-school-projects/-/tree/main/Java/Virtualni_tovarna
- Rezervacni_system_sportovist - kompletní business, software analýza
  - Ve slozce "Analyza_softwaru"

Kompletní výčet
- 1. semestr - poptavac-prace(PHP,html,css,javascript)
- 2. semestr - Sachy(Java), testovani-sachy(Testovani), rezervacni_system_sportovist(Analyza_softwaru), time_tracker(PHP,html,css,javascript)
- 3. semestr - Bowling-system(Java), Virtualni_tovarna(Java), linearni_rovnice(c++)
- 4. semestr - Piskvorky(Java), Messaging-app(Java), Poptavac-prace(Mobile-app-kotlin)